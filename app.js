var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var crypto = require('crypto');

//imports
var routes = require('./routes/index');

/*
@Developer : Santhosh Kumar Gunti
Date : 11-04-2018
Description : Modified the path of login router to the custom project router for TPCL.
*/
// Begin

//var login = require('./routes/login');
var login = require('./custom/mods/login');
//END
var todos = require('./routes/todos');
var licene = require('./routes/lience');
var users = require('./routes/users');
var store = require('./routes/store');
var group = require('./routes/group');
//var admins = require('./routes/admin');
var admins = require('./custom/mods/admin');

/*
@Developer : Santhosh Kumar Gunti
Date : 11-04-2018
Description : Modified the path of formsz router to the custom project router for TPCL.
*/
// Begin
//var formsz = require('./routes/formsz');
var formsz = require('./custom/mods/formsz');
//END
var version = require('./routes/versionManagement');
var log = require('./routes/log')(module);

/*
@Developer : Santhosh Kumar Gunti
Date : 11-04-2018
Description : Modified the path of formsz router to the custom project router for TPCL.
*/
// Begin

//var formszDetails = require('./routes/formszDetails');
var formszDetails = require('./custom/mods/formszDetails');

//END
var pushNotifications = require('./routes/pushNotifications');
var formszCategory = require('./routes/formszCategory');
/*
@Developer : Santhosh Kumar Gunti
Date : 11-04-2018
Description : Modified the path of formsz router to the custom project router for TPCL.
*/
// Begin
//var Tasks = require('./routes/formszTasks.js');
var Tasks = require('./custom/mods/formszTasks.js');
//END
var mongoose = require('mongoose');
var Lience = require('./models/lience.js');
var bookmarks = require('./routes/bookmarks');
var devicemanagement = require('./routes/deviceManagement');
var organizationinfo = require('./routes/organizationalinfo');
/*
Who : SV
Date : 04-04-2018
Description : Modified the path of project router to the custom project router for TPCL.
*/
// Begin
//var project = require('./routes/project');
var project = require('./custom/mods/project');
// End
var activitylog = require('./routes/activityLog');
var liencesInsertion = require('./routes/mobileLicensing');
// @PHANI
//Code commented by Divya on 20-01-2018 for TPCL Project

//var userLicense = require('./routes/userLicense');
//var gridFS = require('./routes/gridfs');
//var dashboardInfo = require('./routes/dashboardDetails');
var dashboardInfo = require('./custom/mods/dashboardDetails');
//Added girdfs dependencies
var Grid = require('gridfs-stream');
var gridFS = require('./custom/mods/gridfs');
var scheduler = require('./routes/scheduler');

 var db_url= process.env.db_url || 'mongodb://appconnect1:cTpcl!32018@mongodb:2718/TPCLDFormsz';
 mongoose.connect(db_url, function(err) {
 //mongoose.connect('mongodb://localhost:27017/TPCLDFormsz',{useMongoClient:true}, function(err) {

    if(err) {
        log.error('connection error', err);
    } else {
        log.info('connection successful');
    }
});
var app = express();
/* app.listen(3000,function(){
          console.log("Running at PORT 3000");
}); */
//Licence 
/*app.use(function(req, res, next){
	 res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE','OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,enctype');
   if(req.method== 'PUT'||req.method== 'POST'||req.method== 'DELETE')
   {
	   console.log("request type=" + req.method);
	  Lience.find({},function (err, post) 
	  {
		  
		 // console.log(eval(eval(decrypt(post[0].bc9342))+eval(decrypt(post[0].a8835a6972c7))+eval(decrypt(post[0].bc894578))));
		  //next();
		 if(req.url!="/login")
		 {
		 if(eval(eval(decrypt(post[0].bc9342))+eval(decrypt(post[0].a8835a6972c7))+eval(decrypt(post[0].bc894578)))<eval(decrypt(post[0].a0af7b4552)))
		  {
			  if(res.statusCode==200||res.statusCode==304)
			 {
				 switch(req.method) {
					case "PUT":
						post[0].bc9342=encrypt((parseInt(decrypt(post[0].bc9342))+1).toString());
						post[0].save(function(err) 
						{
							next();
						});
						break;
					case "POST":
						post[0].bc894578 = encrypt((parseInt(decrypt(post[0].bc894578))+1).toString());
						post[0].save(function(err) 
						{
							next();
						});
						break;
					case "DELETE":
						post[0].a8835a6972c7 = encrypt((parseInt(decrypt(post[0].a8835a6972c7))+1).toString());
						post[0].save(function(err) 
						{
							next();
						});
						break;
					default:
					
						post[0].ab8342 = encrypt((parseInt(decrypt(post[0].ab8342))+1).toString());
						post[0].save(function(err) 
						{
							next();
						});	
				}
			 }else
			 {
				  next();
			 }
		  }else
		  {
			  res.status(202);
				res.json({
				"status": 202,
				"message": "Your licence has been expired "
				});
				
			   
		  }
		 }else
		 {
			 next();
		 }
		  
	  }); 
	  
   }else
   {
	   next();
   }
});*/
 

app.all('/*', function(req, res, next) {
  // CORS headers
  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  // Set custom headers for CORS
  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key,enctype');
  if (req.method == 'OPTIONS') {
    res.status(200).end();
  } else {
    next();
  }
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(bodyParser.json({limit: '500mb'}));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
/* app.use(multer({ dest: './fileupload'})); */
/*  app.all('/api/v1/*', [require('./middlewares/validateRequest')]);   */
app.use('/', routes);
app.use('/', login);
app.use('/api/v1/todos', todos);
app.use('/api/v1/users', users);
app.use('/api/v/store', store);
app.use('/api/v1/group', group);
app.use('/api/v1/admins', admins);
app.use('/api/v1/formsz', formsz);
app.use('/api/v1/versions',version);
app.use('/api/v1/formszDetails', formszDetails);
app.use('/api/v1/pushNotifications', pushNotifications);
app.use('/api/v1/formszCategory', formszCategory);
app.use('/api/v1/tasks', Tasks);
app.use('/api/v1/licence', licene);
app.use('/api/v1/bookmarks',bookmarks);
app.use('/api/v1/devicemanagement',devicemanagement);
app.use('/api/v1/organizationalinfo',organizationinfo);
app.use('/api/v1/projectProcesss',project);
app.use('/api/v1/activitylog',activitylog);
app.use('/api/v1/insertLicenses',liencesInsertion);
//Code commented by Divya on 20-01-2018 for TPCL Project
//app.use('/api/v1/userLicense',userLicense);
app.use('/api/v1/gridFS',gridFS);
// SWATHI
app.use('/api/v1/dashboardDetails',dashboardInfo);
app.use('/api/v1/scheduler',scheduler);



// catch 404 and forward to error handler
app.use(function(req, res, next) {
	
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});
// error handlers
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

function encrypt(text){
  var cipher = crypto.createCipher('aes-256-ctr','magikminds');
  var crypted = cipher.update(text,'utf8','hex');
  crypted += cipher.final('hex');
  return crypted;
}
function decrypt(text){
  var decipher = crypto.createDecipher('aes-256-ctr','magikminds');
 // console.log(decipher);
  var dec = decipher.update(text,'hex','utf8');
  dec += decipher.final('utf8');
  return dec;
}
module.exports = app;
