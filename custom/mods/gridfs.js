var express = require('express');
var conn;
var router = express();
var mongoose = require('mongoose');
var formidable = require('formidable');
var Grid = require('gridfs-stream');
var fs = require('fs');
var FormszDetails = require('../../models/formszDetails.js');
/*
@Developer : SKG
@Date : 3/5/2018
@Description : imported formsz and async for file attachement references

*/
var Formsz = require('../../models/formsz.js');
var async = require('async');

Grid.mongo = mongoose.mongo;

 //r conn = mongoose.createConnection('mongodb://localhost:27017/TPCLDForms', function(err) {
 var conn = mongoose.createConnection('mongodb://appconnect1:cTpcl!32018@localhost:2718/TPCLDFormsz', function(err) {

    if(err) {
        console.log('err '+err);
    } else {
        console.log('connection established ');
    }
});  
var gfs = Grid(conn.db);
//console.log(gfs);
conn.once('open', function () {
  var gfs = Grid(conn.db);
  router.set('gfs',gfs);
console.log("gfss..")
  // all set!
})
/*
@Developer : SKG
@Date : 3/5/2018
@Description : services for attaching the reference in forms

*/
router.post('/attachReferenceToForm/:id', function(req, res) {
	
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {	
	var temp = fields.deletedFiles.split(",");
	console.log("-------------------------------")
	console.log(temp);
	console.log(temp.length);
	console.log("-------------------------------")
	if(temp.length > 0){
		Formsz.update({_id:req.params.id},{$pull:{generatedIdForFileAttachment:{"_id":{$in :temp}}}},{ multi: true }, function(err,success) {
		    if(err) {
		      return err
		    }
		    else {
		      console.log("success");
		    }
		});
	}

      Object.keys(files).forEach(function (key) {
            var path = files[key].path;
            var buffer = fs.readFileSync(path);
            var contentType1 = files[key].type;
            var writestream = gfs.createWriteStream({
                filename: files[key].name,
                mode: "w",
                content_type: contentType1
            });
            fs.createReadStream(path).pipe(writestream);
            writestream.on('close', function(file) {
              console.log(file);
                Formsz.find({
                	_id:req.params.id,
                    generatedIdForFileAttachment: {$elemMatch:{md5:file.md5}}
                }, function(err, record) {

                    if (record.length > 0) {
                    } else {
                          Formsz.update({_id:req.params.id},{$push:{generatedIdForFileAttachment:{"_id":file._id.toString(),"md5":file.md5}}}, function(err,success) {
                            if(err) {
                              return err
                            }
                            else {
                              console.log("success");
                            }
                          })
                        console.log("im in else else");
                    }
                });

            });

      });
            res.json({
                "message": "File Attached Successfully",
                "status": 200
            });
    });
});
/*
@Developer : SKG
@Date : 3/5/2018
@Description : services for getting all references of forms

*/
router.get('/getAllAttachedReferenceOfForm/:id',function(req,res){
      var result = [];
      Formsz.find({
          _id: req.params.id
      }, function(err, data) {
            async.forEachSeries(data[0].generatedIdForFileAttachment,function(user,next) {
              gfs.findOne({_id: user._id}, function (err, file) {
              	if(file != null)
	                result.push(file);
                next();
              });
            }, function(err) {  
            	if(result.length > 0)  { 
                   res.json({
                        "data": result,
                        "status": 200
                    });
           		}else{
                   res.json({
                        "data": result,
                        "status": 204
                    });
           		}
              })
      })
});
/*
@Developer : SKG
@Date : 3/5/2018
@Description : services for getting attachement link of reference attached to form

*/
router.get('/getAttachedReferenceOfForm/:id',function(req,res){
   gfs.findOne({_id: req.params.id}, function (err, file) {
    if (err) {
        return res.status(400).send(err);
    } else if (!file) {
        return res.status(404).send(' ');
    } else {
        
        if(file.filename.split(".")[1] == "xlsx"){
          res.header("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        }else if(file.filename.split(".")[1] == "docx"){
          res.header("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        }else if(file.filename.split(".")[1] == "pdf"){
          res.header("Content-Type", "application/pdf");
        }else if(file.filename.split(".")[1] == "png"){
          res.header("Content-Type", "image/png");
        
        }else if(file.filename.split(".")[1] == "jpg"){
          res.header("Content-Type", "image/jpg");
        }

        res.header("X-Content-Type-Options", "nosniff");
        res.header("Accept-Ranges", "bytes");
        res.header("Content-Length",file.length);

        var readStream = gfs.createReadStream({_id: file._id});
        readStream.on('open', function () {
            console.log('Starting download...');
        });
        readStream.on('data', function (chunk) {
            console.log('Loading...');
        });
        readStream.on('end', function () {
            console.log('Video is ready to play');
        });
        readStream.on('error', function (err) {
            console.log('There was an error with the download' + err);
            res.end();
        });
        readStream.pipe(res);
    }
});
});
//router.set('gfs',gfs);
router.post('/addvideo/:id',function(req,res){
 console.log("imgggggggggggggggggggggggggg ")
   var form = new formidable.IncomingForm();
   form.parse(req, function(err, fields, files){

   if(files.video){
		var path=files.video.path;
		var buffer=fs.readFileSync(path);
		var contentType1=files.video.type;
    var writestream = gfs.createWriteStream({
      filename: files.video.name,
      mode: "w",
       content_type: contentType1,
	    metadata:{
		fieldId:JSON.parse(fields.params).fieldId,
		generatedId:req.params.id
	  }
    });
    fs.createReadStream(path).pipe(writestream);
     writestream.on('close', function (file) {
		 res.json(file)
/*         FormszDetails.find({generatedId:req.params.id},function(err,record) {
          console.log(record)
   //       console.log(record.record)
          console.log("...........................................")
          console.log(fields)
        if(record.length>0) {
          record[0].record[0][JSON.parse(fields.params).fieldId] =file._id.toString();
          var data = record[0].record;
          FormszDetails.update({_id:record[0]._id},{$set:{record:data}}, function(err,success) {
            if(err) {
              return err
            }
            else {
              console.log("success");
               res.json(file)
            }

          })
        
        }
        else {
          console.log("im in else else");
        }
      }) */

     
     });
     }
     else {
     	console.log("im in else");
     }
   }); 
});
//for direct submit
router.post('/addRecordvideo/:id',function(req,res){
 console.log("imgggggggggggggggggggggggggg ")
   var form = new formidable.IncomingForm();
   form.parse(req, function(err, fields, files){
   if(files.video){
		var path=files.video.path;
		var buffer=fs.readFileSync(path);
		var contentType1=files.video.type;
    var writestream = gfs.createWriteStream({
      filename: files.video.name,
      mode: "w",
       content_type: contentType1,
	   metadata:{
		fieldId:fields.fieldId,
		generatedId:req.params.id
	  }

    });
    fs.createReadStream(path).pipe(writestream);
     writestream.on('close', function (file) {
		  res.json(file)
    /*   FormszDetails.find({generatedId:req.params.id},function(err,records) {
      console.log(records)
        if(records.length!=0) {
      console.log(records[0].record[0]);
          records[0].record[0][fields.fieldId] =file._id.toString();
          var data = records[0].record[0];
  
          updateRecord(records,data,file,res)   
        }
        else {
          console.log("im in else else");
        }
      }); */

  
     });
     }
     else {
     	console.log("im in else");
     }
   }); 
});
router.get('/fetchVideo/:generatedId/:fieldId',function(req,res){
  gfs.findOne({"metadata.generatedId":req.params.generatedId,"metadata.fieldId":req.params.fieldId}, function (err, data) {	
	console.log(data);
		if(data.length>0){
			console.log("length");
			res.json({
                    "data": data._id,
                    "status": 200
                })
		}else{
			res.json({
                    "data":"No Video Found",
                    "status": 204
                })
		}
	}); 
});
 function updateRecord(records,data,file,res){
   FormszDetails.update({_id:records[0]._id},{$set:{"record":data}}, function(err,success) {
            if(err) {
              return err
            }
            else {
              console.log("success");
                  res.json(file)
            }
          })  
 }
router.get('/getvideo/:vid',function(req,res){
	console.log(req.params.vid)
  
				 gfs.findOne({_id: req.params.vid}, function (err, file) {
    if (err) {
        return res.status(400).send(err);
    } else if (!file) {
        return res.status(404).send(' ');
    } else {
        res.header("Content-Type","video/mp4");
        res.header("X-Content-Type-Options", "nosniff");
        res.header("Accept-Ranges", "bytes");
        res.header("Content-Length",file.length);

        
        var readStream = gfs.createReadStream({_id: file._id});
        readStream.on('open', function () {
            console.log('Starting download...');
        });
        readStream.on('data', function (chunk) {
            console.log('Loading...');
        });
        readStream.on('end', function () {
            console.log('Video is ready to play');
        });
        readStream.on('error', function (err) {
            console.log('There was an error with the download' + err);
            res.end();
        });
        readStream.pipe(res);

    }
});
});


router.get('/getvideoios/:vid',function(req,res){
  console.log(req.params.vid)
  gfs.findOne({
       _id: req.params.vid
    }, function(err, file) {
        if (err) {
            return res.status(400).send({
                err: errorHandler.getErrorMessage(err)
            });
        }
        if (!file) {
            return res.status(404).send({
                err: 'No se encontró el registro especificado.'
            });
        }

        if (req.headers['range']) {
            var parts = req.headers['range'].replace(/bytes=/, "").split("-");
            var partialstart = parts[0];
            var partialend = parts[1];

            var start = parseInt(partialstart, 10);
            var end = partialend ? parseInt(partialend, 10) : file.length - 1;
            var chunksize = (end - start) + 1;

            res.writeHead(206, {
                'Accept-Ranges': 'bytes',
                'Content-Length': chunksize,
                'Content-Range': 'bytes ' + start + '-' + end + '/' + file.length,
                'Content-Type': file.contentType
            });

            gfs.createReadStream({
                _id: file._id,
                range: {
                    startPos: start,
                    endPos: end
                }
            }).pipe(res);
        } else {
            res.header('Content-Length', file.length);
            res.header('Content-Type', file.contentType);

            gfs.createReadStream({
                _id: file._id
            }).pipe(res);
        }
    });

});
module.exports = router;
