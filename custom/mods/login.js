var jwt = require('jwt-simple');
var passwordHash = require('password-hash');
var crypto = require('crypto');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Users = require('../../models/user.js');
var mailer = require("nodemailer");
var sendautomail = require('../../routes/utils').sendautomail;
var Groups = require('../../models/group.js');
var passport = require('passport');
var LdapStrategy = require('passport-ldapauth');
var deviceManagement =  require('../../models/deviceManagement.js');
var activityLog = require('formsz-tpcl-customizations');
var mobileLicensing = require('../../models/mobileLicensing.js');
var async = require('async');


/**
     * @description LDAP options settings  
     * @developer Venkatesh bendi
     * @moreInfo this funtionality backporting from UE implemenation,by venkates.hbendi
*/
var OPTS = {
   
    server: {
        "url": "ldap://wdvdc001.domain.dev.int:389",
        "bindDN": "CN=Service_GIS_LDAP,OU=Service Accounts,OU=Resources,DC=domain,DC=dev,DC=int",
        "bindCredentials": "5rV_g15_LD@P",
        "searchBase": "DC=domain,DC=dev,DC=int",
        "searchFilter": "(&(objectcategory=person)(objectclass=user)(|(samaccountname={{username}})(mail={{username}})))"
    }
};
//pasport module for ldap conectivity
passport.use(new LdapStrategy(OPTS));


//var pushNotifications = require('../routes/utils').pushNotifications;
//Service Test
router.get('/Encript/:data', function(req, res, next) {
    res.json({
        "message": encrypt(req.params.data)
    });

});
router.get('/Dcript/:data', function(req, res, next) {
    res.json({
        "message": decrypt(req.params.data)
    });

});
//Forgot password
router.post('/forgotpwd', function(req, res, next) {
    Users.find({
        $or: [{
            username: req.body.username
        }, {
            email: req.body.username
        }]
    }, function(err, post) {
        if (post.length > 0) {
            var decryptpassword = decrypt(post[0].password);

            body = "<body>Dear " + post[0].name + ",<br><br>Please find your new credentials listed below<br><br> <strong>Username:</strong>" + req.body.username + "<br> <strong>Password :</strong> " + decryptpassword + "<br><br><br>Thanks,<br>DFormsTeam</body>"
            sendautomail(post[0].email, body, 'Automatic Reply:Account Credentials')


            res.json({
                "message": "Password sent to your mail,Please check once ",
                "status": 200
            });
        } else {
            res.json({
                "message": "Invalid credentials",
                "status": 204
            });
        }
    });

});


//LDAP login 
 /**
     * @description user validation on ldap System  
     * @requestParams {objcet} username and password 
     * @developer Venkatesh bendi
     * @moreInfo this funtionality backporting from UE implemenation,by venkates.hbendi
*/

//logout

// router.post('/logout',function(req,res,next) {
//     var devicedata = req.body.manufacturer+"-"+req.body.uuid;
//     var devicedataInfo = req.body.manufacturer+"-"+req.body.model+"-"+req.body.uuid;

//     //Updated isuserlocatorActive to true on 18-01-2018 for TPCL
//     Users.find({username:req.body.username,isDeleted:false,isUserLocatorActive:true,type:2},function(err,user){
//         if(user.length>0) {
//             var deviceDetailsArray = [];
//             var isUserLocatorActive = true;

//             async.forEachSeries(user[0].deviceDetails,function(singleLoginDeviceData,next) {
//                 var key = Object.keys(singleLoginDeviceData);

//                 if(devicedata==key[0]) {
//                     next();
//                 } else {
//                     deviceDetailsArray.push(singleLoginDeviceData);
//                     next();
                   

//                 }
//             }, function(err) {
//                 if(deviceDetailsArray.length==0) {
//                       isUserLocatorActive = false

//                     }
//                       Users.update({_id:user[0]._id},{$set:{isUserLocatorActive:isUserLocatorActive,deviceDetails:deviceDetailsArray}},function(err,b){


//                 if (err) throw err;
//                 else {
//                     //Code added by Divya for TPCL project on 18-01-2018 for releaseing license when user logout
//                     removeLicenseforUser(req.body.username,devicedataInfo,req.body.uuid, function(value) {
//                         if(value == true) {
//                         activityLog.activityLogforLoginforUser(req.body.manufacturer,req.body.uuid,user,"User Logged Out");

//                               res.json({
//                                 "status": 200,
//                                 "message": "Logout successfull"
//                             });

//                         }
//                     })

//                 }

//             });

//             })
             

           


          
                
           

//         }
//         else {
//             res.json({
//                 "status": 204,
//                 "message": "Invalid credentials"
//             });

//         }
//     })
// });

//logout
// Developer : Siva Kella
//Notes: Indexed the User user[0]
router.post('/logout',function(req,res,next) {
    var devicedata = req.body.manufacturer+"-"+req.body.uuid;
    var devicedataInfo = req.body.manufacturer+"-"+req.body.model+"-"+req.body.uuid;
    //Updated isuserlocatorActive to true on 18-01-2018 for TPCL
    Users.find({username:req.body.username,isDeleted:false,isUserLocatorActive:true,type:2},function(err,user){
        if(user.length>0) {
            var deviceDetailsArray = [];
            var isUserLocatorActive = true;

            async.forEachSeries(user[0].deviceDetails,function(singleLoginDeviceData,next) {
                var key = Object.keys(singleLoginDeviceData);
                if(devicedata==key[0]) {
                    next();
                } else {
                    deviceDetailsArray.push(singleLoginDeviceData);
                    next();
                }
            }, function(err) {
                if(deviceDetailsArray.length==0) {
                      isUserLocatorActive = false;
                    }
                      Users.update({_id:user[0]._id},{$set:{isUserLocatorActive:isUserLocatorActive,deviceDetails:deviceDetailsArray}},function(err,b){


                if (err) throw err;
                else {
                    //Code added by Divya for TPCL project on 18-01-2018 for releaseing license when user logout
                    removeLicenseforUser(req.body.username,devicedataInfo,req.body.uuid, function(value) {
                        if(value == true) {
                        //-->Siva : Modified: TPCL Customizations
                        //Notes: Indexed the User user[0]
                        // activityLog.activityLogforLoginforUser(req.body.manufacturer,req.body.uuid,user[0],"User Logged Out");    
                        activityLog.activityLogforLoginforUser(req.body.manufacturer,req.body.uuid,user[0],"User Logged Out",req.body.model,req.body.version);
                        //<-- End
                              res.json({
                                "status": 200,
                                "message": "Logout successfull"
                            });

                        }
                    })

                }

            });

            })
        }
        else {
            res.json({
                "status": 204,
                "message": "Invalid credentials"
            });
        }
    })
});

router.post('/loginWithLdap', function(req, res, next) {

    if (req.body.username == "root") {
        var encryptpwd = encrypt(req.body.password);
        formszSuperUserProcess(req, res, encryptpwd)
    } else {
        passport.authenticate('ldapauth', {
            session: false
        }, function(err, user, info) {
            if (err) {
                return next(err); // will generate a 500 error
            }
            // Generate a JSON response reflecting authentication status
            if (!user) {

                return res.json({
                    "status": 204,
                    "message": "Invalid credentials"
                });
            } else {
                var roles = getRoles(user.memberOf);
                var userType = getUserType(roles);
                //app_carding_portal

                var defaultGroup = "UEGroup"
                var ldapUser = new Users({
                    username: user.sAMAccountName,
                    name: user.name,
                    //email: user.sAMAccountName+"@ue.com.au",
                    email: "ksagar@ue.com.au",
                    groupname: defaultGroup,
                    type: userType,
                });

                if (userType == 2) {


                    if (userType == req.body.type) {
                        CreateUSer(user, defaultGroup, userType, res, ldapUser);

                    } else {
                        res.json({
                            "status": 204,
                            "message": "You are unuthorized to access this portal"
                        });
                    }
                } else if (userType == 1) {

                    CreateUSer(user, defaultGroup, userType, res, ldapUser);

                } else if (userType == 3) {
                    res.json({
                        "status": 204,
                        "message": "You are unuthorized to access this portal"
                    });
                }

            }

        })(req, res, next);
    }
});


//Login
router.post('/login', function(req, res, next) {
	console.log("iiiiiiiiiiiiiiiiiiiiiiiiii")
    var filter;
    var encryptpwd = encrypt(req.body.password);
    if(req.body.type == 2){
        filter = {
            username: req.body.username,
            password: encryptpwd,
            isDeleted: false,
            type: 2
        }, {
            email: req.body.username,
            password: encryptpwd,
            isDeleted: false
        };
    } else if(req.body.type == undefined){
        filter = {
            username: req.body.username,
            password: encryptpwd,
            //-->Siva Changes: Modified: TPCL Customizations
            //NOTES:
            // isDeleted: false
            isDeleted: false,
            type: {$ne: 2}
            //<-- End 
        }, {
            email: req.body.username,
            password: encryptpwd,
            isDeleted: false
        };
    }

    Users.find({
        $or: [filter]
    },{"isUserUpdatePermissionActive":0,"__v":0,"isDeleted":0},function(err,data){
        var response= genToken(data);
        if(data.length>0)
            {
                //Code commented by Divya Bandaru on 14-03-2018 for First Login in Mobile
                
        		/*if(response.user[0].isUserLocatorActive == true) {
                    res.json({"message":"User is active in other device.Please check",status:201});
                }
                else if(req.body.type == response.user[0].type){
                    CheckISExists(req.body.username,req.body.manufacturer,req.body.model,req.body.platform,req.body.UUID,req.body.version,req.body.macAddress,response,data[0],function(response){
                        res.json(response);
                    });
                }*/
                //--> Code added by Divya Bandaru for first login 

                if(response.user[0].isUserLocatorActive == true) {
                    res.json({"message":"User is active in other device.Please check",status:201});
                }
                else if(req.body.type == response.user[0].type){
                    if(response.user[0].isFirstLogin==true) {
                        res.json(response);
                    }
                    else {
                         CheckISExists(req.body.username,req.body.manufacturer,req.body.model,req.body.platform,req.body.UUID,req.body.version,req.body.macAddress,response,data[0],function(response){
                        res.json(response);
                    });

                    }

                   
                }
                //<-- code ended here
                
                else {
                    var groupName = data[0].groupname;
                     var adminlist ={};
                     //-->Umamaheswari B: Added: TPCL Customizations
                    //Notes: Added to fetch associated dept admin details from groups table in login
                      var arrayForAdminlist = [];
                      async.forEachSeries(groupName, function(singleGroup, next) {
                    /*Groups.find({_id:singleGroup._id},function(err,users){
                      adminlist.adminlist = users[0].adminlist;
                       next();

                    })*/
                     Users.find({groupname:{$elemMatch:{_id:singleGroup._id}},type:1},{_id:1,username:1},function(err,users){
                        if(users.length>0)
                        {
                             arrayForAdminlist.push(users[0]);
                             adminlist.adminlist = arrayForAdminlist;
                           
                        }
                        else
                        {
                            adminlist.adminlist = [];
                        }
                        
                       next();

                    })
                   
                }, function(err) {
                    response.user[0].adminlist = adminlist.adminlist || [];
                  //End
                                        res.json(response);                    




                });
                }
            }
        else
        {
            res.json({
                    "status": 204,
                    "message": "Invalid credentials"
            }); 
        }
    });
    

});

function genToken(user) {
    var expires = expiresIn(1); // 7 days
    var token = jwt.encode({
        exp: expires
    }, require('../../config/secret')());
    return {
        token: token,
        expires: expires,
        user: user,
        "message": "Login Success",
        "status": 200
    };
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

function encrypt(text) {
    var cipher = crypto.createCipher('aes-256-ctr', 'magikminds');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher('aes-256-ctr', 'magikminds');
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

/*
	@Developer : Santhosh kumar Gunti
	@Description : inserted verstion in deviceManagement collection to show the version of the mobile in devicemanagement table in web  
	@Date : 19th april 2018
*/

function CheckISExists(username,manufacturer,model,platform,UUID,version,macAddress,response,data,cb)
{
	console.log("8888888888888888888888888")
  //  console.log("data==");
  //  console.log(data);
    var filter, dataToInsert; 
    if(platform == "iOS"){
            filter = {UUID : UUID};
            dataToInsert = {
                Manufacturer : manufacturer,Model : model,platform : platform,UUID:UUID,version:version
            }
    }
    else{
            filter = {MacAddress : macAddress};
            dataToInsert = {
                MacAddress: macAddress,Manufacturer : manufacturer,Model : model,platform : platform,UUID:UUID,version:version
            }
    }
    deviceManagement.find(filter,function(err,post){
    	console.log("0000000000000000000000000000")
        if (post.length > 0) {
            deviceManagement.update(filter,{$set:dataToInsert},function (err, a) {
                if (err) throw err;
                else {
                    
                }
            });

            if(post[0].status == "Approved"){
                /*activityLog.activityLogforLoginforUser(manufacturer,UUID,data,"User Logged In");
                console.log(response);
                cb(response);*/
                 consumeLicForUser(username,post[0],data,function(licenceStatus) {

                    if(licenceStatus==true) {
                        var deviceInfo =[];
                        var date = new Date();
                        var loggedInDate = date.toISOString();
                        var object = {};
                        var deviceData = post[0].Manufacturer;
                        object[post[0].Manufacturer+"-"+post[0].UUID] = loggedInDate;

                        deviceInfo.push(object);

                         Users.update({_id:data._id},{$set:{
                                isUserLocatorActive : true,lastLoggedInTime:loggedInDate},$push:{deviceDetails:object}},
                            function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                         }
                    });
                         console.log("----------------------")
                      console.log(version)
                         console.log("----------------------")
                        activityLog.activityLogforLoginforUser(manufacturer,UUID,data,"User Logged In",model,version);
                     cb(response);

                    } else {
                        cb({"message":"Licenses exhausted.Cannot login",status:201});

                    }
                });
            }
            else if(post[0].status == "Rejected"){
                cb({
                   "message": "Your Device is Rejected , Please contact administrator.",
                   "status": 201
                });

            }
            else if(post[0].status == "Suspended"){
                cb({
                   "message": "Your Device is suspended , Please contact administrator.",
                   "status": 201
                });

            }else if(post[0].status == "Pending"){
                cb({
                   "message": "Your Device is not yet approved, Please contact administrator.",
                   "status": 201
                });
            }
            else if(post[0].isDeleted == true && post[0].status=="Suspended"){
                cb({
                   "message": "Your Device is not authorised, Please contact administrator.",
                   "status": 201
                });
            }
        } else {
            addMobileDevice(username,platform,manufacturer,UUID,version,macAddress,model,data,function(response){
                cb(response);
                /*releaseLicenseForUser(username,function(licenceStatus) {
                    if(licenceStatus==true) {
                        Users.update({_id:data._id},{$set:{
                                isUserLocatorActive : true
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                         }
                    });
                        activityLog.activityLogforLoginforUser(manufacturer,UUID,data,"User Logged In");
                     cb(response);

                    } else {
                        cb({"message":"License release failed",status:208});

                    }
                })*/
             
            });
        }
    });

}

/*
	@Developer : Santhosh kumar Gunti
	@Description : inserted new field requestedUser in deviceManagement collection to show the first requested user while device registration from mobile  
	@Date : 19th april 2018
*/

//Auto register mobile device
function addMobileDevice(username,platform,manufacturer,UUID,version,macAddress,model,data,cb){

    Users.find({username: username,type:2}, function(err, userInfo) {
        var filter, dataToInsert; 
        if(platform == "iOS"){
                filter = {UUID : UUID};
                dataToInsert = {
                    status: "Pending", groupname : userInfo[0].groupname[0]._id || null, Manufacturer : manufacturer,Model : model,platform : platform,UUID:UUID,version:version,requestedUser : username
                }
        }
        else{
                filter = {MacAddress : macAddress,UUID : UUID};
                dataToInsert = {
                    MacAddress: macAddress,status: "Pending", groupname : userInfo[0].groupname[0]._id || null, Manufacturer : manufacturer,Model : model,platform : platform,UUID:UUID,version:version,requestedUser : username
                }
        }
        deviceManagement.find(filter, function(err, post) {
            if (post.length > 0) {
                    cb({
                    "message": "Device information already exists",
                    "status": 200
                }); 
            }
            else{
                var deviceinfo = new deviceManagement(dataToInsert);
                
                deviceinfo.save(function(err, post) {
                    if (err) {
                        console.log("not inserted");
                    }
                    else{

                        //Added by Diva for TPCL Customisation
                       activityLog.deviceLog(post,data.adminname,"New Device Registered");

                        // GET ADMIN INFO from login and bind it to below  variables
                        var name = "Admin";
                    
                       // var email = "siva.kella@magikminds.com";
                       // var html = "<body>Dear " + name + ",<br><br> device "+post.Manufacturer+" with MacAddress ("+post.MacAddress +")"+" has been "+post.status+"<br><br></body>"
                        var html = "<html><body><pre>"+
                            "Dear Admin, <br><br>"+
                            "Auto request received from "+data.username+"  from Department "+data.groupname[0].name+"."+
                            "The below device has been auto registered to TPCLDynamic Forms Application and it is pending for approval. Please verify and accept/reject the device."+
                            "<br><br>"+
                            "Device Details:"+"<br>"+
                            "Mac-Address :"+post.MacAddress+"<br>"+
                            "Manufacturer: "+post.Manufacturer+"<br><br><br>"+
                            "Thanks,"+"<br>"+
                            "DFormszteam"+
                            "</pre></body></html>"
                            //-->Code added on 14-03-2018 by Divya Bandaru to fetch root email from Database
                             Users.find({type:0},{email:1,_id:0}, function(err,rootUser) {
                            console.log(rootUser);
                            if(rootUser.length>0) {
                                var email = rootUser[0].email;
                                console.log("user email ==" +  email);
                                 sendautomail(email, html, "Device Auto registered: Pending Approval");


                            }
                        });
                             //<-- Code ended here
                             //--> Code commented and added in another callback on 14-03-2018
                        //sendautomail(email, html, "Device Auto registered: Pending Approval");
                        //<-- code ended here
                                              
                        cb({
                           "message": "Your Device has been Registered Successfully, Please contact administrator to Approve the device.",
                           "status": 201
                        });
                    }
                });
            }
        });
    });
}


//Added by Divya For TPCL Project on 18-01-2018
//Function to consume license when user clicks login
 function consumeLicForUser(username,devicedetails,data,callback) {
  //  mobileLicensing.find({isActive:true,assignedTo:username}, function(err,userExists) {

       /* if(err) {
            console.log(err);
            callback(false);

        }
        else if(userExists.length>0) {
            //releaseLicense.update({isActive:true})
            var date = new Date();
             var loggedInTime = date.toISOString();
            mobileLicensing.update({_id:userExists[0]._id},{$set:{
                               isActive:true,assignedTo:username,loggedInTime:loggedInTime 
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                             callback(true);
                         }
                    })
        }
        else if(userExists.length==0) {*/
            mobileLicensing.findOne({isActive:false}, function(err,licenseReleased) {

                if(err) {
                    callback(false);
                }
                 else if(licenseReleased==null) {
                    callback(false);

                }
                else if (licenseReleased) {
                     var date = new Date();
             var loggedInTime = date.toISOString();
             //var object = {};
             var deviceString =devicedetails.Manufacturer+"-"+devicedetails.Model+"-"+devicedetails.UUID ;
           //  object[devicedetails.Manufacturer.concat(devicedetails.model)]= devicedetails.UUID;
                //object[devicedetails.Manufacturer.concat(devicedetails.Model)] = devicedetails.UUID;

                 mobileLicensing.update({_id:licenseReleased._id},{$set:{
                              isActive:true,assignedTo:username,loggedInTime:loggedInTime,deviceDetails:deviceString,department:data.groupname[0].name
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                             callback(true);
                         }
                    })

                }
               
            })


     //   }


  //  })

 }

//Added by Divya For TPCL Project on 18-01-2018
//Function to release license when user clicks logout
 function removeLicenseforUser(username,devicedata,uuid,callback) {
        mobileLicensing.find({isActive:true,assignedTo:username,deviceDetails:devicedata}, function(err,userExists) {
             if(err) {
            callback(false);

        }
        else if(userExists.length>0) {
            var date = new Date();
            var logouttime = date.toISOString();
            mobileLicensing.update({_id:userExists[0]._id},{$set:{
                               isActive:false,assignedTo:null,logouttime:logouttime,deviceDetails:null,department:null
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             callback(true);
                         }
                    })
        }

        });


 }

module.exports = router;
