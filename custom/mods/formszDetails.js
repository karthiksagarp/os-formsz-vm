var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var FormszDetails = require('../../models/formszDetails.js');
var DisplayValues = require('../../models/displayValues.js');
var Formsz = require('../../models/formsz.js');
var mongoid = mongoose.Types.ObjectId();
var ObjectId = require("bson-objectid");
var excelbuilder = require('msexcel-builder');
var sendAutomailwithAttachment = require('../../routes/utils').sendAutomailwithAttachment;
var fs = require('fs');
var request = require('request');
// var dbConfiguration = require('../routes/dbConfiguration.js');
var Tasks = require('../../models/formszTasks.js');
var async = require('async');
var notificationmodel = require('../../models/notificationmodel.js');
var pdf = require('html-pdf');
var activityLog = require('formsz-tpcl-customizations');
var deviceManagement =  require('../../models/deviceManagement.js');


var dateFormat = require('dateformat');

// ================================================================================================================================
// @ Dependencies
var orgDetails;
// <<<<< Developer: Phani Kumar Narina >>>>
// <<<<< PDFMake Related Dependencies >>>>>
var orgInfo = require('../../models/organizationalinfo.js');
var path = require('path');
var PdfPrinter = require('../../node_modules/pdfmake/src/printer');
var pdfMake = require('../../node_modules/pdfmake/build/pdfmake.js');
var pdfFonts = require('../../node_modules/pdfmake/build/vfs_fonts.js');
pdfMake.vfs = pdfFonts.pdfMake.vfs;
// Notes: This fonts are stored in folder public/fonts
var fonts = {
    Roboto: {
        normal: path.join(__dirname, '../../', 'public', '/fonts/Roboto-Regular.ttf'),
        bold: path.join(__dirname, '../../', 'public', '/fonts/Roboto-Medium.ttf'),
        italics: path.join(__dirname, '../../', 'public', '/fonts/Roboto-Italic.ttf'),
        bolditalics: path.join(__dirname, '../../', 'public', '/fonts/Roboto-MediumItalic.ttf')
    }
}
var printer = new PdfPrinter(fonts);
var pdfinfo = {
    title: 'TataPower - Dynamic Formsz',
    author: 'Phani Kumar Narina',
    subject: 'Records Information',
    keywords: 'TPCL'
}
var styles = {
    header: {
        fontSize: 14,
        bold: true,
        margin: [0, 0, 0, 5]
    },
    subheader: {
        fontSize: 12,
        bold: true,
        margin: [0, 5, 0, 5]
    },
    imageHeaders: {
        fontSize: 8,
        bold: true,
        alignment: 'center',
        margin: [0, 5, 0, 5]
    },
    Titleheader: {
        fontSize: 20,
        bold: true,
        alignment: 'center',
        margin: [0, 200, 0, 80]
    },
    tableValue: {
        bold: true,
        fontSize: 8,
        color: 'black'
    },
    List: {
        fontSize: 8,
        bold: false
    },
    tableList: {
        fontSize: 6
    },
    introList: {
        fontSize: 8,
        alignment: 'justify'
    },
    inlineList: {
        fontSize: 8,
        bold: true
    },
    tableExample: {
        alignment: 'left',
        fontSize: 6,
        margin: [0, 5, 0, 5]
    },
    tableHeader: {
        bold: true,
        fontSize: 8,
        color: 'black'
    },
    footer: {
        fontSize: 10,
        alignment: 'center',
        bold: true,
        margin: [0, 5, 0, 5]
    }
}
var defaultStyle = {
    fontSize: 12
}

// >>>>>>> End Styles >>>>>>>
// <<<<< PDFMake Dependencies End >>>>>
getOrg_Details();
// >>>> [Get Organizational Info from Collection]
function getOrg_Details() {
    orgInfo.find({}, function(err, data) {
        orgDetails = data[0];
    })
}



// ================================================================================================================================

/*
@Type: Modified 
@developer: Santhosh Kumar Gunti

@description : setting dynamic path for logo 
*/

var path = require('path');
//Added file before directory as image path recognises only with file tag.
//Modified by Divya Bandaru on 16-04-2018
var inputFilepath = "file:///"+path.join(__dirname, '../..', 'images', 'mainpage_logo.png');

//back porting form Ue by venki 
router.get('/getLatLongWIthForm/:formId/:taskId/:user', function(req, res, next) {
    var data = {};

    Tasks.find({
        _id: req.params.taskId,
        isDeleted: false
    }, function(err, tasks) {
        if (tasks.length > 0) {
            latlongjson(req.params.formId, req.params.taskId, req.params.user, function(ress) {
                data = {
                    latlngData: ress
                }
            });
            setTimeout(function() {
                res.json({
                    "data": data,
                    "status": 200
                })
            }, 1000);

        } else {
            res.json({
                "message": "No data found",
                "status": 204
            });
        }
    })
});

function latlongjson(formId, taskId, user, callback) {
    Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, formsz) {
        var latlongData = [];
        var latlongDataDetails = {};
        var task = [];
        var locationDetails = {};
        if (formsz.length > 0) {
            var mapFields = [];
            latlongDataDetails.formId = formsz[0]._id;
            mapFields = formsz[0].geoFields;
            if(mapFields.length > 0){
                FormszDetails.find({
                    $or: [{
                        formId: formId,
                        taskId: taskId,
                        DSAssignedTo: user
                    }, {
                        formId: formId,
                        taskId: taskId,
                        updatedBy: user
                    }]
                }, function(err, details) {
                    var taskdata = {};
                    var locations = [];
                    if (details.length > 0) {
                        async.forEachSeries(details, function(recorddata, next) {
                            taskdata.recordId = recorddata._id;
                            //--> UE Implementation:11-07-2017
                            //Addition:Divya
                            //Added this variable to check the address is carded or non-carded address
                            taskdata.status = recorddata.status;
                            //added on 19th Sept 2017
                          //  taskdata.markerStyleCode = recorddata.markerStyleCode;
                            //<--

                            for (var i = 0; i < mapFields.length; i++) {
                                var values = recorddata.record[0][mapFields[i]];
                               // var res = values.split(",");
                                if (values == "") {
                                    locationDetails.lat = "";
                                    locationDetails.long = "";
                                    locations.push(locationDetails);
                                    locationDetails = {};
                                    taskdata.geometries = locations;
                                } else {
                                 if(values!= undefined) {

                                    var res = values.split(",");
                                    locationDetails.lat = res[0];
                                    locationDetails.long = res[1];
                                    locations.push(locationDetails);
                                    locationDetails = {};
                                    taskdata.geometries = locations;
                                }
                                }
                            }
                            setTimeout(function() {}, 1000);
                            task.push(taskdata);
                            taskdata = {};
                            locations = [];
                            next();
                        })
                    }

                })
            }
            latlongDataDetails.records = task;
            latlongData.push(latlongDataDetails);
            callback(latlongData);

        }
    })
}
//backporting end
router.get('/getPrePopulatedDataforUser/:taskId/:user/:formId', function (req, res, next) {
    var data = [];
    var taskdata = {}
    var DisplayValues = []
    var DisplayValuesdata = {}
    Tasks.find({_id : req.params.taskId}, function (err, post) {
        if (post.length > 0) {
            var formsids = []
            var count = 0;
            //async.forEachSeries(post[0].assignedFormsz, function (formsz, callback) {
                prepopdatajson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data.push(ress);
                });
                //callback();
                setTimeout(function () {
                    res.json(data)
                }, 1000);
        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 200
            });
        }
    });
});

//Get All Re-Assign Records
router.get('/getReAssignForms', function (req, res, next) {
    FormszDetails.find({IsReassign : true}, req.body, function (err, post) {
        res.json(post);
    });
});
router.get('/getRe-assginedRecods/:id/:user', function (req, res, next) {
    FormszDetails.find({IsReassign : true,updatedBy : req.params.user,formId : req.params.id,status : true}, function (err, post1) {
        if (post1.length > 0) {
            res.json(post1);

        } else {
            res.json({
                message : "No data found",
                status : 204
            });
        }
    });
});

//get latlong data
router.get('/getLatLongWIthForm/:formId/:taskId/:user', function (req, res, next) {
    var data = {};

    Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                latlongjson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data = {
                        latlngData : ress
                    }
                });
                setTimeout(function () {
                    res.json({
                        "data" : data,
                        "status" : 200
                    })
                }, 1000);

        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    })
});

// ==========================================================================================
// >>> Date: 21-April-2018
// >>> Author : Phani Kumar Narina
// >>> Module : [GeneratePDF from Web] single and Bulk Generation
// >>> Parameters : recordIds and metadata information
// >>> Opensource plugin used : PDFMAKE refer: http://pdfmake.org/#/
// >>> Dependencies :
// 1) node_modules/pdfmake/src/printer
// 2) node_modules/pdfmake/build/pdfmake.js
// 3) node_modules/pdfmake/build/vfs_fonts.js
// 4) fonts in public folder
// >>> Type : Creation (Complete New Implementation)
router.post('/generatePDF', function(req, res, next) {
    //console.log("IN Web Generate PDF");
    var headers = [];
    var headerslbl = [];
    var mailData = req.body;
    Formsz.find({
        _id: req.body.formid
    }, function(err, post1) {
        if (post1.length > 0) {
            var recordids = [];
            recordids = req.body.records;
            console.log(recordids);
            FormszDetails.find({
                _id: {
                    $in: recordids
                },
                isDeleted: false
            }, function(err, records) {
                //console.log(records);
                fileid = mongoid;
                // >>>> FINAL LAYOUT ARRAY
                var finalArray = [];
                // >>> LandingPage Logic
                var landingPage = [
                    {
                        image:orgDetails.imageurl,
                        width: 300,
                        alignment: 'center', style: 'Titleheader'
                    },{
                        text: 'RECORD INFORMATION\n',
                        bold: true,
                        alignment: 'center'
                    }
                ]
                //finalArray.push(landingPage);
                // <<<< End LandingPage
                var count = 0;
                var tableHeaderBlock;
                // NO Preview
                var nopreview = "data:image/jpeg;base64,/9j/4QYARXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAiAAAAcgEyAAIAAAAUAAAAlIdpAAQAAAABAAAAqAAAANQALcbAAAAnEAAtxsAAACcQQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpADIwMTY6MDI6MTIgMTY6MDM6MjgAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAJagAwAEAAAAAQAAAJYAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAExgAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAf/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIACQAJAMBIgACEQEDEQH/3QAEAAP/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APTOodQxenYrsrKfsrboO5cT9FjG/nPcsut/1k6oBdW6vpGK7WsFouvcOznh36Cvcm9FnWPrBf8AaALMLpTRUypwlrrrW7rbHtP0vSr/AEayMPrhwG5+J0u9luJiXPxh9rDj9jva91ddWX6Z9b9mZbmbMXOZv9L/AAnvq9JJTtnpX1gqBdR1g2PGuy6hhYT4Sza9n9lSw+tZDMtnTusUjFy7J9C1hJouj/Qvd7mWf8FYucyPrx9Yel3Czq2JhPwqHMHUvsVzrbsZtoml11L9rm79zPTs2Px7v9P7610JsxfrP03LxjTdi249np7choZZVcGV5NNrdjrP8HkVO+mkp2klz37eyf8Amr9t/wC9D+jbf+7G/wCzTH9b9NtSSU//0O76Cdmf1nHdpY3L9U/1LWMdUf8AoLzSjq1PT/rf1qcmjGsyrczFruyAH0te7JbeyvNYQ/8AUsr7P9kybNv6Fl/rf8IvSur0ZWDnM65g1m7az0s7HZ9Kymdzba/+Go/6hXMKzo/U6ftWK2m5rzLjtbuDjz6jSNzX/wBdJTwr68TIxHWh1Vfq4d2KwjJrvOI3KY+mvE6hdhvv+1dDfk/p8HIs/S49jP8AB/4bX/xYY+RidI6hi5dbqcmnPe22t5kg+ji7Tu19Rj2e+q1n6O2r+bXVDp+AA8NxqgLGllgDGjc1302Oge5rly4+2V5GR9Xui5AyMa0AC2S44bCYuqN30bG7P5hm/wBViSmE/wCQP2hH6v8Atb7XP/Bets3JLp/2Vh/sv9lbf1X0vRjSdsbd3H85+fu/fSSU/wD/0fVVzHVv+bH253p+t+059/7N3+tP53qeh+i3fver718+JJKfd3fs+P8AKf7c+zfn/at3pR/wn2f3rpukfsn7E39k+l9l7elxP8v871P+M/SL5iSSU/VSS+VUklP/2f/tDf5QaG90b3Nob3AgMy4wADhCSU0EJQAAAAAAEAAAAAAAAAAAAAAAAAAAAAA4QklNBDoAAAAAAOUAAAAQAAAAAQAAAAAAC3ByaW50T3V0cHV0AAAABQAAAABQc3RTYm9vbAEAAAAASW50ZWVudW0AAAAASW50ZQAAAABDbHJtAAAAD3ByaW50U2l4dGVlbkJpdGJvb2wAAAAAC3ByaW50ZXJOYW1lVEVYVAAAAAEAAAAAAA9wcmludFByb29mU2V0dXBPYmpjAAAADABQAHIAbwBvAGYAIABTAGUAdAB1AHAAAAAAAApwcm9vZlNldHVwAAAAAQAAAABCbHRuZW51bQAAAAxidWlsdGluUHJvb2YAAAAJcHJvb2ZDTVlLADhCSU0EOwAAAAACLQAAABAAAAABAAAAAAAScHJpbnRPdXRwdXRPcHRpb25zAAAAFwAAAABDcHRuYm9vbAAAAAAAQ2xicmJvb2wAAAAAAFJnc01ib29sAAAAAABDcm5DYm9vbAAAAAAAQ250Q2Jvb2wAAAAAAExibHNib29sAAAAAABOZ3R2Ym9vbAAAAAAARW1sRGJvb2wAAAAAAEludHJib29sAAAAAABCY2tnT2JqYwAAAAEAAAAAAABSR0JDAAAAAwAAAABSZCAgZG91YkBv4AAAAAAAAAAAAEdybiBkb3ViQG/gAAAAAAAAAAAAQmwgIGRvdWJAb+AAAAAAAAAAAABCcmRUVW50RiNSbHQAAAAAAAAAAAAAAABCbGQgVW50RiNSbHQAAAAAAAAAAAAAAABSc2x0VW50RiNQeGxAcsAAAAAAAAAAAAp2ZWN0b3JEYXRhYm9vbAEAAAAAUGdQc2VudW0AAAAAUGdQcwAAAABQZ1BDAAAAAExlZnRVbnRGI1JsdAAAAAAAAAAAAAAAAFRvcCBVbnRGI1JsdAAAAAAAAAAAAAAAAFNjbCBVbnRGI1ByY0BZAAAAAAAAAAAAEGNyb3BXaGVuUHJpbnRpbmdib29sAAAAAA5jcm9wUmVjdEJvdHRvbWxvbmcAAAAAAAAADGNyb3BSZWN0TGVmdGxvbmcAAAAAAAAADWNyb3BSZWN0UmlnaHRsb25nAAAAAAAAAAtjcm9wUmVjdFRvcGxvbmcAAAAAADhCSU0D7QAAAAAAEAEsAAAAAQABASwAAAABAAE4QklNBCYAAAAAAA4AAAAAAAAAAAAAP4AAADhCSU0EDQAAAAAABAAAAFo4QklNBBkAAAAAAAQAAAAeOEJJTQPzAAAAAAAJAAAAAAAAAAABADhCSU0nEAAAAAAACgABAAAAAAAAAAE4QklNA/UAAAAAAEgAL2ZmAAEAbGZmAAYAAAAAAAEAL2ZmAAEAoZmaAAYAAAAAAAEAMgAAAAEAWgAAAAYAAAAAAAEANQAAAAEALQAAAAYAAAAAAAE4QklNA/gAAAAAAHAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAOEJJTQQAAAAAAAACAAE4QklNBAIAAAAAAAQAAAAAOEJJTQQwAAAAAAACAQE4QklNBC0AAAAAAAYAAQAAAAI4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADSQAAAAYAAAAAAAAAAAAAAJYAAACWAAAACgBVAG4AdABpAHQAbABlAGQALQAyAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAACWAAAAlgAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAABAAAAABAAAAAAAAbnVsbAAAAAIAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAAAlgAAAABSZ2h0bG9uZwAAAJYAAAAGc2xpY2VzVmxMcwAAAAFPYmpjAAAAAQAAAAAABXNsaWNlAAAAEgAAAAdzbGljZUlEbG9uZwAAAAAAAAAHZ3JvdXBJRGxvbmcAAAAAAAAABm9yaWdpbmVudW0AAAAMRVNsaWNlT3JpZ2luAAAADWF1dG9HZW5lcmF0ZWQAAAAAVHlwZWVudW0AAAAKRVNsaWNlVHlwZQAAAABJbWcgAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAJYAAAAAUmdodGxvbmcAAACWAAAAA3VybFRFWFQAAAABAAAAAAAAbnVsbFRFWFQAAAABAAAAAAAATXNnZVRFWFQAAAABAAAAAAAGYWx0VGFnVEVYVAAAAAEAAAAAAA5jZWxsVGV4dElzSFRNTGJvb2wBAAAACGNlbGxUZXh0VEVYVAAAAAEAAAAAAAlob3J6QWxpZ25lbnVtAAAAD0VTbGljZUhvcnpBbGlnbgAAAAdkZWZhdWx0AAAACXZlcnRBbGlnbmVudW0AAAAPRVNsaWNlVmVydEFsaWduAAAAB2RlZmF1bHQAAAALYmdDb2xvclR5cGVlbnVtAAAAEUVTbGljZUJHQ29sb3JUeXBlAAAAAE5vbmUAAAAJdG9wT3V0c2V0bG9uZwAAAAAAAAAKbGVmdE91dHNldGxvbmcAAAAAAAAADGJvdHRvbU91dHNldGxvbmcAAAAAAAAAC3JpZ2h0T3V0c2V0bG9uZwAAAAAAOEJJTQQoAAAAAAAMAAAAAj/wAAAAAAAAOEJJTQQUAAAAAAAEAAAAAjhCSU0EDAAAAAAE4gAAAAEAAAAkAAAAJAAAAGwAAA8wAAAExgAYAAH/2P/tAAxBZG9iZV9DTQAB/+4ADkFkb2JlAGSAAAAAAf/bAIQADAgICAkIDAkJDBELCgsRFQ8MDA8VGBMTFRMTGBEMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAENCwsNDg0QDg4QFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAJAAkAwEiAAIRAQMRAf/dAAQAA//EAT8AAAEFAQEBAQEBAAAAAAAAAAMAAQIEBQYHCAkKCwEAAQUBAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAABBAEDAgQCBQcGCAUDDDMBAAIRAwQhEjEFQVFhEyJxgTIGFJGhsUIjJBVSwWIzNHKC0UMHJZJT8OHxY3M1FqKygyZEk1RkRcKjdDYX0lXiZfKzhMPTdePzRieUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9jdHV2d3h5ent8fX5/cRAAICAQIEBAMEBQYHBwYFNQEAAhEDITESBEFRYXEiEwUygZEUobFCI8FS0fAzJGLhcoKSQ1MVY3M08SUGFqKygwcmNcLSRJNUoxdkRVU2dGXi8rOEw9N14/NGlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vYnN0dXZ3eHl6e3x//aAAwDAQACEQMRAD8A9M6h1DF6diuysp+ytug7lxP0WMb+c9yy63/WTqgF1bq+kYrtawWi69w7OeHfoK9yb0WdY+sF/wBoAswulNFTKnCWuutbutse0/S9Kv8ARrIw+uHAbn4nS72W4mJc/GH2sOP2O9r3V11Zfpn1v2ZluZsxc5m/0v8ACe+r0klO2elfWCoF1HWDY8a7LqGFhPhLNr2f2VLD61kMy2dO6xSMXLsn0LWEmi6P9C93uZZ/wVi5zI+vH1h6XcLOrYmE/CocwdS+xXOtuxm2iaXXUv2ubv3M9OzY/Hu/0/vrXQmzF+s/TcvGNN2Lbj2entyGhllVwZXk02t2Os/weRU76aSnaSXPft7J/wCav23/AL0P6Nt/7sb/ALNMf1v021JJT//Q7voJ2Z/Wcd2ljcv1T/UtYx1R/wCgvNKOrU9P+t/WpyaMazKtzMWu7IAfS17slt7K81hD/wBSyvs/2TJs2/oWX+t/wi9K6vRlYOczrmDWbtrPSzsdn0rKZ3Ntr/4aj/qFcwrOj9Tp+1YrabmvMuO1u4OPPqNI3Nf/AF0lPCvrxMjEdaHVV+rh3YrCMmu84jcpj6a8TqF2G+/7V0N+T+nwciz9Lj2M/wAH/htf/Fhj5GJ0jqGLl1upyac97ba3mSD6OLtO7X1GPZ76rWfo7av5tdUOn4ADw3GqAsaWWAMaNzXfTY6B7muXLj7ZXkZH1e6LkDIxrQALZLjhsJi6o3fRsbs/mGb/AFWJKYT/AJA/aEfq/wC1vtc/8F62zckun/ZWH+y/2Vt/VfS9GNJ2xt3cfzn5+799JJT/AP/R9VXMdW/5sfbnen637Tn3/s3f60/nep6H6Ld+96vvXz4kkp93d+z4/wAp/tz7N+f9q3elH/CfZ/eum6R+yfsTf2T6X2Xt6XE/y/zvU/4z9IvmJJJT9VJL5VSSU//ZOEJJTQQhAAAAAABdAAAAAQEAAAAPAEEAZABvAGIAZQAgAFAAaABvAHQAbwBzAGgAbwBwAAAAFwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAgAEMAQwAgADIAMAAxADUAAAABADhCSU0EBgAAAAAABwAIAAAAAQEA/+EOomh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMTEgNzkuMTU4MzI1LCAyMDE1LzA5LzEwLTAxOjEwOjIwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNi0wMi0xMlQxNjowMzoyOCswNTozMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5MzU4NTMyYi0zYjFkLTE1NDctOTdmMS1mOTllOTkwMzc3ZWMiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDowNjVkZTNmZi1kMTc0LTExZTUtOWI1ZC1kNThjMzkxZDExMWMiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmMGQ5YWU1OS02ZmU4LWUwNDktOTYwMS1mNzgzOWFmMjcwN2IiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgZGM6Zm9ybWF0PSJpbWFnZS9qcGVnIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGQ5YWU1OS02ZmU4LWUwNDktOTYwMS1mNzgzOWFmMjcwN2IiIHN0RXZ0OndoZW49IjIwMTYtMDItMTJUMTY6MDM6MjgrMDU6MzAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1IChXaW5kb3dzKSIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTM1ODUzMmItM2IxZC0xNTQ3LTk3ZjEtZjk5ZTk5MDM3N2VjIiBzdEV2dDp3aGVuPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDxwaG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+IDxyZGY6QmFnPiA8cmRmOmxpPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDowNWY0YzZjZC1kMTczLTExZTUtOWI1ZC1kNThjMzkxZDExMWM8L3JkZjpsaT4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/iDFhJQ0NfUFJPRklMRQABAQAADEhMaW5vAhAAAG1udHJSR0IgWFlaIAfOAAIACQAGADEAAGFjc3BNU0ZUAAAAAElFQyBzUkdCAAAAAAAAAAAAAAABAAD21gABAAAAANMtSFAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEWNwcnQAAAFQAAAAM2Rlc2MAAAGEAAAAbHd0cHQAAAHwAAAAFGJrcHQAAAIEAAAAFHJYWVoAAAIYAAAAFGdYWVoAAAIsAAAAFGJYWVoAAAJAAAAAFGRtbmQAAAJUAAAAcGRtZGQAAALEAAAAiHZ1ZWQAAANMAAAAhnZpZXcAAAPUAAAAJGx1bWkAAAP4AAAAFG1lYXMAAAQMAAAAJHRlY2gAAAQwAAAADHJUUkMAAAQ8AAAIDGdUUkMAAAQ8AAAIDGJUUkMAAAQ8AAAIDHRleHQAAAAAQ29weXJpZ2h0IChjKSAxOTk4IEhld2xldHQtUGFja2FyZCBDb21wYW55AABkZXNjAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAA81EAAQAAAAEWzFhZWiAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPZGVzYwAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdmlldwAAAAAAE6T+ABRfLgAQzxQAA+3MAAQTCwADXJ4AAAABWFlaIAAAAAAATAlWAFAAAABXH+dtZWFzAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAACjwAAAAJzaWcgAAAAAENSVCBjdXJ2AAAAAAAABAAAAAAFAAoADwAUABkAHgAjACgALQAyADcAOwBAAEUASgBPAFQAWQBeAGMAaABtAHIAdwB8AIEAhgCLAJAAlQCaAJ8ApACpAK4AsgC3ALwAwQDGAMsA0ADVANsA4ADlAOsA8AD2APsBAQEHAQ0BEwEZAR8BJQErATIBOAE+AUUBTAFSAVkBYAFnAW4BdQF8AYMBiwGSAZoBoQGpAbEBuQHBAckB0QHZAeEB6QHyAfoCAwIMAhQCHQImAi8COAJBAksCVAJdAmcCcQJ6AoQCjgKYAqICrAK2AsECywLVAuAC6wL1AwADCwMWAyEDLQM4A0MDTwNaA2YDcgN+A4oDlgOiA64DugPHA9MD4APsA/kEBgQTBCAELQQ7BEgEVQRjBHEEfgSMBJoEqAS2BMQE0wThBPAE/gUNBRwFKwU6BUkFWAVnBXcFhgWWBaYFtQXFBdUF5QX2BgYGFgYnBjcGSAZZBmoGewaMBp0GrwbABtEG4wb1BwcHGQcrBz0HTwdhB3QHhgeZB6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUixStFM4U8BUSFTQVVhV4FZsVvRXgFgMWJhZJFmwWjxayFtYW+hcdF0EXZReJF64X0hf3GBsYQBhlGIoYrxjVGPoZIBlFGWsZkRm3Gd0aBBoqGlEadxqeGsUa7BsUGzsbYxuKG7Ib2hwCHCocUhx7HKMczBz1HR4dRx1wHZkdwx3sHhYeQB5qHpQevh7pHxMfPh9pH5Qfvx/qIBUgQSBsIJggxCDwIRwhSCF1IaEhziH7IiciVSKCIq8i3SMKIzgjZiOUI8Ij8CQfJE0kfCSrJNolCSU4JWgllyXHJfcmJyZXJocmtyboJxgnSSd6J6sn3CgNKD8ocSiiKNQpBik4KWspnSnQKgIqNSpoKpsqzysCKzYraSudK9EsBSw5LG4soizXLQwtQS12Last4S4WLkwugi63Lu4vJC9aL5Evxy/+MDUwbDCkMNsxEjFKMYIxujHyMioyYzKbMtQzDTNGM38zuDPxNCs0ZTSeNNg1EzVNNYc1wjX9Njc2cjauNuk3JDdgN5w31zgUOFA4jDjIOQU5Qjl/Obw5+To2OnQ6sjrvOy07azuqO+g8JzxlPKQ84z0iPWE9oT3gPiA+YD6gPuA/IT9hP6I/4kAjQGRApkDnQSlBakGsQe5CMEJyQrVC90M6Q31DwEQDREdEikTORRJFVUWaRd5GIkZnRqtG8Ec1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4SbmtuxG8eb3hv0XArcIZw4HE6cZVx8HJLcqZzAXNdc7h0FHRwdMx1KHWFdeF2Pnabdvh3VnezeBF4bnjMeSp5iXnnekZ6pXsEe2N7wnwhfIF84X1BfaF+AX5ifsJ/I3+Ef+WAR4CogQqBa4HNgjCCkoL0g1eDuoQdhICE44VHhauGDoZyhteHO4efiASIaYjOiTOJmYn+imSKyoswi5aL/IxjjMqNMY2Yjf+OZo7OjzaPnpAGkG6Q1pE/kaiSEZJ6kuOTTZO2lCCUipT0lV+VyZY0lp+XCpd1l+CYTJi4mSSZkJn8mmia1ZtCm6+cHJyJnPedZJ3SnkCerp8dn4uf+qBpoNihR6G2oiailqMGo3aj5qRWpMelOKWpphqmi6b9p26n4KhSqMSpN6mpqhyqj6sCq3Wr6axcrNCtRK24ri2uoa8Wr4uwALB1sOqxYLHWskuywrM4s660JbSctRO1irYBtnm28Ldot+C4WbjRuUq5wro7urW7LrunvCG8m70VvY++Cr6Evv+/er/1wHDA7MFnwePCX8Lbw1jD1MRRxM7FS8XIxkbGw8dBx7/IPci8yTrJuco4yrfLNsu2zDXMtc01zbXONs62zzfPuNA50LrRPNG+0j/SwdNE08bUSdTL1U7V0dZV1tjXXNfg2GTY6Nls2fHadtr724DcBdyK3RDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23////uAA5BZG9iZQBkQAAAAAH/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAgICAgICAgICAgMDAwMDAwMDAwMBAQEBAQEBAQEBAQICAQICAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDA//AABEIAJYAlgMBEQACEQEDEQH/3QAEABP/xAGiAAAABgIDAQAAAAAAAAAAAAAHCAYFBAkDCgIBAAsBAAAGAwEBAQAAAAAAAAAAAAYFBAMHAggBCQAKCxAAAgEDBAEDAwIDAwMCBgl1AQIDBBEFEgYhBxMiAAgxFEEyIxUJUUIWYSQzF1JxgRhikSVDobHwJjRyChnB0TUn4VM2gvGSokRUc0VGN0djKFVWVxqywtLi8mSDdJOEZaOzw9PjKThm83UqOTpISUpYWVpnaGlqdnd4eXqFhoeIiYqUlZaXmJmapKWmp6ipqrS1tre4ubrExcbHyMnK1NXW19jZ2uTl5ufo6er09fb3+Pn6EQACAQMCBAQDBQQEBAYGBW0BAgMRBCESBTEGACITQVEHMmEUcQhCgSORFVKhYhYzCbEkwdFDcvAX4YI0JZJTGGNE8aKyJjUZVDZFZCcKc4OTRnTC0uLyVWV1VjeEhaOzw9Pj8ykalKS0xNTk9JWltcXV5fUoR1dmOHaGlqa2xtbm9md3h5ent8fX5/dIWGh4iJiouMjY6Pg5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6vr/2gAMAwEAAhEDEQA/AN/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3QW757w6Z6x8w7E7W672TLAoeSl3PvHb+FrgpTyDRQV1fDWys0fqAWMkjke/de6K1m/5nfwdwjvEe9MZmJo3MbR7c2xvbPKCPyKnG7bnomQj6ESkH8e/de6S6fzYfhEzBT2VuGMH6SSda9grH/r3G3SeR/h7917oR9s/wAxr4TbsqIKTHfITZdBVT6QsO54s7s/Qz/pWWfdWIw9LEx/F5OffuvdGt2pvvY+/KNshsfeW1d5UCCNnrdq7hxO4aRBKCYy9RiaurhUSBTa55t7917pV+/de697917r3v3Xuve/de697917r3v3Xuve/de6/9Df49+691737r3Xvfuvde9+691CyWSx2Hx9blsvX0WKxeNpZq3I5LJVUFDj6Cjpo2lqKutrKqSKnpaWniUs8jsqIoJJA9+691UD8iv5wfUmw5a/bfQGCPdm5acz0z7smq5sF1Zj6qM6CYMwIJcxvARSqVYY6BKVxYpVke/de6pp7j+dfyu7ymq4t1dt53bm36sSJ/cvrKSbYW20ppJPN9pUyYmc7kzMcTWCtW5Cc2UcfX37r3RSMbjZ9wZTwYPFZLdGenkIMGExmT3RnaiYsS3lGPp8lkpJixuTIb+/de6Mhgvh38tdzRwT4n43dxSQVSJJTVOT2s23qeaJ1DpKsm5KvEMsboQQWVQb+/de6V038v75sQRNPJ8cd6NGq6isGZ2JUzkWvZaeHdrzM3+ABPv3Xugv3V8Zfkpsmjmrd2/HzuXC4yK4nr5dhZrI45BYk+SqwkWVpwLKfqRwL/T37r3QLYfI1u2cutVtzKZbae4qSQutRgcjlNq7gpZYyCxc4+fG5SNkYchx+Pp7917qwvpX+aJ8seonpqLPbnou7tsROolxHaCs+4lhZ0M5oN+4iGHNipCJaM18WSjBY3X6Ee691d/8Zf5lPx5+RtVjdq1GQqOqO0MgUhg2Dv2ppKdczWMbGDZ26oWXBboYsyhIVanr3J/4CgC/v3XurCffuvde9+691737r3Xvfuvde9+691//0d/j37r3Xvfuvde9+690Xn5J/J3qr4sbDbfHZ2WkjeulnoNp7TxKxVe7N7ZuKHznEbcxbyw+doYyr1NTK0VJRxMHnlQFdXuvdasnyo+andXy2y1RDvOvba3WcFe1Vt3p3btdK+26OKMj7Op3fXJHTTb93BEigtLUoKCCRn+1powxZvde6ZPjp8Pu/flLWK3Vu00h2fFO1Lke0d2yVGF68x8sPjWanochHTVFduyvpg6hqXFQ1HjuBLJCOffuvdXpdHfyf/jxsKGkyfcFfmu9dzogeekzLybb68ppy0cqii2dhqoVFeKeQMgfJV1asqW1Rqb+/de6s+2jsPY/X+OXEbE2dtfZmLVYkGO2tgMVgKMrCnji10+KpaWJzGgsCQT7917pWe/de697917r3v3Xugc7S+PXR/dlDPj+1uq9kb4jqEVDWZrA0UmZgCAhGodwQRwZ3GyoCQHp6mJwDwffuvdVJ9//AMmTbeQjrc/8aN+Ve1chaWdOu+x6mr3BtOpdpGk+2w+7oop91beRIgI4lqVysVzzpHI917qjntrpvszpXc8uwu5NiZbZO4HDT0dFl44qrFZ6mp5GAym1NwUbTYfcdFDJGSJ6OZpYGX1rE4sPde6sQ+G380HsboyoxOwO8anOdqdNxx0+Oo89NI+U7L64pkbTFNBWTE1m/drUkR0PR1LvlKaJV+2mmSMUze691su7J3ttLsfaeB31sTcOL3XtDdGOhyuA3BhqpKzHZKhnuFlgmTlXjkVo5Y3Cywyo0ciq6so917pUe/de697917r3v3Xuv//S3+Pfuvde9+690W/5TfJ3YHxS6tyHY29WkyOQnlbEbI2ZQTxRZvfO7JqeWahwWNMoZaWmRYmmrq11aGho43lYMQkb+691qGd192dk/IfsbL9p9q5v+Lblyi/Z4/H0hmi25s3b0cry0O0NnY+V2/h2CoS5Z3N6mvqWepqXkmcke690ev8AlpfETpP5N7r3Pmu2t50mYbr2alqY+gaWSqxmR3LQTPAKXem68jqgmy+xfvGal+wxzWNUAMhIitFTz+691tEYnEYnAYygwmCxmOwuGxVJBQYvEYmipsbjMbQ00YipqKgoKOKGlo6SniUKkcaKiKLAAe/de6cPfuvde9+691737r3Xvfuvde9+691737r3XvfuvdBb3B0t1j33srIdfdr7Rxm7ttV9pEgrY2jrsVXoCKfMYDLU7RZLBZqkJJiqqWWKZQSuoqzKfde61cfm18C99fEfMPuXE1GS310Rl8glPg99zRRvm9nVdZNoodtdkpSRQ00UsjMsVJmokjpa+SySpTzsqSe690y/CT5s7x+IG9DTVRye5ejt1ZSObsPYEJNTUYapl0QT9gbDppXCUm6KOBQa+iQxwZqniCvaqjp5l917rbW2fu/bHYG1tv722Xm8fuTae6cTRZzb+dxcwnoMpi8hCs9LV08llYB439SMFkjYFXVWBA917pSe/de697917r//09/j37r3TLuTceD2ft7O7s3Pk6XC7c2ziMjn89mK5zHR4vD4ikmr8lkKpwGKwUlHA8jWBNl4BPv3XutN75dfJ/cnyy7jynY+Sasx+y8UlVg+qdp1TeNNs7LE2oZGsp1Ywpufd5jStychLPGDFShzFTKPfuvdDd09/Lc7y7o+N2c7928aWgy9U0WT6q62ysX2lf2bs+jSVsrnRkpzGNv1ecI/37qTL4a1I/JOYoamCVfde6JHsvee++pN+4jfGyMtmNh9kbAzVZHRVr0klJl8DmKOR6DObc3FhK1U89NNoejyuKrEMVRHqjkX9LD3XutsH4Q/N7Zny82ZJBURUG0u6Np0FM/YfXiVTyRCJ3Wlj3ls2WqP3WX2RlqqyqzaqjG1D/a1Xr8M1R7r3R6Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690ybl21t/eO38ztTdeGx24dtbhx1XiM5g8vSQ12MyuMroWgq6KtpJ1eKennichlI/3n37r3Wpb88PhjlfiJ2NSPt6PIZPpDfVTUN1vuCplmrKnbmShjkq6vrbcddJqlkyeNpY2nxVTKxfIY6NgWaemnJ917oxv8qb5gz9Vb9pPjfv3KN/oz7NzLL11V1kpMGx+y8k5YbfhZyWp9vdiVFxFGD4qfNadKqK2Vh7r3WzB7917r3v3Xuv/1N/j37r3VGv85P5FtittbV+L23K0rWb4hp989oeCSxTZOMyDxbZ21UaGN03TuSheonjYKTS4zSbpMQfde6qz+DXxfb5Xd84rZmZhnPWe0aaDenbNUglVarbsFWYcTs6KpjeIxVm+MtCaaTTIsqY2GrkUFlX37r3W4bRUVHjaOkx2OpKagx9BTQUVDQ0UEVLR0VHSxJBTUlJTQJHBTU1NBGqRxooREAAAA9+691Uz/MQ/l3UvfFNku6+lMdRYzvPHUSPuDbyGCgxXcOLx8Hjio62djHT4/ftBSxhMdkpPRUIq0lWfEYpqb3XutcXZ28t+9Sb8xW9dlZbN9f8AZWwM3WR0dZJRSUWa29mqKR6DN7d3DhK9E81NKUejyuKrEMNTEWjlUjSw917ra++EHzh2Z8u9ny0dTFRbT7p2ljqaXsLr5ah3heFnSkTemzJaljU5XZOVq2CgsWqcZUv9rVXbwzVPuvdI3v7+bf8Ay8Pi32vuLo7v/wCR+L6y7T2rSYTIZraeb2B2zUzRY7cWNgy2FyVDlMPsLJYPM46toqhbT0VVURRyh4XZZo5I1917oHP+H8f5Rf8A3mdtH/0X3dH/ANrX37r3Xv8Ah/H+UX/3mdtH/wBF93R/9rX37r3Xv+H8f5Rf/eZ20f8A0X3dH/2tffuvdGr+Kn8xX4WfNzN7y218W++9tds7g6/xmIzW7cLjsPvDb2TxeHzlTWUWOykdHvLbm3J8pj3rKF4ZZqMVEdNK0azGNpYg/uvdHV9+691737r3XvfuvdAx8gujtnfI3qTeHUW94SMVujHlaHKwxJJkts7go2FVgN04ZmaMxZTA5OOOeOzqJVVonJikdT7r3Wlt2BsLdPWe994dZ73p5MVvPYW46zb2b+zaSER5PFzx1GPzuFn0xP8AZ5GnamyWPnT6RyxspuPfuvdbb/wH+R7fJn45bU3bmKmObf8AtZ32F2XGpAeTd23qelV80EvqEG6sTUUuTQ6VQNVPGv8Amz7917o6Pv3Xuv/V39ZJI4Y3lldIookaSSSRlSOONFLO7uxCoiKCSSbAe/de60j/AJHdwz99d7ds9x1U0n2G7d11528KnSGoNi7dH8C2dSMUAQJHgMfHUOQAGlndjyT7917rZU/lcdCRdM/F/b+5cnjzS747sli7L3NJPGgq4MRkKcQ7EwhkVmZqXG7UENQENilTXT8XJv7r3Vj/AL917oBvkT8n+gPiXsbH9lfI/tPbHUWxMpujG7Lx25t1zVcOOq905ehyuTxuEhajpayZq2roMHVzKNGnRTuSRb37r3VS3yK6N+OX8yrr/NfK/wCBHZfX/ZnYO366s27u5dmZanXCdnVW3qeES7Z3NFUpRz7Z7JxuPaJ8VX1cUK1lJJFFUF6SWnqKf3XuqN9o7u371LvzGbx2bls7172X1/m6yKkrHpJaDO7azdE70Gb2/uDCV6xmSGUK9HlcVWIYamFmikUgqw917q4Lt3qP43/z4/jfJs/dqYvpn5sdM4WXI7V3ZQU/3VdtevrNEEmUxSzOmQ3x0HvbJRxxZPGyu1Tial1VnSrSmqan3XutJfsf4X/LLq35A7q+LW4Ohe0M73rtGpVarZXXWy91dgPuDD1MpjxG89n1W3MRWrnNk7jitLRVwCD9UMojqI5Yl917pZ5T+W//ADFMLi5czk/gr8s6fG08DVNRPH0tvCtlp6dIzLJNUUOOo6zIwxxRqS5aEaAPVb37r3RN8jSZLDZavwGboMrg8/i5GiyeBzuOyGEzmOkRzGyV+Gy1PR5OjKyKV/ciXkW9+690dz+W58yc18DvmV1B8h6Z8hUbOocmdj9zYKg88sm4+md6VFJQbyhShhqqNcnk9rPHTZ/GQyyCI5HFRBrqzA+6919AL/h67+VT/wB5v9LfQH/i4Zz8gEf8uP62P+wPHv3XuuL/AM7H+VPGjyP84Olgkas7n7/OmyoCzGwwZJsB+PfuvdWSbM3jtjsTZ20+wNk5qi3JszfW2sFvHaO4sa7yY/PbY3Ni6XNYDNUDyJG70WUxVbFPESqkpILgfT37r3Sl9+691r0fznOh4MPufrv5H4Ki8Ue7FTrDsOSGNUhfM4ymqcpsHM1LglpayqxsdfjmJA9EFMt+PfuvdAx/KA7jl2H8lcx1TWTsuA7x2pUrTQFkSCLfXX1LV5zFVTM/+7MhtR8lTlVsZGgi+ukD37r3Wz97917r/9bdC+d/YVX1f8Qe/d242dabLJsHIbew8xNnTK7znptn0EkIDKxmhqM6JF0+pSmofT37r3Wol1T17J2d2Z1d1PSMU/v9vvZ+yDMAWWnxuTytJTZSoew/RT4SOoc/Thf6+/de63mKCho8XQ0eNx9PFR0GOpKehoaSBdENLR0kKU9NTwoOFighjVVH4A9+691L9+691rY/8KmP+3d/V/8A4uJ1T/77juj37r3Wnh8Dvnj3n/L07zoO6el8gMjj8gtFie1eqcvXz02y+4NnU0zv/A86I1mXE7kxImlkwecjjepxdS7KyzUc1TTTe691ug752H0D/Nx6Cx3zd+EWQooe346NMZ2N1zlJKHCZ3L53DUUTZHrTszG+dqfbXa23IWVcTlnb7PI0jxBppqCamqYPde6pw2ruvfnU2+8bu3aGUz/XnZXX+eqkpatqSXG5/bWex8j0OZwGfwuQjUvDIA9JlMXWRmGpgZo5FKlWHuvdbVvwp+cm1vltsvJUYpMJtX5A7Vwsb7w2BUVk1PjsoIxJDQbv2nWSJV5LIbByOSl0yWWoq8NPMaepVmaGWp917qinuz/hSB8j/id35vDov5Qfy6KTaO5Nn1SCtwuB77letyWCrHf+D712huDKdZJt/ee087BGZKWohFMpcNBMYZ4pY1917o42y+4v5R3/AAoF2Dkuu927TixneuAwZq02xvCixXXXyk62jNJUxHdXVu+MXNkRu7auJr55HY0NVlcOZEhbLY9BJDE/uvdacn8yL+XN3D/La70Tq/sGd94dd7yiymb6P7kpMccdieyds4uamTJ4/I0aPNT4LsXaAr6dM1jFkZFSohqqctTTxsPde6Fz+T//AC1qH+Zh8hN27E3jvzOde9S9SbQxe9+yMntKPHPvfcK53MS4bb2z9qVeXpsji8DNkXoquoqslPR1op4aYRxwmSUOnuvdDD/O4/lH7S/ln1nWO8unuxN3b36X7nfde2KPC9jT4eu33sfee2cDT5qWCbP4TGYCi3NtzcOOlnkgc0ENRQy0xikeVZEf37r3W9r8Af8AshD4Uf8AipHxv/8AfObN9+690bf37r3RO/n51anbvxD7v2zHDHLlsVtCq3zt13TVJBntgSR7voftiOVnrBh3pQfys7A8H37r3WpL1D2BU9b9o9UdoYyUwvszsDZW67kN6sZBmaIZWF1upaOowVVURspIBVzfj37r3W815ofD9x5Y/B4/N59a+Hw6dfl8l9Hj0c6r2tz7917r/9fac/nH5mXG/EfHY6KV4zuTuPr/ABMyIxHlpqSHPbhlRwOGjLYNb3BF7fm3v3XuqdP5ZO3abcvze6cirIFqafb9Fv8A3cyMbCOpw2z8hS4upHNy1Nk8tE4A/I549+691tz+/de697917rWx/wCFTH/bu/q//wAXE6p/99x3R7917rQd9+691Zh/KK+Q/wAkOhPnj0JiPjduCkpMl3z2ZsPp3sDZe5WyNR13vvZ2ezS09bPvLG45jVR1uyMZNV5LGZSlX76gkieNfJTzT08vuvdbz/8AML/l30PyCo67uLpmgx2H75xdEpy+JMkGMw/cOKoISsGJzFQ+ikx29qGBdGLy0mlXFqSsY0/ilpfde61sts7m3z1ZvnGbq2pk8/152X15uCpFHWNSSY7cW1dw46VqLL4TN4evRdSOFekyeMrI2p6qBmjlVkZW9+691cJ2l1f8b/573xwfr7sOPD9OfNXqDE1WS2ZvDF04qa/bdfULHDJn9vRVMyV29ejt5VscUWZws0r1GNnKK7R1UdHWTe691pJ9m9Z/J/8Al6/J5drbpizXSXyV6I3Pj917Q3Nh5mlpTNT1Ejbe39sbMTQCj3d13uyGBkbXG8FXSvNQ10KyLNCvuvdbs3yY/uh/Ol/kZzd6x4TEYft7bXWuY7u2/S0qyVMuw/kB0PT5uHsLaeNjjqqiuhxe8ocLl8TTQVMjl8ZmKaeUM6ow917rSn+F3zZ77+C3btF3x8dNw43Ebgr8Adv7m25ujFnObM39s6tqqPLnbG78RHUUFa1PHkKOGenq6KppK+jlVvFMqySI/uvdLn+YF/MZ+TH8xrduG3r8g8ptegxewsHmqDr3rTrzFZDC9fbKbNUtMNxZWgp8zlc7nsruDcX8NpxU1tdWTyLFEsMQjiGn37r3X0mPgD/2Qh8KP/FSPjf/AO+c2b7917o2/v3XuoWSx1Hl8dX4nIwJU4/KUVVjq6me+ioo62CSmqoHsQdEsErKf8D7917rQxz2MkxLblwXqjnwmQ3NgeCNUUuGyWSxUfK6l1RmkH04uPfuvdbk399pf9kV/wBIfnf7n/ZUP73fca28v3n+iP8Ai2rXq1+b7n83vq/N/fuvdf/Q2cP51Kufjh1k4VjGnemC1kX0qW2TvsRl/wActwP8ffuvdVpfymGjHzY2wG0626x7NEdxyWEW3mYKbcHQD/sPfuvdbWvv3Xuve/de61sf+FTH/bu/q/8A8XE6p/8Afcd0e/de60HffuvdWo/yQ6/A43+a58NKjcrUq42beO/MbSfdoJIzuXK9R78oNpiMEgLVHPTQiFv7MhBtf37r3V9/88D5t/Ir+Xx/NH+LHf8A03XSVu3Mh8V5Nsb063zlbWRbC7YwGP7j3PkdzbWyyL5oMRn6WmyVBPjcxTxGrx1SULCameanl917o6m8Nn/H3+b/ANAUHzT+FeSoaDuvH4+LFdj9b5aShw+dymexFDFJWdYdoUSStDgeycHAdOEzhLUeRpDCjTS4+SCem917qm3bW5N89Wb6xm6dsZDcHXfZnXe4KkUdW9LJjdx7U3HjJXosvhc1iK5AHQlXpMnjKtHpquBmjlV42VvfuvdXDdl9afG7+e58cH627MTD9P8AzS6ixVXldmbyxFMtTkts5CoVKd9x7aSqljrd6dJ7xrI4oc3g5pjNQTFVdoqmOirZfde6XP8AJQ+Ofbvwu/l+/I3qH5Y7Rl2XkdjfIP5FZHILXCSr2ruLriDZmzjLvXZ+QnWBs5sDc32FfU0dQ0cLuhdJY45UkUe69187SiKtSxMhUxv5JISospp5JZJKbSPwv27LYfge/de66r/+AFd/1BVf/uPJ7917r6wnwB/7IQ+FH/ipHxv/APfObN9+690bf37r3XvfuvdaJ3ZTRt2B2o8ekxHsnsxlKj0lDvTcBuBb6Ee/de62lvDUf8NS+LRJ9x/skP6OfJ/zJ7V/r/o/3j37r3X/0dqr+cLt+ozPw+bJQQtKu0u1+u9w1TgcU9JNU5HbM0zt/YS+4lW/PqYD37r3VK38tvdNLtH5tdIVlbI8dNnqjeOyWKG2uq3TtDKw4qNhcB0bLUcNwfp9RyPfuvdbe/v3Xuve/de61sf+FTH/AG7v6v8A/FxOqf8A33HdHv3XutB337r3Qh9Q9p7q6K7c6r7w2KkEu9Om+xtm9n7Wp6tddHXZjZWdos5Di6yO6iSjzEVI9JKpIBjnIJtf37r3W+D/ADTvjHt3+c9/Lk6Z+UPxLlj3T2VsrDVHdHSOKknhhrd5YHcOOhxfbnR9fJHUSUWL3uZ8EKdEkMiQblwKUcjRRyyzR+691pjfCf5s/ID+XZ8gR2z1HLW4/MYyu/uj3L05u9MjhML2DhMRWyR5Xr/sPCTxLW7d3Xgqjzfw6veAV+Frb+mSnkngl917rdJ3ZtT4+/zhPj9RfND4X5ChxveeMx8OH7G62zM1DiM5kc7iKKN6rq/tKjjmMGD7CwlPxgc8S1HkKMxI0slBLDNSe691Tbt3cW+uq99Y/cu2shuHrrs3rrcFUlLVNSyYzc20dy413ocvhsxiq1PyNdJksbVI9NWUztFKrxOCfde62MevPkdF/Mz+Inevxxxm+aHoT5L7x6b3fsDcdTT4mLcFBSUe7cLUbZquxtkYrI1VPLmtp5KLItBUQidMjhZ5zGzhxS1M/uvdfO++SHxl7k+HvcW6fj53xs+bZvYGyjFaGNparbu6dsyvLT4PfOxM1JDAm4tlbgipyaapVVlhlSSmqY4aqGWJfde6AKv/AOAFd/1BVf8A7jye/de6+sJ8Af8AshD4Uf8AipHxv/8AfObN9+690bf37r3TPuHN0W2cBnNyZJimO2/h8nm8g621LRYqinr6pl1EC6wU7EXIHv3XutDXKV0+Vp8xlSTJVZuXNZjUzHVLVZyrrMkhZjclpJa0XP8AX37r3W6T/cCq/wBk4/0W/aSfe/7LP/cL7HR+795/ou/u/wDbeO/+d8/ptf8AV7917r//0t2H5k9aVfcHxa7068x0Zmyud68zc+EhWNpXmz2BjTceBhjjX1PLNmMRAqD/AFRHv3XutNzYe+q/r/d+wu0sOCcnsPdW1d/UKjktJt7K0WZnpyOL/cUcEsLLxcOQeD7917rej29ncZunAYPc2EqVrcNuLD43O4isS2irxmXoochQVKWJGmekqEYc/Q+/de6d/fuvda2P/Cpj/t3f1f8A+LidU/8AvuO6PfuvdaDvv3Xuve/de6t//lQ/zd+1P5ae867beSxWV7W+LO/M2uW7D6kp8hDT57amdqVhpq7svqOpyUseMo9zvSwp/EsRUPBQZ5IVDTUtSI6pfde62kt1dOfyP/54cdD2Tht2bSj72y1JjKTKZzYO7YukflDRSUtDAg2/vvZOYhWo3dPiY3ipFqsjh8zTqtOkdFVtTqur3XuhU+EH8jzqP+Xl3pN35078qfknFRNiqvFbu2FubMdeLsLfe1kpq37TH9lQ0my6D+Njbc1SaugrojQ1VDOrFJVjkljf3Xull8xvix0h86qHe3bHxO7G6r3j8g+sKuLa2+6XYm99rZ/C71q8Zj4amm2B2LV4HI11NtfsGgxboMTkazRLElqSrBpSklJ7r3WvXhM3vzqjflHnsFWbi637Q633FVRwVD00uL3Rs3dGMdqPK4jLYysQWYAvS5HHVKPTVlM7RSq8Tgn3XuriN/df/G3+ex8cX6m7bixnUnzK6kxVXmdk74w1JFU5TbOTqI4aSbeG0IqqaCp3f0/u6qjgh3Bt6eYSUshRGeOojoa9vde60YvlR8Zu6vh/2xvnoL5AbQk2h2JtSmqpx4Xmq9tbw21UCqhw+/dg5qWGnXcWytwCFjBUBVmp5lkpaqOGqhliX3XuvqF/AH/shD4Uf+KkfG//AN85s337r3Rt/fuvdEZ/mQdqf6J/h329X0tUtLnN64iLq/bYP65sn2BMMDViIggrLS7enrqkEXK+C/49+691qx/H/rebtbvHpnq6ghLQ7s7G2njayNYzKIdu4vIRZ3cUzxpYmCl25hqlnItpUX9+691vBe/de6//09/j37r3Wlx8v+lZPj78lO2eso6NqXb8O4Zt3bG1JIYZ9i73lqM7hI4JZUVakYeplqsbIVuokoiPfuvdX4/yku/4ez/jweqMvWeTefQtXDtho55mkq8jsDKmpr9iZfVI5Mq0lKlRin0DSjY5dVi4v7r3Vq3v3Xuqm/5yPwC7R/mO/FraHRfUm9tgbC3NtzvbZfalTmOx49xS4Gowu2tq7+wFZjYBtihyORXKT1O7YJIi0fi0ROGYEr7917rWk/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvde/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvde/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvdYJf+Ep3zUnkSaX5F/FBp4reGo+y7eWpgsbgwVK7YWeAgj+ww9+6908Vn/CXH595GjGOyPy2+P+RxwVVGOyGf+QNdjwqjSq/Y1eMmpNCrwBosB7917o0vwf8A5Fv80v8Al+d2YvuzoH5R/EmlneODEb+68ydN3LDsLtbZ4qfNNtreGOotrgrUUjSPNjMnAv3uMqmLRlo3mik917q7n+YD/L3ofkpiJu2uraDFbe+ReIxdMlZTNUx0mC7XxWPphHFtXcdfIlNBS7jx0K+PDZuRUEYApasGkZHpPde61ocLmt9dV77pM3g6zcfXHZ/W25KmOGd6aTFbp2ZunFu9Hk8XlcZWJ6XAL01fQVKPT1lM7RyK8Tgn3XuriuwNjfG7+e18ZK/pPuOnxPVfzD6vwWUy+wd74enSTKbUztZSR49t87IWqkSr3V1Luqqip4NzbcmlLQ3SN3jqY8dkR7r3V2fxj6xznSXxs+PfTG58jicvuTqPo/qfrDcGWwC1iYLKZvYOw8BtTK5HDJkY4sgmKrq7EyS04nVZhCyhwGv7917ocffuvda2X84n5AQb27Z2l0Dt+t8+G6ipG3NvVqeZjTz9gbroIlxGJnQO0E0u2dpSmYkDVHJlyp9S8e691j/k2dKybs7r3v3nkqRzheptuvtDbVU6yJHNvrfVOr5d6dyniqDhdmRGKVQbocqv9ffuvdbKPv3Xuv/U3+PfuvdU7fzfPjZN2B1ViPkFtXGS1m7elaesh3fDRQPPWZTqbIyipzFQY4w8s52PlFXJqFX0Ub1rE/Qe/de6o7+J/wAi8x8Wu8drdt46OpyO3ljfbnY2BoT5JdydeZeeCTKJSxK8a1WW2/PFHlMcpdVeopvETpma/uvdbmG19z7f3rtvA7w2plqPPbZ3PiMfnsBmsfJ5qHK4jK0sVbj6+lkspaGppplYXAYXsQDce/de6fffuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691Vx/MG/l7Yv5KYyp7T6ppcXgPkFhKBI5BK8WOwvbeGx8JFPtjdFSQsFFuWjhXx4fMyf5jilqy1IUel917rWXxGX3z1ZvulzGGq9x9cdodbbkqEhnankxO6tl7qxTvSZLGZLHViemRQWp66hqEemrKZ2SRZIZAT7r3W1F8EPnjtj5YbabbG5xjNqd87Wxsc+7NoQStFjtzY6Ix07b52KKqR6ir2/U1DAVVKWkqcTUOIZiyPBPN7r3Q4/Lr5L7a+KnS24ezMysGR3BJbAdebVd2E2799ZKGb+C4gLG8cqY6n8T1eQmDL9vj6eZwdYRW917rTfr67eG/921mTrjkN7djdjbtepqRSwvNld4b83llwqQUdNEpc1GXzFasUEai0UWlf0px7r3W5B8Pfj3Q/GToDY/Vqfbz7hp6WTcG/srTgFMzv7cGit3LWq6m0tNS1JWipWsP8ipYQRcH37r3RnPfuvdf/1d/j37r3UerpKWvpamhrqanraGtp5qSso6uGOppaulqY2hqKapp5leGennhco6OCrKSCCD7917rUQ+efxCyXxM7cenwdJUS9Ldg1ddk+q8veSaPCzASVuW60ydTISyZHbSkvjS7MazE6CGaWnnC+690Pn8tv57Q/HjJU/SPb2UmXpDcmXkl2vuSrcyw9Rbky9Q0tVFXO13p+utx5CYySsLx4ivlaYhaeeZovde62doJ4amGGpppoqinqIo54J4JElhnhlQSRTQyxlklilRgyspIYG49+691l9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3VXn8wX+Xxi/kxjJ+0urYMdt/5BYHHJETK0OPwvbWGx8JWl2ruupIWKj3DRwL48PmX5p+KWqLUZVqb3XutZLF5XfXV2+KXLYmp3L1t2f1xuSdYZpKaTE7q2VuvESvS11BkcdWIQssZ1QVlHUJJTVlM5RxJDICfde6GD5J/J/tX5V7xwe7+z6ygi/uxt+nwG29sYFail2vhDJFTtuLOUdHVSzSLmt2V9Os1XNI7tFBHFTIxhhBb3XurRv5Svw3nylfRfLXsnFaMVRpVU/RGFyEPqr5qmGSiyvalTTSrYU4geSiwJa5ZHqKwKoalkPuvdbBXv3Xuve/de6//9bf49+691737r3QT93dKdffITrbcXVfZmHGW2zuGBbSwuKfLYLLU15MVuTbuR0PJitwYSrtNTTqCAwKSK8TyRv7r3Woj8pfit2X8Tewn2Rv2D+MbazL1cnXfZFJRmDb/YOGjUvJC8frhw+8cZTsFymIdi0Z/epzNSSRyn3XujU/B7+ZFu74zw4/rLs+mzXYfRcbR0+H+1kNdvbquFnUaNurVSB9z7LhTUTiXkWqowQaN2jX7Vvde62Yetuz+vu4do4zfnWG7sJvbaOXTVRZvBVa1NP5AkcktHWQsEq8bk6VZVE9JUxxVMDHTJGrce/de6Xnv3Xuve/de697917r3v3Xuve/de697917r3v3XuiyfJf5cdMfFXbAzXZW4PLuDIU8km1OvcF4chvnd86mRFXE4czRfb45JImE2Qq3p6CnCkPKH0o3uvdan/yZ+Qm4vlD29m+3Ny7X2zs6qyFJS4bG4LbVPEZKXb+KaVcOu6NwCGnqN47np6RxHNkZUjRY1WCnSOnjjQe690b7+X5/L6y/yZyuN7W7VxtXiPjtia1KiipJ/NR5Duyuo5rvicSVMdRTdb0s8WjI5JCjZJg1JRsV+4ni917raToaGixlFR43G0dLjsdjqWnocfj6GnipKKhoqSJKelo6Olp0jgpqWmgjVI40VURFAAAAHv3XupXv3Xuve/de6//X3+Pfuvde9+691737r3QbdtdRddd5bGy/XHaW18fuzaWZVDUY+uV0mpKyEN9nlsRXwNFXYfNY93LU9XTSRTwsTpYAkH3XutZX5dfy0e3vjlNld4dfQ5ruPpeKaoqUy+No2r+xNjYxEMwj3xgMbTK+dx1FCGDZnGxMCsZkqqan1Bm917oknUHd3afR+4/77dL9g5fZmWqmT+INiJ4K7bu5Eguq0+6dtVi1OB3FFFcgeeL7iE/okjYce691dX0b/Ojxz09Lh/kf1jXY2sQCJ999UI+axFSdUUaTZLZGVq489i3K6pJmo6rIp+EQWAPuvdWl9afMn4t9vRRNsPvLr7JVUoj04fJZyHbG4Q8igiJtvbnGHzXkUnSwEB0tweffuvdGUilinjjmhkjmhlRZIpYnWSOSNwGR43QlXRlNwQSCPfuvdZPfuvdNmXzeG2/Rvkc9l8ZhMfGbSV+Xr6XG0cZ0s1nqqyWGBDpUnlvoD7917omXan8xn4fdTx1sWS7gwu8c3RqpG2usUl7AzFS7X/ail2+KnCU7pb1GprIEQ8MwPHv3Xuqke/v5xXbW9oa3b/QO0KTqHCTGaD++26Hx+6+wamn8jKlRisKkc+0dsSVNO1j52yssTC66T+n3Xuqk6yu3d2DvB6vI1u7exuxt7ZFIvNVSZbeW/N35SaTTBS08aity+Ul1sBHDEgggH6VjT6e691dh8Nv5SuRydRiOy/lrSJR4pEpsjhOiKStSeetm1+WCbtfK0LtTmmSMBzgaKV43ZgKydtD0p917rYAoaGixlFR43G0dLjsdjqWnocfj6GnipKKhoqSJKelo6Olp0jgpqWmgjVI40VURFAAAAHv3XupXv3Xuve/de697917r/9Df49+691737r3Xvfuvde9+691737r3VefyP/lofG/5B1OQ3LS4ip6l7HrjLNPvbrmKix8WVrZBxU7p2nNC+29xOXJZ5jDBWyMbmo+nv3XuqZe4/wCU78q+tJqus2PQ7e7x21CJJIazaNfBt3dyU6SeOMVmzdy1UUU9ZKtmKUGRrLXNhYe/de6rx3111vXYda+K7M673hsyugYB6be+z8vhfE36wUqcnQpRsGHqDxysrL6gbe/de6acLvDNYNl/u1vjcmEaIgp/d3e+fxYhb6jQmKzFPHESB9ABwPfuvdLKXujuGeIw1Hdvb89Ow0mGftffTwstrAFG3BpYW9+690HWUzC5uU/x3PVWdmJH7ed3BXZ2VmY3XTDla+tcsxPGkXJP59+690LnXPx/7y7WMcPV/THYu7actGq1uK2lXY/b8AmNkknz+XjxWAp4X0mzGfmxt9PfuvdWadLfya+5t1vT5LvPfGA6nxBcNNtrZ702+t6zIjpqgmy0iQbPw/3EbNaSI5QoV/Tzce691dj8evh90B8YqAxdWbHpqbcFTTiny2/dwSHcG/c2n9ta7ctahqKeml41UtGtLRXAIhB59+690Zz37r3Xvfuvde9+691737r3Xvfuvdf/0d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3UHJfw37Go/jH2P8N8Z+7/iX2/2Pi/tfcfdfseP+urj37r3RKd/f8Nx+ST/AEh/7J9955m8v8Y/0SfxTz3GvyW/3I67/W/v3Xugkh/4aK8w8X+yZeW5tf8A0eaf8beT9u1v94/w9+690ZrrL/ZLPuKP/Q//ALLJ99pH2X+j/wD0X/xO3NvF/Af8u1fX/H37r3RpBawta1ha30t+LW4tb37r3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3Xuv/Z";
                if (records.length > 0) {
                    // >>>> records async
                    async.forEachSeries(records, function(data, next) {
                        count= count+1;
                        var contentList = [];
                        var selectedFormRecordFields = {};
                        var recordInformation = data.record[0];
                        var key = Object.keys(data.record[0]);
                        async.forEachSeries(key, function(singlekey, nextIteration) {
                            selectedFormRecordFields[singlekey] = data.record[0][singlekey];
                            var date = data.updatedTime;
                            date = date.toUTCString();
                            tableHeaderBlock = {
                               // pageBreak: 'before',
                                style: 'tableExample',
                                table: {
                                    color: '#444',
                                    widths: [100, '*'],
                                    body: [
                                        [{
                                            text: 'Record- #' + ''+count+'',
                                            colSpan: 2,
                                            alignment: 'left',
                                            fillColor: '#dddddd'
                                        }, {}],
                                        [{
                                                rowSpan: 2,
                                                image:orgDetails.imageurl,
                                                width: 100
                                            },
                                            {
                                                text: '' + post1[0].name + '',
                                                style: 'subheader',
                                                alignment: 'center'
                                            }
                                        ],
                                        [{
                                                text: ''
                                            },
                                            {
                                                columnGap: 20,
                                                alignment: 'justify',
                                                columns: [{
                                                    type: 'none',
                                                    style: 'List',
                                                    width: 'auto',
                                                    ul: [
                                                        'Department Admin :' + req.body.metaDataInfo[0].departmentAdmin + '',
                                                        'Group Admin :' + req.body.metaDataInfo[0].groupAdmin + '',
                                                        'Task Name :' + req.body.metaDataInfo[0].taskName + '',
                                                        'Project Name :' + req.body.metaDataInfo[0].projectName + '',
                                                    ]
                                                }, {
                                                    type: 'none',
                                                    style: 'List',
                                                    width: 'auto',
                                                    ul: [
                                                        'Submitted on :' + date + '',
                                                        'Submitted By:' + data.updatedBy + ''
                                                    ]
                                                }]
                                            }
                                        ]
                                    ]
                                }
                            }
                            nextIteration();
                        }, function(err) {
                            // >>>> Loop FormSkeleton
                            post1[0].FormSkeleton.forEach(function(recorddata) {
                                var LabelKey = recorddata.lable;
                                var widgetSelected = recorddata.type.view;
                                var LabelValue = selectedFormRecordFields[recorddata.id];
                                var fieldStructure = {
                                    style: 'tableExample',
                                    color: '#444',
                                    table: {
                                        widths: [200, '*'],
                                        body: []
                                    }
                                }
                                // ============= Normal Fields =============
                                if (widgetSelected == "textbox" || widgetSelected == "textarea" || widgetSelected == "checkbox" || widgetSelected == "radio" || widgetSelected == "rating" || widgetSelected == "map" || widgetSelected == "barcode"|| widgetSelected == "mapInteractionField" || widgetSelected == "goto") {
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, 'N/A']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)
                                }
                                if(widgetSelected == "select"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, '']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)
                                }
                                if(widgetSelected == "calculation"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, '0']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)
                                }
                                if(widgetSelected == "calender"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, 'N/A']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, dateFormat(LabelValue, "yyyy-mm-dd h:MM:ss")]);
                                    }
                                    contentList.push(fieldStructure)
                                }
                                if(widgetSelected == "camera" || widgetSelected == "sign"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    else{
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+LabelValue+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    contentList.push(fieldStructure);
                                }
                                if(widgetSelected == "video"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    else{
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    contentList.push(fieldStructure);
                                }
                                // ============= END Normal Fields =============

                                // ============= Group BLOCK ================//
                                if (recorddata.type.view == "group") {
                                    console.log("In Group Block")
                                    var group_Header =[];
                                    var group_Body = [
                                        [
                                            {
                                                table: {
                                                    widths: [200, '*'],
                                                    headerRows: 1,
                                                    pageBreak: 'before',
                                                    body:[]
                                                }
                                            }
                                        ]
                                    ];
                                    var groupFieldArray = [];
                                    group_Header.push({ text: ''+LabelKey+'', fontSize: 8,alignment: 'left',fillColor: '#dddddd',color: '#2B2C2C',colSpan:2},{});
                                    groupFieldArray.push(group_Header);
                                    var groupData = recorddata.type.fields;
                                    groupData.forEach(function(group) {
                                        var groupLable = group.lable;
                                        var groupValue = selectedFormRecordFields[group.id];
                                        var groupWidgetType = group.type.view;
                                        if(groupWidgetType == "textbox" || groupWidgetType == "textarea" || groupWidgetType == "checkbox" || groupWidgetType == "radio" || groupWidgetType == "rating" || groupWidgetType == "map" || groupWidgetType == "barcode" || groupWidgetType == "mapInteractionField" || groupWidgetType == "goto"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'N/A',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "select"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "calculation"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'0',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if (groupWidgetType =="calender") {
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'N/A',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+dateFormat(groupValue, "yyyy-mm-dd h:MM:ss")+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "camera" || groupWidgetType == "sign"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                            else{
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+groupValue+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                        }
                                        if(groupWidgetType == "video"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                            else{
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                        }
                                        group_Body[0][0].table.body = groupFieldArray;
                                    });
                                    contentList.push(group_Body);
                                }
                                // ================= END Group BLOCK=================== //

                                //  *****************SECTION BLOCK********************* //
                                if (recorddata.type.view == "section") {
                                    // console.log("In Section Block");
                                    // [Section Case:1] Inititalise sectionHeader Array
                                    var section_Header =[];
                                    // Section Main Layout
                                    var sectionMainLayout = {
                                        style: 'tableExample',
                                        color: '#444',
                                        table: {
                                            widths:'*',
                                            body: [
                                                // Push Header in Array[0]
                                                // Array-2 Push an empty Array.
                                                //In Array2 Generate new Array
                                            ]
                                        }
                                    }
                                    // Inititalize section Body array
                                    section_Header.push({ text: ''+LabelKey+'', fontSize: 8,alignment: 'left',fillColor: '#66A5F0',color: '#2B2C2C'});
                                    sectionMainLayout.table.body.push(section_Header);
                                    console.log("printing section Header");
                                    var sectionData = recorddata.type.fields;
                                    // [Section Case:2] Inititalize section Body array
                                    var sectionMainArr = [];
                                    var sectionBody = [];
                                    // NOTE : SectionBody will have section related individual fields and groups
                                    sectionMainArr.push(sectionBody);
                                    // Push section body inside sectionMainArr 
                                    // and push the sectionMainArr in sectionMainLayout
                                    sectionMainLayout.table.body.push(sectionMainArr);
                                    var sectionData = recorddata.type.fields;
                                    sectionData.forEach(function(section){
                                        var sectionFieldLabel = section.data.lable;
                                        var sectionFieldValue = selectedFormRecordFields[section.data.id];
                                        var sectionFieldWidget = section.data.type.view;
                                        // >>>> Structure for Section Fields >>>>>
                                        var sectionFieldTable = {
                                            style: 'tableExample',
                                            color: '#444',
                                            table: {
                                                widths: [200, '*'],
                                                body: []
                                            }
                                        }
                                        // =========== Structure for Section Fields ================
                                        if (section.type == "field") {
                                            if(sectionFieldWidget == "textbox" || sectionFieldWidget == "textarea" || sectionFieldWidget == "checkbox" || sectionFieldWidget == "radio" || sectionFieldWidget == "rating" || sectionFieldWidget == "map" || sectionFieldWidget == "barcode" || sectionFieldWidget == "mapInteractionField" || sectionFieldWidget == "goto")
                                            {    
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: 'N/A',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "select"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: '',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "calculation"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: '0',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if (sectionFieldWidget =="calender") {
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: 'N/A',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+dateFormat(sectionFieldValue, "yyyy-mm-dd h:MM:ss")+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "camera" || sectionFieldWidget == "sign"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+sectionFieldValue+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                            }
                                            if(sectionFieldWidget == "video"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                            }
                                            sectionBody.push(sectionFieldTable);
                                        }
                                        // ========== END Structure for Section Fields ===========
                                        // #########################################################//
                                        if (section.type == "group") {
                                            var sectionGroupLayout = {
                                                style: 'tableExample',
                                                color: '#444',
                                                table: {
                                                    widths:'*',
                                                    body: []
                                                }
                                            }

                                            var SectiongroupHeader =[];
                                            var sectionGroupFieldTable = {
                                                style: 'tableExample',
                                                color: '#444',
                                                table: {
                                                    widths: [200, '*'],
                                                    body: []
                                                },
                                                layout: {
                                                    hLineWidth: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 1 : 1;
                                                    },
                                                    vLineWidth: function (i, node) {
                                                        return (i === 0 || i === node.table.widths.length) ? 1 : 1;
                                                    },
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? '#81868C' : '#A0A0A0';
                                                    },
                                                    vLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.widths.length) ? '#81868C' : '#A0A0A0';
                                                    }
                                                }
                                            }
                                            SectiongroupHeader.push({ text: ''+section.data.lable+'', style: 'sectionHeader',alignment: 'left',fillColor: '#dddddd',color: '#2B2C2C'});
                                            sectionGroupLayout.table.body.push(SectiongroupHeader);
                                            var sectionGroupArr = [];
                                            var sectionGroupBody = [];
                                            sectionGroupArr.push(sectionGroupBody);
                                            // Push section body inside sectionGroupArr 
                                            // and push the sectionGroupArr in sectionGroupLayout
                                            sectionGroupLayout.table.body.push(sectionGroupArr);
                                            var sectionGroupData = section.data.type.fields;
                                            // >>>> ++ Loop section Group individual Fields ++
                                            sectionGroupData.forEach(function(sectionGroup){
                                                // console.log("Inside section Group Loop")
                                                var sectionGroupWidgetType = sectionGroup.type.view;
                                                var sectionGroupFieldLabel = sectionGroup.lable;
                                                var sectionGroupFieldValue = selectedFormRecordFields[sectionGroup.id];
                                                if(sectionGroupWidgetType == "textbox" || sectionGroupWidgetType == "textarea" || sectionGroupWidgetType == "checkbox" || sectionGroupWidgetType == "radio" || sectionGroupWidgetType == "rating" || sectionGroupWidgetType == "map" || sectionGroupWidgetType == "barcode" || sectionGroupWidgetType == "mapInteractionField" || sectionGroupWidgetType == "goto"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'N/A'}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "select"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "calculation"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'0'}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if (sectionGroupWidgetType =="calender") {
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                       sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'N/A'}]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+dateFormat(sectionGroupFieldValue, "yyyy-mm-dd h:MM:ss")+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "camera" || sectionGroupWidgetType == "sign"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+sectionGroupFieldValue+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "video"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                }
                                               
                                            });
                                            sectionGroupBody.push(sectionGroupFieldTable)
                                            sectionBody.push(sectionGroupLayout);
                                        }
                                        // >>> End Section Group
                                    })
                                    contentList.push(sectionMainLayout)
                                }
                                //  ***************** END SECTION BLOCK********************* //
                            });
                            // <<<<==== End FormSkeleton loop
                            console.log("contentList Pushing completed!");
                        })
                        // # Finally push all build structure to finalArray and send it to PDFLayout
                        finalArray.push(
                            [tableHeaderBlock], 
                            [contentList]
                        );
                        next();
                    }, function(err) {
                        // ===> FINAL PDF LAYOUT STARTS ====>
                        var finalLayout = {
                            footer: function(currentPage, pageCount) {
                                return [{
                                    text: +currentPage.toString() + ' / ' + pageCount,
                                    style: 'footer'
                                }];
                            },
                            pageMargins: [40, 50, 40, 50],
                            info: pdfinfo,
                            content: finalArray,
                            styles,
                            defaultStyle
                        }
                        // console.log(JSON.stringify(finalLayout));
                        var pdfDoc = printer.createPdfKitDocument(finalLayout);
                        var fileName = '' + post1[0].name+ '';
                        var output_Filepath = "./"+ mongoid +".pdf";
                        var stream = pdfDoc.pipe(fs.createWriteStream("./"+ mongoid +".pdf"));
                        pdfDoc.end();
                        stream.on('finish', function() {
                            console.log('congratulations, your pdf created,sending attachement please check your mail');
                            sendAutomailwithAttachment(mailData.mailid,"Dear User,<br> Please find requested record Information in the attachement for the Form : "+fileName+"<br><br>Thanks,<br>DFormsTeam", "RECORD INFO", output_Filepath,mailData.altemail, function(mailresponse){
                                if(mailresponse==true) {
                                    // >>> Remove the PDF file from filestorage after email is sent!
                                    fs.unlink("./"+ mongoid +".pdf",function(err){
                                        if(err) return console.log(err);
                                    }); 
                                    res.json({
                                        "message" : "Mail sent successfully",
                                        "status" : 200
                                    });
                                }
                                else {
                                    res.json({"message":"Failed to send mail","status":500});
                                }
                            });
                        });
                        // <<=== END FINAL PDF LAYOUT <<===
                        // >>>>>> END
                    });
                    // Function Err end!
                }
                // End records.length
            });
        }
        // ==> post1.length End 
    });
});
// <<< End 
//============================================================================================

//PDF for mobile requests

router.post('/mobilegeneratePDF', function(req, res, next) {
    //console.log("IN Mobile Generate PDF");
    var headers = [];
    var headerslbl = [];
    var mailData = req.body;
    Formsz.find({
        _id: req.body.formid
    }, function(err, post1) {
        if (post1.length > 0) {
            var recordids = [];
            recordids = req.body.records;
            console.log(recordids);
            FormszDetails.find({
                _id: {
                    $in: recordids
                },
                isDeleted: false
            }, function(err, records) {
                //console.log(records);
                fileid = mongoid;
                // >>>> FINAL LAYOUT ARRAY
                var finalArray = [];
                var count = 0;
                var tableHeaderBlock;
                // NO Preview
                var nopreview = "data:image/jpeg;base64,/9j/4QYARXhpZgAATU0AKgAAAAgABwESAAMAAAABAAEAAAEaAAUAAAABAAAAYgEbAAUAAAABAAAAagEoAAMAAAABAAIAAAExAAIAAAAiAAAAcgEyAAIAAAAUAAAAlIdpAAQAAAABAAAAqAAAANQALcbAAAAnEAAtxsAAACcQQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKFdpbmRvd3MpADIwMTY6MDI6MTIgMTY6MDM6MjgAAAOgAQADAAAAAQABAACgAgAEAAAAAQAAAJagAwAEAAAAAQAAAJYAAAAAAAAABgEDAAMAAAABAAYAAAEaAAUAAAABAAABIgEbAAUAAAABAAABKgEoAAMAAAABAAIAAAIBAAQAAAABAAABMgICAAQAAAABAAAExgAAAAAAAABIAAAAAQAAAEgAAAAB/9j/7QAMQWRvYmVfQ00AAf/uAA5BZG9iZQBkgAAAAAH/2wCEAAwICAgJCAwJCQwRCwoLERUPDAwPFRgTExUTExgRDAwMDAwMEQwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwBDQsLDQ4NEA4OEBQODg4UFA4ODg4UEQwMDAwMEREMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIACQAJAMBIgACEQEDEQH/3QAEAAP/xAE/AAABBQEBAQEBAQAAAAAAAAADAAECBAUGBwgJCgsBAAEFAQEBAQEBAAAAAAAAAAEAAgMEBQYHCAkKCxAAAQQBAwIEAgUHBggFAwwzAQACEQMEIRIxBUFRYRMicYEyBhSRobFCIyQVUsFiMzRygtFDByWSU/Dh8WNzNRaisoMmRJNUZEXCo3Q2F9JV4mXys4TD03Xj80YnlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vY3R1dnd4eXp7fH1+f3EQACAgECBAQDBAUGBwcGBTUBAAIRAyExEgRBUWFxIhMFMoGRFKGxQiPBUtHwMyRi4XKCkkNTFWNzNPElBhaisoMHJjXC0kSTVKMXZEVVNnRl4vKzhMPTdePzRpSkhbSVxNTk9KW1xdXl9VZmdoaWprbG1ub2JzdHV2d3h5ent8f/2gAMAwEAAhEDEQA/APTOodQxenYrsrKfsrboO5cT9FjG/nPcsut/1k6oBdW6vpGK7WsFouvcOznh36Cvcm9FnWPrBf8AaALMLpTRUypwlrrrW7rbHtP0vSr/AEayMPrhwG5+J0u9luJiXPxh9rDj9jva91ddWX6Z9b9mZbmbMXOZv9L/AAnvq9JJTtnpX1gqBdR1g2PGuy6hhYT4Sza9n9lSw+tZDMtnTusUjFy7J9C1hJouj/Qvd7mWf8FYucyPrx9Yel3Czq2JhPwqHMHUvsVzrbsZtoml11L9rm79zPTs2Px7v9P7610JsxfrP03LxjTdi249np7choZZVcGV5NNrdjrP8HkVO+mkp2klz37eyf8Amr9t/wC9D+jbf+7G/wCzTH9b9NtSSU//0O76Cdmf1nHdpY3L9U/1LWMdUf8AoLzSjq1PT/rf1qcmjGsyrczFruyAH0te7JbeyvNYQ/8AUsr7P9kybNv6Fl/rf8IvSur0ZWDnM65g1m7az0s7HZ9Kymdzba/+Go/6hXMKzo/U6ftWK2m5rzLjtbuDjz6jSNzX/wBdJTwr68TIxHWh1Vfq4d2KwjJrvOI3KY+mvE6hdhvv+1dDfk/p8HIs/S49jP8AB/4bX/xYY+RidI6hi5dbqcmnPe22t5kg+ji7Tu19Rj2e+q1n6O2r+bXVDp+AA8NxqgLGllgDGjc1302Oge5rly4+2V5GR9Xui5AyMa0AC2S44bCYuqN30bG7P5hm/wBViSmE/wCQP2hH6v8Atb7XP/Bets3JLp/2Vh/sv9lbf1X0vRjSdsbd3H85+fu/fSSU/wD/0fVVzHVv+bH253p+t+059/7N3+tP53qeh+i3fver718+JJKfd3fs+P8AKf7c+zfn/at3pR/wn2f3rpukfsn7E39k+l9l7elxP8v871P+M/SL5iSSU/VSS+VUklP/2f/tDf5QaG90b3Nob3AgMy4wADhCSU0EJQAAAAAAEAAAAAAAAAAAAAAAAAAAAAA4QklNBDoAAAAAAOUAAAAQAAAAAQAAAAAAC3ByaW50T3V0cHV0AAAABQAAAABQc3RTYm9vbAEAAAAASW50ZWVudW0AAAAASW50ZQAAAABDbHJtAAAAD3ByaW50U2l4dGVlbkJpdGJvb2wAAAAAC3ByaW50ZXJOYW1lVEVYVAAAAAEAAAAAAA9wcmludFByb29mU2V0dXBPYmpjAAAADABQAHIAbwBvAGYAIABTAGUAdAB1AHAAAAAAAApwcm9vZlNldHVwAAAAAQAAAABCbHRuZW51bQAAAAxidWlsdGluUHJvb2YAAAAJcHJvb2ZDTVlLADhCSU0EOwAAAAACLQAAABAAAAABAAAAAAAScHJpbnRPdXRwdXRPcHRpb25zAAAAFwAAAABDcHRuYm9vbAAAAAAAQ2xicmJvb2wAAAAAAFJnc01ib29sAAAAAABDcm5DYm9vbAAAAAAAQ250Q2Jvb2wAAAAAAExibHNib29sAAAAAABOZ3R2Ym9vbAAAAAAARW1sRGJvb2wAAAAAAEludHJib29sAAAAAABCY2tnT2JqYwAAAAEAAAAAAABSR0JDAAAAAwAAAABSZCAgZG91YkBv4AAAAAAAAAAAAEdybiBkb3ViQG/gAAAAAAAAAAAAQmwgIGRvdWJAb+AAAAAAAAAAAABCcmRUVW50RiNSbHQAAAAAAAAAAAAAAABCbGQgVW50RiNSbHQAAAAAAAAAAAAAAABSc2x0VW50RiNQeGxAcsAAAAAAAAAAAAp2ZWN0b3JEYXRhYm9vbAEAAAAAUGdQc2VudW0AAAAAUGdQcwAAAABQZ1BDAAAAAExlZnRVbnRGI1JsdAAAAAAAAAAAAAAAAFRvcCBVbnRGI1JsdAAAAAAAAAAAAAAAAFNjbCBVbnRGI1ByY0BZAAAAAAAAAAAAEGNyb3BXaGVuUHJpbnRpbmdib29sAAAAAA5jcm9wUmVjdEJvdHRvbWxvbmcAAAAAAAAADGNyb3BSZWN0TGVmdGxvbmcAAAAAAAAADWNyb3BSZWN0UmlnaHRsb25nAAAAAAAAAAtjcm9wUmVjdFRvcGxvbmcAAAAAADhCSU0D7QAAAAAAEAEsAAAAAQABASwAAAABAAE4QklNBCYAAAAAAA4AAAAAAAAAAAAAP4AAADhCSU0EDQAAAAAABAAAAFo4QklNBBkAAAAAAAQAAAAeOEJJTQPzAAAAAAAJAAAAAAAAAAABADhCSU0nEAAAAAAACgABAAAAAAAAAAE4QklNA/UAAAAAAEgAL2ZmAAEAbGZmAAYAAAAAAAEAL2ZmAAEAoZmaAAYAAAAAAAEAMgAAAAEAWgAAAAYAAAAAAAEANQAAAAEALQAAAAYAAAAAAAE4QklNA/gAAAAAAHAAAP////////////////////////////8D6AAAAAD/////////////////////////////A+gAAAAA/////////////////////////////wPoAAAAAP////////////////////////////8D6AAAOEJJTQQAAAAAAAACAAE4QklNBAIAAAAAAAQAAAAAOEJJTQQwAAAAAAACAQE4QklNBC0AAAAAAAYAAQAAAAI4QklNBAgAAAAAABAAAAABAAACQAAAAkAAAAAAOEJJTQQeAAAAAAAEAAAAADhCSU0EGgAAAAADSQAAAAYAAAAAAAAAAAAAAJYAAACWAAAACgBVAG4AdABpAHQAbABlAGQALQAyAAAAAQAAAAAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAACWAAAAlgAAAAAAAAAAAAAAAAAAAAABAAAAAAAAAAAAAAAAAAAAAAAAABAAAAABAAAAAAAAbnVsbAAAAAIAAAAGYm91bmRzT2JqYwAAAAEAAAAAAABSY3QxAAAABAAAAABUb3AgbG9uZwAAAAAAAAAATGVmdGxvbmcAAAAAAAAAAEJ0b21sb25nAAAAlgAAAABSZ2h0bG9uZwAAAJYAAAAGc2xpY2VzVmxMcwAAAAFPYmpjAAAAAQAAAAAABXNsaWNlAAAAEgAAAAdzbGljZUlEbG9uZwAAAAAAAAAHZ3JvdXBJRGxvbmcAAAAAAAAABm9yaWdpbmVudW0AAAAMRVNsaWNlT3JpZ2luAAAADWF1dG9HZW5lcmF0ZWQAAAAAVHlwZWVudW0AAAAKRVNsaWNlVHlwZQAAAABJbWcgAAAABmJvdW5kc09iamMAAAABAAAAAAAAUmN0MQAAAAQAAAAAVG9wIGxvbmcAAAAAAAAAAExlZnRsb25nAAAAAAAAAABCdG9tbG9uZwAAAJYAAAAAUmdodGxvbmcAAACWAAAAA3VybFRFWFQAAAABAAAAAAAAbnVsbFRFWFQAAAABAAAAAAAATXNnZVRFWFQAAAABAAAAAAAGYWx0VGFnVEVYVAAAAAEAAAAAAA5jZWxsVGV4dElzSFRNTGJvb2wBAAAACGNlbGxUZXh0VEVYVAAAAAEAAAAAAAlob3J6QWxpZ25lbnVtAAAAD0VTbGljZUhvcnpBbGlnbgAAAAdkZWZhdWx0AAAACXZlcnRBbGlnbmVudW0AAAAPRVNsaWNlVmVydEFsaWduAAAAB2RlZmF1bHQAAAALYmdDb2xvclR5cGVlbnVtAAAAEUVTbGljZUJHQ29sb3JUeXBlAAAAAE5vbmUAAAAJdG9wT3V0c2V0bG9uZwAAAAAAAAAKbGVmdE91dHNldGxvbmcAAAAAAAAADGJvdHRvbU91dHNldGxvbmcAAAAAAAAAC3JpZ2h0T3V0c2V0bG9uZwAAAAAAOEJJTQQoAAAAAAAMAAAAAj/wAAAAAAAAOEJJTQQUAAAAAAAEAAAAAjhCSU0EDAAAAAAE4gAAAAEAAAAkAAAAJAAAAGwAAA8wAAAExgAYAAH/2P/tAAxBZG9iZV9DTQAB/+4ADkFkb2JlAGSAAAAAAf/bAIQADAgICAkIDAkJDBELCgsRFQ8MDA8VGBMTFRMTGBEMDAwMDAwRDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAENCwsNDg0QDg4QFA4ODhQUDg4ODhQRDAwMDAwREQwMDAwMDBEMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwM/8AAEQgAJAAkAwEiAAIRAQMRAf/dAAQAA//EAT8AAAEFAQEBAQEBAAAAAAAAAAMAAQIEBQYHCAkKCwEAAQUBAQEBAQEAAAAAAAAAAQACAwQFBgcICQoLEAABBAEDAgQCBQcGCAUDDDMBAAIRAwQhEjEFQVFhEyJxgTIGFJGhsUIjJBVSwWIzNHKC0UMHJZJT8OHxY3M1FqKygyZEk1RkRcKjdDYX0lXiZfKzhMPTdePzRieUpIW0lcTU5PSltcXV5fVWZnaGlqa2xtbm9jdHV2d3h5ent8fX5/cRAAICAQIEBAMEBQYHBwYFNQEAAhEDITESBEFRYXEiEwUygZEUobFCI8FS0fAzJGLhcoKSQ1MVY3M08SUGFqKygwcmNcLSRJNUoxdkRVU2dGXi8rOEw9N14/NGlKSFtJXE1OT0pbXF1eX1VmZ2hpamtsbW5vYnN0dXZ3eHl6e3x//aAAwDAQACEQMRAD8A9M6h1DF6diuysp+ytug7lxP0WMb+c9yy63/WTqgF1bq+kYrtawWi69w7OeHfoK9yb0WdY+sF/wBoAswulNFTKnCWuutbutse0/S9Kv8ARrIw+uHAbn4nS72W4mJc/GH2sOP2O9r3V11Zfpn1v2ZluZsxc5m/0v8ACe+r0klO2elfWCoF1HWDY8a7LqGFhPhLNr2f2VLD61kMy2dO6xSMXLsn0LWEmi6P9C93uZZ/wVi5zI+vH1h6XcLOrYmE/CocwdS+xXOtuxm2iaXXUv2ubv3M9OzY/Hu/0/vrXQmzF+s/TcvGNN2Lbj2entyGhllVwZXk02t2Os/weRU76aSnaSXPft7J/wCav23/AL0P6Nt/7sb/ALNMf1v021JJT//Q7voJ2Z/Wcd2ljcv1T/UtYx1R/wCgvNKOrU9P+t/WpyaMazKtzMWu7IAfS17slt7K81hD/wBSyvs/2TJs2/oWX+t/wi9K6vRlYOczrmDWbtrPSzsdn0rKZ3Ntr/4aj/qFcwrOj9Tp+1YrabmvMuO1u4OPPqNI3Nf/AF0lPCvrxMjEdaHVV+rh3YrCMmu84jcpj6a8TqF2G+/7V0N+T+nwciz9Lj2M/wAH/htf/Fhj5GJ0jqGLl1upyac97ba3mSD6OLtO7X1GPZ76rWfo7av5tdUOn4ADw3GqAsaWWAMaNzXfTY6B7muXLj7ZXkZH1e6LkDIxrQALZLjhsJi6o3fRsbs/mGb/AFWJKYT/AJA/aEfq/wC1vtc/8F62zckun/ZWH+y/2Vt/VfS9GNJ2xt3cfzn5+799JJT/AP/R9VXMdW/5sfbnen637Tn3/s3f60/nep6H6Ld+96vvXz4kkp93d+z4/wAp/tz7N+f9q3elH/CfZ/eum6R+yfsTf2T6X2Xt6XE/y/zvU/4z9IvmJJJT9VJL5VSSU//ZOEJJTQQhAAAAAABdAAAAAQEAAAAPAEEAZABvAGIAZQAgAFAAaABvAHQAbwBzAGgAbwBwAAAAFwBBAGQAbwBiAGUAIABQAGgAbwB0AG8AcwBoAG8AcAAgAEMAQwAgADIAMAAxADUAAAABADhCSU0EBgAAAAAABwAIAAAAAQEA/+EOomh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8APD94cGFja2V0IGJlZ2luPSLvu78iIGlkPSJXNU0wTXBDZWhpSHpyZVN6TlRjemtjOWQiPz4gPHg6eG1wbWV0YSB4bWxuczp4PSJhZG9iZTpuczptZXRhLyIgeDp4bXB0az0iQWRvYmUgWE1QIENvcmUgNS42LWMxMTEgNzkuMTU4MzI1LCAyMDE1LzA5LzEwLTAxOjEwOjIwICAgICAgICAiPiA8cmRmOlJERiB4bWxuczpyZGY9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkvMDIvMjItcmRmLXN5bnRheC1ucyMiPiA8cmRmOkRlc2NyaXB0aW9uIHJkZjphYm91dD0iIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtbG5zOmRjPSJodHRwOi8vcHVybC5vcmcvZGMvZWxlbWVudHMvMS4xLyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHhtcDpDcmVhdGVEYXRlPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiB4bXA6TW9kaWZ5RGF0ZT0iMjAxNi0wMi0xMlQxNjowMzoyOCswNTozMCIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDo5MzU4NTMyYi0zYjFkLTE1NDctOTdmMS1mOTllOTkwMzc3ZWMiIHhtcE1NOkRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDowNjVkZTNmZi1kMTc0LTExZTUtOWI1ZC1kNThjMzkxZDExMWMiIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpmMGQ5YWU1OS02ZmU4LWUwNDktOTYwMS1mNzgzOWFmMjcwN2IiIHBob3Rvc2hvcDpDb2xvck1vZGU9IjMiIHBob3Rvc2hvcDpJQ0NQcm9maWxlPSJzUkdCIElFQzYxOTY2LTIuMSIgZGM6Zm9ybWF0PSJpbWFnZS9qcGVnIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpmMGQ5YWU1OS02ZmU4LWUwNDktOTYwMS1mNzgzOWFmMjcwN2IiIHN0RXZ0OndoZW49IjIwMTYtMDItMTJUMTY6MDM6MjgrMDU6MzAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1IChXaW5kb3dzKSIvPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0ic2F2ZWQiIHN0RXZ0Omluc3RhbmNlSUQ9InhtcC5paWQ6OTM1ODUzMmItM2IxZC0xNTQ3LTk3ZjEtZjk5ZTk5MDM3N2VjIiBzdEV2dDp3aGVuPSIyMDE2LTAyLTEyVDE2OjAzOjI4KzA1OjMwIiBzdEV2dDpzb2Z0d2FyZUFnZW50PSJBZG9iZSBQaG90b3Nob3AgQ0MgMjAxNSAoV2luZG93cykiIHN0RXZ0OmNoYW5nZWQ9Ii8iLz4gPC9yZGY6U2VxPiA8L3htcE1NOkhpc3Rvcnk+IDxwaG90b3Nob3A6RG9jdW1lbnRBbmNlc3RvcnM+IDxyZGY6QmFnPiA8cmRmOmxpPmFkb2JlOmRvY2lkOnBob3Rvc2hvcDowNWY0YzZjZC1kMTczLTExZTUtOWI1ZC1kNThjMzkxZDExMWM8L3JkZjpsaT4gPC9yZGY6QmFnPiA8L3Bob3Rvc2hvcDpEb2N1bWVudEFuY2VzdG9ycz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+ICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgPD94cGFja2V0IGVuZD0idyI/Pv/iDFhJQ0NfUFJPRklMRQABAQAADEhMaW5vAhAAAG1udHJSR0IgWFlaIAfOAAIACQAGADEAAGFjc3BNU0ZUAAAAAElFQyBzUkdCAAAAAAAAAAAAAAABAAD21gABAAAAANMtSFAgIAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAEWNwcnQAAAFQAAAAM2Rlc2MAAAGEAAAAbHd0cHQAAAHwAAAAFGJrcHQAAAIEAAAAFHJYWVoAAAIYAAAAFGdYWVoAAAIsAAAAFGJYWVoAAAJAAAAAFGRtbmQAAAJUAAAAcGRtZGQAAALEAAAAiHZ1ZWQAAANMAAAAhnZpZXcAAAPUAAAAJGx1bWkAAAP4AAAAFG1lYXMAAAQMAAAAJHRlY2gAAAQwAAAADHJUUkMAAAQ8AAAIDGdUUkMAAAQ8AAAIDGJUUkMAAAQ8AAAIDHRleHQAAAAAQ29weXJpZ2h0IChjKSAxOTk4IEhld2xldHQtUGFja2FyZCBDb21wYW55AABkZXNjAAAAAAAAABJzUkdCIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAEnNSR0IgSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABYWVogAAAAAAAA81EAAQAAAAEWzFhZWiAAAAAAAAAAAAAAAAAAAAAAWFlaIAAAAAAAAG+iAAA49QAAA5BYWVogAAAAAAAAYpkAALeFAAAY2lhZWiAAAAAAAAAkoAAAD4QAALbPZGVzYwAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAWSUVDIGh0dHA6Ly93d3cuaWVjLmNoAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGRlc2MAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAALklFQyA2MTk2Ni0yLjEgRGVmYXVsdCBSR0IgY29sb3VyIHNwYWNlIC0gc1JHQgAAAAAAAAAAAAAAAAAAAAAAAAAAAABkZXNjAAAAAAAAACxSZWZlcmVuY2UgVmlld2luZyBDb25kaXRpb24gaW4gSUVDNjE5NjYtMi4xAAAAAAAAAAAAAAAsUmVmZXJlbmNlIFZpZXdpbmcgQ29uZGl0aW9uIGluIElFQzYxOTY2LTIuMQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAdmlldwAAAAAAE6T+ABRfLgAQzxQAA+3MAAQTCwADXJ4AAAABWFlaIAAAAAAATAlWAFAAAABXH+dtZWFzAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAACjwAAAAJzaWcgAAAAAENSVCBjdXJ2AAAAAAAABAAAAAAFAAoADwAUABkAHgAjACgALQAyADcAOwBAAEUASgBPAFQAWQBeAGMAaABtAHIAdwB8AIEAhgCLAJAAlQCaAJ8ApACpAK4AsgC3ALwAwQDGAMsA0ADVANsA4ADlAOsA8AD2APsBAQEHAQ0BEwEZAR8BJQErATIBOAE+AUUBTAFSAVkBYAFnAW4BdQF8AYMBiwGSAZoBoQGpAbEBuQHBAckB0QHZAeEB6QHyAfoCAwIMAhQCHQImAi8COAJBAksCVAJdAmcCcQJ6AoQCjgKYAqICrAK2AsECywLVAuAC6wL1AwADCwMWAyEDLQM4A0MDTwNaA2YDcgN+A4oDlgOiA64DugPHA9MD4APsA/kEBgQTBCAELQQ7BEgEVQRjBHEEfgSMBJoEqAS2BMQE0wThBPAE/gUNBRwFKwU6BUkFWAVnBXcFhgWWBaYFtQXFBdUF5QX2BgYGFgYnBjcGSAZZBmoGewaMBp0GrwbABtEG4wb1BwcHGQcrBz0HTwdhB3QHhgeZB6wHvwfSB+UH+AgLCB8IMghGCFoIbgiCCJYIqgi+CNII5wj7CRAJJQk6CU8JZAl5CY8JpAm6Cc8J5Qn7ChEKJwo9ClQKagqBCpgKrgrFCtwK8wsLCyILOQtRC2kLgAuYC7ALyAvhC/kMEgwqDEMMXAx1DI4MpwzADNkM8w0NDSYNQA1aDXQNjg2pDcMN3g34DhMOLg5JDmQOfw6bDrYO0g7uDwkPJQ9BD14Peg+WD7MPzw/sEAkQJhBDEGEQfhCbELkQ1xD1ERMRMRFPEW0RjBGqEckR6BIHEiYSRRJkEoQSoxLDEuMTAxMjE0MTYxODE6QTxRPlFAYUJxRJFGoUixStFM4U8BUSFTQVVhV4FZsVvRXgFgMWJhZJFmwWjxayFtYW+hcdF0EXZReJF64X0hf3GBsYQBhlGIoYrxjVGPoZIBlFGWsZkRm3Gd0aBBoqGlEadxqeGsUa7BsUGzsbYxuKG7Ib2hwCHCocUhx7HKMczBz1HR4dRx1wHZkdwx3sHhYeQB5qHpQevh7pHxMfPh9pH5Qfvx/qIBUgQSBsIJggxCDwIRwhSCF1IaEhziH7IiciVSKCIq8i3SMKIzgjZiOUI8Ij8CQfJE0kfCSrJNolCSU4JWgllyXHJfcmJyZXJocmtyboJxgnSSd6J6sn3CgNKD8ocSiiKNQpBik4KWspnSnQKgIqNSpoKpsqzysCKzYraSudK9EsBSw5LG4soizXLQwtQS12Last4S4WLkwugi63Lu4vJC9aL5Evxy/+MDUwbDCkMNsxEjFKMYIxujHyMioyYzKbMtQzDTNGM38zuDPxNCs0ZTSeNNg1EzVNNYc1wjX9Njc2cjauNuk3JDdgN5w31zgUOFA4jDjIOQU5Qjl/Obw5+To2OnQ6sjrvOy07azuqO+g8JzxlPKQ84z0iPWE9oT3gPiA+YD6gPuA/IT9hP6I/4kAjQGRApkDnQSlBakGsQe5CMEJyQrVC90M6Q31DwEQDREdEikTORRJFVUWaRd5GIkZnRqtG8Ec1R3tHwEgFSEtIkUjXSR1JY0mpSfBKN0p9SsRLDEtTS5pL4kwqTHJMuk0CTUpNk03cTiVObk63TwBPSU+TT91QJ1BxULtRBlFQUZtR5lIxUnxSx1MTU19TqlP2VEJUj1TbVShVdVXCVg9WXFapVvdXRFeSV+BYL1h9WMtZGllpWbhaB1pWWqZa9VtFW5Vb5Vw1XIZc1l0nXXhdyV4aXmxevV8PX2Ffs2AFYFdgqmD8YU9homH1YklinGLwY0Njl2PrZEBklGTpZT1lkmXnZj1mkmboZz1nk2fpaD9olmjsaUNpmmnxakhqn2r3a09rp2v/bFdsr20IbWBtuW4SbmtuxG8eb3hv0XArcIZw4HE6cZVx8HJLcqZzAXNdc7h0FHRwdMx1KHWFdeF2Pnabdvh3VnezeBF4bnjMeSp5iXnnekZ6pXsEe2N7wnwhfIF84X1BfaF+AX5ifsJ/I3+Ef+WAR4CogQqBa4HNgjCCkoL0g1eDuoQdhICE44VHhauGDoZyhteHO4efiASIaYjOiTOJmYn+imSKyoswi5aL/IxjjMqNMY2Yjf+OZo7OjzaPnpAGkG6Q1pE/kaiSEZJ6kuOTTZO2lCCUipT0lV+VyZY0lp+XCpd1l+CYTJi4mSSZkJn8mmia1ZtCm6+cHJyJnPedZJ3SnkCerp8dn4uf+qBpoNihR6G2oiailqMGo3aj5qRWpMelOKWpphqmi6b9p26n4KhSqMSpN6mpqhyqj6sCq3Wr6axcrNCtRK24ri2uoa8Wr4uwALB1sOqxYLHWskuywrM4s660JbSctRO1irYBtnm28Ldot+C4WbjRuUq5wro7urW7LrunvCG8m70VvY++Cr6Evv+/er/1wHDA7MFnwePCX8Lbw1jD1MRRxM7FS8XIxkbGw8dBx7/IPci8yTrJuco4yrfLNsu2zDXMtc01zbXONs62zzfPuNA50LrRPNG+0j/SwdNE08bUSdTL1U7V0dZV1tjXXNfg2GTY6Nls2fHadtr724DcBdyK3RDdlt4c3qLfKd+v4DbgveFE4cziU+Lb42Pj6+Rz5PzlhOYN5pbnH+ep6DLovOlG6dDqW+rl63Dr++yG7RHtnO4o7rTvQO/M8Fjw5fFy8f/yjPMZ86f0NPTC9VD13vZt9vv3ivgZ+Kj5OPnH+lf65/t3/Af8mP0p/br+S/7c/23////uAA5BZG9iZQBkQAAAAAH/2wCEAAEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQECAgICAgICAgICAgMDAwMDAwMDAwMBAQEBAQEBAQEBAQICAQICAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDA//AABEIAJYAlgMBEQACEQEDEQH/3QAEABP/xAGiAAAABgIDAQAAAAAAAAAAAAAHCAYFBAkDCgIBAAsBAAAGAwEBAQAAAAAAAAAAAAYFBAMHAggBCQAKCxAAAgEDBAEDAwIDAwMCBgl1AQIDBBEFEgYhBxMiAAgxFEEyIxUJUUIWYSQzF1JxgRhikSVDobHwJjRyChnB0TUn4VM2gvGSokRUc0VGN0djKFVWVxqywtLi8mSDdJOEZaOzw9PjKThm83UqOTpISUpYWVpnaGlqdnd4eXqFhoeIiYqUlZaXmJmapKWmp6ipqrS1tre4ubrExcbHyMnK1NXW19jZ2uTl5ufo6er09fb3+Pn6EQACAQMCBAQDBQQEBAYGBW0BAgMRBCESBTEGACITQVEHMmEUcQhCgSORFVKhYhYzCbEkwdFDcvAX4YI0JZJTGGNE8aKyJjUZVDZFZCcKc4OTRnTC0uLyVWV1VjeEhaOzw9Pj8ykalKS0xNTk9JWltcXV5fUoR1dmOHaGlqa2xtbm9md3h5ent8fX5/dIWGh4iJiouMjY6Pg5SVlpeYmZqbnJ2en5KjpKWmp6ipqqusra6vr/2gAMAwEAAhEDEQA/AN/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3QW757w6Z6x8w7E7W672TLAoeSl3PvHb+FrgpTyDRQV1fDWys0fqAWMkjke/de6K1m/5nfwdwjvEe9MZmJo3MbR7c2xvbPKCPyKnG7bnomQj6ESkH8e/de6S6fzYfhEzBT2VuGMH6SSda9grH/r3G3SeR/h7917oR9s/wAxr4TbsqIKTHfITZdBVT6QsO54s7s/Qz/pWWfdWIw9LEx/F5OffuvdGt2pvvY+/KNshsfeW1d5UCCNnrdq7hxO4aRBKCYy9RiaurhUSBTa55t7917pV+/de697917r3v3Xuve/de697917r3v3Xuve/de6/9Df49+691737r3Xvfuvde9+691CyWSx2Hx9blsvX0WKxeNpZq3I5LJVUFDj6Cjpo2lqKutrKqSKnpaWniUs8jsqIoJJA9+691UD8iv5wfUmw5a/bfQGCPdm5acz0z7smq5sF1Zj6qM6CYMwIJcxvARSqVYY6BKVxYpVke/de6pp7j+dfyu7ymq4t1dt53bm36sSJ/cvrKSbYW20ppJPN9pUyYmc7kzMcTWCtW5Cc2UcfX37r3RSMbjZ9wZTwYPFZLdGenkIMGExmT3RnaiYsS3lGPp8lkpJixuTIb+/de6Mhgvh38tdzRwT4n43dxSQVSJJTVOT2s23qeaJ1DpKsm5KvEMsboQQWVQb+/de6V038v75sQRNPJ8cd6NGq6isGZ2JUzkWvZaeHdrzM3+ABPv3Xugv3V8Zfkpsmjmrd2/HzuXC4yK4nr5dhZrI45BYk+SqwkWVpwLKfqRwL/T37r3QLYfI1u2cutVtzKZbae4qSQutRgcjlNq7gpZYyCxc4+fG5SNkYchx+Pp7917qwvpX+aJ8seonpqLPbnou7tsROolxHaCs+4lhZ0M5oN+4iGHNipCJaM18WSjBY3X6Ee691d/8Zf5lPx5+RtVjdq1GQqOqO0MgUhg2Dv2ppKdczWMbGDZ26oWXBboYsyhIVanr3J/4CgC/v3XurCffuvde9+691737r3Xvfuvde9+691//0d/j37r3Xvfuvde9+690Xn5J/J3qr4sbDbfHZ2WkjeulnoNp7TxKxVe7N7ZuKHznEbcxbyw+doYyr1NTK0VJRxMHnlQFdXuvdasnyo+andXy2y1RDvOvba3WcFe1Vt3p3btdK+26OKMj7Op3fXJHTTb93BEigtLUoKCCRn+1powxZvde6ZPjp8Pu/flLWK3Vu00h2fFO1Lke0d2yVGF68x8sPjWanochHTVFduyvpg6hqXFQ1HjuBLJCOffuvdXpdHfyf/jxsKGkyfcFfmu9dzogeekzLybb68ppy0cqii2dhqoVFeKeQMgfJV1asqW1Rqb+/de6s+2jsPY/X+OXEbE2dtfZmLVYkGO2tgMVgKMrCnji10+KpaWJzGgsCQT7917pWe/de697917r3v3Xugc7S+PXR/dlDPj+1uq9kb4jqEVDWZrA0UmZgCAhGodwQRwZ3GyoCQHp6mJwDwffuvdVJ9//AMmTbeQjrc/8aN+Ve1chaWdOu+x6mr3BtOpdpGk+2w+7oop91beRIgI4lqVysVzzpHI917qjntrpvszpXc8uwu5NiZbZO4HDT0dFl44qrFZ6mp5GAym1NwUbTYfcdFDJGSJ6OZpYGX1rE4sPde6sQ+G380HsboyoxOwO8anOdqdNxx0+Oo89NI+U7L64pkbTFNBWTE1m/drUkR0PR1LvlKaJV+2mmSMUze691su7J3ttLsfaeB31sTcOL3XtDdGOhyuA3BhqpKzHZKhnuFlgmTlXjkVo5Y3Cywyo0ciq6so917pUe/de697917r3v3Xuv//S3+Pfuvde9+690W/5TfJ3YHxS6tyHY29WkyOQnlbEbI2ZQTxRZvfO7JqeWahwWNMoZaWmRYmmrq11aGho43lYMQkb+691qGd192dk/IfsbL9p9q5v+Lblyi/Z4/H0hmi25s3b0cry0O0NnY+V2/h2CoS5Z3N6mvqWepqXkmcke690ev8AlpfETpP5N7r3Pmu2t50mYbr2alqY+gaWSqxmR3LQTPAKXem68jqgmy+xfvGal+wxzWNUAMhIitFTz+691tEYnEYnAYygwmCxmOwuGxVJBQYvEYmipsbjMbQ00YipqKgoKOKGlo6SniUKkcaKiKLAAe/de6cPfuvde9+691737r3Xvfuvde9+691737r3XvfuvdBb3B0t1j33srIdfdr7Rxm7ttV9pEgrY2jrsVXoCKfMYDLU7RZLBZqkJJiqqWWKZQSuoqzKfde61cfm18C99fEfMPuXE1GS310Rl8glPg99zRRvm9nVdZNoodtdkpSRQ00UsjMsVJmokjpa+SySpTzsqSe690y/CT5s7x+IG9DTVRye5ejt1ZSObsPYEJNTUYapl0QT9gbDppXCUm6KOBQa+iQxwZqniCvaqjp5l917rbW2fu/bHYG1tv722Xm8fuTae6cTRZzb+dxcwnoMpi8hCs9LV08llYB439SMFkjYFXVWBA917pSe/de697917r//09/j37r3TLuTceD2ft7O7s3Pk6XC7c2ziMjn89mK5zHR4vD4ikmr8lkKpwGKwUlHA8jWBNl4BPv3XutN75dfJ/cnyy7jynY+Sasx+y8UlVg+qdp1TeNNs7LE2oZGsp1Ywpufd5jStychLPGDFShzFTKPfuvdDd09/Lc7y7o+N2c7928aWgy9U0WT6q62ysX2lf2bs+jSVsrnRkpzGNv1ecI/37qTL4a1I/JOYoamCVfde6JHsvee++pN+4jfGyMtmNh9kbAzVZHRVr0klJl8DmKOR6DObc3FhK1U89NNoejyuKrEMVRHqjkX9LD3XutsH4Q/N7Zny82ZJBURUG0u6Np0FM/YfXiVTyRCJ3Wlj3ls2WqP3WX2RlqqyqzaqjG1D/a1Xr8M1R7r3R6Pfuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+690ybl21t/eO38ztTdeGx24dtbhx1XiM5g8vSQ12MyuMroWgq6KtpJ1eKennichlI/3n37r3Wpb88PhjlfiJ2NSPt6PIZPpDfVTUN1vuCplmrKnbmShjkq6vrbcddJqlkyeNpY2nxVTKxfIY6NgWaemnJ917oxv8qb5gz9Vb9pPjfv3KN/oz7NzLL11V1kpMGx+y8k5YbfhZyWp9vdiVFxFGD4qfNadKqK2Vh7r3WzB7917r3v3Xuv/1N/j37r3VGv85P5FtittbV+L23K0rWb4hp989oeCSxTZOMyDxbZ21UaGN03TuSheonjYKTS4zSbpMQfde6qz+DXxfb5Xd84rZmZhnPWe0aaDenbNUglVarbsFWYcTs6KpjeIxVm+MtCaaTTIsqY2GrkUFlX37r3W4bRUVHjaOkx2OpKagx9BTQUVDQ0UEVLR0VHSxJBTUlJTQJHBTU1NBGqRxooREAAAA9+691Uz/MQ/l3UvfFNku6+lMdRYzvPHUSPuDbyGCgxXcOLx8Hjio62djHT4/ftBSxhMdkpPRUIq0lWfEYpqb3XutcXZ28t+9Sb8xW9dlZbN9f8AZWwM3WR0dZJRSUWa29mqKR6DN7d3DhK9E81NKUejyuKrEMNTEWjlUjSw917ra++EHzh2Z8u9ny0dTFRbT7p2ljqaXsLr5ah3heFnSkTemzJaljU5XZOVq2CgsWqcZUv9rVXbwzVPuvdI3v7+bf8Ay8Pi32vuLo7v/wCR+L6y7T2rSYTIZraeb2B2zUzRY7cWNgy2FyVDlMPsLJYPM46toqhbT0VVURRyh4XZZo5I1917oHP+H8f5Rf8A3mdtH/0X3dH/ANrX37r3Xv8Ah/H+UX/3mdtH/wBF93R/9rX37r3Xv+H8f5Rf/eZ20f8A0X3dH/2tffuvdGr+Kn8xX4WfNzN7y218W++9tds7g6/xmIzW7cLjsPvDb2TxeHzlTWUWOykdHvLbm3J8pj3rKF4ZZqMVEdNK0azGNpYg/uvdHV9+691737r3XvfuvdAx8gujtnfI3qTeHUW94SMVujHlaHKwxJJkts7go2FVgN04ZmaMxZTA5OOOeOzqJVVonJikdT7r3Wlt2BsLdPWe994dZ73p5MVvPYW46zb2b+zaSER5PFzx1GPzuFn0xP8AZ5GnamyWPnT6RyxspuPfuvdbb/wH+R7fJn45bU3bmKmObf8AtZ32F2XGpAeTd23qelV80EvqEG6sTUUuTQ6VQNVPGv8Amz7917o6Pv3Xuv/V39ZJI4Y3lldIookaSSSRlSOONFLO7uxCoiKCSSbAe/de60j/AJHdwz99d7ds9x1U0n2G7d11528KnSGoNi7dH8C2dSMUAQJHgMfHUOQAGlndjyT7917rZU/lcdCRdM/F/b+5cnjzS747sli7L3NJPGgq4MRkKcQ7EwhkVmZqXG7UENQENilTXT8XJv7r3Vj/AL917oBvkT8n+gPiXsbH9lfI/tPbHUWxMpujG7Lx25t1zVcOOq905ehyuTxuEhajpayZq2roMHVzKNGnRTuSRb37r3VS3yK6N+OX8yrr/NfK/wCBHZfX/ZnYO366s27u5dmZanXCdnVW3qeES7Z3NFUpRz7Z7JxuPaJ8VX1cUK1lJJFFUF6SWnqKf3XuqN9o7u371LvzGbx2bls7172X1/m6yKkrHpJaDO7azdE70Gb2/uDCV6xmSGUK9HlcVWIYamFmikUgqw917q4Lt3qP43/z4/jfJs/dqYvpn5sdM4WXI7V3ZQU/3VdtevrNEEmUxSzOmQ3x0HvbJRxxZPGyu1Tial1VnSrSmqan3XutJfsf4X/LLq35A7q+LW4Ohe0M73rtGpVarZXXWy91dgPuDD1MpjxG89n1W3MRWrnNk7jitLRVwCD9UMojqI5Yl917pZ5T+W//ADFMLi5czk/gr8s6fG08DVNRPH0tvCtlp6dIzLJNUUOOo6zIwxxRqS5aEaAPVb37r3RN8jSZLDZavwGboMrg8/i5GiyeBzuOyGEzmOkRzGyV+Gy1PR5OjKyKV/ciXkW9+690dz+W58yc18DvmV1B8h6Z8hUbOocmdj9zYKg88sm4+md6VFJQbyhShhqqNcnk9rPHTZ/GQyyCI5HFRBrqzA+6919AL/h67+VT/wB5v9LfQH/i4Zz8gEf8uP62P+wPHv3XuuL/AM7H+VPGjyP84Olgkas7n7/OmyoCzGwwZJsB+PfuvdWSbM3jtjsTZ20+wNk5qi3JszfW2sFvHaO4sa7yY/PbY3Ni6XNYDNUDyJG70WUxVbFPESqkpILgfT37r3Sl9+691r0fznOh4MPufrv5H4Ki8Ue7FTrDsOSGNUhfM4ymqcpsHM1LglpayqxsdfjmJA9EFMt+PfuvdAx/KA7jl2H8lcx1TWTsuA7x2pUrTQFkSCLfXX1LV5zFVTM/+7MhtR8lTlVsZGgi+ukD37r3Wz97917r/9bdC+d/YVX1f8Qe/d242dabLJsHIbew8xNnTK7znptn0EkIDKxmhqM6JF0+pSmofT37r3Wol1T17J2d2Z1d1PSMU/v9vvZ+yDMAWWnxuTytJTZSoew/RT4SOoc/Thf6+/de63mKCho8XQ0eNx9PFR0GOpKehoaSBdENLR0kKU9NTwoOFighjVVH4A9+691L9+691rY/8KmP+3d/V/8A4uJ1T/77juj37r3Wnh8Dvnj3n/L07zoO6el8gMjj8gtFie1eqcvXz02y+4NnU0zv/A86I1mXE7kxImlkwecjjepxdS7KyzUc1TTTe691ug752H0D/Nx6Cx3zd+EWQooe346NMZ2N1zlJKHCZ3L53DUUTZHrTszG+dqfbXa23IWVcTlnb7PI0jxBppqCamqYPde6pw2ruvfnU2+8bu3aGUz/XnZXX+eqkpatqSXG5/bWex8j0OZwGfwuQjUvDIA9JlMXWRmGpgZo5FKlWHuvdbVvwp+cm1vltsvJUYpMJtX5A7Vwsb7w2BUVk1PjsoIxJDQbv2nWSJV5LIbByOSl0yWWoq8NPMaepVmaGWp917qinuz/hSB8j/id35vDov5Qfy6KTaO5Nn1SCtwuB77letyWCrHf+D712huDKdZJt/ee087BGZKWohFMpcNBMYZ4pY1917o42y+4v5R3/AAoF2Dkuu927TixneuAwZq02xvCixXXXyk62jNJUxHdXVu+MXNkRu7auJr55HY0NVlcOZEhbLY9BJDE/uvdacn8yL+XN3D/La70Tq/sGd94dd7yiymb6P7kpMccdieyds4uamTJ4/I0aPNT4LsXaAr6dM1jFkZFSohqqctTTxsPde6Fz+T//AC1qH+Zh8hN27E3jvzOde9S9SbQxe9+yMntKPHPvfcK53MS4bb2z9qVeXpsji8DNkXoquoqslPR1op4aYRxwmSUOnuvdDD/O4/lH7S/ln1nWO8unuxN3b36X7nfde2KPC9jT4eu33sfee2cDT5qWCbP4TGYCi3NtzcOOlnkgc0ENRQy0xikeVZEf37r3W9r8Af8AshD4Uf8AipHxv/8AfObN9+690bf37r3RO/n51anbvxD7v2zHDHLlsVtCq3zt13TVJBntgSR7voftiOVnrBh3pQfys7A8H37r3WpL1D2BU9b9o9UdoYyUwvszsDZW67kN6sZBmaIZWF1upaOowVVURspIBVzfj37r3W815ofD9x5Y/B4/N59a+Hw6dfl8l9Hj0c6r2tz7917r/9fac/nH5mXG/EfHY6KV4zuTuPr/ABMyIxHlpqSHPbhlRwOGjLYNb3BF7fm3v3XuqdP5ZO3abcvze6cirIFqafb9Fv8A3cyMbCOpw2z8hS4upHNy1Nk8tE4A/I549+691tz+/de697917rWx/wCFTH/bu/q//wAXE6p/99x3R7917rQd9+691Zh/KK+Q/wAkOhPnj0JiPjduCkpMl3z2ZsPp3sDZe5WyNR13vvZ2ezS09bPvLG45jVR1uyMZNV5LGZSlX76gkieNfJTzT08vuvdbz/8AML/l30PyCo67uLpmgx2H75xdEpy+JMkGMw/cOKoISsGJzFQ+ikx29qGBdGLy0mlXFqSsY0/ilpfde61sts7m3z1ZvnGbq2pk8/152X15uCpFHWNSSY7cW1dw46VqLL4TN4evRdSOFekyeMrI2p6qBmjlVkZW9+691cJ2l1f8b/573xwfr7sOPD9OfNXqDE1WS2ZvDF04qa/bdfULHDJn9vRVMyV29ejt5VscUWZws0r1GNnKK7R1UdHWTe691pJ9m9Z/J/8Al6/J5drbpizXSXyV6I3Pj917Q3Nh5mlpTNT1Ejbe39sbMTQCj3d13uyGBkbXG8FXSvNQ10KyLNCvuvdbs3yY/uh/Ol/kZzd6x4TEYft7bXWuY7u2/S0qyVMuw/kB0PT5uHsLaeNjjqqiuhxe8ocLl8TTQVMjl8ZmKaeUM6ow917rSn+F3zZ77+C3btF3x8dNw43Ebgr8Adv7m25ujFnObM39s6tqqPLnbG78RHUUFa1PHkKOGenq6KppK+jlVvFMqySI/uvdLn+YF/MZ+TH8xrduG3r8g8ptegxewsHmqDr3rTrzFZDC9fbKbNUtMNxZWgp8zlc7nsruDcX8NpxU1tdWTyLFEsMQjiGn37r3X0mPgD/2Qh8KP/FSPjf/AO+c2b7917o2/v3XuoWSx1Hl8dX4nIwJU4/KUVVjq6me+ioo62CSmqoHsQdEsErKf8D7917rQxz2MkxLblwXqjnwmQ3NgeCNUUuGyWSxUfK6l1RmkH04uPfuvdbk399pf9kV/wBIfnf7n/ZUP73fca28v3n+iP8Ai2rXq1+b7n83vq/N/fuvdf/Q2cP51Kufjh1k4VjGnemC1kX0qW2TvsRl/wActwP8ffuvdVpfymGjHzY2wG0626x7NEdxyWEW3mYKbcHQD/sPfuvdbWvv3Xuve/de61sf+FTH/bu/q/8A8XE6p/8Afcd0e/de60HffuvdWo/yQ6/A43+a58NKjcrUq42beO/MbSfdoJIzuXK9R78oNpiMEgLVHPTQiFv7MhBtf37r3V9/88D5t/Ir+Xx/NH+LHf8A03XSVu3Mh8V5Nsb063zlbWRbC7YwGP7j3PkdzbWyyL5oMRn6WmyVBPjcxTxGrx1SULCameanl917o6m8Nn/H3+b/ANAUHzT+FeSoaDuvH4+LFdj9b5aShw+dymexFDFJWdYdoUSStDgeycHAdOEzhLUeRpDCjTS4+SCem917qm3bW5N89Wb6xm6dsZDcHXfZnXe4KkUdW9LJjdx7U3HjJXosvhc1iK5AHQlXpMnjKtHpquBmjlV42VvfuvdXDdl9afG7+e58cH627MTD9P8AzS6ixVXldmbyxFMtTkts5CoVKd9x7aSqljrd6dJ7xrI4oc3g5pjNQTFVdoqmOirZfde6XP8AJQ+Ofbvwu/l+/I3qH5Y7Rl2XkdjfIP5FZHILXCSr2ruLriDZmzjLvXZ+QnWBs5sDc32FfU0dQ0cLuhdJY45UkUe69187SiKtSxMhUxv5JISospp5JZJKbSPwv27LYfge/de66r/+AFd/1BVf/uPJ7917r6wnwB/7IQ+FH/ipHxv/APfObN9+690bf37r3XvfuvdaJ3ZTRt2B2o8ekxHsnsxlKj0lDvTcBuBb6Ee/de62lvDUf8NS+LRJ9x/skP6OfJ/zJ7V/r/o/3j37r3X/0dqr+cLt+ozPw+bJQQtKu0u1+u9w1TgcU9JNU5HbM0zt/YS+4lW/PqYD37r3VK38tvdNLtH5tdIVlbI8dNnqjeOyWKG2uq3TtDKw4qNhcB0bLUcNwfp9RyPfuvdbe/v3Xuve/de61sf+FTH/AG7v6v8A/FxOqf8A33HdHv3XutB337r3Qh9Q9p7q6K7c6r7w2KkEu9Om+xtm9n7Wp6tddHXZjZWdos5Di6yO6iSjzEVI9JKpIBjnIJtf37r3W+D/ADTvjHt3+c9/Lk6Z+UPxLlj3T2VsrDVHdHSOKknhhrd5YHcOOhxfbnR9fJHUSUWL3uZ8EKdEkMiQblwKUcjRRyyzR+691pjfCf5s/ID+XZ8gR2z1HLW4/MYyu/uj3L05u9MjhML2DhMRWyR5Xr/sPCTxLW7d3Xgqjzfw6veAV+Frb+mSnkngl917rdJ3ZtT4+/zhPj9RfND4X5ChxveeMx8OH7G62zM1DiM5kc7iKKN6rq/tKjjmMGD7CwlPxgc8S1HkKMxI0slBLDNSe691Tbt3cW+uq99Y/cu2shuHrrs3rrcFUlLVNSyYzc20dy413ocvhsxiq1PyNdJksbVI9NWUztFKrxOCfde62MevPkdF/Mz+Inevxxxm+aHoT5L7x6b3fsDcdTT4mLcFBSUe7cLUbZquxtkYrI1VPLmtp5KLItBUQidMjhZ5zGzhxS1M/uvdfO++SHxl7k+HvcW6fj53xs+bZvYGyjFaGNparbu6dsyvLT4PfOxM1JDAm4tlbgipyaapVVlhlSSmqY4aqGWJfde6AKv/AOAFd/1BVf8A7jye/de6+sJ8Af8AshD4Uf8AipHxv/8AfObN9+690bf37r3TPuHN0W2cBnNyZJimO2/h8nm8g621LRYqinr6pl1EC6wU7EXIHv3XutDXKV0+Vp8xlSTJVZuXNZjUzHVLVZyrrMkhZjclpJa0XP8AX37r3W6T/cCq/wBk4/0W/aSfe/7LP/cL7HR+795/ou/u/wDbeO/+d8/ptf8AV7917r//0t2H5k9aVfcHxa7068x0Zmyud68zc+EhWNpXmz2BjTceBhjjX1PLNmMRAqD/AFRHv3XutNzYe+q/r/d+wu0sOCcnsPdW1d/UKjktJt7K0WZnpyOL/cUcEsLLxcOQeD7917rej29ncZunAYPc2EqVrcNuLD43O4isS2irxmXoochQVKWJGmekqEYc/Q+/de6d/fuvda2P/Cpj/t3f1f8A+LidU/8AvuO6PfuvdaDvv3Xuve/de6t//lQ/zd+1P5ae867beSxWV7W+LO/M2uW7D6kp8hDT57amdqVhpq7svqOpyUseMo9zvSwp/EsRUPBQZ5IVDTUtSI6pfde62kt1dOfyP/54cdD2Tht2bSj72y1JjKTKZzYO7YukflDRSUtDAg2/vvZOYhWo3dPiY3ipFqsjh8zTqtOkdFVtTqur3XuhU+EH8jzqP+Xl3pN35078qfknFRNiqvFbu2FubMdeLsLfe1kpq37TH9lQ0my6D+Njbc1SaugrojQ1VDOrFJVjkljf3Xull8xvix0h86qHe3bHxO7G6r3j8g+sKuLa2+6XYm99rZ/C71q8Zj4amm2B2LV4HI11NtfsGgxboMTkazRLElqSrBpSklJ7r3WvXhM3vzqjflHnsFWbi637Q633FVRwVD00uL3Rs3dGMdqPK4jLYysQWYAvS5HHVKPTVlM7RSq8Tgn3XuriN/df/G3+ex8cX6m7bixnUnzK6kxVXmdk74w1JFU5TbOTqI4aSbeG0IqqaCp3f0/u6qjgh3Bt6eYSUshRGeOojoa9vde60YvlR8Zu6vh/2xvnoL5AbQk2h2JtSmqpx4Xmq9tbw21UCqhw+/dg5qWGnXcWytwCFjBUBVmp5lkpaqOGqhliX3XuvqF/AH/shD4Uf+KkfG//AN85s337r3Rt/fuvdEZ/mQdqf6J/h329X0tUtLnN64iLq/bYP65sn2BMMDViIggrLS7enrqkEXK+C/49+691qx/H/rebtbvHpnq6ghLQ7s7G2njayNYzKIdu4vIRZ3cUzxpYmCl25hqlnItpUX9+691vBe/de6//09/j37r3Wlx8v+lZPj78lO2eso6NqXb8O4Zt3bG1JIYZ9i73lqM7hI4JZUVakYeplqsbIVuokoiPfuvdX4/yku/4ez/jweqMvWeTefQtXDtho55mkq8jsDKmpr9iZfVI5Mq0lKlRin0DSjY5dVi4v7r3Vq3v3Xuqm/5yPwC7R/mO/FraHRfUm9tgbC3NtzvbZfalTmOx49xS4Gowu2tq7+wFZjYBtihyORXKT1O7YJIi0fi0ROGYEr7917rWk/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvde/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvde/6BWPm5/wB5I/FT/wA5O3v/ALGPfuvdYJf+Ep3zUnkSaX5F/FBp4reGo+y7eWpgsbgwVK7YWeAgj+ww9+6908Vn/CXH595GjGOyPy2+P+RxwVVGOyGf+QNdjwqjSq/Y1eMmpNCrwBosB7917o0vwf8A5Fv80v8Al+d2YvuzoH5R/EmlneODEb+68ydN3LDsLtbZ4qfNNtreGOotrgrUUjSPNjMnAv3uMqmLRlo3mik917q7n+YD/L3ofkpiJu2uraDFbe+ReIxdMlZTNUx0mC7XxWPphHFtXcdfIlNBS7jx0K+PDZuRUEYApasGkZHpPde61ocLmt9dV77pM3g6zcfXHZ/W25KmOGd6aTFbp2ZunFu9Hk8XlcZWJ6XAL01fQVKPT1lM7RyK8Tgn3XuriuwNjfG7+e18ZK/pPuOnxPVfzD6vwWUy+wd74enSTKbUztZSR49t87IWqkSr3V1Luqqip4NzbcmlLQ3SN3jqY8dkR7r3V2fxj6xznSXxs+PfTG58jicvuTqPo/qfrDcGWwC1iYLKZvYOw8BtTK5HDJkY4sgmKrq7EyS04nVZhCyhwGv7917ocffuvda2X84n5AQb27Z2l0Dt+t8+G6ipG3NvVqeZjTz9gbroIlxGJnQO0E0u2dpSmYkDVHJlyp9S8e691j/k2dKybs7r3v3nkqRzheptuvtDbVU6yJHNvrfVOr5d6dyniqDhdmRGKVQbocqv9ffuvdbKPv3Xuv/U3+PfuvdU7fzfPjZN2B1ViPkFtXGS1m7elaesh3fDRQPPWZTqbIyipzFQY4w8s52PlFXJqFX0Ub1rE/Qe/de6o7+J/wAi8x8Wu8drdt46OpyO3ljfbnY2BoT5JdydeZeeCTKJSxK8a1WW2/PFHlMcpdVeopvETpma/uvdbmG19z7f3rtvA7w2plqPPbZ3PiMfnsBmsfJ5qHK4jK0sVbj6+lkspaGppplYXAYXsQDce/de6fffuvde9+691737r3Xvfuvde9+691737r3Xvfuvde9+691Vx/MG/l7Yv5KYyp7T6ppcXgPkFhKBI5BK8WOwvbeGx8JFPtjdFSQsFFuWjhXx4fMyf5jilqy1IUel917rWXxGX3z1ZvulzGGq9x9cdodbbkqEhnankxO6tl7qxTvSZLGZLHViemRQWp66hqEemrKZ2SRZIZAT7r3W1F8EPnjtj5YbabbG5xjNqd87Wxsc+7NoQStFjtzY6Ix07b52KKqR6ir2/U1DAVVKWkqcTUOIZiyPBPN7r3Q4/Lr5L7a+KnS24ezMysGR3BJbAdebVd2E2799ZKGb+C4gLG8cqY6n8T1eQmDL9vj6eZwdYRW917rTfr67eG/921mTrjkN7djdjbtepqRSwvNld4b83llwqQUdNEpc1GXzFasUEai0UWlf0px7r3W5B8Pfj3Q/GToDY/Vqfbz7hp6WTcG/srTgFMzv7cGit3LWq6m0tNS1JWipWsP8ipYQRcH37r3RnPfuvdf/1d/j37r3UerpKWvpamhrqanraGtp5qSso6uGOppaulqY2hqKapp5leGennhco6OCrKSCCD7917rUQ+efxCyXxM7cenwdJUS9Ldg1ddk+q8veSaPCzASVuW60ydTISyZHbSkvjS7MazE6CGaWnnC+690Pn8tv57Q/HjJU/SPb2UmXpDcmXkl2vuSrcyw9Rbky9Q0tVFXO13p+utx5CYySsLx4ivlaYhaeeZovde62doJ4amGGpppoqinqIo54J4JElhnhlQSRTQyxlklilRgyspIYG49+691l9+691737r3Xvfuvde9+691737r3Xvfuvde9+691737r3VXn8wX+Xxi/kxjJ+0urYMdt/5BYHHJETK0OPwvbWGx8JWl2ruupIWKj3DRwL48PmX5p+KWqLUZVqb3XutZLF5XfXV2+KXLYmp3L1t2f1xuSdYZpKaTE7q2VuvESvS11BkcdWIQssZ1QVlHUJJTVlM5RxJDICfde6GD5J/J/tX5V7xwe7+z6ygi/uxt+nwG29sYFail2vhDJFTtuLOUdHVSzSLmt2V9Os1XNI7tFBHFTIxhhBb3XurRv5Svw3nylfRfLXsnFaMVRpVU/RGFyEPqr5qmGSiyvalTTSrYU4geSiwJa5ZHqKwKoalkPuvdbBXv3Xuve/de6//9bf49+691737r3QT93dKdffITrbcXVfZmHGW2zuGBbSwuKfLYLLU15MVuTbuR0PJitwYSrtNTTqCAwKSK8TyRv7r3Woj8pfit2X8Tewn2Rv2D+MbazL1cnXfZFJRmDb/YOGjUvJC8frhw+8cZTsFymIdi0Z/epzNSSRyn3XujU/B7+ZFu74zw4/rLs+mzXYfRcbR0+H+1kNdvbquFnUaNurVSB9z7LhTUTiXkWqowQaN2jX7Vvde62Yetuz+vu4do4zfnWG7sJvbaOXTVRZvBVa1NP5AkcktHWQsEq8bk6VZVE9JUxxVMDHTJGrce/de6Xnv3Xuve/de697917r3v3Xuve/de697917r3v3XuiyfJf5cdMfFXbAzXZW4PLuDIU8km1OvcF4chvnd86mRFXE4czRfb45JImE2Qq3p6CnCkPKH0o3uvdan/yZ+Qm4vlD29m+3Ny7X2zs6qyFJS4bG4LbVPEZKXb+KaVcOu6NwCGnqN47np6RxHNkZUjRY1WCnSOnjjQe690b7+X5/L6y/yZyuN7W7VxtXiPjtia1KiipJ/NR5Duyuo5rvicSVMdRTdb0s8WjI5JCjZJg1JRsV+4ni917raToaGixlFR43G0dLjsdjqWnocfj6GnipKKhoqSJKelo6Olp0jgpqWmgjVI40VURFAAAAHv3XupXv3Xuve/de6//X3+Pfuvde9+691737r3QbdtdRddd5bGy/XHaW18fuzaWZVDUY+uV0mpKyEN9nlsRXwNFXYfNY93LU9XTSRTwsTpYAkH3XutZX5dfy0e3vjlNld4dfQ5ruPpeKaoqUy+No2r+xNjYxEMwj3xgMbTK+dx1FCGDZnGxMCsZkqqan1Bm917oknUHd3afR+4/77dL9g5fZmWqmT+INiJ4K7bu5Eguq0+6dtVi1OB3FFFcgeeL7iE/okjYce691dX0b/Ojxz09Lh/kf1jXY2sQCJ999UI+axFSdUUaTZLZGVq489i3K6pJmo6rIp+EQWAPuvdWl9afMn4t9vRRNsPvLr7JVUoj04fJZyHbG4Q8igiJtvbnGHzXkUnSwEB0tweffuvdGUilinjjmhkjmhlRZIpYnWSOSNwGR43QlXRlNwQSCPfuvdZPfuvdNmXzeG2/Rvkc9l8ZhMfGbSV+Xr6XG0cZ0s1nqqyWGBDpUnlvoD7917omXan8xn4fdTx1sWS7gwu8c3RqpG2usUl7AzFS7X/ail2+KnCU7pb1GprIEQ8MwPHv3Xuqke/v5xXbW9oa3b/QO0KTqHCTGaD++26Hx+6+wamn8jKlRisKkc+0dsSVNO1j52yssTC66T+n3Xuqk6yu3d2DvB6vI1u7exuxt7ZFIvNVSZbeW/N35SaTTBS08aity+Ul1sBHDEgggH6VjT6e691dh8Nv5SuRydRiOy/lrSJR4pEpsjhOiKStSeetm1+WCbtfK0LtTmmSMBzgaKV43ZgKydtD0p917rYAoaGixlFR43G0dLjsdjqWnocfj6GnipKKhoqSJKelo6Olp0jgpqWmgjVI40VURFAAAAHv3XupXv3Xuve/de697917r/9Df49+691737r3Xvfuvde9+691737r3VefyP/lofG/5B1OQ3LS4ip6l7HrjLNPvbrmKix8WVrZBxU7p2nNC+29xOXJZ5jDBWyMbmo+nv3XuqZe4/wCU78q+tJqus2PQ7e7x21CJJIazaNfBt3dyU6SeOMVmzdy1UUU9ZKtmKUGRrLXNhYe/de6rx3111vXYda+K7M673hsyugYB6be+z8vhfE36wUqcnQpRsGHqDxysrL6gbe/de6acLvDNYNl/u1vjcmEaIgp/d3e+fxYhb6jQmKzFPHESB9ABwPfuvdLKXujuGeIw1Hdvb89Ow0mGftffTwstrAFG3BpYW9+690HWUzC5uU/x3PVWdmJH7ed3BXZ2VmY3XTDla+tcsxPGkXJP59+690LnXPx/7y7WMcPV/THYu7actGq1uK2lXY/b8AmNkknz+XjxWAp4X0mzGfmxt9PfuvdWadLfya+5t1vT5LvPfGA6nxBcNNtrZ702+t6zIjpqgmy0iQbPw/3EbNaSI5QoV/Tzce691dj8evh90B8YqAxdWbHpqbcFTTiny2/dwSHcG/c2n9ta7ctahqKeml41UtGtLRXAIhB59+690Zz37r3Xvfuvde9+691737r3Xvfuvdf/0d/j37r3Xvfuvde9+691737r3Xvfuvde9+691737r3UHJfw37Go/jH2P8N8Z+7/iX2/2Pi/tfcfdfseP+urj37r3RKd/f8Nx+ST/AEh/7J9955m8v8Y/0SfxTz3GvyW/3I67/W/v3Xugkh/4aK8w8X+yZeW5tf8A0eaf8beT9u1v94/w9+690ZrrL/ZLPuKP/Q//ALLJ99pH2X+j/wD0X/xO3NvF/Af8u1fX/H37r3RpBawta1ha30t+LW4tb37r3Xfv3Xuve/de697917r3v3Xuve/de697917r3v3Xuv/Z";
                if (records.length > 0) {
                    // >>>> records async
                    async.forEachSeries(records, function(data, next) {
                        count= count+1;
                        var contentList = [];
                        var selectedFormRecordFields = {};
                        var recordInformation = data.record[0];
                        var key = Object.keys(data.record[0]);
                        async.forEachSeries(key, function(singlekey, nextIteration) {
                            selectedFormRecordFields[singlekey] = data.record[0][singlekey];
                            var date = data.updatedTime;
                            date = date.toUTCString();
                            tableHeaderBlock = {
                                style: 'tableExample',
                                table: {
                                    color: '#444',
                                    widths: [100, '*'],
                                    body: [
                                        [{
                                                rowSpan: 2,
                                                image:orgDetails.imageurl,
                                                width: 100
                                            },
                                            {
                                                text: '' + post1[0].name + '',
                                                style: 'subheader',
                                                alignment: 'center'
                                            }
                                        ],
                                        [{
                                                text: ''
                                            },
                                            {
                                                columnGap: 20,
                                                alignment: 'justify',
                                                columns: [{
                                                    type: 'none',
                                                    style: 'List',
                                                    width: 'auto',
                                                    ul: [
                                                        'Submitted on :' + date + ''
                                                    ]
                                                }, {
                                                    type: 'none',
                                                    style: 'List',
                                                    width: 'auto',
                                                    ul: [
                                                        'Submitted By:' + data.updatedBy + ''
                                                    ]
                                                }]
                                            }
                                        ]
                                    ]
                                }
                            }
                            nextIteration();
                        }, function(err) {
                            // >>>> Loop FormSkeleton
                            post1[0].FormSkeleton.forEach(function(recorddata) {
                                var LabelKey = recorddata.lable;
                                var widgetSelected = recorddata.type.view;
                                var LabelValue = selectedFormRecordFields[recorddata.id];
                                var fieldStructure = {
                                    style: 'tableExample',
                                    color: '#444',
                                    table: {
                                        widths: [200, '*'],
                                        body: []
                                    }
                                }
                                // ============= Normal Fields =============
                                if (widgetSelected == "textbox" || widgetSelected == "textarea" || widgetSelected == "checkbox" || widgetSelected == "radio" || widgetSelected == "rating" || widgetSelected == "map" || widgetSelected == "barcode" || widgetSelected == "mapInteractionField" || widgetSelected == "goto") {
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, 'N/A']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)
                                }
                                if(widgetSelected == "select"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, '']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)

                                }
                                if(widgetSelected == "calculation"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, '0']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, LabelValue]);
                                    }
                                    contentList.push(fieldStructure)

                                }
                                if(widgetSelected == "calender"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey, 'N/A']);
                                    } else {
                                        fieldStructure.table.body.push([LabelKey, dateFormat(LabelValue, "yyyy-mm-dd h:MM:ss")]);
                                    }
                                    contentList.push(fieldStructure)

                                }
                                if(widgetSelected == "camera" || widgetSelected == "sign"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    else{
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+LabelValue+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    contentList.push(fieldStructure);
                                }
                                if(widgetSelected == "video"){
                                    if (LabelValue == undefined || LabelValue == "") {
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    else{
                                        fieldStructure.table.body.push([LabelKey , {
                                            image: ''+nopreview+'',
                                            width: 100,
                                            height: 100
                                        }]);
                                    }
                                    contentList.push(fieldStructure);
                                }
                                // ============= END Normal Fields =============

                                // ============= Group BLOCK ================//
                                if (recorddata.type.view == "group") {
                                    console.log("In Group Block")
                                    var group_Header =[];
                                    var group_Body = [
                                        [
                                            {
                                                table: {
                                                    widths: [200, '*'],
                                                    headerRows: 1,
                                                    pageBreak: 'before',
                                                    body:[]
                                                }
                                            }
                                        ]
                                    ];
                                    var groupFieldArray = [];
                                    group_Header.push({ text: ''+LabelKey+'', fontSize: 8,alignment: 'left',fillColor: '#dddddd',color: '#2B2C2C',colSpan:2},{});
                                    groupFieldArray.push(group_Header);
                                    var groupData = recorddata.type.fields;
                                    groupData.forEach(function(group) {
                                        var groupLable = group.lable;
                                        var groupValue = selectedFormRecordFields[group.id];
                                        var groupWidgetType = group.type.view;
                                        if(groupWidgetType == "textbox" || groupWidgetType == "textarea" || groupWidgetType == "checkbox" || groupWidgetType == "radio" || groupWidgetType == "rating" || groupWidgetType == "map" || groupWidgetType == "barcode" || groupWidgetType == "mapInteractionField" || groupWidgetType == "goto"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'N/A',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "select"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "calculation"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'0',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+groupValue+'',fontSize: 8}]);
                                            }
                                        }
                                        if (groupWidgetType =="calender") {
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:'N/A',fontSize: 8}]);
                                            }
                                            else{
                                                groupFieldArray.push([{text: ''+groupLable+'',fontSize: 8} , {text:''+dateFormat(groupValues, "yyyy-mm-dd h:MM:ss")+'',fontSize: 8}]);
                                            }
                                        }
                                        if(groupWidgetType == "camera" || groupWidgetType == "sign"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                            else{
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+groupValue+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                        }
                                        if(groupWidgetType == "video"){
                                            if (groupValue == undefined || groupValue == "") {
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                            else{
                                                groupFieldArray.push([{text:''+groupLable+'',fontSize: 8} , {
                                                    image: ''+nopreview+'',
                                                    width: 100,
                                                    height: 100
                                                }]);
                                            }
                                        }
                                        group_Body[0][0].table.body = groupFieldArray;
                                    });
                                    contentList.push(group_Body);
                                }
                                // ================= END Group BLOCK=================== //

                                //  *****************SECTION BLOCK********************* //
                                if (recorddata.type.view == "section") {
                                    // console.log("In Section Block");
                                    // [Section Case:1] Inititalise sectionHeader Array
                                    var section_Header =[];
                                    // Section Main Layout
                                    var sectionMainLayout = {
                                        style: 'tableExample',
                                        color: '#444',
                                        table: {
                                            widths:'*',
                                            body: [
                                                // Push Header in Array[0]
                                                // Array-2 Push an empty Array.
                                                //In Array2 Generate new Array
                                            ]
                                        }
                                    }
                                    // Inititalize section Body array
                                    section_Header.push({ text: ''+LabelKey+'', fontSize: 8,alignment: 'left',fillColor: '#66A5F0',color: '#2B2C2C'});
                                    sectionMainLayout.table.body.push(section_Header);
                                    console.log("printing section Header");
                                    var sectionData = recorddata.type.fields;
                                    // [Section Case:2] Inititalize section Body array
                                    var sectionMainArr = [];
                                    var sectionBody = [];
                                    // NOTE : SectionBody will have section related individual fields and groups
                                    sectionMainArr.push(sectionBody);
                                    // Push section body inside sectionMainArr 
                                    // and push the sectionMainArr in sectionMainLayout
                                    sectionMainLayout.table.body.push(sectionMainArr);
                                    var sectionData = recorddata.type.fields;
                                    sectionData.forEach(function(section){
                                /*
                                Modified by santhosh on 10th may 2018, througing error when section.data is getting undefined 
                                */
                                if(section.data != undefined){
                                        var sectionFieldLabel = section.data.lable;
                                        var sectionFieldValue = selectedFormRecordFields[section.data.id];
                                        var sectionFieldWidget = section.data.type.view;
                                        // >>>> Structure for Section Fields >>>>>
                                        var sectionFieldTable = {
                                            style: 'tableExample',
                                            color: '#444',
                                            table: {
                                                widths: [200, '*'],
                                                body: []
                                            }
                                        }
                                        // =========== Structure for Section Fields ================
                                        if (section.type == "field") {
                                            if(sectionFieldWidget == "textbox" || sectionFieldWidget == "textarea" || sectionFieldWidget == "checkbox" || sectionFieldWidget == "radio" || sectionFieldWidget == "rating" || sectionFieldWidget == "map" || sectionFieldWidget == "barcode" || sectionFieldWidget == "mapInteractionField" || sectionFieldWidget == "goto")
                                            {    
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: 'N/A',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "select"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: '',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "calculation"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: '0',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+sectionFieldValue+'',fontSize: 8}]);
                                                }
                                            }
                                            if (sectionFieldWidget =="calender") {
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: 'N/A',fontSize: 8}]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8},{text: ''+dateFormat(sectionFieldValue, "yyyy-mm-dd h:MM:ss")+'',fontSize: 8}]);
                                                }
                                            }
                                            if(sectionFieldWidget == "camera" || sectionFieldWidget == "sign"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+sectionFieldValue+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                            }
                                            if(sectionFieldWidget == "video"){
                                                if (sectionFieldValue == undefined || sectionFieldValue == "") {
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                                else{
                                                    sectionFieldTable.table.body.push([{text:''+sectionFieldLabel+'',fontSize: 8} , {
                                                        image: ''+nopreview+'',
                                                        width: 100,
                                                        height: 100
                                                    }]);
                                                }
                                            }
                                            sectionBody.push(sectionFieldTable);
                                        }
                                        // ========== END Structure for Section Fields ===========
                                        // #########################################################//
                                        if (section.type == "group") {
                                            var sectionGroupLayout = {
                                                style: 'tableExample',
                                                color: '#444',
                                                table: {
                                                    widths:'*',
                                                    body: []
                                                }
                                            }

                                            var SectiongroupHeader =[];
                                            var sectionGroupFieldTable = {
                                                style: 'tableExample',
                                                color: '#444',
                                                table: {
                                                    widths: [200, '*'],
                                                    body: []
                                                },
                                                layout: {
                                                    hLineWidth: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? 1 : 1;
                                                    },
                                                    vLineWidth: function (i, node) {
                                                        return (i === 0 || i === node.table.widths.length) ? 1 : 1;
                                                    },
                                                    hLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.body.length) ? '#81868C' : '#A0A0A0';
                                                    },
                                                    vLineColor: function (i, node) {
                                                        return (i === 0 || i === node.table.widths.length) ? '#81868C' : '#A0A0A0';
                                                    }
                                                }
                                            }
                                            SectiongroupHeader.push({ text: ''+section.data.lable+'', style: 'sectionHeader',alignment: 'left',fillColor: '#dddddd',color: '#2B2C2C'});
                                            sectionGroupLayout.table.body.push(SectiongroupHeader);
                                            var sectionGroupArr = [];
                                            var sectionGroupBody = [];
                                            sectionGroupArr.push(sectionGroupBody);
                                            // Push section body inside sectionGroupArr 
                                            // and push the sectionGroupArr in sectionGroupLayout
                                            sectionGroupLayout.table.body.push(sectionGroupArr);
                                            var sectionGroupData = section.data.type.fields;
                                            // >>>> ++ Loop section Group individual Fields ++
                                            sectionGroupData.forEach(function(sectionGroup){
                                                // console.log("Inside section Group Loop")
                                                var sectionGroupWidgetType = sectionGroup.type.view;
                                                var sectionGroupFieldLabel = sectionGroup.lable;
                                                var sectionGroupFieldValue = selectedFormRecordFields[sectionGroup.id];
                                                if(sectionGroupWidgetType == "textbox" || sectionGroupWidgetType == "textarea" || sectionGroupWidgetType == "checkbox" || sectionGroupWidgetType == "radio" || sectionGroupWidgetType == "rating" || sectionGroupWidgetType == "map" || sectionGroupWidgetType == "barcode" || sectionGroupWidgetType == "mapInteractionField" || sectionGroupWidgetType == "goto"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'N/A'}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "select"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "calculation"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'0'}]);                                        
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+sectionGroupFieldValue+''}]);
                                                    }
                                                }
                                                if (sectionGroupWidgetType =="calender") {
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                       sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:'N/A'}]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {text:''+dateFormat(sectionGroupFieldValue, "yyyy-mm-dd h:MM:ss")+''}]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "camera" || sectionGroupWidgetType == "sign"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+sectionGroupFieldValue+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                }
                                                if(sectionGroupWidgetType == "video"){
                                                    if (sectionGroupFieldValue == undefined || sectionGroupFieldValue == "") {
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                    else{
                                                        sectionGroupFieldTable.table.body.push([{text:''+sectionGroupFieldLabel+''} , {
                                                            image: ''+nopreview+'',
                                                            width: 100,
                                                            height: 100
                                                        }]);
                                                    }
                                                }
                                               
                                            });
                                            sectionGroupBody.push(sectionGroupFieldTable)
                                            sectionBody.push(sectionGroupLayout);
                                        }
                                        // >>> End Section Group
                                    }
                                    })
                                    contentList.push(sectionMainLayout)
                                }
                                //  ***************** END SECTION BLOCK********************* //
                            });
                            // <<<<==== End FormSkeleton loop
                            console.log("contentList Pushing completed!");
                        })
                        // # Finally push all build structure to finalArray and send it to PDFLayout
                        finalArray.push(
                            [tableHeaderBlock], 
                            [contentList]
                        );
                        next();
                    }, function(err) {
                        // ===> FINAL PDF LAYOUT STARTS ====>
                        var finalLayout = {
                            footer: function(currentPage, pageCount) {
                                return [{
                                    text: +currentPage.toString() + ' / ' + pageCount,
                                    style: 'footer'
                                }];
                            },
                            pageMargins: [40, 50, 40, 50],
                            info: pdfinfo,
                            content: finalArray,
                            styles,
                            defaultStyle
                        }
                        // console.log(JSON.stringify(finalLayout));
                        var pdfDoc = printer.createPdfKitDocument(finalLayout);
                        var altEmail = '' +post1[0].altemail+'';
                        var fileName = '' + post1[0].name+ '';
                        var output_Filepath = "./"+ mongoid +".pdf";
                        var stream = pdfDoc.pipe(fs.createWriteStream("./"+ mongoid +".pdf"));
                        pdfDoc.end();
                        stream.on('finish', function() {
                            console.log('congratulations, your pdf created,sending attachemnt please check your mail!');
                            sendAutomailwithAttachment(req.body.sendTo,"<br>"+req.body.body+"", "RECORD INFO", output_Filepath,altEmail, function(mailresponse){   
                                if(mailresponse==true) {
                                    // >>> Remove the PDF file from filestorage after email is sent!
                                    fs.unlink("./"+ mongoid +".pdf",function(err){
                                        if(err) return console.log(err);
                                    }); 
                                    res.json({
                                        "message" : "Mail sent successfully",
                                        "status" : 200
                                    });
                                }
                                else {
                                    res.json({"message":"Failed to send mail","status":500});
                                }
                            });
                        });
                        // <<=== END FINAL PDF LAYOUT <<===
                        // >>>>>> END
                    });
                    // Function Err end!
                }
                // End records.length
            });
        }
        // ==> post1.length End 
    });
});


/*function latlongjson(formId, taskId, user, callback) {

    Formsz.find({_id : formId,isVisible : true}, function (err, formsz) {
        var latlongData = [];
        var latlongDataDetails = {};
        var task = [];
        var locationDetails = {};
        if (formsz.length > 0) {
            var mapFields = [];
            latlongDataDetails.formId = formsz[0]._id;
            mapFields = formsz[0].geoFields;
            FormszDetails.find({formId : formId,taskId : taskId,DSAssignedTo : user}, function (err, details) {
                var taskdata = {};
                var locations = [];
                if (details.length > 0) {
                    async.forEachSeries(details, function (recorddata, next) {
                        taskdata.recordId = recorddata._id;
                       // taskdata.status = recorddata.status;

                        for (var i = 0; i < mapFields.length; i++) {
                            var values = recorddata.record[0][mapFields[i]];
                            if (values == "") {
                                locationDetails.lat = "";
                                locationDetails.long = "";
                                locations.push(locationDetails);
                                locationDetails = {};
                                taskdata.geometries = locations;
                            } else {
                                var res = values.split(",");
                                locationDetails.lat = res[0];
                                locationDetails.long = res[1];
                                locations.push(locationDetails);
                                locationDetails = {};
                                taskdata.geometries = locations;
                            }
                        }
                        setTimeout(function () {}, 1000);
                        task.push(taskdata);
                        taskdata = {};
                        locations = [];
                        next();
                    })
                }

            })
            latlongDataDetails.records = task;
            latlongData.push(latlongDataDetails);
            callback(latlongData);

        }
    })
}*/


//download Service for prepopulatedData
router.get('/downloadService/:taskId/:formId/:user', function (req, res, next) {
    var data = [];
        Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                downloadjson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data.push(ress);
                });
                setTimeout(function () {
                    res.json(data)
                }, 1000);

        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    });
});

router.post('/downloadService', function (req, res, next) {
  //  /:taskId/:formId/:user
    var data = [];
        Tasks.find({_id : req.body.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                downloadjson(req.body.formId, req.body.taskId, req.body.user, function (ress) {
                    data.push(ress);
                });
                //Activity log for form Download
               activityLog.activityLogFortaskOrFormDownload(req.body,"Form Downloaded");


                setTimeout(function () {
                    res.json(data)
                }, 1000);

        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    });
});

function downloadjson(id,taskid, user, callback) {
    var reqfields = {};
    var taskdata = {};
    var DisplayValues = [];
    var DisplayValuesData = {};
    var DownloadRecordsData = {};
    var DownloadRecords = [];
    var AllFieldsData = {};
    var AllFields = [];
    var i = 0;
    var DownloadedFormsz = [];
    var values = {};
    var DownloadedFormszData = {};
    Formsz.find({_id : id}, function (err, formsz) {
        if (formsz.length > 0) {
            if (formsz[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = formsz[0]._id;
                taskdata.FormName = formsz[0].name;
                taskdata.isMediaAvailable = formsz[0].isMediaAvailable;

                callback(taskdata);
            } else {
                reqfields = Object.keys(formsz[0].requiredField[0]);
                taskdata.formId = formsz[0]._id;
                taskdata.FormName = formsz[0].name;
                taskdata.isMediaAvailable = formsz[0].isMediaAvailable;
                //FormszDetails.find({formId : id,taskId : taskid,DSAssignedTo : user}, function (err, formszDetails) {
                    FormszDetails.find({
                            $or: [{
                                formId: id,
                                taskId: taskid,
                                DSAssignedTo: user
                            }, {
                                formId: id,
                                taskId:taskid,
                                updatedBy: user,
                                IsReassign: true
                            }]    }, function(err, formszDetails) {

                    if (formszDetails.length > 0) {
                        formszDetails.forEach(function (recorddata) {
                            values = Object.keys(recorddata.record[0]);
                            DownloadRecordsData.recordId = recorddata._id,
                            DownloadRecordsData.IsReassign = recorddata.IsReassign,
                            DownloadRecordsData.comments = recorddata.comments
                                for (var i = 0; i < reqfields.length; i++) {
                                    DisplayValuesData.fieldId = reqfields[i];
                                    DisplayValuesData.fieldIdName = recorddata.record[0][reqfields[i]];
                                    DisplayValues.push(DisplayValuesData);
                                    DisplayValuesData = {};
                                }
                                setTimeout(function () {}, 1000);
                            DownloadRecordsData.DisplayValues = DisplayValues;
                            DisplayValues = [];

                            for (var i = 0; i < values.length; i++) {
                                //AllFieldsData.fieldId = values[i];
                                //AllFieldsData.fieldIdName = recorddata.record[0][values[i]];
                                 AllFieldsData[values[i]] = recorddata.record[0][values[i]];
                                AllFields.push(AllFieldsData);
                                AllFieldsData = {};
                            }
                            setTimeout(function () {}, 1000);
                            DownloadRecordsData.AllFields = AllFields;
                            AllFields = [];
                            DownloadRecords.push(DownloadRecordsData);
                            taskdata.DownloadRecords = DownloadRecords;
                            DownloadRecordsData = {}
                        });
                        callback(taskdata);
                    } else {
                        taskdata.DownloadRecords = DownloadRecords;
                        callback(taskdata);
                    }
                });
            }
        }
    });
}

router.get('/getRe-assginedRecods/:formId/:user', function (req, res, next) {
 var allrecords = [];
 var data = [];
 var resultJson = {};
 FormszDetails.find({updatedBy : req.params.user,IsReassign:true,formId:req.params.formId}, function (err, post) {
  if (post.length > 0) {
   post.forEach(function (displayobject) {
    DisplayValues.find({recordId : displayobject._id}, function (err, post1) {
     Formsz.find({  _id : post1[0].formId,  isVisible : true}, function (err, formobj) {
      Tasks.find({"assignedFormsz.formId" : formobj[0]._id,isVisible : true}, function (err, task) {
       resultJson.displayValues = post1[0].displayValues
        resultJson.type = task.length > 0 ? "Task" : "form"
        resultJson.startDate = task.length > 0 ? task[0].startDate : ""
        resultJson.endDate = task.length > 0 ? task[0].endDate : ""
        resultJson.description = task.length > 0 ? task[0].description : formobj[0].description
        resultJson.formId = formobj[0]._id
        resultJson.formName = formobj[0].name
                resultJson.FormSkeleton = formobj[0].FormSkeleton
                resultJson.requiredField = formobj[0].requiredField
                resultJson.recordId=    displayobject._id
        resultJson.taskId = task.length > 0 ? task[0]._id : ""
        resultJson.taskName = task.length > 0 ? task[0].name : ""
        resultJson.comments = displayobject.comments
        data.push(resultJson);
       resultJson = {};
      });
     });
    });
   });
   setTimeout(function () {
    res.json(data)
   }, 1000);
  } else {
   res.json({
    "message" : "No data found",
    "status" : 204
   })
  }
 });
});

router.get('/getReForms/:user', function (req, res, next) {
    FormszDetails.distinct("formId", {IsReassign : true,updatedBy : req.params.user,status : true}, function (err, post1) {
        if (post1.length > 0) {
            getFormname(post1, function (ress) {
                res.json(ress);
            });
        } else {
            res.json({
                message : "No data found",
                status : 204
            });
        }
    });
});

/* GET /get user records based on FormId */
router.get('/:formid/:user/:taskId', function(req, res, next) {
    FormszDetails.find({formId: req.params.formid,updatedBy: req.params.user,taskId: req.params.taskId,status:true}).sort({updatedTime:1}).exec(
    function(err, records) {
        if (records.length > 0) {
            var records1 = [];
            async.forEachSeries(records, function(info, next) {
                    var recorddata = {};
                    DisplayValues.find({
                        recordId: info._id
                    }, function(err, displayFields) {
                        if (displayFields.length > 0) {
                            for (var i = 0; i < displayFields.length; i++) {
                                recorddata.recordId = displayFields[i].recordId;
                                recorddata.displayFields = displayFields[i].displayValues;
                                recorddata.createdTime = displayFields[i].createdTime;
                                records1.push(recorddata);

                                next();
                               
                                
                            }
                        }
                        else {
                             next();        
                                }
                    })
                },
                function(err) {
                    res.json({
                        data: records1,
                        status: 200
                    });
                })

        }
        else {
            res.json({
                "message": "No data found",
                "status": "204"
            });
        }
    }) 
    
});

// get prePOPRecords for user
router.get('/getprePOPRecords/:user/:formId/:recordId', function(req, res, next) {
   // FormszDetails.find({DSAssignedTo: req.params.user,formId: req.params.formId,_id: req.params.recordId,isDeleted: false}, function(err, userdetails) {
        FormszDetails.find({
        $or: [{
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            DSAssignedTo: req.params.user
        }, {
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            updatedBy: req.params.user,
            IsReassign: true
        }]
    }, function(err, userdetails) {
        var data = [];
        if (userdetails.length > 0) {
            async.forEach(userdetails, function(item) {
                data.push({
                    prepopulatedData: item.record,
                    taskId: item.taskId
                });
            });
            res.json(data);
        } else {
            res.json({
                "message": "No data found",
                "status": "204"
            });
        }
    });
});
router.get('/getPrePOPData/:formid/:user/:taskId', function (req, res, next) {

    Formsz.find({_id : req.params.formid,isVisible : true}, function (err, post) {
        if (post.length > 0) {
            FormszDetails.find({formId : req.params.formid,DSAssignedTo : req.params.user,updatedBy : null,taskId : req.params.taskId,isDeleted : false}, function (err, post1) {
                if (post1.length > 0) {
                    var userdata = [];
                    post1.forEach(function (dbUserObj1) {
                        userdata.push({
                            record : dbUserObj1.record[0],
                            recordId : dbUserObj1._id
                        });
                    });
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : userdata,
                        DisplayFields : post[0].requiredField
                    });
                } else {
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : [],
                        DisplayFields : post[0].requiredField
                    });
                }
            });

        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            })
        }
    });
});

//Get Formsz Details from Date and Todate
router.get('/getformszDetail/:formid/:user/:fromdate/:todate/:taskid', function (req, res, next) {
    var updatedby = req.params.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
    Formsz.find({
        _id : req.params.formid,
        isVisible : true
    }, function (err, post) {
        if (post.length > 0) {
       //  FormszDetails.find([{$or:[{formId : req.params.formid,isDeleted : false,updatedTime : {$gte : new Date(req.params.fromdate),$lte : new Date(new Date(req.params.todate).setDate(new Date(req.params.todate).getDate() + 1))},updatedBy : updatedby,taskId : req.params.taskid}]}, function (err, post1) {

            FormszDetails.find({formId : req.params.formid,isDeleted : false,updatedTime : {$gte : new Date(req.params.fromdate),$lte : new Date(new Date(req.params.todate).setDate(new Date(req.params.todate).getDate() + 1))},updatedBy : updatedby,taskId : req.params.taskid}, function (err, post1) {
                if (post1.length > 0) {
                    var userdata = [];
                    var recordIds = [];
                    var formInfo = {};
                    var comment = [];
                    var recordinfo = [];
                    var reassinegdReocrdIds = [];
                    formInfo.formId = post[0]._id;
                    formInfo.createdBy = post[0].createdBy;
                    formInfo.createdTime = post[0].createdTime;
                    formInfo.description = post[0].description;
                    formInfo.version = post1[0].version;
                    post1.forEach(function (dbUserObj1) {
                        var lat = "";
                        var long ="";
                         lat = dbUserObj1.lat;
                         long = dbUserObj1.long;
                          if(lat==null && long==null) {
                            InsertedLocation = "";  
                        }else{
                            InsertedLocation = lat + "," + long 
                        }
                        if (dbUserObj1.IsReassign) {
                            reassinegdReocrdIds.push(dbUserObj1._id);
                        }
                        userdata.push(dbUserObj1.record[0]);
                        dbUserObj1.record[0]._id = dbUserObj1._id;
                        //userdata.push({"record":dbUserObj1.record[0],"videoId":dbUserObj1.videoId});
                        recordIds.push(dbUserObj1._id);
                        comment.push({
                            "id" : dbUserObj1._id,
                            "Comments" : dbUserObj1.comments,
                            "updatedBy" : dbUserObj1.updatedBy,
                            "lastUpdatedBy" : dbUserObj1.lastUpdatedBy,
                            "assignedTo" : dbUserObj1.assignedTo,
                        });
                        recordinfo.push({
                            "UpdatedBy" : dbUserObj1.updatedBy,
                            "lastModifiedBy":dbUserObj1.lastModifiedBy,
                            "UpdatedTime" : dbUserObj1.updatedTime,
                            "lastUpdatedBy" : dbUserObj1.lastUpdatedBy,
                            "InsertedLocation" :  lat +","+ long 
                        });
                    });
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : userdata,
                        recordIdsList : recordIds,
                        formInfo : formInfo,
                        reassignRecordIds : reassinegdReocrdIds,
                        comments : comment,
                        "recordInfo" : recordinfo
                    });

                    //res.json({Skelton:post[0].FormSkeleton,records:userdata});
                } else {
                    res.json({
                        "message" : "No Data Found",
                        "status" : 204
                    });
                }
            });
        } else {
            res.json({
                "message" : "No Data Found",
                "status" : 204
            });

        }
    });
});

//Get all Formsz Records
router.get('/getformszDetails', function (req, res, next) {
    FormszDetails.find({isDeleted : false}, function (err, todos) {
        res.json(todos);
    });
});

//get single record based on the ID
router.get('/:id', function (req, res, next) {
    FormszDetails.findOne({_id : req.params.id,isDeleted : false}, function (err, post) {
        if (err)
            return next(err);
        res.json(post);
    });
});

// Formsz Details
router.post('/create', function (req, res, next) {
     deviceManagement.find({UUID:req.body.UUID,isDeleted:false}, function(err,device) {
        if(device.length>0) {
            if(device[0].status=="Suspended"|| device[0].status=="Rejected") {

                res.json({
                    "message":"Your device is Suspended,so you cannot submit the record",
                    "status":203
                })

            }
            else {
    Formsz.find({_id : req.body.formId,isVisible : false}, function (err, post1) {
        if (post1.length > 0) {
            res.json({
                "message" : "This action cannot be performed as the form no longer exists",
                status : 203
            });
        } else {
            var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id : req.body.formId,
                isVisible : true
            }, function (err, formObj) {
                
                displayrecords = Object.keys(formObj[0].requiredField[0]);
            });
            req.body.status = true;
            FormszDetails.create(req.body, function (err, post) {
                setTimeout(function () {
                    var i = 0;
                    for (i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue=post.record[0][displayrecords[i]]||"";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues
                        ({
                            formId : post.formId,
                            recordId : post._id,
                            displayValues : data
                        });
                    displayJosn.save(function (err, a) {
                        // if (err) throw err;
                    })
                    res.json({
                        "recordId":post._id,
                        "sqliteDBId":post.sqliteDBId,
                        "message" : "Submitted Successfully",
                        status : 200
                    })
                }, 1000);
            });
        }
    });
}
}
else {
    res.json({"message":"Device details not available","status":203});
}
});
});
//PUT /Update records/:id  online -offline
/*router.put('/:id', function (req, res, next) {

    req.body.DSAssignedTo = null;
    Formsz.find({_id : req.body.formId,isVisible : false}, function (err, post1) {
        if (post1.length > 0) {
            res.json({
                "message" : "This action cannot be performed as the form no longer exists",
                status : 203
            });
        } else {
            req.body.status = true;
            var displayrecords = [];
            var displaydata = {};
            var data = [];
            Formsz.find({
                _id : req.body.formId,
                isVisible : true
            }, function (err, formObj) {
                
                displayrecords = Object.keys(formObj[0].requiredField[0]);
                /* var requiredFieldArray = formObj[0].requiredField;
                for(var i=0;i<requiredFieldArray.length;i++) {
                    displayrecords.push(requiredFieldArray[i].id);
                    
                } */
            /*});
            FormszDetails.findByIdAndUpdate(req.params.id, req.body,{new:true}, function (err, post) {
                setTimeout(function () {
                    var i = 0;
                    for (i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue=post.record[0][displayrecords[i]]||"";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    DisplayValues.update({formId : post.formId,
                            recordId : post._id},{$set:{
                                displayValues : data
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                         }
                    })
                    res.json({
                        "message" : "Updated Successfully",
                        status : 200
                    })
                }, 1000);*/

                /* res.json({
                    "message" : "Updated Successfully",
                    status : 200
                }); */
/*          });
        }
    });
});*/


router.put('/:id', function(req, res, next) {
    deviceManagement.find({UUID:req.body.UUID,isDeleted:false}, function(err,device) {
        if(device.length>0) {
            if(device[0].status=="Suspended"|| device[0].status=="Rejected") {
                res.json({
                    "message":"Your device is Suspended,so you cannot submit the record",
                    "status":203
                })

            }
            else {
                    req.body.DSAssignedTo = null;
                    req.body.status = true;
              Formsz.find({
            _id: req.body.formId,
            isVisible: false
        }, function(err, post1) {
        if (post1.length > 0) {
            res.json({
                "message": "This action cannot be performed as the form no longer exists",
                status: 203
            });
        } else {
            req.body.status = true;
            req.body.DSAssignedTo = null;

            FormszDetails.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
                FormszDetails.find({
                    taskId: post.taskId,
                    status: false
                }, function(err, results) {
                    if (err) {
                        //console.log(err);
                    } else {
                        var count = results.length;
                        var offlineJobIdArray=[];
                        if (count <= 0) {
                            Tasks.update({
                                _id: post.taskId
                            }, {
                                $set: {
                                    isClosed: true
                                }
                            }, function(err, a) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    offlineJobIdArray.push(post.taskId);
                                    
                                    res.json({
                                "message": "Updated Successfully",
                                "offlineJobIdArray":offlineJobIdArray,
                                status: 200
                            });
                                } // else loop ends here
                            }) // tasks update ends here

                          
                            
                        } else {
                            res.json({
                                "message": "Updated Successfully",
                                "offlineJobIdArray":offlineJobIdArray,
                                status: 200
                            });
                        }

                    }
                });

            });
           
        }
    });

            }
        }
        else {
            res.json({"message":"Device details not available","status":203});
        }
    })


        
    
});


// Re-Assign
/*router.post('/ReAssign', function (req, res, next) {
    var commnets = [];
    var data = req.body.recordsId;
    var i = 0;
    for (i = 0; i <= data.length; i++) {
        FormszDetails.find({_id : data[i]}, function (err, post) {
            if (post.length > 0) {
                post.forEach(function (reassginobjects) {
                    var json = [];
                    if (JSON.parse(reassginobjects.comments)) {
                        json = JSON.parse(reassginobjects.comments);
                    }
                    json.push({
                        "Comment" : req.body.comments,
                        "UpdatedTime" : new Date()
                    });
                    reassginobjects.IsReassign = true;
                    reassginobjects.status = true;
                    reassginobjects.assignedTo = req.body.assignedTo;
                    reassginobjects.comments = JSON.stringify(json);
                    reassginobjects.save(function (err) {});

                });

            }

        });
    }
    res.json({
        "message" : "Re-Assign Successfully",
        "status" : 200
    })
});*/
router.post('/ReAssign', function(req, res, next) {
    var commnets = [];
    var recIdsArray = req.body.recordsId;
    var i = 0;
    async.forEachSeries(recIdsArray, function(recIdObj, next) {
            FormszDetails.find({
                _id: recIdObj
            }, function(err, post) {
                if (post.length > 0) {
                    post.forEach(function(reassginobjects) {
                        var json = [];
                        if (JSON.parse(reassginobjects.comments)) {
                            json = JSON.parse(reassginobjects.comments);
                        }
                        json.push({
                            "Comment": req.body.comments,
                            "UpdatedTime": new Date()
                        });
                        
                        reassginobjects.IsReassign = true;
                        reassginobjects.status = false;
                        reassginobjects.assignedTo = req.body.assignedTo;
                        reassginobjects.comments = JSON.stringify(json);
                        reassginobjects.save(function(err, reassignedObj) {
                            if (reassignedObj) {
                                // Roja edited code  
                                /*users.find({
                                    type: "1"
                                }, function(err, userObj) {*/
                                    Tasks.find({
                                        _id: reassignedObj.taskId
                                    }, function(err, taskObj) {
                                        if (taskObj.length > 0) {
                                           Tasks.update({_id:taskObj[0]._id}, {
                                            $set: {
                                                isClosed: false
                                            }
                                        }, function(err, a) {
                                            if (err) throw err;
                                            else {
                                                console.log("updated");
                                            }
                                        }); 
                                         var reassignedfrom;

                                            var formId = taskObj[0].assignedFormsz[0].formId
                                            DisplayValues.find({
                                                recordId: reassginobjects._id
                                            }, {
                                                displayValues: 1
                                            }, function(err, dpvalsObj) {
                                                if(req.body.reassignedfrom=="task") {
                                                    reassignedfrom = 1;
                                                } else {
                                                    reassignedfrom = 2
                                                }
                                                var dpvalue = dpvalsObj[0].displayValues[0];
                                                notificationmodelObject = new notificationmodel({
                                                   // adminName: userObj[0].name,
                                                    taskName: taskObj[0].name,
                                                    taskId: reassginobjects.taskId,
                                                    recordId: reassginobjects._id,
                                                    comments: reassignedObj.comments,
                                                    user: req.body.assignedTo,
                                                    displayValues: dpvalue.filedValue,
                                                    actionType: reassignedfrom,
                                                    formId: formId,
                                                    adminId: req.body.adminId

                                                });
                                                notificationmodelObject.save(function(err, notifyObj) {
                                                    if (err)
                                                        return next(err);
                                                    else {
                                                        console.log("inserted");
                                                    }

                                                });
                                            });
                                        } else {
                                            console.log("no length");
                                        }
                                    }); //tasks.find end

                             //   }); // roja code end 
                            } else {
                                console.log(err);
                            }



                        });
                    });
                    next();
                } else {
                    res.json({
                        "message": "No data found",
                        "status": 204
                    });
                }

            });
        },
        function(err) {
            res.json({
                "message": "Re-Assign Successfully",
                "status": 200
            })
        });

});

//Delete Formsz Records
router.delete ('/delete/:id', function (req, res, next) {
    FormszDetails.find({_id : req.params.id}, function (err, post) {
        if (post.length > 0) {
            post.forEach(function (post1) {
                post1.isDeleted = true;
                post1.save(function (err, post) {
                    if (err)
                        return next(err);
                    res.json({
                        "status" : 200,
                        "message" : "Deleted Successfully"
                    });
                });
            });
        } else {
            res.json({
                "status" : 204,
                "message" : "No Data Found"
            });
        }
    });
});

router.get('/getRe-assginedRecodsMobile/:id', function (req, res, next) {
    //res.json({message:"hiiiii"})
    var allrecords = [];
    var data = [];
    var resultJson = {};
    FormszDetails.find({updatedBy : req.params.id,IsReassign:true}, function (err, post) {
        if (post.length > 0) {
            post.forEach(function (displayobject) {
                DisplayValues.find({recordId : displayobject.id}, function (err, post1) {
                    Formsz.find({_id : post1[0].formId,isVisible : true}, function (err, formobj) {
                        if(formobj.length>0)
                        {
                            if(displayobject.taskId=="form")
                            {
                                        resultJson.displayValues = post1[0].displayValues
                                        resultJson.recordId= displayobject._id
                                        resultJson.type = "form"
                                        resultJson.startDate = ""
                                        resultJson.endDate = ""
                                        resultJson.description = formobj[0].description
                                        resultJson.formId = formobj[0]._id
                                        resultJson.formName = formobj[0].name
                                        resultJson.taskId = ""
                                        resultJson.taskName = ""
                                        resultJson.comments = displayobject.comments
                                        data.push(resultJson);
                                        resultJson = {};
                             }else
                            {
                            
                                        resultJson.displayValues = post1[0].displayValues
                                        resultJson.recordId= displayobject._id
                                        resultJson.type = "task"
                                        resultJson.startDate = ""
                                        resultJson.endDate = ""
                                        resultJson.description = formobj[0].description
                                        resultJson.formId = formobj[0]._id
                                        resultJson.formName = formobj[0].name
                                        resultJson.taskId = displayobject.taskId
                                        resultJson.taskName = ""
                                        resultJson.comments = displayobject.comments
                                        data.push(resultJson);
                                        resultJson = {};
                            }
                        }
                    });
                });
            });
            setTimeout(function () {
                res.json(data)
            }, 1000);
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            })
        }
    });
});
//Excel commented by Divya Bandaru. As new code added below.
/*router.post('/download', function (req, res, next) {
    var headers=[];
    var headerslbl=[];
    Formsz.find({_id:req.body.formid},function(err,post1)
        {
             if(post1.length>0)
              {
               post1[0].FormSkeleton.forEach(function (recorddata) {
                 if(recorddata.type.view=="section" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   if(data[i].data.type.view == 'group') {
                    
                    var group = data[i].data.type.fields
                    for (var j=0;j < group.length;j++) {
                    //console.log(group[j])   
                    headers.push(group[j].id);
                    headerslbl.push(group[j].lable)
                    }
                   }
                   else {
                    headers.push(data[i].data.id);
                    headerslbl.push(data[i].data.lable);
                   }
                   
                  }
                  
                 }
                 
                 
                }
                 else if(recorddata.type.view=="group" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   headers.push(recorddata.type.fields[i].id);
                   headerslbl.push(recorddata.type.fields[i].lable)
                  }
                  
                 }
                 
                } 
                else
                {
                headers.push(recorddata.id);
                headerslbl.push(recorddata.lable)
                }
                
               });

              }         
        })
    var updatedby = req.body.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
    var emailids=[]
    emailids=req.body.records;
    FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
        if (post.length > 0) {
            fileid = mongoid;
            var workbook = excelbuilder.createWorkbook('./', mongoid + ".xlsx");
            var sheet1 = workbook.createSheet('sheet1', 100, 10000);
            var k = 1;
            var j = 1;
            var count=1
            for (var i = 0; i < post.length; i++) {
                for (var j = 0; j < headerslbl.length; j++) 
                {
                    if (i == 0) {
                        sheet1.set(j + 1, k, headerslbl[j]);
                    }
                }
                var data={}
                var data=post[i].record[0];
                var theTypeIs = Object.keys(post[i].record[0]);
                  for (var j = 0; j < theTypeIs.length; j++) 
                {
                    if(headers.indexOf(theTypeIs[j])>-1)
                    {
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);
                    }
                    
                    
                }                   
            count=count+1
            }
            workbook.save(function (err) {
                if (err)
                    console.log(err);
                else
                console.log('congratulations, your workbook created');
                var buffer = fs.readFileSync("./" + mongoid + ".xlsx", {
                        encoding : 'base64'
                    });*/
                /*
                    @dexcription" Modification done for allow multiple reciepantes while mailing
                    @modified Date: 03-03-18
                    @developer: Umamaheswari B
                    */
                //--> TPCL Customizations: Uma: Modified: To handle Multiple mail receipents under CC
               // sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail);    
               // sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4);
                //<-- End
/*                res.json({
                    "message" : "Mail sent successfully",
                    "status" : 200
                });
            });
        }
        
    }); 
});*/
//Code modified and added by Divya Bandaru on 16-04-2018
//Service for Excel download.

router.post('/task/download', function (req, res, next) {
    var headers=[];
    var headerslbl=[];
    var formname;
    Formsz.find({_id:req.body.formid},function(err,post1)
        {
            formname = post1[0].name;
             if(post1.length>0)
              {
               post1[0].FormSkeleton.forEach(function (recorddata) {
                 if(recorddata.type.view=="section" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   if(data[i].data.type.view == 'group') {
                    
                    var group = data[i].data.type.fields
                    for (var j=0;j < group.length;j++) {
                    //console.log(group[j])   
                    headers.push(group[j].id);
                    headerslbl.push(group[j].lable)
                    }
                   }
                   else {
                    headers.push(data[i].data.id);
                    headerslbl.push(data[i].data.lable);
                   }
                   
                  }
                  
                 }
                 
                 
                }
                 else if(recorddata.type.view=="group" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   headers.push(recorddata.type.fields[i].id);
                   headerslbl.push(recorddata.type.fields[i].lable)
                  }
                  
                 }
                 
                } 
                else
                {
                headers.push(recorddata.id);
                headerslbl.push(recorddata.lable)
                }
                
               });

              } 
    var updatedby = req.body.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
/*
Modified by santhosh on 10th may 2018, writing 100 rows and 200 columns in excel
*/
    var emailids=[]
    emailids=req.body.records;
    FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
 //   FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedBy : updatedby}, function (err, post) {
        if (post.length > 0) {
            fileid = mongoid;
            var workbook = excelbuilder.createWorkbook('./', formname + ".xlsx");
            var sheet1 = workbook.createSheet('sheet1', 200, 100);
            var k = 1;
            var j = 1;
            var count=1;
            headerslbl.push("DepartmentAdmin","GroupAdmin","TaskName","ProjectName");
            for (var i = 0; i < post.length; i++) {
                for (var j = 0; j < headerslbl.length; j++) 
                {
                    if (i == 0) {
                        sheet1.set(j + 1, k, headerslbl[j]);
                    }
                }
                var data={}
                var data=post[i].record[0];
                var theTypeIs = Object.keys(post[i].record[0]);
               theTypeIs.push("DepartmentAdmin","GroupAdmin","TaskName","ProjectName");
               headers.push("DepartmentAdmin","GroupAdmin","TaskName","ProjectName");
              
                
                  for (var j = 0; j < theTypeIs.length; j++) 
                {
                    
                  

                    if(headers.indexOf(theTypeIs[j])>-1)
                    {
                        if(theTypeIs[j]=="GroupAdmin") {
                            sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].groupAdmin);

                        }
                        else if(theTypeIs[j]=="DepartmentAdmin") {
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].departmentAdmin);

                    }else if(theTypeIs[j]=="TaskName") {
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].taskName);

                    }else if(theTypeIs[j]=="ProjectName") {
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].projectName);

                    }
                    else {

                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);

                    }
                        

                    }

                   // else if(headers.indexOf(theTypeIs[j])==-1) {
                        
                  //  }
                    
                    
                }                   
            count=count+1
            }
            console.log("workbook");
           // console.log(workbook);
            workbook.save(function (err) {
                if (err)
                  //  console.log(err);
                return err;
                else
                console.log('congratulations, your workbook created');
                var buffer = fs.readFileSync("./" + formname + ".xlsx", {
                        encoding : 'base64'
                    });
                /*
                    @dexcription" Modification done for allow multiple reciepantes while mailing
                    @modified Date: 03-03-18
                    @developer: Umamaheswari B
                    */
                //--> TPCL Customizations: Uma: Modified: To handle Multiple mail receipents under CC
                sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + formname + ".xlsx", req.body.altemail, function(callbackResponse){
                    if(callbackResponse==true) {
                         res.json({
                    "message" : "Mail sent successfully",
                    "status" : 200
                });

                    }
                    else {
                        console.log("entered else");
                     res.json({"message":"Failed to send mail","status":500});

                    }
                });    
               // sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4);
                //<-- End
               
            });
        }
        
    });

        })
 
});


function getFormname(id, callback) {
    Formsz.find({_id : {$in : id}}, function (err, data) {
        callback(data);
    });
}

/*function prepopdatajson(id, taskid, user, callback) {
    var data = [];
    var taskdata = {}
    var reqfields = []
    var DisplayValues = [];
    var DisplayValuesData = {};
    var records = [];
    var recordsdata = {};
    var i = 0;
    Formsz.find({_id : id}, function (err, post1) {
        if (post1.length > 0) {
            if (post1[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                data.push(taskdata);
                taskdata = {};
                callback(data);

            } else {
                reqfields = Object.keys(post1[0].requiredField[0]);

                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;

            //  FormszDetails.find({formId : id,taskId : taskid,DSAssignedTo : user}, function (err, post3) {
                 FormszDetails.find({
                    $or: [{
                        formId: id,
                        taskId: taskid,
                        DSAssignedTo: user
                    }, {
                        formId: id,
                        taskId: taskid,
                        updatedBy: user,
                        IsReassign: true
                    }]
                }, function(err, post3) {
                    if (post3.length > 0) {
                        post3.forEach(function (recorddata) {
                            DisplayValuesData.recordId = recorddata._id
                                for (var i = 0; i < reqfields.length; i++) {
                                    recordsdata.fieldId = reqfields[i];
                                    recordsdata.fieldIdName = recorddata.record[0][reqfields[i]];
                                    records.push(recordsdata);
                                    recordsdata = {};
                                }
                                setTimeout(function () {}, 1000);
                            DisplayValuesData.record = records
                                records = [];
                            DisplayValues.push(DisplayValuesData);
                            taskdata.DisplayValues = DisplayValues;
                            DisplayValuesData = {}
                        });
                        data.push(taskdata);
                        taskdata = {};
                        callback(data);
                    } else {
                        data.push(taskdata);
                        taskdata.DisplayValues = DisplayValues;
                        callback(data);
                    }
                });
            }
        }
    });
};*/
function prepopdatajson(id, taskid, user, callback) {
    var data = [];
    var taskdata = {}
    var reqfields = []
    var DisplayValues = [];
    var DisplayValuesData = {};
    var records = [];
    var recordsdata = {};
    var i = 0;
    Formsz.find({
        _id: id
    }, function(err, post1) {
        if (post1.length > 0) {
            if (post1[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                data.push(taskdata);
                taskdata = {};
                callback(data);

            } else {
                reqfields = Object.keys(post1[0].requiredField[0]);

                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                
                FormszDetails.find({
                    $or: [{
                        formId: id,
                        taskId: taskid,
                        DSAssignedTo: user
                    }, {
                        formId: id,
                        taskId: taskid,
                        updatedBy: user,
                        IsReassign: true
                    }]
                }, function(err, post3) {

               
                    if (post3.length > 0) {
                        post3.forEach(function(recorddata) {
                            DisplayValuesData.recordId = recorddata._id,
                        
                                DisplayValuesData.status = recorddata.status,
                                DisplayValuesData.IsReassign = recorddata.IsReassign,
                                DisplayValuesData.comments = recorddata.comments
                            
                            for (var i = 0; i < reqfields.length; i++) {
                                recordsdata.fieldId = reqfields[i];
                                recordsdata.fieldIdName = recorddata.record[0][reqfields[i]];
                                records.push(recordsdata);
                                recordsdata = {};
                            }
                            setTimeout(function() {}, 1000);
                            DisplayValuesData.record = records
                            records = [];
                            DisplayValues.push(DisplayValuesData);
                            taskdata.DisplayValues = DisplayValues;
                            DisplayValuesData = {}
                        });
                        data.push(taskdata);
                        taskdata = {};
                        callback(data);
                    } else {
                        data.push(taskdata);
                        taskdata.DisplayValues = DisplayValues;
                        callback(data);
                    }
                });
            }
        }
    });
};

//bacporting from Ue
router.get('/DashboardStatistics/:user', function(req, res, next) {
    var resultJson = {};
    Tasks.distinct("_id", {
        'assignedUsers.userName': req.params.user,
        isClosed: false
    }, function(err, taskObj) {
        resultJson.taskPending = taskObj.length > 0 ? taskObj.length : 0;
        Tasks.distinct("_id", {
            'assignedUsers.userName': req.params.user,
            isClosed: true
        }, function(err, taskComp) {
            resultJson.taskComp = taskComp.length > 0 ? taskComp.length : 0;
            FormszDetails.find({
                taskId: {
                    $ne: null
                },
                status: true,
                updatedBy: req.params.user
            }, function(err, cardedAddress) {
                resultJson.cardedAddress = cardedAddress.length > 0 ? cardedAddress.length : 0;
                
                    FormszDetails.find({
                $or: [{
                    taskId: {
                        $ne: null
                    },
                    status: false,
                    DSAssignedTo: req.params.user
                }, {taskId: {
                        $ne: null
                    },
                    status: false,
                    updatedBy: req.params.user}]
                }, function(err, noncardedAddress) {

                    resultJson.noncardedAddress = noncardedAddress.length > 0 ? noncardedAddress.length : 0;
                    res.json({
                        data: resultJson,
                        status: 200
                    });
                });
            });
        });
    });
});
//<--

// notifications service
//--> UE Implementation : 11-07-2017
//Addition: Roja 
router.get('/getNotifications/:user', function(req, res, next) {
    notificationmodel.find({
        user: req.params.user
    }).sort({createdTime:-1}).exec(
    function(err, resultObj) {
        if (resultObj.length > 0) {
            res.json({
                data: resultObj,
                "status": 200
            });
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    }
    ) 
    //);

});
//<--
// for notification service change for mobile
//--> UE Implementation : 12-07-2017
//Addition: Roja 
router.put('/notificationChange/:id', function(req, res, next) {
    notificationmodel.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, notification) {
        res.json({
            "message": "Updated successfully",
            "status": 200
        })
    })

});


router.post('/task/download1', function (req, res, next) {
 var headers=[];
 var headerslbl=[];
 var formname ="";

Formsz.find({_id:req.body.formid},function(err,post1)
 {
    if(post1.length>0)
  {
    formname = post1[0].name;
   post1[0].FormSkeleton.forEach(function (recorddata) {
     if(recorddata.type.view=="section" )
    {
     var data =recorddata.type.fields
     if(data.length>0)
     {
      for (var i=0;i<data.length;i++)
      {
       if(data[i].data.type.view == 'group') {
        var group = data[i].data.type.fields
        for (var j=0;j < group.length;j++) {
        //console.log(group[j])   
        headers.push(group[j].id);
        headerslbl.push(group[j].lable)
        }
       }
       else {
        headers.push(data[i].data.id);
        headerslbl.push(data[i].data.lable);
       }
       
      }
      
     }
     
     
    }
     else if(recorddata.type.view=="group" )
    {
     var data =recorddata.type.fields
     if(data.length>0)
     {
      for (var i=0;i<data.length;i++)
      {
       headers.push(recorddata.type.fields[i].id);
       headerslbl.push(recorddata.type.fields[i].lable)
      }
      
     }
     
    } 
    else
    {
    headers.push(recorddata.id);
    headerslbl.push(recorddata.lable)
    }
    
   });
  } 
 })
 var updatedby = req.body.user;
 if (updatedby == "All") {
  updatedby = {
   $ne : null
  }
 }
 var emailids=[]
 emailids=req.body.records;
 FormszDetails.find({taskId:req.body.taskid,formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
  if (post.length > 0) {
   fileid = mongoid;
   var workbook = excelbuilder.createWorkbook('./', formname + ".xlsx");
   var sheet1 = workbook.createSheet('sheet1', 100, 10000);
   var k = 1;
   var j = 1;
   var count=1
   for (var i = 0; i < post.length; i++) {
       
    for (var j = 0; j < headerslbl.length; j++) 
    {
     if (i == 0) {
      sheet1.set(j + 1, k, headerslbl[j]);
     }
    }
    var data={}
    var data=post[i].record[0];
    var theTypeIs = Object.keys(post[i].record[0]);
      for (var j = 0; j < theTypeIs.length; j++) 
    {
     if(headers.indexOf(theTypeIs[j])>-1)
     {
      sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);
     }
     
     
    }      
   count=count+1
   }
   workbook.save(function (err) {
    if (err)
     console.log(err);
    else
    console.log('congratulations, your workbook created');
     var buffer = fs.readFileSync("./" + formname + ".xlsx", {
      encoding : 'base64'
     }); 
    //sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4, function(mailresponse) {
      /*  sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail){
        if(mailresponse==true) {
                                    fs.unlink("./"+ formname +".xlsx",function(err){
                                    if(err) return console.log(err);
                                        }); 
                                        res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }

    });*/
                   

//     sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx" ,req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4, function(mailresponse) {
         sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx" ,req.body.altemail, function(mailresponse) {

    //console.log("2144444444444444444444")

        if(mailresponse==true) {
                                    fs.unlink("./"+ formname +".xlsx",function(err){
                                    if(err) return console.log(err);
                                        }); 
                                        res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }

    });
    res.json({
     "message" : "Mail sent successfully",
     "status" : 200
    });
   });
  }
  
 }); 
});

router.post('/syncMultipleRecords', function(req,res,next) {
    var data =[];
    var updateFormArray =[];
    var insertFormArray =[];
    var formIdArray=[];

  async.forEachSeries(req.body, function(singleTaskForm,next) {
    var response ={};
   
    multipleUpdateServiceHit(singleTaskForm.formId,singleTaskForm.updateArray,function(value,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
     if(value == true) {
        if(singleTaskForm.insertArray.length==0) {
        /*   var singleFormObject = {};
            singleFormObject.updateFormArray = updateJobArray;
            singleFormObject.updateFormIdArray=offlineJobIdArray;*/

           // updateFormArray.push(singleFormObject);
          response = {"updatedFormArray":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray};
            data.push(response);
           response ={};
            next();
          // res.json({"message":"sync successfull","updateJobArray":updatejobArray,"offlineJobIdArray":offlineJobIdArray,"status":200});
        }
        else {
         multipleCreateServiceHit(singleTaskForm.formId,singleTaskForm.insertArray,function(serviceHitValue,insertJobArray) {
            
              if(serviceHitValue== true) {
                /*   var singleFormObject = {};
                singleFormObject.updateFormArray = updateJobArray;
                singleFormObject.updateFormIdArray=offlineJobIdArray;
                singleFormObject.insertFormArray = insertJobArray
                updateFormArray.push(singleFormObject);*/
                response = {"updatedFormArray":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedFormArray":insertJobArray};

               //  response = {"updatedFormArray":updatejobArray,"insertedFormArray":insertJobArray,"offlineFormIdArray":offlineJobIdArray};
                data.push(response);
                response ={};
                next();
             // res.json({"message":"sync successfull","updateJobArray":updatejobArray,"insertJobArray":insertJobArray,"offlineJobIdArray":offlineJobIdArray,"status":200});
                }
                else {
                   // res.json({"message":"failed","status":204});
                   data.push({"message":"failed","status":204});
                }
              });


        }
             

      }  
    });

 //   }
 

   }, function(err) {
    res.json({data,status:200,"message":"Sync successfull"});
   })
  
});
/*function multipleUpdateServiceHit(formId,updateArray,callback) {
      
    Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, post1) {
        if(post1.length>0) {
              var updateArrayData = [];
        var offlineFormIdArray=[];
        var offlineTaskIdArray=[];
         
        
             async.forEachSeries(updateArray,function(singleUpdate,next) {
            singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;
  
                              FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
                                   updateArrayData.push(post._id);
                                    FormszDetails.find({formId:formId,DSAssignedTo:{$ne:null},taskId:post.taskId}, function(err,norecordsAvailable) {
                                        if(norecordsAvailable.length==0) {
                                            console.log("entered here");
                                            offlineFormIdArray.push(formId);
                                            FormszDetails.find({taskId:post.taskId,status:false}, function(err,taskIds) {
                                                if(taskIds.length==0) {
                                                     Tasks.update({
                                                    _id: post.taskId
                                                }, {
                                                    $set: {
                                                        isClosed: true
                                                    }
                                                }, function(err, a) {
                                                    if(err) {
                                                        console.log(err);
                                                    }
                                                    else {
                                                        console.log("true");
                                                        offlineTaskIdArray.push(taskIds);
                                                        next();


                                                    }
                                                });

                                                }
                                           


                                            })
                                        }
                                        else {
                                            next();
                                        }
                                    })

               

            });
       
     
            
            
                    
            

    }, function(err) {
        console.log("2026");
        console.log(updateArrayData);
        console.log(offlineFormIdArray);
       callback(true,updateArrayData,offlineFormIdArray,offlineTaskIdArray);
    })

        }
        else {
                   console.log("failed as form doesnt exist");

        }
    });
  

}*/
function multipleCreateServiceHit(formId,insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    console.log("failed as form doesnt exist");
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

router.post('/syncMultipleRecordsForMultipleTasks', function(req,res,next) {
     var updateJobArray =[];
    var offlineJobIdArray =[];
    var updatejobArray =[];
    var formIdArray =[];

async.forEachSeries(req.body,function(taskObject,next1) {

   
    Tasks.find({_id:taskObject.taskId}, function(err,task) {

            multipleUpdateServiceHitForTasks(task[0]._id,taskObject.updateArray,function(value,updateAddressesArray,offlineJobIdArray,formIdArray) {
     if(value == true) {

        if( taskObject.insertArray==undefined) {
         var updateJobObject ={};

            updateJobObject.updateAddressesArray = updateAddressesArray;
            updateJobObject.offlineJobIdArray =offlineJobIdArray;
            updateJobObject.formIdArray =formIdArray;
            updateJobArray.push(updateJobObject);
            next1();
        }
        else {
             var updateJobObject ={};

            updateJobObject.updateAddressesArray = updateAddressesArray;
            updateJobObject.offlineJobIdArray =offlineJobIdArray;
            updateJobObject.formIdArray =formIdArray;
           
         multipleCreateServiceHitForTasks(task[0]._id,taskObject.insertArray,function(serviceHitValue,insertJobArray) {
                if(serviceHitValue== true) {
                    updateJobObject.insertAddresses = insertJobArray;
                    updateJobArray.push(updateJobObject);
                    next1();
                }
                else {
                    console.log("something failed");
                    next1();
                }
              });


        }
             

      }  
    });

    })

}, function(err) {
    res.json({"message":"Sync Successfull", data:updateJobArray,status:200 })


})
    
   

});

function multipleUpdateServiceHitForTasks(taskId,updateArray,callback) {

         var updateArrayData = [];
        var offlineJobIdArray=[];
        var formIdArray = [];

             async.forEachSeries(updateArray,function(singleUpdate,next) {
            
            singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;

            FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
                if(err) {
                    return err;
                }
                else {
                    updateArrayData.push(post._id);
                    FormszDetails.find({formId:post.formId,taskId:post.taskId,DSAssignedTo:{$ne:null},status:false}, function(err,recordsWithForm) {
                        if(recordsWithForm.length==0) {
                              formIdArray.push(post.formId);
                              FormszDetails.find({taskId:taskId,DSAssignedTo:{$ne:null},status:false}, function(err,taskIdArray) {
                                if(taskIdArray.length==0) {
                                    offlineJobIdArray.push(taskId);
                                    next();
                                }
                                else {
                                    next();
                                }
                              })


                        }
                        else {
                            next();

                        }

                    })
                }


            });


    }, function(err) {

       callback(true,updateArrayData,offlineJobIdArray,formIdArray);
    })

  

}
function multipleCreateServiceHitForTasks(taskId,insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: singleInsert.formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   /* if(singleInsert.record[0]["Dropdown__name_161930"].trim().toLowerCase()=="yes"){
                            console.log("if");
                            singleInsert.markerStyleCode =4;
                        }
                        else {
                            singleInsert.markerStyleCode =2;
                        }*/
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

router.post('/syncmultipleFormRecords', function(req,res,next){
    multipleUpdateServiceHit(req.body.updateArray,function(status,updatedFormIdList,updatedPreppopList,updatedtaskIdList) {
        if(status==true) {
            //updateArrayData,offlineFormIdArray,offlineTaskIdArray
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    res.json({updatedFormIdList:updatedPreppopList,updatedPreppopList:updatedFormIdList,updatedtaskIdList:updatedtaskIdList,insertArrayList:insertArrayList})
                }
                if(status==false) {
                }
            })
        }
        else {
            console.log("entered update service else");
        }

    })
});

/*router.post('/syncmultipleFormRecords', function(req,res,next){
    if(req.body.updateArray.length>0) {
        multipleUpdateServiceHit(req.body.updateArray,function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
        //updateArrayData,offlineFormIdArray,offlineTaskIdArray
        if(status==true) {
            console.log(updatedFormIdList);
            console.log(updatedPreppopList);
            console.log(updatedtaskIdList);
            if(req.body.insertArray.length>0) {
                 multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    console.log(insertArrayList);
                    res.json({updatedFormIdList:offlineFormIdArray,updatedPreppopList:updateArrayData,updatedtaskIdList:offlineTaskIdArray,insertArrayList:insertArrayList,"message":"Sync Successfully done"});
                }
                
            })

            }
            else {
                res.json({updatedFormIdList:offlineFormIdArray,updatedPreppopList:updateArrayData,updatedtaskIdList:offlineTaskIdArray,"message":"Sync Successfully done"});


            }
           
        }
        else {
            console.log("entered update service else");
        }

    })

    }
    else {
          multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    console.log(insertArrayList);
                    res.json({insertArrayList:insertArrayList,"message":"Sync Successfully done"});
                }
                
            })

    }
    
});*/
function multipleUpdateServiceHit(updateArray,callback) {
      
   /* Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, post1) {*/
     //   if(post1.length>0) {
        var updateArrayData = [];
        var offlineFormIdArray=[];
        var offlineTaskIdArray=[];
         
        
             async.forEachSeries(updateArray,function(singleUpdate,next) {
                Formsz.find({
                _id: singleUpdate.formId,
                isVisible: true
            }, function(err, post1) {
                 singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;
            FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
            updateArrayData.push(post._id);
                FormszDetails.find({formId:singleUpdate.formId,DSAssignedTo:{$ne:null},taskId:post.taskId}, function(err,norecordsAvailable) {
                    if(norecordsAvailable.length==0) {
                         offlineFormIdArray.push(singleUpdate.formId);
                        FormszDetails.find({taskId:post.taskId,status:false}, function(err,taskIds) {
                        if(taskIds.length==0) {
                        Tasks.update({
                        _id: singleUpdate.taskId
                            }, {
                             $set: {
                                    isClosed: true
                                    }
                                }, function(err, a) {
                                    if(err) {
                                        console.log(err);
                                    }
                                    else {
                                        offlineTaskIdArray.push(singleUpdate.taskId);
                                        next();


                                    }
                                });

                                }
                                else {
                                    console.log("task cannot be updated");
                                    next();
                                }
                                           


                                            })
                                        }
                                        else {
                                            next();
                                        }
                                    })

               

            });
       
     

            });

           
            
            
                    
            

    }, function(err) {
       callback(true,updateArrayData,offlineFormIdArray,offlineTaskIdArray);
    })

        /*}
        else {
                   console.log("failed as form doesnt exist");

        }*/
 //   });
  

}

function multipleCreateServiceHitService(insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: singleInsert.formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    console.log("failed as form doesnt exist");
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

//Task Sync Multiple records
/*router.post('/syncMultipleRecordsForMultipleTasksDownloaded', function(req,res,next) {
    if(req.body.updateArray.length>0) {

multipleUpdateServiceHit(req.body.updateArray, function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
    if(status==true) {
        if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"message":"Sync successfully"});
        }
        

    }

})        
    }
    else {
         if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            console.log("insert array missing");
        }

    }

});*/

router.post('/syncMultipleRecordsForMultipleTasksDownloaded', function(req,res,next) {

multipleUpdateServiceHit(req.body.updateArray, function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
    if(status==true) {
        if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"message":"Sync successfully"});
        }
        

    }

})        
   

});

module.exports = router;
