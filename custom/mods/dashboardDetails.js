var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var mongoid = mongoose.Types.ObjectId();
var Users = require('../../models/user.js');//for users data
var Group = require('../../models/group.js');//for departments data
var deviceManagement =  require('../../models/deviceManagement.js');//for device data
var tasks = require('../../models/projectTasksModel.js');//projectTask
var project = require('../../models/projectModel.js');//projects
var FormszTask = require('../../models/formszTasks.js');//formsTask
var mobileLicensing = require('../../models/mobileLicensing.js');//mobileLicensing

var fs = require('fs');
var formidable = require('formidable');
var Store = require('../../models/store.js');
var sendautomail = require('../../routes/utils').sendautomail;
var defoultImageurl = require('../../routes/utils').defoultimageurl;
var ObjectID = require("bson-objectid");
//var log = require('../../logs')(module);
var async = require('async');
var mobileLicensing = require('../../models/mobileLicensing.js');
var FormsDetails = require('../../models/formszDetails.js');//for forms details
var Forms = require('../../models/formsz.js');//for forms


//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getUsersCount Service.
router.get('/getUsersCount/:userType/:groupId', function(req, res, next) {

    var users = {};
    //--> Swathi: Modified: 12-Apr:TPCL:Dashboards
   // Notes: for dashboard Required changes.
    if (req.params.userType == '0') { //for root login

        Users.find({ type: 2,isDeleted:false }, {
                                  name: 1,
                                  username: 1,
                                  zone: 1,
                                  vender: 1,
                                  groupname: 1,lastLoggedInTime:1
        }).exec(function(err1, post) {

            var totalUsers = {};
            users.totalUsersCount = post;

            Users.find({
                groupname: [],
                type: 2,isDeleted:false 
            }, {
                name: 1,
                username: 1,
                zone: 1,
                vender: 1,
                groupname: 1,createdDateTime:1
            }).exec(function(err, results) {

                var departmentuserscount = [];
                var deptcountobject = {};

                Group.find({}, {
                    name: 1,
                    userlist: 1,groupadminlist:1,adminlist:1
                 
                }).exec(function(err, groupnames) {
                    users.totaldepartmentusers = groupnames;
                })
                users.groupsUnAssigned = results;
                //when user login from mobile isUserLocatorActive key cheged to true  and user logout from mobile this key chnge to false then only this will work
                Users.find({
                    isUserLocatorActive: true,
                    type: 2,isDeleted:false 
                }, {
                    name: 1,
                    username: 1,
                    zone: 1,
                    vender: 1,
                    groupname: 1,
                    deviceDetails:1,lastLoggedInTime:1
                }).exec(function(err2, activeusers) {
                    users.activeUsers = activeusers;
                    res.json({
                        data: users,
                        status: 200
                    });
                })
            })

        })
    } else { //admin login

        Users.find({
           // groupname: req.params.groupId,
           groupname:{ $elemMatch: { _id: req.params.groupId }},
            type: 2,isDeleted:false 
        }, {
            name: 1,
            username: 1,
            zone: 1,
            vender: 1,
            groupname: 1,lastLoggedInTime:1
        }).exec(function(err1, post) {
            users.totalUsersCount = post;
            Users.find({
                isUserLocatorActive: true,
                type: 2,isDeleted:false ,
                groupname: { $elemMatch: { _id: req.params.groupId }}
            }, {
                name: 1,
                username: 1,
                zone: 1,
                vender: 1,
                groupname: 1,
                deviceDetails:1,lastLoggedInTime:1
            }).exec(function(err2, results) {
                users.activeUsers = results;
                res.json({
                    data: users,
                    status: 200
                });
            })
        })
    }
});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDevicesCount Service.
router.get('/getDevicesCount/:userType/:groupId', function(req, res, next) {
    var devices = {};
    var departmentuserscount = [];
    if (req.params.userType == '0') { //root login
        deviceManagement.find({isDeleted:false}, {
            Model: 1,
            Manufacturer: 1,
            MacAddress: 1,
            status: 1,
            platform:1,
            version:1,
            UUID:1,
            _id: 0,
            requestedUser:1,
            groupname: 1
        }).exec(function(err, post) {
            devices.totalDevicesCount = post;

            deviceManagement.find({
                status: {
                    $eq: "Pending"
                },isDeleted:false
            }, {
                Model: 1,
                Manufacturer: 1,
                MacAddress: 1,
                status: 1,
                platform:1,
            version:1,
            UUID:1,
                _id: 0,
                requestedUser:1,
                groupname: 1
            }).exec(function(err, results) {

                Group.find({}, {
                    name: 1
                }).exec(function(err, groupnames) {

                    async.forEachSeries(groupnames, function(item, next) {


                        deviceManagement.find({
                            groupname: item._id,isDeleted:false
                        }, {
                            Manufacturer: 1,
                            Model: 1,
                            _id: 0
                        }).exec(function(err3, devicesCount) {
                            var departmentObject = {};
                            if (devicesCount.length > 0) {
                                departmentObject.departmentAdmin_id = item._id;
                                departmentObject.departmentAdmin = item.name;
                                departmentObject.devices = devicesCount
                                departmentuserscount.push(departmentObject);

                                devices.totaldepartmentusers = departmentuserscount;
                            }

                            next();

                        })
                    }, function(err) {

                        devices.pendingdevices = results;
                        res.json({
                            data: devices,
                            status: 200
                        });

                    })

                })
            });
        });
    } else { //admin login
        deviceManagement.find({
            groupname: req.params.groupId,isDeleted:false
        }, {
            Manufacturer: 1,
            Model: 1,
            MacAddress: 1,
            status: 1,

            platform:1,
            version:1,
            UUID:1,
            _id: 0, requestedUser:1
        }).exec(function(err3, devicesCount) {

            devices.totalDevicesCount = devicesCount;
            deviceManagement.find({
                status: {
                    $eq: "Pending"
                },
                groupname: req.params.groupId,isDeleted:false
            }, {
                Model: 1,
                Manufacturer: 1,
                MacAddress: 1,
                status: 1,
                 status: 1,
            platform:1,
            version:1,
            UUID:1,
                _id: 0, requestedUser:1,
            }).exec(function(err, results) {
                devices.pendingdevices = results;
                res.json({
                    data: devices,
                    status: 200
                });
            });
        })
    }

});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
router.get('/getDepartmentsCount',function(req,res,next){
    var admins = {};
    //--> Swathi: Modified: 12-Apr:TPCL:Dashboards
// Notes: modified TPCl getDepartmentsCount Service.
    Users.find({type:1,isDeleted:false},{name:1,username:1,email:1,_id:0,createdDateTime:1,groupname:1}).exec(function(err,post) {
       
       admins.totalDepartmentCount = post;
       
       Users.find({groupname:[],type:1,isDeleted:false},{name:1,username:1,email:1,groupname:1,_id:0,createdDateTime:1}).exec(function(err1,unassigned){
          admins.unassignedDepartments = unassigned; })

        Users.find({type:3,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0,groupname:1}).exec(function(err,post) {
          admins.totaladminsCount = post;
      
          Users.find({groupname:[],type:3,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0,groupname:1}).exec(function(err1,unassigned){
              admins.unassignedAdmins = unassigned;
      
              res.json({data:admins,status:200});
          })
        });
    });
});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
/*router.get('/getDashboardData',function(req,res,next){
    var dashboardData = {};
    var projectuserscount = [];
    Users.find({isUserLocatorActive : true,type:2},{name:1,username:1,vender:1,zone:1,groupname:1}).exec(function (err, users) {
    
       dashboardData.totalUsersCount =users;
       var todate = new Date((new Date().getTime() - (90 * 24 * 60 * 60 * 1000)))
       var currentDate = new Date()    
        // modified createdTime to lastLoggedInTime by Swathi on 25-01-2018
        Users.find({lastLoggedInTime:{$gte: todate, $lte: currentDate },type:2,isUserLocatorActive:false},{name:1,username:1,vender:1,zone:1,groupname:1}) .exec(function(err1,inactive){
         
         //Swathi updated startdate enddate in query on 19-01-2018    
        tasks.find({endDate: {$lte:currentDate},isClosed:{$eq:false}},{name:1,users:1,projectID:1,startDate:1,endDate:1}).exec(function(err3,taskDetails){
                  
                async.forEachSeries(taskDetails, function(item,next) {

                project.find({_id:item.projectID} ,{name:1}).exec(function(err,projectnames){
                    if(projectnames.length>0){
                        var taskObject = {};
                        taskObject.projectname = projectnames[0].name;
                        taskObject.task = item 
                        projectuserscount.push(taskObject);
                        dashboardData.totalprojecttasks=projectuserscount;
                    }
              
                    next();
                    })
                }, function(err)
                  {
                      dashboardData.inactiveUsers = inactive;
                      res.json({data:dashboardData,status:200});
          })

  })  

  })
    
  });

  });*/

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
router.get('/getDashboardData',function(req,res,next){
    var dashboardData     = {};
    var projectCount      = [];
    var todate            = new Date((new Date().getTime() - (90 * 24 * 60 * 60 * 1000)))
    var currentDate       = new Date()  
     var totallic   = [];
    Users.count({type:2,isDeleted:false }).exec(function (err, Totuserscnt) {
       dashboardData.totaluserscnt = Totuserscnt; 
    });
    mobileLicensing.count().exec(function(err, results) {
        dashboardData.totallicCount = results; 
    });   
/*
Modified by santhosh to get all licenses details with out grouping with department on 10th may 2018
*/
    // Users.find({isUserLocatorActive : true,type:2},{name:1,username:1,vender:1,zone:1,groupname:1}).exec(function (err, activeusers) {
       // mobileLicensing.find({ isActive: true},{licenseId:0}).exec(function(err, results) {
       mobileLicensing.aggregate([{ 
        $match : {isActive: true} 
                        },{ 
        $group : { _id : "$_id", docs: { $push: { assignedTo: "$assignedTo", deviceDetails: "$deviceDetails",loggedInTime: "$loggedInTime" , department: "$department"} } } ,
                        }]).exec(function(err1,results){


               
                var userObject = {};
                var adminlistArr = [];
                var userDetailsArr = [];

             async.forEachSeries(results, function(item,next) {

                     userObject.mobileLicensingDetails = results ;

                     // totallic.push(userObject);

                    async.forEachSeries(item.docs ,function(item2,next2){

                        Group.find({name:item2.department},{adminlist:1}).exec(function(err,adminlists){

                          if (adminlists.length > 0) {

                            async.forEachSeries(adminlists,function(adminlist1,next3){
                        adminlistArr.push(adminlist1.adminlist);
                            })


                          }
                            
                            userObject.adminlists = adminlistArr;

                        })
                        next2();
                    });
             async.forEachSeries(item.docs ,function (items,next1) {
                    
                     Users.find({username:items.assignedTo} ,{zone:1,vender:1,groupname:1}).exec(function(err,userDetails){
                        
                        if (userDetails.length > 0 ){
                         userDetailsArr.push(userDetails[0]);

                        }
                    userObject.userDetails = userDetailsArr;

                   
                    next1();
                    })
                }, function(err1) {
                     totallic.push(userObject);
                    next();

                  

                   dashboardData.consumedLics = totallic;
 
                })
                
            
                }, function(err)
                  {

           
        Users.aggregate([{ 
        $match : {lastLoggedInTime:{$lte: todate},type: '2',isUserLocatorActive : false,isDeleted:false } 
                        },{ 
        $group : { _id : "$groupname.name", docs: { $push: { username: "$username", vender: "$vender",zone: "$zone" , groupname: "$groupname",name: "$name",deviceDetails: "$deviceDetails" ,lastLoggedInTime:"$lastLoggedInTime"} } } ,
                        }]).exec(function(err1,inactive){

                            var inactiveObject = {};
                           
                            var totalUsersInactive = [];
                           
         async.forEachSeries(inactive, function(results,next3) {
         
             var totalInactiveUsers = [];

          async.forEachSeries(results._id,function(names,next2){

             Group.find({name:names},{adminlist:1}).exec(function(err,adminlists1){

                          if (adminlists1.length > 0) {
                           
                
                            async.forEachSeries(adminlists1,function(totaladminlist,next6){
                              
                                  totalInactiveUsers.push(totaladminlist.adminlist);
                                  next6();
                                   next2();
                                  
                            },function(err){
                             
                            })
                          
                             }

                        })
           
                  },function(err){
                             inactiveObject.adminlist = totalInactiveUsers;
                           inactiveObject.inactiveUsersData = results.docs;

                           totalInactiveUsers = [];
                        
                         totalUsersInactive.push(inactiveObject);

                         inactiveObject={};
                    })

               
                dashboardData.inactiveUsers = totalUsersInactive;
                next3();
             },function(err){

               
            
             })

          FormszTask.find({startDate: {$lte:currentDate},isClosed:false,isDeleted:false},{name:1,userGroup:1,endDate:1,assignedFormsz:1,assignedUsers:1,assignedGroupadmin:1,isDeleted:1}).exec(function(err3,taskDetails){
               var arrayTaskList = [];
               var totalObject = {};
               async.forEachSeries(taskDetails ,function(item,next){

                        Group.find({_id:item.userGroup},{adminlist:1}).exec(function(err,adminlists){

                          if (adminlists.length > 0) {

                             async.forEachSeries(adminlists,function(admins,next4){
                                totalObject.assignedDepartmentadmin = admins.adminlist;
                            })

                            totalObject.name = item.name;
                            totalObject.endDate = item.endDate;
                            totalObject.assignedFormsz = item.assignedFormsz;
                            totalObject.assignedUsers = item.assignedUsers;
                            totalObject.assignedGroupadmin = item.assignedGroupadmin;
                            totalObject.isDeleted = item.isDeleted;
                           

                           arrayTaskList.push(totalObject);
                           totalObject = {};

                          }else{
                             totalObject.name = item.name;
                            totalObject.endDate = item.endDate;
                            totalObject.assignedFormsz = item.assignedFormsz;
                            totalObject.assignedUsers = item.assignedUsers;
                            totalObject.assignedGroupadmin = item.assignedGroupadmin;
                            totalObject.isDeleted = item.isDeleted;
                            totalObject.assignedDepartmentadmin = "N/A";
                           arrayTaskList.push(totalObject);
                           totalObject = {};

                          }
                       next();
                        })
                       
                    },function(err){
            dashboardData.totaltasks=arrayTaskList;
              
             project.find({isDeleted:false},{name:1,createdBy:1,startDate:1,endDate:1,zonalAdmins:1,isDeleted:1}).exec(function(err3,projectDetails){      
            dashboardData.totalprojects=projectDetails;
             res.json({data:dashboardData,status:200});
             
                    });

             
              
               });
             })
                     
            })
                   
                  })
       })


  });

router.get('/getProjectDashboardData/:departmentAdmin_id/:userType',function (req,res,next) {
     var dashboardData = {};
    var totalprojectsDetails = [];
     if (req.params.userType == 1) {//for department Admin
     project.find({deparmentId:req.params.departmentAdmin_id,isDeleted:false },{name:1,startDate:1,endDate:1,isDeleted:1,zonalAdmins:1}).exec(function(err3,projectDetails){      
                   var projectObject = {};
              async.forEachSeries(projectDetails,function(item,next){
                 tasks.count({projectID:item._id}).exec(function(err,taskCount){

                  projectObject.tasks = taskCount;
                  projectObject.projects = item;
                 totalprojectsDetails.push(projectObject);
                 projectObject = {};

                  next();

                 
             })

            },function(err){
            dashboardData.totalprojects=totalprojectsDetails;
             res.json({data:dashboardData,status:200});
            })
           
             
        })
        }
     else if (req.params.userType == 3){//for group Admin
         project.find({'zonalAdmins.0.id':req.params.departmentAdmin_id,isDeleted:false},{name:1,startDate:1,endDate:1,isDeleted:1,zonalAdmins:1}).exec(function(err3,projectDetails){      
            
           var projectObject = {};

              async.forEachSeries(projectDetails,function(item,next){
                 tasks.count({projectID:item._id}).exec(function(err5,taskCount){

                  projectObject.tasks = taskCount;
                  projectObject.projects = item;
                totalprojectsDetails.push(projectObject);
                 projectObject = {};
                  next();


             })
               
            },function(err){
            dashboardData.totalprojects=totalprojectsDetails;
             res.json({data:dashboardData,status:200});
            })
           
            // dashboardData.totalprojects=projectDetails;
            //  res.json({data:dashboardData,status:200});
        })
     }   

});
router.get('/getGroupAdminDashboardData/:groupId',function (req,res,next) {
     var dashboardData = {};
   
 Users.find({
           // groupname: req.params.groupId,
           groupname:{ $elemMatch: { _id: req.params.groupId }},
            type: 3,isDeleted:false 
        }, {
            name: 1,
            username: 1,
            zone: 1,
            vender: 1,
               email: 1
        }).exec(function(err1, post) {
            dashboardData.totalGroupAdmins = post;
            res.json({data:dashboardData,status:200});
            
        })
    

});
/*router.get('/getFormsDashboardData/:group_id/:userType',function (req,res,next) {
     
    var toatlForms            = [];
    var unassignedForms       = {};
    var unassignedFormsCount  = [];
     if (req.params.userType == 0) {


    Forms.find({formType:"form",isVisible:true },{name:1,createdBy:1,createdTime:1}).exec(function(err1, post) {

                         var dashboardData         = {};
                        
                            var forms              = {};
                         
                async.forEachSeries(post,function(item,next){
                      
                 FormsDetails.find({formId:item._id+""},{record:1}).exec(function(err2,records){
                
                          
                 FormszTask.count({assigned:{$elemMatch:{form:item._id+""}}}).exec(function(err4,assigned){
                     

                 FormsDetails.find({formId:item._id+""},{updatedTime:1}).sort({"updatedTime": -1}).limit(1).exec(function(err2,time){
                    forms.formsData = item;
                     forms.record    = records;

                     forms.updatedTime = time;
                    forms.assigned  = assigned;
                toatlForms.push(forms);
                forms = {};
                next();
                     })
                
                      
                             });

             
                   });
                 
                },function(err){
                 Forms.find({formzCategory:[],isVisible:true},{name:1,createdBy:1,createdTime:1,userGroup:1}).exec(function(err5,unassigned){
                       
                     dashboardData.unassignedForms = unassigned;
                     dashboardData.totalForms = toatlForms;

                    res.json({data:dashboardData,status:200});

                     

                });
                  
                    
                   
                })
        })



      }else{
// ,userGroup:{$elemMatch:{ req.params.group_id}}
 Forms.find({formType:"form",isVisible:true,userGroup:req.params.group_id },{name:1,createdBy:1,createdTime:1}).exec(function(err1, post) {

                         var dashboardData         = {};
                        
                            var forms              = {};
                         
                async.forEachSeries(post,function(item,next){
                      
                 FormsDetails.find({formId:item._id+""},{record:1}).exec(function(err2,records){
                
                          
                 FormszTask.count({assigned:{$elemMatch:{form:item._id+""}}}).exec(function(err4,assigned){
                     

                 FormsDetails.find({formId:item._id+""},{updatedTime:1}).sort({"updatedTime": -1}).limit(1).exec(function(err2,time){
                    forms.formsData = item;
                     forms.record    = records;

                     forms.updatedTime = time;
                    forms.assigned  = assigned;
                toatlForms.push(forms);
                forms = {};
                next();
                     })
                
                      
                             });

             
                   });
                 
                },function(err){
                 Forms.find({formzCategory:[],isVisible:true},{name:1,createdBy:1,createdTime:1,userGroup:1}).exec(function(err5,unassigned){
                       
                     dashboardData.unassignedForms = unassigned;
                     dashboardData.totalForms = toatlForms;

                    res.json({data:dashboardData,status:200});

                     

                });
                  
                    
                   
                })
        })
        
      }
   
});*/

router.get('/getFormsDashboardData/:group_id/:userType',function (req,res,next) {
     
    var toatlForms            = [];
    var unassignedForms       = {};
    var unassignedFormsCount  = [];
     if (req.params.userType == 0) {


    Forms.find({formType:"form",isVisible:true },{name:1,createdBy:1,createdTime:1}).exec(function(err1, post) {

                         var dashboardData         = {};
                        
                            var forms              = {};
                         
                async.forEachSeries(post,function(item,next){
                      
                 FormsDetails.find({formId:item._id+""},{record:1}).exec(function(err2,records){
                
                          
                 FormszTask.count({assigned:{$elemMatch:{form:item._id+""}}}).exec(function(err4,assigned){
                     

                 FormsDetails.find({formId:item._id+""},{updatedTime:1}).sort({"updatedTime": -1}).limit(1).exec(function(err2,time){
                    forms.formsData = item;
                     forms.record    = records;

                     forms.updatedTime = time;
                    forms.assigned  = assigned;
                toatlForms.push(forms);
                forms = {};
                next();
                     })
                
                      
                             });

             
                   });
                 
                },function(err){
                 Forms.find({formType:"form",formzCategory:[],isVisible:true},{name:1,createdBy:1,createdTime:1,userGroup:1}).exec(function(err5,unassigned){
                       
                     dashboardData.unassignedForms = unassigned;
                     dashboardData.totalForms = toatlForms;

                    res.json({data:dashboardData,status:200});

                     

                });
                  
                    
                   
                })
        })



      }else{
// ,userGroup:{$elemMatch:{ req.params.group_id}}
 Forms.find({formType:"form",isVisible:true,userGroup:req.params.group_id },{name:1,createdBy:1,createdTime:1}).exec(function(err1, post) {

                         var dashboardData         = {};
                        
                            var forms              = {};
                         
                async.forEachSeries(post,function(item,next){
                      
                 FormsDetails.find({formId:item._id+""},{record:1}).exec(function(err2,records){
                
                          
                 FormszTask.count({assigned:{$elemMatch:{form:item._id+""}}}).exec(function(err4,assigned){
                     

                 FormsDetails.find({formId:item._id+""},{updatedTime:1}).sort({"updatedTime": -1}).limit(1).exec(function(err2,time){
                    forms.formsData = item;
                     forms.record    = records;

                     forms.updatedTime = time;
                    forms.assigned  = assigned;
                toatlForms.push(forms);
                forms = {};
                next();
                     })
                
                      
                             });

             
                   });
                 
                },function(err){
                 Forms.find({formType:"form",formzCategory:[],isVisible:true,userGroup:req.params.group_id},{name:1,createdBy:1,createdTime:1,userGroup:1}).exec(function(err5,unassigned){
                       
                     dashboardData.unassignedForms = unassigned;
                    
                     dashboardData.totalForms = toatlForms;

                    res.json({data:dashboardData,status:200});

                     

                });
                  
                    
                   
                })
        })
        
      }
   
});



module.exports = router;
