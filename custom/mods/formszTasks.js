var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Tasks = require('../../models/formszTasks.js');
var Formsz = require('../../models/formsz.js');    
var FormszDetails = require('../../models/formszDetails.js');
var DisplayValues1 = require('../../models/displayValues.js');
var User = require('../../models/user.js');
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var pushNotifications = require('../../routes/utils').pushNotifications;
var getdeviceKeysByUser = require('../../routes/utils').getdeviceKeysByUser;
var async = require('async');
var notificationmodel = require('../../models/notificationmodel.js');
var Groups = require('../../models/group.js');
var fs = require('fs');

var Users = require('../../models/user.js');


var logForPrepopData = require('./logForPrepopData')(module);

//Added By Naveen Adepu
var multer = require('multer');
var parseXlsx = require('excel');
var versionManage = require('../../models/versionManagement.js');

// Added by Naveen Adepu 11-APR-2018 to fix number format issue in formsz 
var xlsxtojson = require('xlsx-to-json')
// End of Change

//Update Tasks
var arr = [];
//get Task by ID
router.get('/:id' , function (req,res,next) {
Tasks.findOne({_id:req.params.id, isDeleted : false}, function (err,tasks){
  if(tasks){
    res.json({data:tasks,status:200});
  }
  else {
    res.json({message:"No data found",status:204});
  }
});
  
});
router.put('/:id', function(req, res, next) {

    Tasks.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, post) {
        if (err) return next(err);
    else {
            /*  if (req.body.assignedUsers) {
            var value = req.body.assignedUsers;
            for (var i = 0; i < value.length; i++) {
                arr.push(value[i].userId);
            }

             var value = getdeviceKeysByUser(arr, function(data) {
                 pushNotifications(value, "Task has been Updated", "Task Updated", {
                     data: "Task Updated"
                 });
            });
        }*/
        res.json({
            "message":"Task updated successfully",
            "status": 200
        });
    }
    

    });
});

//create Task
router.post('/createTask', function (req, res, next) {
  var arr = [];
  mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
  Tasks.find({
    name: req.body.name,
    userGroup: req.body.userGroup,
    isDeleted: false
  }, function (err, post) {
    if (post.length > 0) {
      res.json({
        "message": "This Task already exists",
        "status": 204
      });
    } else {
      req.body._id = mongoid;
      Tasks.create(req.body, function (err, a) {
        if (err) {
          return err
        } else {
          if (req.body.uploadFileInfo.length>0) {
            async.forEachSeries(req.body.uploadFileInfo, function (singleUserTaskData, next) {
              data = singleUserTaskData.formInfo;

              try {
                // for (var i = 0; i < data.length; i++)
                async.forEachSeries(data, function (singlerecord, nextIteration) {
                  var record = [];
                  record.push(singlerecord);
                  FormDetails = new FormszDetails({
                      formId: singleUserTaskData.form,
                      taskId: mongoid,
                      DSAssignedTo: singleUserTaskData.user,
                      record: record,
                      prepopulatedRecord: record
                    })
                    FormDetails.save(function (err, post) {
                      if (post) {
                        var displayrecords = [];
                        var data = [];
                        Formsz.find({
                          _id: singleUserTaskData.form,
                          isVisible: true
                        }, function (err, forms) {
                          if (forms.length > 0) {
                            displayrecords = Object.keys(forms[0].requiredField[0]);
                            for (var i = 0; i < displayrecords.length; i++) {
                              var displaydata = {};
                              displaydata.filedId = displayrecords[i]
                                displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                              data.push(displaydata);
                            }
                            var displayJosn = new DisplayValues1({
                                formId: post.formId,
                                recordId: post._id,
                                displayValues: data
                              });
                            displayJosn.save(function (err, a1) {
                              if (err) {
                                console.log("err");
                              } else {
                               nextIteration();
                              }
                            })
                              //nextIteration();


                          }
                        });

                      }
                    });

                }, function (err) {

                  notificationmodelObject = new notificationmodel({
                      adminName: req.body.createdBy,
                      taskName: req.body.name,
                      taskId: a._id,
                      user: singleUserTaskData.user,
                      actionType: "assignjob"

                    });
                  notificationmodelObject.save(function (err, notifyObj) {
                    if (err)
                      return next(err);
                    else {
                      console.log("saved notifi");

                    }
                  });

                  next();
                })

                //})
                //   }

              } catch (error) {
                console.log("req.body.uploadFileInfo[0].formInfo is not there" + error);
              }
            }, function (err) {
              if (req.body.assignedUsers) {
                var value = req.body.assignedUsers;
                for (var i = 0; i < value.length; i++) {
                  arr.push(value[i].userId);
                }
                var value = getdeviceKeysByUser(arr, function (data) {
                    pushNotifications(value, "New Task has been Created", "Task Created", {
                      data: "New Task Created"
                    });
                  });
              }
              //  setTimeout(function() {
              res.json({
                "message": "Task created successfully",
                "status": 200
              })

              //  },1000);

            }); //--loop async
          }
          else {
            async.forEachSeries(req.body.assignedUsers,function(user,next) {
              notificationmodelObject = new notificationmodel({
                      adminName: req.body.createdBy,
                      taskName: req.body.name,
                      taskId: a._id,
                      user: user.userName,
                      actionType: "assignjob"

                    });
                  notificationmodelObject.save(function(err,notifyObj) {
                    if (err)
                      return next(err);
                    else {
                      console.log("saved notifi");
                      next();

                    }
                  });

            }, function(err) {
              if(req.body.assignedUsers.length>0) {
                 var value = req.body.assignedUsers;
               for (var i = 0; i < value.length; i++) {
                  arr.push(value[i].userId);
                }
                var value = getdeviceKeysByUser(arr, function (data) {
                    pushNotifications(value, "New Task has been Created", "Task Created", {
                      data: "New Task Created"
                    });
                  });
              }

             // }
             

              res.json({
                "message": "Task created successfully",
                "status": 200
              })
              })
             

          }
        }

        //   })

      });
    }
  });
});





//Get List of Tasks
router.get('/getTasks/:usergroup', function(req, res, next) {
    Tasks.find({userGroup: req.params.usergroup,isDeleted: false}).sort({createdDateTime:-1}).exec(function(err, post) {
        //if (err) return next(err);
        if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else

        {
            res.json({
                "message": "No tasks avaliable",
                "status": 204
            });
        }

    });
});

/*router.get('/getTasks/:usergroup', function(req, res, next) {
    Tasks.find({userGroup: req.params.usergroup,isDeleted: false}, function(err, post) {
        //if (err) return next(err);
       
        if (post.length > 0) {
           console.log(post);
           var taskobjectsArray =[];
        async.forEachSeries(post,function(taskObject,next) {
          Groups.find({_id:taskObject.userGroup}, function(err,group) {
            if(group.length>0) {
              console.log(group[0].name);
              var taskObjectFormation ={};
              taskObjectFormation.name = taskObject.name;
              taskObjectFormation.userGroup = taskObject.userGroup;
              taskObjectFormation.createdBy = taskObject.createdBy;
              taskObjectFormation.assignedUsers = taskObject.assignedUsers;
              taskObjectFormation.assignedFormsz = taskObject.assignedFormsz;
              taskObjectFormation.assigned = taskObject.assigned;
              taskObjectFormation.assignedGroupadmin = taskObject.assignedGroupadmin;
              taskObjectFormation.description = taskObject.description;
              taskObjectFormation.createdDateTime = taskObject.createdDateTime;
              taskObjectFormation.startDate = taskObject.startDate;
              taskObjectFormation.endDate = taskObject.endDate;
              taskObjectFormation.workInstruction = taskObject.workInstruction;
              taskObjectFormation._id = taskObject._id;
              taskObjectFormation.isAllowMap = taskObject.isAllowMap;
              taskObjectFormation.userGroupName = group[0].name;
              console.log(taskObjectFormation);
              taskobjectsArray.push(taskObjectFormation);
              console.log(taskobjectsArray);
              next();

            }
          })

        },function(err) {
          res.json({"data":taskobjectsArray,"status":200});
        })
*/
           /* res.json({
                "data": post,
                "status": 200
            });*/
       // } else

        /*{
            res.json({
                "message": "No tasks avaliable",
                "status": 204
            });
        }

    });
});*/
//get tasks for Group Admin @developer:renuka
router.get('/getTasksGA/:groupadminId',function(req,res,next){
   Tasks.find({'assignedGroupadmin.groupadminId':req.params.groupadminId},function(err,post){
    if (post.length > 0) {
      res.json({
          "data": post,
          "status": 200
      });
    }
    else{
      res.json({
        "message": "No tasks avaliable",
        "status": 204
      });
    }

   })
});
//Get List of Tasks by user
router.get('/getTasksbyUser/:user', function(req, res, next) {
    Tasks.find({'assignedUsers.userId': req.params.user,"isClosed":false}).sort({createdDateTime:-1}).exec(function(err, tasks) {
      var data = [];
      if(tasks.length>0)
      {
        async.forEach(tasks, function (task){
          getData(task,function(ress){
            data.push(ress);
          })
        })
        setTimeout(function () {
          res.json({
            "data" : data,
            "status" : 200
          })
        }, 1000);
            }
            else {
              res.json({
                  "message": "No tasks avaliable",
                  "status": 204
              });

            }
          });
      });
/*
@desc 
        Work instruction, references field and value added to the  existing array
     * @author Santhosh Kumar Gunti
*/
function getData(task,callback) {
  var jsondata={};
  var forms=[];
  var formids=[];
   form= task.assignedFormsz;
   for(var i=0;i<form.length;i++) {
     formids.push(form[i].formId);
   }
  Formsz.find({_id:{$in:formids},isVisible:true}, function(err,formsz){
    var formdata =  {};
    var formArray = [];
    var formdetails = {};
        if(formsz.length>0) {
      formsz.forEach(function(form){
        formdata.formId = form._id;
        formdata.formszCategory = form.formzCategory;
        formdata.formszDescription = form.description;
        formdata.formName=form.name;
        formdata.formWorkInstruction=form.workInstruction;
        formdata.references=form.references;
        formdata.version = form.version;
        formArray.push (formdata);
        formdata = {};
      });
      task.assignedFormsz=formArray;
      jsondata = task;
      formArray = [];
        callback(jsondata);
    }
    else {
        console.log("no forms available to this task");
    }
  });
}

router.get('/getTasksbyformsz/:taskid/:user', function(req, res, next) {
    Tasks.find({_id: req.params.taskid,'assignedUsers.userName': req.params.user,'assigned.user':req.params.user}, function(err, tasks) {
      if (tasks.length > 0) {
          var formszSkalton = {}
          var forms = [];
         var ids = [];
         var formIds = [];
         ids = tasks[0].assigned;
         for(var i = 0;i<ids.length;i++) {
           if(ids[i].user == req.params.user) {
             formIds.push(ids[i].form);
           }
         }
         Formsz.find({_id:{$in:formIds}}, function(err, post1) {
            if(post1.length>0) {
              post1.forEach(function(formszdate) {
                  formszSkalton.formName = formszdate.name;
                  formszSkalton.formId = formszdate._id;
                  formszSkalton.formzCategory = formszdate.formzCategory;
                  formszSkalton.formszDescription = formszdate.description
                  forms.push(formszSkalton);
                  formszSkalton = {};
              });
              res.json({
                  "data": forms,
                  "status": 200
              });
            }
            else {
              res.json({"message":"No forms associated with this Task"})
            }
          });
       }
       else
        {
            res.json({
                "message": "No tasks avaliable",
                "status": 204
            });
        }
      });
    });


router.get('/getUsersAndFormsAssinegdToTask/:taskid', function(req, res, next) {
    var json = {};
    Tasks.distinct("assignedUsers",{_id: req.params.taskid}, function(err, users) {
    if(users.length>0) {
          json.users = users;
    Tasks.distinct("assignedFormsz",{_id: req.params.taskid}, function(err,forms) {
      if(forms.length>0) {
        json.forms = forms;
        res.json({"data":json,"status":200})
      }
      else {
        console.log(err);
      }
    })
    } 
        else {
    res.json({
                "status": 204,
                message: " Task dosn't exists"
            });   
    }
            
    });

});


router.get('/getformszDetail/:formid/:user/:fromdate/:todate/:taskid', function(req, res, next) {
    var updatedby = req.params.user;
    if (updatedby == "All") {
        updatedby = {
            $ne: null
        }
    }
    Formsz.find({_id: req.params.formid,isVisible: true}, function(err, post) {
        if (post.length > 0) {
            FormszDetails.find({formId: req.params.formid,isDeleted: false,updatedTime: {$gte: new Date(req.params.fromdate),$lte: new Date(new Date(req.params.todate).setDate(new Date(req.params.todate).getDate() + 1))},updatedBy: updatedby, taskId: req.params.taskid}, function(err, post1) {
                if (post1.length > 0) {
                    var userdata = [];
                    var recordIds = [];
                    var formInfo = {};
                    var reassinegdReocrdIds = [];
          var recordinfo = [];
                    formInfo.formId = post[0]._id;
                    formInfo.createdBy = post[0].createdBy;
                    formInfo.createdTime = post[0].createdTime;

                    post1.forEach(function(dbUserObj1) {
            var lat = "";
            var long ="";
             lat = dbUserObj1.lat;
             long = dbUserObj1.long;
                        if (dbUserObj1.IsReassign) {
                            reassinegdReocrdIds.push(dbUserObj1._id);
                        }
            dbUserObj1.record[0]._id = dbUserObj1._id;
                        userdata.push(dbUserObj1.record[0]);
                        recordIds.push(dbUserObj1._id);
            /* comment.push({
              "id" : dbUserObj1._id,
              "Comments" : dbUserObj1.comments,
              "updatedBy" : dbUserObj1.updatedBy,
              "assignedTo" : dbUserObj1.assignedTo,
            }); */
            recordinfo.push({
              "UpdatedBy" : dbUserObj1.updatedBy,
              "UpdatedTime" : dbUserObj1.updatedTime,
              
              "InsertedLocation" :  lat +","+ long  
            });
                    });
                    res.json({
                        Skelton: post[0].FormSkeleton,
                        records: userdata,
                        recordIdsList: recordIds,
                        formInfo: formInfo,
                        reassignRecordIds: reassinegdReocrdIds,
            "recordInfo" : recordinfo
                    });

                    //res.json({Skelton:post[0].FormSkeleton,records:userdata});
                } else {
                    res.json({
                        "message": "No Data Found",
                        "status": 204
                    });
                }
            });
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });

        }
    });
});
/*
@Type: Modified 
@developer: Santhosh Kumar Gunti

@description : Filter added for sending shared group forms for group admin 11-04-2018
*/
//Get List of User and Formsz
router.get('/getTaskUsersFormszs/:groupname', function(req, res, next) {
    var data = {};
    var formszlist = [];
    var userslist = [];
    var groupadminlist=[];
    Formsz.find({
      $or:[{userGroup    : req.params.groupname}, {shareGroup:{ $elemMatch: { _id: req.params.groupname } }}],
//      userGroup: req.params.groupname,
      isVisible: true,formType:"form",formzCategory:{$ne:[]},requiredField:{$ne:[]}}, {name: 1}, function(err, post1) {
        if (post1.length > 0) {
            post1.forEach(function(formszdata) {
                formszlist.push({"formId":formszdata._id,"formName":formszdata.name});

            });
            User.find({
               groupname: {$elemMatch:{_id:req.params.groupname}},
               isDeleted: false,
               //type: 2
               type: 3
            }, {
                username: 1
            }, function(err, post) {
                post.forEach(function(groupadmindata) {
                    groupadminlist.push({groupadminId:groupadmindata._id,groupadminName:groupadmindata.username});
                });
            });
            User.find({
               groupname: {$elemMatch:{_id:req.params.groupname}},
               isDeleted: false,
               // type: 2
               type: 2
            }, {
                username: 1
            }, function(err, post) {
                post.forEach(function(userdata) {
                    userslist.push({userId:userdata._id,userName:userdata.username});
                });
                data.allForms = formszlist;
                data.allUsers = userslist;
                data.allgroupadmin = groupadminlist;
                res.json(data);
            });
        } else {
            User.find({
               groupname: {$elemMatch:{_id:req.params.groupname}},
               isDeleted: false,
               // type: 2
               type: 3
            }, {
                username: 1
            }, function(err, post) {
                post.forEach(function(groupadmindata) {
                    groupadminlist.push({groupadminId:groupadmindata._id,groupadminName:groupadmindata.username});
                });
            });
            User.find({
                groupname: req.params.groupname,
                isDeleted: false,
               // type: 2
               type: 2
            }, {
                username: 1
            }, function(err, post) {
                post.forEach(function(userdata) {
                    userslist.push({userId:userdata._id,userName:userdata.username});
                });
                data.allForms = formszlist;
                data.allUsers = userslist;
                data.allgroupadmin = groupadminlist;
                res.json(data);
            });
            //res.json(data);
        }
    });

});
//Delete Task
router.delete('/deleteTask/:id', function(req, res, next) {
  FormszDetails.find({taskId:req.params.id,IsReassign:true}, function(err, tasks){
    if(tasks.length>0) {
      res.json({"message":"Task cannot be deleted,as it is in progress.","status":208})
    }
    else {
       Tasks.findByIdAndRemove(req.params.id, req.body, function(err, post) {
        if (!err) {
            /*var arr = [];
            var value = post.assignedUsers;
            for (var i = 0; i < value.length; i++) {
                arr.push(value[i].userId);
            }
             var value = getdeviceKeysByUser(arr, function(data) {
                 pushNotifications(value, "Task has been Deleted", "Task has been Deleted", {
                   data: "Task Deleted"
                 });
             });*/

            res.json({
                "status": 200,
                "message": "Task Deleted Successfully"
            });
        }
    });
      
    }
  })
   
});

//get datasheet assigned to User in task
router.get('/getRecordsforUser/:taskid/:username/:formid', function(req,res,next) {
  
   FormszDetails.find({$or:[{formId:req.params.formid,DSAssignedTo:req.params.username,taskId:req.params.taskid,isDeleted:false}, {taskId:req.params.taskid,formId:req.params.formid,updatedBy:req.params.username,prepopulatedRecord:{$ne:0}}] }, function (err,details) {
    if(details.length>0) {
      var array = []
      var data ={}
      details.forEach(function(dbobj){
        //console.log(dbobj)
        data.recordId = dbobj._id;
        data.prepopulatedRecord = dbobj.prepopulatedRecord
        array.push(data)
        data={}
      })
      res.json({data:array,status:200})
    } 
    else {
      res.json({"message":"No records found","status":204})
    }
  });
  
});

// Start of Change by Naveen Adepu 
// Change for TPCL for xlsx upload



 var fileName;
var storage = multer.diskStorage({ //multers disk storage settings
  destination: function (req, file, cb) {
    cb(null, './public/')
  },
  filename: function (req, file, cb) {
    var datetimestamp = Date.now();
    fileName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]
    cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});
var upload = multer({ //multer settings
        storage: storage
      }).single('file');
        

 

router.post('/convertexcel/:groupId', function(req, res, next) {

  startimestamp = Date.now();
   upload(req,res,function(err){
    parseXlsx('public/'+fileName, function(err, data) {      
      if(err) throw err;        
      console.log( "The file was saved!",Date.now()-startimestamp);
      createvalidators(data,req.params.groupId,function(response){
        fs.unlink('public/'+fileName);
        res.json({"data":response,"status":200})
      });
    });
  })     
});

router.post('/convertexcel', function(req, res, next) {

  startimestamp = Date.now();
   upload(req,res,function(err){
    parseXlsx('public/'+fileName, function(err, data) {      
      if(err) throw err;        
      console.log( "The file was saved!",Date.now()-startimestamp);
      createvalidators(data,"",function(response){
        fs.unlink('public/'+fileName);
        res.json({"data":response,"status":200})
      });
    });
  })     
});

//-------------------------------------------------------------
//Start Change by Naveen Adepu 16-Apr-2018
// Modified to fix number format issue in prepop excel upload.
// router.post('/xlsxtojsontemp', function(req,res,next){
//     startimestamp = Date.now();
//     upload(req,res,function(err){
//         parseXlsx('public/'+fileName, function(err, data) {  
//             console.log("dataaaaaaaaaa")
//             console.log(data)  
//             console.log(err)        
//             if(err) throw err;     
//             createjsonformwidgets(data,function(response){
//               fs.unlink('public/'+fileName);
//                 res.json({"data":response,"status":200})              
//             });    
//         });
//     })  
// });
router.post('/xlsxtojsontemp', function(req,res,next){

    startimestamp = Date.now();
    upload(req,res,function(err)
    { 
      //console.log("req.file.path",req.file.path)
      try {
            xlsxtojson(
            {
            input: req.file.path, //the same path where we uploaded our file
            output: null, //since we don't need output.json
            }, 
            function(err,result)
            {
              if(err) {
                        return res.json({error_code:1,err_desc:err, data: null});
                      }
              createFromWidgets(result,function(response)
              {
                fs.unlink('public/'+fileName);
                res.json({"data":response,"status":200})              
              });
                 //   res.json( result);
                });
            } 
            catch (e){
                res.json({"status":203,"data":"Corupted excel file"});
            } 
    })  
});
// End of Change by Naveen Adepu 16-Apr-2018

// Start of Change By Naveen Adepu 11-Apr-2018

function createFromWidgets(data,callback){
  console.log(data)
    var jsondata=[];
    var count =0;
  if(data.length>1){
    for (var i = 0; i <= data.length-1; i++) {
      count++;
      var record={};
      var datetimestamp = Date.now();
      datetimestamp = datetimestamp+"-"+count;
      console.log("records ....")
      console.log("data[i].type",data[i].Type)  

      if(data[i].Type=="textBox") {
        var lable = 'TextBox Name';
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        
        if( data[i]["Required"] === "true"){
          
          record.required = true;
        }else{
          record.required=false;
        }
    //   record.required = data[i][6];
        record.placeHolder = "";
        record.isPrimary = false;
        record.readOnly = false;
        record.defaultValue = data[i]["Default value"];
        record.inputType = "Text";
        record.minInputVal = "";
        record.maxInputVal = "";
        record.isInptAllowDecimals = false;
        record.type = {"view":""}; 
        record.type.view = "textbox";
      }
      else if(data[i].Type=="textArea"){
        var lable = "Textarea Name";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        if( data[i]["Required"] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.placeHolder = "";
        record.isPrimary = false;
        record.minInputVal = "";
        record.maxInputVal = "";
        record.type = {"view":""};
        record.type.view = "textarea";
      }
      else if(data[i].Type=="radio"){ 
        var valu_arr = [];
         if(data[i]["Option values"]!=""){
           var result = data[i]["Option values"].slice(1, -1).split(",");
            var internalValues = data[i]["Option internal values"].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= internalValues[k];
             valu_arr.push(value);
           }
           lable = "Radiobutton Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i]["Lable name"];
           record.isPrimary = false;
           record.defaultValue = data[i]["Default value"];
           if( data[i]["Required"] === "true"){
             record.required = true;
            }else{
              record.required=false;
            }
         //  record.required = data[i][6];
           record.type = {"view":"radio","values":valu_arr}
        }       
 
      }
      else if(data[i].Type=="select"){
         var valu_arr = [];
         if(data[i]["Option values"]!=""){
           var result = data[i]["Option values"].slice(1, -1).split(",");
           var internalValues = data[i]["Option internal values"].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= internalValues[k];
             valu_arr.push(value);
           }
           lable = "Dropdown Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i]["Lable name"];
           if( data[i]["Required"] === "true"){
             record.required = true;
            }else{
              record.required=false;
            }
           //record.required = data[i][6];
           record.placeHolder = "";
           record.isPrimary = false;
           record.defaultValue = data[i]["Default value"];
           record.isAllowMultiselection = false;
           record.type = {"view":"select","values":valu_arr}
        }       
      }
      else if(data[i].Type=="checkBox"){
         var valu_arr = [];
         if(data[i]["Option values"]!=""){
           var result = data[i]["Option values"].slice(1, -1).split(",");
           var internalValues = data[i]["Option internal values"].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= internalValues[k];
             valu_arr.push(value);
           }
           lable = "Checkbox Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i]["Lable name"];
           record.isPrimary = false;
           record.defaultValue = data[i]["Default value"];
           if( data[i]["Required"] === "true"){
            record.required = true;
            }else{
              record.required=false;
            }
        //record.required = data[i][6];
           record.type = {"view":"checkbox","values":valu_arr}
        }
      }
      else if(data[i].Type=="map"){
         var lable = "Location";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
           if( data[i]["Required"] === "true"){
            record.required = true;
            }else{
              record.required=false;
            }
                    //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "map";
      }
      else if(data[i].Type=="barcode"){
        var lable = "barcode";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
         if( data[i]["Required"] === "true"){
        record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.placeHolder = "";
        record.type = {"view":""};
        record.type.view = "barcode";
      }
      else if(data[i].Type=="calender"){
        var lable = "Date";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        record.isPrimary = false;
        record.minDate = "";
        record.maxDate = "";
        record.defaultValue = data[i]["Default value"];
        record.placeHolder = "";
        record.type = {"view":""};
        record.type.view = "calender"; 
/*
@Developer : SKG
@Date : 3/5/2018
@Description : added typeOfDateSelected to calender while creating form throgh prepop

*/
record.typeOfDateSelected = "Manual Entry";
      }
      else if(data[i].Type=="camera"){
        var lable = "Image Name";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        if( data[i]["Required"] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }// record.required = data[i][6];
        record.imageSize = "";
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "camera"; 
      }
      else if(data[i].Type=="sign"){
        var lable = "Signature";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        if( data[i]["Required"] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }// record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "sign"; 
      }
      else if(data[i].Type=="video"){
        var lable = "video";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        if( data[i]["Required"] === "true"){
          record.required = true;
        }else{
            record.required=false;
        }
        record.videoDuration = "20";
        record.type = {"view":""};
        record.type.view = "video"; 
        record.isPrimary = false;
      }
      else if(data[i].Type=="rating"){
        var lable = "Rating";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        record.min = 0;
        record.max = 5;
        record.minLable = "Poor";
        record.maxLable = "Excellent";
        record.defaultValue = data[i]["Default value"];
       if( data[i]["Required"] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "rating"; 
      }
      else if(data[i].Type=="mapInteraction"){
        var lable = "Map Interaction Field";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        record.required = false;
        record.isPrimary = false;
        
        record.type = {"view":""};
        record.type.view = "mapInteractionField"; 
      }
      else if(data[i].Type=="goto"){
        var lable = "Goto Field";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i]["Lable name"];
        if( data[i]["Required"] === "true"){
        record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "goto"; 
      }
      //-->SivaKella: Added: To ignore all invalid TYPE data in excel
      else {
        console.log("NULL CASE...");
        continue;
      }
      //End

  //     
 //console.log(data[i][0])
   if(data[i]["Section"]!=""){
        // To handle section and group fields
        var sectrecord = {};
        var secflag = false;
        var secgrpflag = false;
        if(data[i]["Group"]!=""){
        // For Section-Group fields..
          loopforsecetionrecord : for( var s = 0 ; s < jsondata.length; s++){
            if(jsondata[s].lable == data[i]["Section"]){
              // get the Section record if already created
              secflag = true;
              var secgrprecflag = false;
              loopforsectiongrprecord : for(var g = 0 ; g < jsondata[s].type.fields.length; g++){
                if(jsondata[s].type.fields[g].type=="group"){
                  if(jsondata[s].type.fields[g].data.lable == data[i]["Group"]){
                    // get the created group of section
                    jsondata[s].type.fields[g].data.type.fields.push(record);
                    secgrprecflag = true;
                    break loopforsectiongrprecord;
                  }

/*                  else{
                    // create group for section
                    var grprecord = {};
                    grprecord.type = "group";
                    grprecord.data={};
                    grprecord.data.lable = data[i][1];
                    grprecord.data.type={};
                    grprecord.data.type.view = "group";
                    grprecord.data.type.fields = [];
                    grprecord.data.type.fields.push(record);
                    // add group record to section
                    jsondata[s].type.fields.push(grprecord);
                    break loopforsectiongrprecord;
                  }*/
                }


              }
              if(!secgrprecflag) {
                    // create group for section
                    var grprecord = {};
                    grprecord.type = "group";
                    grprecord.data={};
                    grprecord.data.lable = data[i]["Group"];
                    grprecord.data.type={};
                    grprecord.data.type.view = "group";
                    grprecord.data.type.fields = [];
                    grprecord.data.type.fields.push(record);
                    // add group record to section
                    jsondata[s].type.fields.push(grprecord);                
                
              }
              break loopforsecetionrecord;
            }
          }
        }else{
          // For Section fields
          for( var s = 0 ; s < jsondata.length; s++){
            if(jsondata[s].lable == data[i]["Section"]){
              // get the Section record if already created
              secflag = true;
              var sectionfieldrec = {};
              sectionfieldrec.type = "field";
              sectionfieldrec.data = record;
              jsondata[s].type.fields.push(sectionfieldrec)
            }
          } 
        }

        if(!secflag){
          // first time creation of section record with group or direct section field
          var sectionrecord = {};
          sectionrecord.lable = data[i]["Section"];
          sectionrecord.type={ "view": "section","fields":[]};
          if(data[i]["Group"]!=""){
            // first time group record
            var grprecord = {};
            grprecord.type = "group";
            grprecord.data={};
            grprecord.data.lable = data[i]["Group"];
            grprecord.data.type={};
            grprecord.data.type.view = "group";
            grprecord.data.type.fields = [];
            grprecord.data.type.fields.push(record)
            sectionrecord.type.fields.push(grprecord);
          }else{
            // firt time field record 
            var sectionfieldrec = {};
            sectionfieldrec.type = "field";
            sectionfieldrec.data = record;

            sectionrecord.type.fields.push(sectionfieldrec);
          }
          jsondata.push(sectionrecord);
        }
      }




      else if(data[i]["Group"]!==""){
        // Handles direct group fields
        var grprecord = {};
        var grpflag = false;
        grouploop:    for (var q = 0; q < jsondata.length; q++) {
          if( jsondata[q].lable == data[i]["Group"]){
            grprecord = jsondata[q];
            grpflag = true;
            grprecord.type.fields.push(record);
            break grouploop;
          }
        }
        if(!grpflag){
          grprecord.lable = data[i]["Group"];
          grprecord.type = {"view":"group"};
          grprecord.type.fields = [];
          grprecord.type.fields.push(record);
          jsondata.push(grprecord);
        }
      }
      else if(data[i]["Group"]==""){
        // Handles direct Fields
        jsondata.push(record);
      }
    }
  } 
  console.log("jsondata.... done here...");
//  console.log(jsondata);

  callback(jsondata);
}


// End of Change by Naveen Adepu 11-Apr-2018
//-----------------------------------------------------------

//oob task new servicec start by venki


// router.get('/getTaksForUser/:userId', function(req, res, next) {
   
//    Tasks.find({
//         assigned:{$elemMatch:{user:{$in:[req.params.userId]}}},
//     }, function(err, post) {
//         //if (err) return next(err);
//         if (post.length > 0) {
//             res.json({
//                 "data": post,
//                 "status": 200
//             });
//         } else {
//             res.json({
//                 "message": "No Tasks available,Please create new task",
//                 "status": 204
//             });
//         }

//     });

  
// });


router.get('/getTaksForUser/:userId', function(req, res, next) {
  console.log("9999999")
    var projectLsit = []
    var zoneList =[]
    
    Tasks.find({
         assigned:{$elemMatch:{user:{$in:[req.params.userId]}}},
         isDeleted:false
    },{name:1,description:1,startDate:1,endDate:1,createdBy:1,userGroup:1}, function(err, post) {
      var tasks =[];

      /*
        @Date : 21st april 2018
        @Developer Santhosh Kumar Gunti
        @Description: sending created by id instead created by name 
      */
      //START
            async.forEachSeries(post, function(obj, nextIn) {
              console.log(obj.userGroup)
                  User.find({
                     "groupname._id": obj.userGroup,
                     isDeleted: false,
                     type: 1
                  }, {
                      username: 1
                  }, function(err, post1) {
                    obj['createdBy'] = post1[0].username;
                  nextIn();
                  });
            },function(){
                  res.json({
                            "data": post,
                            "status": 200
                  });
            });
            //END

    });
});


/*
@Type: Modified 
@developer: Santhosh Kumar Gunti
@description : geting the current version of form from versionMangement table and sending it to mobile to show version of the form 
*/

/*router.get('/getTaksFromsForUser/:taskId/:userId', function(req, res, next) {
    
      Tasks.find({
           assigned:{$elemMatch:{user:{$in:[req.params.userId]}}},
           _id : req.params.taskId,
            isDeleted:false
          },{assigned:1,startDate:1,endDate:1},function(err,taskAssingments){
              
              if(taskAssingments.length > 0)
              {
                Formsz.find({
                  _id : taskAssingments[0].assigned[0].form
                },function(err,formDetails){

                    versionManage.find({formId : taskAssingments[0].assigned[0].form} ,function(err,vm){
                      taskAssingments[0].assigned[0]['version'] = vm[0].version+1;
                      taskAssingments[0].assigned[0]['category'] = formDetails[0].formzCategory.map(a => a.name).toString();
                      taskAssingments[0].assigned[0]['description'] = formDetails[0].description;
                      res.json({
                          "data": taskAssingments,
                          "status": 200
                      });
                   }).sort({ createdTime : -1 }).limit(1);
               });
              }
              else
              {
                res.json({
                      "message": "No forms are available",
                      "status": 204
                  });
              }
              
          })  
    
    
    
   
});
*/

/*
@Type: Modified 
@developer: Santhosh Kumar Gunti
@description : geting the current version of form from versionMangement table and sending it to mobile to show version of the form 
*/

router.get('/getTaksFromsForUser/:taskId/:userId', function(req, res, next) {
    var response = [];
    Tasks.find({assigned:{$elemMatch:{user:{$in:[req.params.userId]}}},
           _id : req.params.taskId,
            isDeleted:false
          },{_id:1,assigned:1,startDate:1,endDate:1},function(err,post){
            var assignedForms =[];
            var taskData ={};
            async.forEachSeries(post[0].assigned, function(obj, nextIn) {
              taskData ={};
              taskData._id = post[0]._id;
              taskData.startDate = post[0].startDate;
              taskData.endDate = post[0].endDate;
              if(obj.user == req.params.userId){
                    Formsz.find({
                      _id : obj.form
                    },function(err,formDetails){
                      versionManage.find({formId : obj.form} ,function(err,vm){
                          obj['version'] = vm[0].version+1;
                          obj['category'] = formDetails[0].formzCategory.map(a => a.name).toString();
                          obj['description'] = formDetails[0].description;
                          assignedForms.push(obj);
                          nextIn();
                      }).sort({ createdTime : -1 }).limit(1);
                   });
              }
              else
              {
                   nextIn();
              }
         },function(){
              taskData.assigned = assignedForms;
              response.push(taskData);
              res.json({
                      "data": response,
                      "status": 200
              });
           });
    });
});

router.get('/getRecordsOfDirectTaskForm/:taskId/:formId/:userName', function(req, res, next) {
  var records =[] 
  var formDisplayfields = [];
  var workIns ;
  var workReferences;
  FormszDetails.find
  ( {$or:[{ formId : req.params.formId,taskId : req.params.taskId,DSAssignedTo  : req.params.userName,status:false},
          { formId : req.params.formId,taskId : req.params.taskId,assignedTo: req.params.userName,IsReassign:true
          }
    ]},{prepopulatedRecord:1,IsReassign:1,record:1,comments:1},function(err,projectTaskAssingments){
        console.log(projectTaskAssingments)
          Formsz.find({_id:req.params.formId},{requiredField:1,workInstruction:1,references:1,_id:0,FormSkeleton:1,dependentFields:1},function(err,form)
          {
              
            versionManage.find({formId : req.params.formId} ,function(err,vm){
              version = vm[0].version
        

              formDisplayfields = form[0].requiredField; 
              formSkelton =  form[0].FormSkeleton;

              workIns =  form[0].workInstruction
              workReferences = form[0].references
              var obj ={}
              obj.formSkelton = formSkelton;
              obj.records = projectTaskAssingments;
              obj.displayValues = formDisplayfields;
              obj.workInstruction = workIns;
              obj.version = version;
              obj.dependentFields = form[0].dependentFields;
              obj.references = workReferences;
              res.json({
                      "data": obj,
                      "status": 200
                  });

            }).sort({ createdTime : -1 }).limit(1);

              
          })
          
            
    })  
    
    
   
});


router.post('/addTaskRecord', function(req,res,next){
  
  //console.log("reqreqreqreq")
  console.log(req.body)
  req.body.status = true;
  
  //check already record got submitted by other users
  if(req.body.recordId == undefined || req.body.recordId == null )
  { 
        //console.log("heeeee")
        FormszDetails.create(req.body, function(err, data)
        {   
          // Who : SV
          // Date : 25-04-2018
          // Added record id to the response
          // Begin
          res.json({"message": "Form submited successfully","status": 200,"recordId": data._id});
          // End

        });
  }
  else
  { 
    
      FormszDetails.update({_id:req.body.recordId}, {$set: {'assignedTo':null,'record':req.body.record,'IsReassign':false,'DSAssignedTo':null,'status':true,'updatedBy':req.body.updatedBy,lat:req.body.lat,long:req.body.long,generatedId:req.body.generatedId}}, function(err, doc){
          // Who : SV
          // Date : 25-04-2018
          // Added record id to the response
          // Begin
          res.json({"message": "Form submited successfully","status": 200,"recordId":req.body.recordId});
          // End
                 
      });

  }
  

    
});


router.get('/downloadDirectTask/:taskId', function (req, res, next) {
    var data = [];
    Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
    if (Tasks.length > 0) {
      getTaskInfo(req.params.taskId,function(taskInfo)
      {   
        //getProjectInfo(taskInfo.projectID,function(projectInfo){
            
             taskInfo = taskInfo.toJSON()
           
              var response = {};
              var taskFormsList = [];
              response.taskInfo = JSON.stringify(taskInfo);
              //var taskInfoJson = JSON.parse(taskInfo)

            Tasks.find({
                 _id : req.params.taskId,
                  isDeleted:false
                },{assigned:1,startDate:1,endDate:1},function(err,taskAssingments){
                    console.log("taskAssingments")
                    
                    for (var i = 0; i < taskAssingments[0].assigned.length; i++) {
                      var obj = {};
                      obj.formId = taskAssingments[0].assigned[i].form;
                      obj.fileAssigned =taskAssingments[0].assigned[i].fileAssigned
                     taskFormsList.push(obj);
                    };

                    response.taskFormsList = taskFormsList;
                    console.log(response)
                    res.json(response);

              
            })  
    


            /*  taskAssignments.find({
                taskId : taskInfo._id
              },{formId:1,_id:0},function(err,taskForms){
                   for (var i = 0; i < taskForms.length; i++) {
                     taskFormsList.push(taskForms[i].formId)
                   };
                   response.taskFormsList = taskFormsList
                   res.json(response);
              })*/

        //})
         
          
        
      })
        

 
    } else {
      res.json({
        "message" : "No data found",
        "status" : 204
      });
    }
  });
});

function getTaskInfo(taskId,callback)
{
  Tasks.find({_id:taskId},function(err,taskInfo)
  {
    callback(taskInfo[0])

  })
}

function downloadForm(id,taskid, user, callback) {
 

  Formsz.find({_id : id}, function (err, formsz) {
    callback(formsz[0])
   /* getFrequencyOfForm(id,taskid, user,function(formFrequency){
       callback(formsz[0],formFrequency)
    })*/
   
  });
}

function getPrepoRecordsOfForm(formId,taskId,userId,callback)
{ 

  
  var directTaskPrepopRecords = [];
    FormszDetails.find
    ( { formId : formId,taskId : taskId,DSAssignedTo : userId,status:false},{prepopulatedRecord:1,IsReassign:1,record:1,comments:1},function(err,projectTaskAssingments){
      
      console.log("lengthttttttt");
      console.log(projectTaskAssingments);
      if(projectTaskAssingments.length>0)
      { 
          for (var i = 0; i < projectTaskAssingments.length; i++) {
            
            var obj ={};
            obj.recordId = projectTaskAssingments[i]._id;
            obj.record =  projectTaskAssingments[i].prepopulatedRecord[0]

            directTaskPrepopRecords.push(obj)
          }

          callback(directTaskPrepopRecords);
      }
      else
      {
          callback([]);
      }
          
    });

   
}

function getReassignRecordsOfForm(formId,taskId,userId,callback)
{ 

  
  var directTaskPrepopRecords = [];
    FormszDetails.find
    ( { formId : formId,taskId : taskId,assignedTo: userId,IsReassign:true
      },{IsReassign:1,record:1,comments:1},function(err,projectTaskAssingments){
      
      console.log("lengthttttttt");
      console.log(projectTaskAssingments);
      if(projectTaskAssingments.length>0)
      { 
          for (var i = 0; i < projectTaskAssingments.length; i++) {
            
            var obj ={};
            obj.recordId = projectTaskAssingments[i]._id;
            obj.record =  projectTaskAssingments[i].record[0]
            obj.comments = projectTaskAssingments[i].comments

            directTaskPrepopRecords.push(obj)
          }

          callback(directTaskPrepopRecords);
      }
      else
      {
          callback([]);
      }
          
    });

   
}





router.get('/downloadService/:taskId/:formId/:user', function (req, res, next) {
    var data = [];
    Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
      console.log("111111111")
    if (tasks.length > 0) 
    {
      console.log("2222222222222")
      getTaskInfo(req.params.taskId,function(taskInfo)
      {      taskInfo = taskInfo.toJSON()
             downloadForm(req.params.formId, req.params.taskId, req.params.user, function (formInfo) {
                  getPrepoRecordsOfForm(req.params.formId, req.params.taskId, req.params.user,function (prepopRecords){
                      getReassignRecordsOfForm(req.params.formId, req.params.taskId, req.params.user,function (reassignRecords){
                            var response = {};
                            formInfo = formInfo.toJSON();
                            console.log(formInfo)
                            console.log("formInfo")
                            response.formInfo = JSON.stringify(formInfo);
                            response.taskInfo = JSON.stringify(taskInfo);
                            response.prepopRecords = prepopRecords;
                            response.reassingRecords = reassignRecords;
                            console.log("mobile")
                            console.log(response)
                            res.json(response)
                      })
                    
                    }) 
               
               

            });
        
         
      })
    } else {
      res.json({
        "message" : "No data found",
        "status" : 204
      });
    }
  });
});



router.get('/getHistroy/:formid/:taskId/:userId', function (req, res, next) {
  var updatedby = req.params.user;
  var records =[];
  Formsz.find({
    _id : req.params.formid,
    isVisible : true
  }, function (err, post) {
    if (post.length > 0) {
      FormszDetails.find({formId :req.params.formid, taskId : req.params.taskId, updatedBy:req.params.userId,IsReassign:false},{record:1,updatedTime:1,generatedId:1},function (err, post1) {
	   if (post1.length > 0) {
          for (var i = 0; i < post1.length; i++) {
            var obj ={}
            delete post1[i].record[0].RUID
            obj.record = post1[i].record[0]
            obj.recordId = post1[i]._id
            obj.generatedId = post1[i].generatedId
            obj.updatedTime = post1[i].updatedTime
            
            records.push(obj)
          };

          res.json({
            data:records,
            "status" :200
          })

        } else {
          res.json({
            "message" : "No Data Found",
            "status" : 204
          });
        }
      });
    } else {
      res.json({
        "message" : "No Data Found",
        "status" : 204
      });

    }
  });
});

//oob task new servicec end

//Code added by Siva on 14-03-2018
function createjsonformwidgets(data,callback){
    var jsondata=[];
    //console.log(data);
    var count =0;
  if(data.length>2){
    for (var i = 1; i <= data.length-1; i++) {
      count++;
      var record={};
      var datetimestamp = Date.now();
      datetimestamp = datetimestamp+"-"+count;
      
      if(data[i][3]=="textBox") {
        var lable = 'TextBox Name';
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        
        if( data[i][6] === "true"){
          
          record.required = true;
        }else{
          record.required=false;
        }
    //   record.required = data[i][6];
        record.placeHolder = "";
        record.isPrimary = false;
        record.readOnly = false;
        record.defaultValue = data[i][4];
        record.inputType = "Text";
        record.minInputVal = "";
        record.maxInputVal = "";
        record.isInptAllowDecimals = false;
        record.type = {"view":""}; 
        record.type.view = "textbox";
      }
      else if(data[i][3]=="textArea"){
        var lable = "Textarea Name";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
                if( data[i][6] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.placeHolder = "";
        record.isPrimary = false;
        record.minInputVal = "";
        record.maxInputVal = "";
        record.type = {"view":""};
        record.type.view = "textarea";
      }
      else if(data[i][3]=="radio"){ 
        var valu_arr = [];
         if(data[i][5]!=""){
           var result = data[i][5].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= result[k];
             valu_arr.push(value);
           }
           lable = "Radiobutton Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i][2];
           record.isPrimary = false;
           record.defaultValue = data[i][4];
           if( data[i][6] === "true"){
             record.required = true;
            }else{
              record.required=false;
            }
         //  record.required = data[i][6];
           record.type = {"view":"radio","values":valu_arr}
        }       
 
      }
      else if(data[i][3]=="select"){
         var valu_arr = [];
         if(data[i][5]!=""){
           var result = data[i][5].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= result[k];
             valu_arr.push(value);
           }
           lable = "Dropdown Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i][2];
           if( data[i][6] === "true"){
             record.required = true;
            }else{
              record.required=false;
            }
           //record.required = data[i][6];
           record.placeHolder = "";
           record.isPrimary = false;
           record.defaultValue = data[i][4];
           record.isAllowMultiselection = false;
           record.type = {"view":"select","values":valu_arr}
        }       
      }
      else if(data[i][3]=="checkBox"){
         var valu_arr = [];
         if(data[i][5]!=""){
           var result = data[i][5].slice(1, -1).split(",");
           for (var k = 0; k < result.length; k++) {
             var value = {};
             value.lable = result[k];
             value.value= result[k];
             valu_arr.push(value);
           }
           lable = "Checkbox Name"
           var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
           record.id = unique_id;
           record.lable = data[i][2];
           record.isPrimary = false;
           record.defaultValue = data[i][4];
           if( data[i][6] === "true"){
            record.required = true;
            }else{
              record.required=false;
            }
        //record.required = data[i][6];
           record.type = {"view":"checkbox","values":valu_arr}
        }
      }
      else if(data[i][3]=="map"){
         var lable = "Location";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
           if( data[i][6] === "true"){
            record.required = true;
            }else{
              record.required=false;
            }
                    //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "map";
      }
      else if(data[i][3]=="barcode"){
        var lable = "barcode";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
         if( data[i][6] === "true"){
        record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.placeHolder = "";
        record.type = {"view":""};
        record.type.view = "barcode";
      }
      else if(data[i][3]=="calender"){
        var lable = "Date";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        record.isPrimary = false;
        record.minDate = "";
        record.maxDate = "";
        record.defaultValue = data[i][4];
        record.placeHolder = "";
        record.type = {"view":""};
        record.type.view = "calender";
/*
@Developer : SKG
@Date : 3/5/2018
@Description : added typeOfDateSelected to calender while creating form throgh prepop

*/ 
record.typeOfDateSelected = "Manual Entry"; 
      }
      else if(data[i][3]=="camera"){
        var lable = "Image Name";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        if( data[i][6] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }// record.required = data[i][6];
        record.imageSize = "";
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "camera"; 
      }
      else if(data[i][3]=="sign"){
        var lable = "Signature";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        if( data[i][6] === "true"){
          record.required = true;
        }else{
          record.required=false;
        }// record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "sign"; 
      }
      else if(data[i][3]=="video"){
        var lable = "video";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        if( data[i][6] === "true"){
          record.required = true;
        }else{
            record.required=false;
        }
        record.videoDuration = "20";
        record.type = {"view":""};
        record.type.view = "video"; 
        record.isPrimary = false;
      }
      else if(data[i][3]=="rating"){
        var lable = "Rating";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        record.min = 0;
        record.max = 5;
        record.minLable = "Poor";
        record.maxLable = "Excellent";
        record.defaultValue = data[i][4];
       if( data[i][6] === "true"){
        record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "rating"; 
      }
      else if(data[i][3]=="mapInteraction"){
        var lable = "Map Interaction Field";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        record.required = false;
        record.isPrimary = false;
        
        record.type = {"view":""};
        record.type.view = "mapInteractionField"; 
      }
      else if(data[i][3]=="goto"){
        var lable = "Goto Field";
        var unique_id = lable.replace(" ","__")+"_"+datetimestamp;
        record.id = unique_id;
        record.lable = data[i][2];
        if( data[i][6] === "true"){
        record.required = true;
        }else{
          record.required=false;
        }
        //record.required = data[i][6];
        record.isPrimary = false;
        record.type = {"view":""};
        record.type.view = "goto"; 
      }
      //-->SivaKella: Added: To ignore all invalid TYPE data in excel
      else {
        console.log("NULL CASE...");
        continue;
      }
      //<-- End

  //     
 //console.log(data[i][0])
   if(data[i][0]!=""){
        // To handle section and group fields
        var sectrecord = {};
        var secflag = false;
        var secgrpflag = false;
        if(data[i][1]!=""){
        // For Section-Group fields..
          loopforsecetionrecord : for( var s = 0 ; s < jsondata.length; s++){
            if(jsondata[s].lable == data[i][0]){
              // get the Section record if already created
              secflag = true;
              var secgrprecflag = false;
              loopforsectiongrprecord : for(var g = 0 ; g < jsondata[s].type.fields.length; g++){
                if(jsondata[s].type.fields[g].type=="group"){
                  if(jsondata[s].type.fields[g].data.lable == data[i][1]){
                    // get the created group of section
                    jsondata[s].type.fields[g].data.type.fields.push(record);
                    secgrprecflag = true;
                    break loopforsectiongrprecord;
                  }

/*                  else{
                    // create group for section
                    var grprecord = {};
                    grprecord.type = "group";
                    grprecord.data={};
                    grprecord.data.lable = data[i][1];
                    grprecord.data.type={};
                    grprecord.data.type.view = "group";
                    grprecord.data.type.fields = [];
                    grprecord.data.type.fields.push(record);
                    // add group record to section
                    jsondata[s].type.fields.push(grprecord);
                    break loopforsectiongrprecord;
                  }*/
                }


              }
              if(!secgrprecflag) {
                    // create group for section
                    var grprecord = {};
                    grprecord.type = "group";
                    grprecord.data={};
                    grprecord.data.lable = data[i][1];
                    grprecord.data.type={};
                    grprecord.data.type.view = "group";
                    grprecord.data.type.fields = [];
                    grprecord.data.type.fields.push(record);
                    // add group record to section
                    jsondata[s].type.fields.push(grprecord);                
                
              }
              break loopforsecetionrecord;
            }
          }
        }else{
          // For Section fields
          for( var s = 0 ; s < jsondata.length; s++){
            if(jsondata[s].lable == data[i][0]){
              // get the Section record if already created
              secflag = true;
              var sectionfieldrec = {};
              sectionfieldrec.type = "field";
              sectionfieldrec.data = record;
              jsondata[s].type.fields.push(sectionfieldrec)
            }
          } 
        }

        if(!secflag){
          // first time creation of section record with group or direct section field
          var sectionrecord = {};
          sectionrecord.lable = data[i][0];
          sectionrecord.type={ "view": "section","fields":[]};
          if(data[i][1]!=""){
            // first time group record
            var grprecord = {};
            grprecord.type = "group";
            grprecord.data={};
            grprecord.data.lable = data[i][1];
            grprecord.data.type={};
            grprecord.data.type.view = "group";
            grprecord.data.type.fields = [];
            grprecord.data.type.fields.push(record)
            sectionrecord.type.fields.push(grprecord);
          }else{
            // firt time field record 
            var sectionfieldrec = {};
            sectionfieldrec.type = "field";
            sectionfieldrec.data = record;

            sectionrecord.type.fields.push(sectionfieldrec);
          }
          jsondata.push(sectionrecord);
        }
      }




      else if(data[i][1]!==""){
        // Handles direct group fields
        var grprecord = {};
        var grpflag = false;
        grouploop:    for (var q = 0; q < jsondata.length; q++) {
          if( jsondata[q].lable == data[i][1]){
            grprecord = jsondata[q];
            grpflag = true;
            grprecord.type.fields.push(record);
            break grouploop;
          }
        }
        if(!grpflag){
          grprecord.lable = data[i][1];
          grprecord.type = {"view":"group"};
          grprecord.type.fields = [];
          grprecord.type.fields.push(record);
          jsondata.push(grprecord);
        }
      }
      else if(data[i][1]==""){
        // Handles direct Fields
        jsondata.push(record);
      }
    }
  } 
  console.log("jsondata");
  console.log(jsondata);

  callback(jsondata);
}





  function createvalidators(data,groupId,callback){
    var fields = [];
    var lengths = null;
    var minLen = null;
    var maxLen = null;
    var fieldvalidator = null;
    var fieldStatus = null;
    // console.log(data[0]);
    // console.log(data[1]);
    // console.log(data[2]);
    // console.log(data[3]);

    for(var i=0; i < data[2].length;i++){
      
        var field={};
        fields.push(field);

        var sectionStr = '';
        var groupStr = '';

        // console.log(data[0][i]);
        //  console.log(data[1][i]);
         if (data[0][i] !== '') {
            sectionStr = "SN@" + data[0][i].toString() + "-";
         };
         if (data[1][i] !== '') {
            groupStr = "GN@" + data[1][i].toString() + "-";
         }


         //field.name=data[2][i].toString();
         field.name=sectionStr +  groupStr + data[2][i].toString().trim();
         data[2][i] = field.name;



         if(JSON.stringify(data[3][i]).split(":")[1] != undefined){
            var fieldtype = JSON.stringify(data[3][i]).split(":")[1].trim();
         }
          else{
            fieldtype = "userId";
          }
      if((fieldtype=="textbox\"")||(fieldtype=="Number\"")||(fieldtype=="textarea\""))
      {

        //console.log(field)
        lengths = data[4][i].toString().split(",");
        field.minLen = lengths[0].split(":")[1].trim().toString();
        field.maxLen = lengths[1].split(":")[1].trim().toString();

        field.minimum_length = function (param,fieldValue) {    
                  if(fieldValue.minLen=="")
                    fieldValue.minLen=0;
                return param.toString().length  >= parseInt(fieldValue.minLen);   
              }
        field.maximum_length = function (param,fieldValue) {
                if(fieldValue.maxLen=="")
                  return true;
        //        console.log(fieldValue,param.toString().length <= parseInt(fieldValue.maxLen))
                return param.toString().length <= parseInt(fieldValue.maxLen);    
              }
        field = {};
        maxLen=null;
        minLen= null;
      }
      else if ((fieldtype == "select\"")||(fieldtype == "radio\"")){
//        field.name=data[2][i].toString();
        field.values = data[4][i].toString().trim().split(",");
      }   
    }
    recordLoop: for(var recordId=3;recordId<data.length;recordId++){
            var recordStatus=true;
    fieldLoop:    for(var fieldId=0;fieldId<data[2].length;fieldId++){
              var fieldStatus = true;
    validatorloop:    for(var fieldValidatorId=0;fieldValidatorId<fields.length;fieldValidatorId++){
                if(fields[fieldValidatorId].name==data[2][fieldId]){
                  fieldvalidator =  fields[fieldValidatorId];
                  break validatorloop;
                }
                else{
                  fieldvalidator = null;
                }         
              }
              if(fieldvalidator != null){
                for(k in fieldvalidator){   
                  // for number, textarea,textbox type fields
                  if((k=="minimum_length")||(k=="maximum_length")){ 
                    fieldStatus = fieldvalidator[k](data[recordId][fieldId],fieldvalidator);
                    if(!fieldStatus){
                      recordStatus = fieldStatus;
                    }
                  }
                  // for select and radio type fields
                  else if (k=="values"){        
                    var index = fieldvalidator[k].indexOf(data[recordId][fieldId]);
                    if(index==-1)
                    recordStatus = false;
                  }
                }                     
              } 
            }
            data[recordId].push(recordStatus);          
          }

    writeToJson(data, groupId,function(response) {
     
      callback(response)
    });   
  }


/*function writeToJson(data,callback){
    
    //-->Siva Kella : Added
    data.shift();
    data.shift();
    //<-- End
    var fieldnames = data.shift();
    fieldnames.push("recordStatus")
    data.shift();
    data.shift();
    

    var json=[];
    var record={};
    for(var recordId=0;recordId < data.length;recordId++){
      
      for(var fieldId=0;fieldId<data[recordId].length;fieldId++){
        record[fieldnames[fieldId]] = data[recordId][fieldId];
      }
    json.push(record);
    record ={};
    }
    //console.log(json)
    callback(json)
  }*/
  function writeToJson(data,groupId,callback){
    //-->Siva Kella : Added
    data.shift();
    data.shift();
    //<-- End
    var fieldnames = data.shift();
    console.log("writeToJson");
    fieldnames.push("recordStatus")
    var fieldTypes = data.shift();
    data.shift();
    

    var json=[];
    var record={};
    //--> TPCL Customizations: SIva Kella: Added: Handled Empty record
    var EmptyRecord = true;
    //<-- End
    
    Users.find({type:2,groupname: { $elemMatch: { _id: groupId } }},{username:1,_id:0},function(err,userList)
    { 

    for(var recordId=0;recordId < data.length;recordId++){
      
      for(var fieldId=0;fieldId<data[recordId].length;fieldId++){
        //--> TPCL Customizations: SIva Kella: handled Empty record fieldId != "recordStatus" && 
        if ( EmptyRecord && data[recordId][fieldId] != '') { 
          EmptyRecord = false}
        //<-- End  
      
        // Modified : Change by Naveen Adepu 
        // Date : 12 Apr 2018
      if(typeof(data[recordId][fieldId])!="boolean"){
          record[fieldnames[fieldId]] = data[recordId][fieldId].trim();
        }
      else{
          record[fieldnames[fieldId]] = data[recordId][fieldId];
        }
    if(JSON.stringify(fieldTypes[fieldId]).split(":")[1] == " calender\""){
          var oaDate = data[recordId][fieldId];
          var date = new Date();
          if(oaDate == ""){
            record[fieldnames[fieldId]] = "";
            
          }else{
            var currentdate = new Date(); 
	          date.setTime((oaDate - 25569) * 24 * 3600 * 1000);
            date.setHours(currentdate.getHours());
            date.setMinutes(currentdate.getMinutes());
	          record[fieldnames[fieldId]] = date;
  	      }
        }

        // End of Change
      }



      //--> TPCL Customizations: SIva Kella: Modified: Handled Empty record
      //console.log(record)
      if (EmptyRecord == false) {  

      	if(groupId != ""){
	        if(record.UserId != "" && userList.map(function(obj){ return obj.username; }).indexOf(record.UserId) != -1)
	        {
	              json.push(record);
	        }else{
	          var msg = record.UserId+" user not exist";
	           logForPrepopData.info(msg);
	        }
	    }else{
	          json.push(record);
        }

      }
      // json.push(record);
    //<-- End
    record ={};

    //--> TPCL Customizations: SIva Kella: Added: Handled Empty record
    EmptyRecord = true;
    //<-- End
    
    }
    //console.log(json)
    callback(json)

    });

  }


  
  

// End of Change By Naveen Adepu. 
module.exports = router;
