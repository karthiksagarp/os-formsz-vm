var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var project = require('../../models/projectModel.js');
var versionManage = require('../../models/versionManagement.js');
var Users = require('../../models/user.js');
var Formsz = require('../../models/formsz.js');
var tasks = require('../../models/projectTasksModel.js');
var taskAssignments = require('../../models/projectTaksAssignMents.js');
var async = require('async');
var formszCategory = require('../../models/formszCategory.js');
var Users = require('../../models/user.js');
var FormszDetails = require('../../models/formszDetails.js');
var DisplayValues = require('../../models/displayValues.js');
var getdeviceKeysByUserName = require('../../routes/utils').getdeviceKeysByUserName;
var pushNotifications = require('../../routes/utils').pushNotifications;
var notificationmodel = require('../../models/notificationmodel.js');
var downloadFormInfo = require('../../models/formDownloadInfo.js');


router.post('/insertDownloadedFormInfo', function(req,res,next){
  downloadFormInfo.create(req.body, function(err, a) {
    if(!err)
    { 
      res.json({"message": "Inserted successfully","status": 200});
    }
  });
});


router.get('/getDownloadedFormInfo/:formId', function(req,res,next){
  /*downloadFormInfo.find({'formId': req.params.formId},function(err, data) {
    if(data.length > 0){
        downloadFormInfo.aggregate([
            {$match : {formId: req.params.formId}},
            {$group : {_id:"$username"}, }
          ], function(error,userData){
            FormszDetails.count({formId: req.params.formId},function(formErr, formData){
              res.json({
                "data": {'numberOfUsers': userData.length, 'numberOfSubmissions':formData, 'lastDownloadedDate': data[data.length-1].dowmloadedDateTime, 'lastDownloadedBy':data[data.length-1].username},
                "status": 200
              });
            });
        });
    }
    else{
      res.json({
              "data": {'numberOfUsers': 0, 'numberOfSubmissions':0, 'lastDownloadedDate': "", 'lastDownloadedBy':""},
              "status": 200
            });
    }
   
  });*/

  var numberOfUsers = 0;
  var numberOfSubmissions = 0;
  var lastDownloadedDate = '';
  var lastDownloadedBy = '';
  downloadFormInfo.find({'formId': req.params.formId},function(err, data) {
    if(data.length > 0){
      lastDownloadedDate = data[data.length-1].dowmloadedDateTime;
      lastDownloadedBy = data[data.length-1].username;
    }
   
    downloadFormInfo.aggregate([
        {$match : {formId: req.params.formId}},
        {$group : {_id:"$username"}, }
      ], function(error,userData){
        if(userData.length){

        }
        FormszDetails.count({formId: req.params.formId, status: true},function(formErr, formData){
          res.json({
            "data": {'numberOfUsers': userData.length, 'numberOfSubmissions':formData, 'lastDownloadedDate': lastDownloadedDate, 'lastDownloadedBy':lastDownloadedBy},
            "status": 200
          });
        });
    });
  });
});

router.get('/getFormszCatagory', function(req, res, next) {
    formszCategory.find({
       
    }, function(err, post) {
        //if (err) return next(err);
        if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else {
            res.json({
                "message": "No categories available,Please create new category",
                "status": 204
            });
        }

    });
});

router.post('/addProject', function(req,res,next){
	var obj ={}
	obj['id'] = req.body.zonalAdmins._id
	obj['username'] = req.body.zonalAdmins.username
	var arr =[];
	arr.push(obj)

	req.body.zonalAdmins = arr;
  project.find({name: req.body.name,userGroup: req.body.userGroup},function(err, category) {
    if (category.length > 0) {
      res.json({"message": "Project already exits","status": 204});
         }
        else 
        {
            project.create(req.body, function(err, a) {
              if(!err)
              {	
                res.json({"message": "Project created successfully","status": 200});
              }
            });
        }
  });
});

router.delete('/deleteProjectTask/:id', function(req, res, next) {
    tasks.findById(req.params.id, function(err, post) {
     if(err){
          console.log(err);
         }
        if (post) {
           var today = new Date();
           if(today > post.startDate){
                res.json({
                        "message": "ProjectTask is under Progress,so you cannot delete it.",
                        "status": 208
                    });
                
           }
           else{
                post.isDeleted = true;
                 post.save(function(err) {
                     taskAssignments.find({taskId:req.params.id},function(err,post1){

                      if(post1.length > 0){
                        post1[0].isDeleted = true;

                        post1[0].save(function(err){
                          console.log(err);
                        })
                      }

                      res.json({
                          "message": "ProjectTask  deleted successfully",
                          "status": 200
                      });  

                     })
                //if (err) throw err;
                    console.log(err);
                  });
           } 
        }
        else {
            res.json({
                "message": "No data found",
                "status": 204
            })
        }

    });
});

router.delete('/deleteProject/:id', function(req, res, next) {
    
    project.findById(req.params.id, function(err, post) {
     if(err){
          console.log(err);
         }
         console.log(post)
        if (post) 
        {
          tasks.find({projectID:req.params.id},function(err,post1){

                if(post1.length == 0)
                {

                  post.isDeleted = true;

                  post.save(function(err) 
                  {
                        res.json({
                          "message": "ProjectTask  deleted successfully",
                          "status": 200
                        });
                  });

                }
                else
                {
                    res.json({
                      "message": "Can't delete as This project attached with "+post1.length +" Task(s)",
                      "status": 208
                    });
                }
          })
         
            
        }
        else {
            res.json({
                "message": "No data found",
                "status": 204
            })
        }

    });

});

router.post('/updateProject', function(req,res,next){
    var obj ={}
    obj['id'] = req.body.zonalAdmins._id
    obj['username'] = req.body.zonalAdmins.username
    var arr =[];
    arr.push(obj)

//    req.body.zonalAdmins = arr;
    console.log(req.body)
    if(req.body.zonalAdmins._id!=undefined )
    {
      project.update({_id: req.body.projectId},{$set:{category:req.body.category,name:req.body.name,aliasName:req.body.aliasName,description:req.body.description,zonalAdmins:arr,zone:req.body.zone,startDate:req.body.startDate,endDate:req.body.endDate,remarks1:req.body.remarks1,remarks2:req.body.remarks2,createdBy:req.body.createdBy,deparmentId:req.body.deparmentId}},function(err, project) {
         console.log(project)
         console.log("project")
         res.json({"message": "Task updated successfully","status": 200});
      });
    }
    else
    {
      project.update({_id: req.body.projectId},{$set:{category:req.body.category,name:req.body.name,aliasName:req.body.aliasName,description:req.body.description,zone:req.body.zone,startDate:req.body.startDate,endDate:req.body.endDate,remarks1:req.body.remarks1,remarks2:req.body.remarks2,createdBy:req.body.createdBy,deparmentId:req.body.deparmentId}},function(err, project) {
         console.log(project)
         console.log("project")
         res.json({"message": "Task updated successfully","status": 200});
      });
    }
    
});


/* FormszDetails.update({_id:req.body.recordId}, {$set: {'assignedTo':null,'record':req.body.record,'IsReassign':false}}, function(err, doc){
        res.json({"message": "Form submited successfully","status": 200}); 
               
    });*/




router.post('/addProjectTask', function(req,res,next){
  tasks.find({name: req.body.taskBasicInfo.name,deparmentId: req.body.taskBasicInfo.deparmentId,projectId: req.body.taskBasicInfo.projectId},function(err, data) {
    if (data.length > 0) {
      res.json({"message": "Task already exits","status": 204});
         }
        else 
        {
            tasks.create(req.body.taskBasicInfo, function(err, data) {
              	if(!err)
              	{	
                  res.json({"message": "Task created successfully","status": 200});
                }
            });
        }
  });
});

router.post('/addProjectTaskAssignment',function(req,res,next){
console.log("-------------------------------")
console.log(req.body.taskBasicInfo)
console.log("-------------------------------")
            		for (var i = 0; i < req.body.tasskAssignMents.length; i++) {
              			req.body.tasskAssignMents[i].taskId = req.body.taskBasicInfo._id
              		}
                  for (var i = 0; i < req.body.tasskAssignMents.length; i++) {

                    console.log(req.body.tasskAssignMents[i].records);

                    var result = req.body.tasskAssignMents[i].records.map(a => a.UserId).filter((value, index, self) => self.indexOf(value) === index);

                    console.log(result);

                    req.body.tasskAssignMents[i].formId = req.body.tasskAssignMents[i].form.formId;
                    req.body.tasskAssignMents[i].formName=req.body.tasskAssignMents[i].form.formName;

                    if(req.body.tasskAssignMents[i].users.length > 0){
                      req.body.tasskAssignMents[i].users = req.body.tasskAssignMents[i].users;
                    }else{
                      req.body.tasskAssignMents[i].users = result;
                    }
                                
        
/*              taskAssignments.update({records:{$elemMatch:{RUID:req.body.RUID}}}, {$set: {'records.$.latReqDate':date1,'records.$.user':req.body.updatedBy}}, function(err, doc){
                if(err){
                    console.log(err)
                }
                // Who : SV
                // Date : 25-04-2018
                // Added record id to the response
                // Begin
                res.json({"message": "Form submitted successfully","status": 200, "recordId":data._id});
                // End
            });
*/
                    taskAssignments.create(req.body.tasskAssignMents[i],function(err,created)
                    {

                        tasks.update({_id:created.taskId}, {$set: {users:req.body.taskBasicInfo.users,taskType: req.body.taskBasicInfo.taskType}}, function(err, data) {
                                                  if(err) {
                        console.log(err);
                      }
                      else {
                        var formname= created.formName || null;
                      for(var i=0;i<=created.users.length-1;i++) {
                            notificationmodelObject = new notificationmodel({
                            taskName: req.body.taskBasicInfo.name,
                            taskId: req.body.taskBasicInfo._id,
                            user: created.users[i],
                           actionType: "Project Task Assigned for form "+ formname

                    });
                  notificationmodelObject.save(function(err,notifyObj) {
                    if (err)
                      return next(err);
                    else {
                      console.log("saved notifi");

                    }
                  });   


                    }
                     getdeviceKeysByUserName(created.users, function(deviceIds) {
                      pushNotifications(deviceIds, "New Project Task has been Created", "Project Task Created", {
                      data: "New Project Task Assigned For Form " + formname
                    });

                    })
                      //  console.log("created");
                     
                      }
                        });
                      

                      
                    })
                   


                  };
                  res.json({"message": "Task Assigned successfully","status": 200});

});

router.post('/addProjectTaskRecord', function(req,res,next){
	
  req.body.status = true;
	//check already record got subited by other users
  if(req.body.recordId == undefined)
  {
      FormszDetails.find({RUID:req.body.RUID},function(err,result){
        FormszDetails.create(req.body, function(err, data)
        {   
              //var date1 = new Date(req.body.updatedTime);

              var date1 = new Date();

              taskAssignments.update({records:{$elemMatch:{RUID:req.body.RUID}}}, {$set: {'records.$.latReqDate':date1,'records.$.user':req.body.updatedBy}}, function(err, doc){
                if(err){
                    console.log(err)
                }
                // Who : SV
                // Date : 25-04-2018
                // Added record id to the response
                // Begin
                res.json({"message": "Form submitted successfully","status": 200, "recordId":data._id});
                // End
            });
         
          });
      /*}*/
      })
  }
  else
  { 
    
    FormszDetails.update({_id:req.body.recordId}, {$set: {'assignedTo':null,'version':req.body.version,'record':req.body.record,'IsReassign':false,generatedId:req.body.generatedId}}, function(err, doc){
        // Who : SV
        // Date : 25-04-2018
        // Added record id to the response
        // Begin
        res.json({"message": "Form submitted successfully","status": 200,"recordId":req.body.recordId});
        // End
               
    });

 
  }
	

  	
});

router.post('/addmultiPleRecords', function(req,res,next){
  //check already record got subited by other users
        FormszDetails.create(req.body.formsRecordsList, function(err, data)
        {     
           
           
              var date1 = new Date();
              async.forEachSeries(req.body.ruidsList, function(ruid, next) 
              {
                 
                var date1 = new Date();

                taskAssignments.update({records:{$elemMatch:{RUID:ruid}}}, {$set: {'records.$.latReqDate':date1,'records.$.user':req.body.updatedBy}}, function(err, doc){
                  if(err){
                      console.log(err)
                  }
                  next()
                });

              },function(){
                 res.json({"message": "Form submited successfully","status": 200});
                 
             })

             
         
        });
    
  
  
  

    
});

router.post('/addProjectPlainTaskRecord', function(req,res,next){
	req.body.status = true;
	FormszDetails.create(req.body, function(err, data)
  	{		
  			
  			if(err){
			        console.log(err)
			    }
			res.json({"message": "Form submited successfully","status": 200});
   
    });
	
  	
});

router.get('/getZonalAdmins/:departmentId', function(req, res, next) {
    Users.find({
        groupname:{$elemMatch:{_id:{$in:[req.params.departmentId]}}},
        type:"3"
    },{username:1}, function(err, post) {
        //if (err) return next(err);
        if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else {
            res.json({
                "message": "No categories available,Please create new category",
                "status": 204
            });
        }

    });
});

router.get('/getProjectTaksForUser/:departmentId/:userId', function(req, res, next) {
    var projectLsit = []
    var zoneList =[]
    tasks.find({
        users:req.params.userId,isDeleted:false
    }, function(err, post) {
      var tasks =[];
         async.forEachSeries(post, function(task, next) {
         
              var taskObj ={}
              taskObj = task.toJSON()

              project.find({
                _id: task.projectID
              },function(err,project){

                //project inofrmation
                taskObj.zone = project[0].zone
                taskObj.projectName = project[0].name
                taskObj.projectDescription = project[0].description
                taskObj.projectSDate = project[0].startDate
                taskObj.projecteDate = project[0].endDate
                taskObj.assignName = project[0].zonalAdmins[0].username;
                
            if(projectLsit.indexOf(project[0].name) ==-1)
                projectLsit.push(project[0].name)

              if(zoneList.indexOf(project[0].zone) ==-1)
                zoneList.push(project[0].zone)
                
                tasks.push(taskObj);
                next();
                //user information
                // Users.find({_id:task.zonalAdminId},{username:1,_id:0},function(err,user){
                   
                //     taskObj.assignName = user[0].username
                //     tasks.push(taskObj);
                //     next();
                // })
               
              })



           },function(){
            res.json({
                  "data": tasks,
                  "projects":projectLsit,
                  "zones":zoneList,
                  "status": 200
              });
           });


        /*project.find({name: req.body.name,userGroup: req.body.userGroup},function(err, category) {

        })*/
        //if (err) return next(err);
        /*if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else {
            res.json({
                "message": "No Tasks available",
                "status": 204
            });
        }
*/
    });
});

/*router.get('/getProjectTaksForUser/:departmentId/:userId', function(req, res, next) {
	console.log("00000000000000000000000000000111111111111111")
    tasks.find({
        deparmentId:req.params.departmentId,
        users:req.params.userId
    }, function(err, post) {
        //if (err) return next(err);
        if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else {
            res.json({
                "message": "No Tasks available",
                "status": 204
            });
        }

    });
});
*/
router.get('/isProjectHasRecords/:projectId', function(req, res, next) {

    tasks.count({
        projectID:req.params.projectId
    }, function(err, post) {
        //if (err) return next(err);
        /*if (post.length > 0) {
            res.json({
                "count": post.length,
                "status": 200
            });
        } else {
            res.json({
                "message": "No Tasks available",
                "status": 204
            });
        }*/

          res.json({
                "count":post,
                "status": 200
            });
 

    });
});

router.get('/getProjectTaksFromsForUser/:taskId/:tasktype/:userId', function(req, res, next) {
    if(req.params.tasktype)
    {
    	taskAssignments.find({
        	 users: req.params.userId,
        	 taskId : req.params.taskId	
        	},function(err,projectTaskAssingments){
            
        			if(projectTaskAssingments.length > 0)
        			{
                for(var i=0;i<projectTaskAssingments.length;i++) {

                  for(var j=0;j<projectTaskAssingments[i].records.length;j++) {

                      selectedUserAssignments = projectTaskAssingments[i].records.filter(function(el) {
                          return el.UserId == "2003";
                      }); 

                      projectTaskAssingments[i].records = selectedUserAssignments;

                      
                  }

                }
        				  res.json({
			                "data": projectTaskAssingments,
			                "status": 200
	            		});
        			}
        			else
        			{
        				res.json({
			                "message": "No forms are available",
			                "status": 204
	            		});
        			}
	        		
        	})	
    }
    else
    {
    	taskAssignments.find({
        	 users: req.params.userId,
        	 taskId : req.params.taskId	
        	},function(err,projectTaskAssingments){
        			if(projectTaskAssingments.length > 0)
        			{
        				res.json({
			                "data": projectTaskAssingments,
			                "status": 200
	            		});
        			}
        			else
        			{
        				res.json({
			                "message": "No forms are available",
			                "status": 204
	            		});
        			}
	        		
        })
    }
    
   
});

router.get('/downloadService/:taskId/:formId/:user', function (req, res, next) {
    var data = [];
    tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
    if (tasks.length > 0) {
      getTaskInfo(req.params.taskId,function(taskInfo)
      {   
          getProjectInfo(taskInfo.projectID,function(projectInfo){
            
             taskInfo = taskInfo.toJSON()
             taskInfo.projectSDate = projectInfo.startDate
             taskInfo.projecteDate = projectInfo.endDate
             taskInfo.projectName  = projectInfo.name
             taskInfo.projectDescription = projectInfo. description

             downloadForm(req.params.formId, req.params.taskId, req.params.user, function (formInfo,formFrequncy) {
              console.log("formFrequncy")
              console.log(formFrequncy)
            //data.push(ress);
                getPrepoRecordsOfForm(req.params.formId, req.params.taskId, req.params.user,formFrequncy,function (prepopRecords){
                  var response = {};
                  formInfo = formInfo.toJSON();

                  formInfo.formFrequncy = formFrequncy;

                  response.formInfo = JSON.stringify(formInfo);
                  response.taskInfo = JSON.stringify(taskInfo);
                  response.prepopRecords = prepopRecords;

                  res.json(response)
              })
            });
          })
         
      })
        

 
    } else {
      res.json({
        "message" : "No data found",
        "status" : 204
      });
    }
  });
});

//download service for task
router.get('/downloadProjectTask/:taskId', function (req, res, next) {
    var data = [];
    tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
    if (tasks.length > 0) {
      getTaskInfo(req.params.taskId,function(taskInfo)
      {   
         getProjectInfo(taskInfo.projectID,function(projectInfo){
            
             taskInfo = taskInfo.toJSON()
             taskInfo.projectSDate = projectInfo.startDate
             taskInfo.projecteDate = projectInfo.endDate
             taskInfo.projectName  = projectInfo.name
             taskInfo.projectDescription = projectInfo. description
              var response = {};
              var taskFormsList = [];
              response.taskInfo = JSON.stringify(taskInfo);
              //var taskInfoJson = JSON.parse(taskInfo)
              taskAssignments.find({
                taskId : taskInfo._id
              },{formId:1,_id:0},function(err,taskForms){
                   for (var i = 0; i < taskForms.length; i++) {
                     taskFormsList.push(taskForms[i].formId)
                   };
                   response.taskFormsList = taskFormsList
                   res.json(response);
              })

            })
         
          
        
      })
        

 
    } else {
      res.json({
        "message" : "No data found",
        "status" : 204
      });
    }
  });
});



function getTaskInfo(taskId,callback)
{
  tasks.find({_id:taskId},function(err,taskInfo)
  {
    callback(taskInfo[0])

  })
}

function getProjectInfo(projectId,callback)
{
  project.find({_id:projectId},function(err,projectInfo)
  {
    callback(projectInfo[0])

  })
}

function getPrepoRecordsOfForm(formId,taskId,userId,formFrequncy,callback)
{
    
    taskAssignments.find({
       formId : formId,
       taskId : taskId,
       users  : userId
       //"records.user": req.params.userName
      },{_id:0,records:1,formFrequency:1},function(err,projectTaskAssingments){
         callback(JSON.stringify(projectTaskAssingments[0]));
      })
}

function downloadForm(id,taskid, user, callback) {
 

  Formsz.find({_id : id}, function (err, formsz) {
    getFrequencyOfForm(id,taskid, user,function(formFrequency){
       callback(formsz[0],formFrequency)
    })
   
  });
}


function  getFrequencyOfForm(formId,taskId,userId,callback)
{   
    taskAssignments.find({formId :formId,taskId:taskId}, function (err, formsz) {
      callback(formsz[0].formFrequency)
   
    });
}
//currrent working 
router.get('/getRecordsOfProjectTaskForm/:taskId/:formId/:userName', function(req, res, next) {
	var records =[] 
	var formDisplayfields = [];
  var workIns ;
  var workReferences;
  var version;
	taskAssignments.find({
    	 formId : req.params.formId,
    	 taskId : req.params.taskId,
    	 users  : req.params.userName
    	 //"records.user": req.params.userName
    	},{_id:0,records:1,formFrequency:1},function(err,projectTaskAssingments){
    		
    		Formsz.find({_id:req.params.formId},{requiredField:1,workInstruction:1,references:1,version:1,_id:0,FormSkeleton:1,dependentFields:1},function(err,form){

        versionManage.find({formId : req.params.formId} ,function(err,vm){
          version = vm[0].version
        

				formDisplayfields = form[0].requiredField; 
				formSkelton =  form[0].FormSkeleton;
        workIns =  form[0].workInstruction
        workReferences = form[0].references


				async.forEachSeries(projectTaskAssingments[0].records, function(recordObject, next) 
				{		
				      if(recordObject.latReqDate == null) 
				      {		
				        		
				        		
			        			var obj ={}
					         	delete recordObject.user;
					         	//delete recordObject.RUID;
					         	delete recordObject.latReqDate;
					         	obj.record =  recordObject;
					         	obj.displayValues = formDisplayfields[0];
console.log("0000000000000000000000000000000")
console.log(req.params.userName)
console.log(obj)
console.log("99999999999999999999999999999")
                    if(obj.record.UserId == req.params.userName){
                      records.push(obj);
                    }


					    }
					    else
					    {	
					    	
                if(projectTaskAssingments[0].formFrequency == "None")
                { 
                    if(recordObject.latReqDate == null) 
                    {   
                        
                        
                        var obj ={}
                        delete recordObject.user;
                        //delete recordObject.RUID;
                        delete recordObject.latReqDate;
                        obj.record =  recordObject;
                        obj.displayValues = formDisplayfields[0];

                        records.push(obj);




                    }

                }

					    	else if(projectTaskAssingments[0].formFrequency == "Daily")
					    	{	
					    		

					  			var storedDate = new Date(recordObject.latReqDate);
					  			var todayDate = new Date()
                  
                 
                  var existingDate = new Date(storedDate.getDate(),storedDate.getMonth(),storedDate.getFullYear())
                  var currentDate = new Date(todayDate.getDate(),todayDate.getMonth(),todayDate.getFullYear())

					    	 	if(+existingDate !== +currentDate) 
				        		{
			        				var obj ={}
						         	delete recordObject.user;
						         	//delete recordObject.RUID;
						         	delete recordObject.latReqDate;
						         	obj.record =  recordObject;
						         	obj.displayValues = formDisplayfields[0];
						         	records.push(obj);
							        
				        		}
					    	}
					    	else if(projectTaskAssingments[0].formFrequency == "Monthly")
					    	{
					    		
					    		var storedDate = new Date(recordObject.latReqDate);
					    		var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                  var firstDay = new Date(y, m, 1);
                  var lastDay = new Date(y, m + 1, 0);

                
							 	  var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
								    
								 if(!isValidRequest) 
				        		{	
                      if(recordObject.user == req.params.userName)
				        			{
				        				var obj ={}
							         	delete recordObject.user;
							         	//delete recordObject.RUID;
							         	delete recordObject.latReqDate;
							         	obj.record =  recordObject;
							         	obj.displayValues = formDisplayfields[0];
							         	records.push(obj);
							         }
				        		}
				        		
					    	}

					    	else if(projectTaskAssingments[0].formFrequency == "Weekly")
					    	{
              	
                var curr = new Date; // get current date
                var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6

                var firstDay = new Date(curr.setDate(first));
                var lastDay = new Date(curr.setDate(last));
                var storedDate = new Date(recordObject.latReqDate);
                
                var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
								
                if(!isValidRequest) 
				        		{
				        			if(recordObject.user == req.params.userName)
				        			{	
				        				var obj ={}
							         	delete recordObject.user;
							         	//delete recordObject.RUID;
							         	delete recordObject.latReqDate;
							         	obj.record =  recordObject;
							         	obj.displayValues = formDisplayfields[0];
                       	records.push(obj);
							        }
				        		}


					    	}

                else if(projectTaskAssingments[0].formFrequency == "Yearly")
                {
                
                    var dt = new Date()
                    var month = dt.getMonth()
                    var year = dt.getFullYear()
                    //current year 
                    if(month == 0 || month == 1 || month == 2)
                    {
                      var firstDay = new Date(year-1, 3, 1);
                      var lastDay = new Date(year, 3, 0);
                    }
                    else
                    {
                      var firstDay = new Date(year, 3, 1)
                      var lastDay = new Date(year+1, 3, 0);
                    }

                var storedDate = new Date(recordObject.latReqDate);

                var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
                
                if(!isValidRequest) 
                    {
                      if(recordObject.user == req.params.userName)
                      { 
                        var obj ={}
                        delete recordObject.user;
                        //delete recordObject.RUID;
                        delete recordObject.latReqDate;
                        obj.record =  recordObject;
                        obj.displayValues = formDisplayfields[0];
                        records.push(obj);
                      }
                    }
                }

					    }
					    next();

			    },function(err){
			    		console.log(form[0].dependentFields)
              console.log("form[0].dependentFields")
			    		var obj ={}
			    		obj.formSkelton = formSkelton;
			    		obj.records = records;
              obj.displayValues = formDisplayfields;
              obj.workInstruction = workIns;
              obj.references = workReferences
              obj.dependentFields = form[0].dependentFields;
              obj.version = version;
               	res.json({
			                "data": obj,
			                "status": 200
		            	});
         			});
    			}).sort({ createdTime : -1 }).limit(1);
			})
        		
    	})	
    
    
   
});

// get prePOPRecords for user
router.get('/getProjectTaskRecordData/:user/:formId/:taskId/:recordId/:recordType', function(req, res, next) {
  if(req.params.recordType){
    taskAssignments.find({
        formId : req.params.formId,
       taskId : req.params.taskId,
       users  : req.params.user
       //"records.user": req.params.userName
      },{_id:0,records:1,formFrequency:1},function(err,projectTaskAssingments){
console.log(projectTaskAssingments)
        async.forEachSeries(projectTaskAssingments[0].records, function(recordObject, next) 
        {   
          console.log(recordObject.RUID)
          if(recordObject.RUID == req.params.recordId){
                console.log(req.params.recordId)

            res.json({
                      "data": recordObject,
                      "status": 200
                  });
          }else {
            next();
          }
        });

/*          if (projectTaskAssingments.length > 0) {
              res.json({
                    "data": projectTaskAssingments,
                    "status": 200
                });
          } else {
              res.json({
                  "message": "No data found",
                  "status": "204"
              });
          }
*/
        });

  }else{
        FormszDetails.find({
        $or: [{
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            DSAssignedTo: req.params.user
        }, {
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            updatedBy: req.params.user,
            IsReassign: true
        }]
    }, function(err, response) {
        var data = [];
        if (response.length > 0) {
            async.forEach(response, function(item) {
                data.push({
                    prepopulatedData: item.record,
                    taskId: item.taskId
                });
            });
            res.json(data);
        } else {
            res.json({
                "message": "No data found",
                "status": "204"
            });
        }
    });
  }
});

router.get('/getMarkers/:taskId/:formId/:userName', function(req, res, next) {
  var records =[] 
  var formDisplayfields = [];
  var workIns ;
  var workReferences;
  var geoField;
  var prepopRecordList = [];
  taskAssignments.find({
       formId : req.params.formId,
       taskId : req.params.taskId,
       users  : req.params.userName
       //"records.user": req.params.userName
      },{_id:0,records:1,formFrequency:1},function(err,projectTaskAssingments){
        
        Formsz.find({_id:req.params.formId},{requiredField:1,workInstruction:1,references:1,_id:0,FormSkeleton:1,geoFields:1},function(err,form){
        formDisplayfields = form[0].requiredField; 
        formSkelton =  form[0].FormSkeleton;
        workIns =  form[0].workInstruction
        workReferences = form[0].references
        geoField = form[0].geoFields[0]
        async.forEachSeries(projectTaskAssingments[0].records, function(recordObject, next) 
        {   
               
                if(recordObject.latReqDate == null) 
                {   
                    
                    var obj ={}
                    delete recordObject.user;
                    //delete recordObject.RUID;
                    delete recordObject.latReqDate;
                    //obj.displayValues = formDisplayfields[0];
                    if(geoField!=undefined && geoField!="")
                    {
                      if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                      {
                        obj.recordId ="";
                        obj.RUID =recordObject.RUID;
                        obj.geometries =recordObject[geoField].split(",");
                        obj.status = false;
                        obj.record = recordObject;
                        records.push(obj);
                      }

                    }
                    
                   
               }
              else
              { 
                
                if(projectTaskAssingments[0].formFrequency == "Daily")
                { 
                  

                  var storedDate = new Date(recordObject.latReqDate);
                  var todayDate = new Date()
                  
                 
                  var existingDate = new Date(storedDate.getDate(),storedDate.getMonth(),storedDate.getFullYear())
                  var currentDate = new Date(todayDate.getDate(),todayDate.getMonth(),todayDate.getFullYear())

                  if(+existingDate !== +currentDate) 
                    {
                          var obj ={}
                          delete recordObject.user;
                          //delete recordObject.RUID;
                          delete recordObject.latReqDate;
                          //obj.displayValues = formDisplayfields[0];
                          if(geoField!=undefined && geoField!="")
                          {
                            if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                            {
                              obj.recordId ="";
                              obj.RUID =recordObject.RUID;
                              obj.geometries =recordObject[geoField].split(",");
                              obj.status = false;
                              obj.record = recordObject;
                              records.push(obj);
                            }

                          }
                      
                    }
                    else
                    {
                       FormszDetails.find({RUID:recordObject.RUID,updatedBy:req.params.userName},{},{sort:{"updatedTime":1}},function(err,res){
                          console.log(res)
                          console.log("resssssss")
                          var obj ={}
                          delete recordObject.user;
                          //delete recordObject.RUID;
                          delete recordObject.latReqDate;
                          if(geoField!=undefined && geoField!="")
                          {
                            if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                            { 
                              console.log("ppppppppppppppp")
                              obj.recordId ="";
                              obj.RUID =recordObject.RUID;
                              obj.geometries =recordObject[geoField].split(",");
                              obj.status = true;
                              obj.record = recordObject;
                              records.push(obj);

                              console.log("flkdf")
                              console.log(records)
                            }

                          }

                       })
                    }

                }
                else if(projectTaskAssingments[0].formFrequency == "Monthly")
                {
                  
                  var storedDate = new Date(recordObject.latReqDate);
                  var date = new Date(), y = date.getFullYear(), m = date.getMonth();
                  var firstDay = new Date(y, m, 1);
                  var lastDay = new Date(y, m + 1, 0);

                
                  var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
                    
                 if(!isValidRequest) 
                    { 
                          var obj ={}
                          delete recordObject.user;
                          //delete recordObject.RUID;
                          delete recordObject.latReqDate;
                          //obj.displayValues = formDisplayfields[0];
                          if(geoField!=undefined && geoField!="")
                          {
                            if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                            {
                              obj.recordId ="";
                              obj.RUID =recordObject.RUID;
                              obj.geometries =recordObject[geoField].split(",");
                              obj.status = false;
                              obj.record = recordObject;
                              records.push(obj);
                            }

                          }
                    }
                    
                }

                else if(projectTaskAssingments[0].formFrequency == "Weekly")
                {
                
                var curr = new Date; // get current date
                var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
                var last = first + 6; // last day is the first day + 6

                var firstDay = new Date(curr.setDate(first));
                var lastDay = new Date(curr.setDate(last));
                var storedDate = new Date(recordObject.latReqDate);
                
                var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
                
                if(!isValidRequest) 
                    {
                          var obj ={}
                          delete recordObject.user;
                          //delete recordObject.RUID;
                          delete recordObject.latReqDate;
                          //obj.displayValues = formDisplayfields[0];
                          if(geoField!=undefined && geoField!="")
                          {
                            if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                            {
                              obj.recordId ="";
                              obj.RUID =recordObject.RUID;
                              obj.geometries =recordObject[geoField].split(",");
                              obj.status = false;
                              obj.record = recordObject;
                              records.push(obj);
                            }

                          }
                    }


                }

                else if(projectTaskAssingments[0].formFrequency == "Yearly")
                {
                
                    var dt = new Date()
                    var month = dt.getMonth()
                    var year = dt.getFullYear()
                    //current year 
                    if(month == 0 || month == 1 || month == 2)
                    {
                      var firstDay = new Date(year-1, 3, 1);
                      var lastDay = new Date(year, 3, 0);
                    }
                    else
                    {
                      var firstDay = new Date(year, 3, 1)
                      var lastDay = new Date(year+1, 3, 0);
                    }

                var storedDate = new Date(recordObject.latReqDate);

                var isValidRequest = isStriedDateBetweenSandEDate(firstDay,lastDay,storedDate)
                
                if(!isValidRequest) 
                {
                        var obj ={}
                          delete recordObject.user;
                          //delete recordObject.RUID;
                          delete recordObject.latReqDate;
                          //obj.displayValues = formDisplayfields[0];
                          if(geoField!=undefined && geoField!="")
                          {
                            if(recordObject[geoField]!=undefined && recordObject[geoField]!="")
                            {
                              obj.recordId ="";
                              obj.RUID =recordObject.RUID;
                              obj.geometries =recordObject[geoField].split(",");
                              obj.status = false;
                              obj.record = recordObject;
                              records.push(obj);
                            }

                          }                    
                 }
              }
              }
              next();
          },function(err){
              if (form.length > 0) {
                  FormszDetails.find({formId :req.params.formId, updatedBy:req.params.userName},{record:1,updatedTime:1,generatedId:1,long:1,lat:1},function (err, post1) {
                   
                    if (post1.length > 0) {
                      for (var i = 0; i < post1.length; i++) {
                        var obj ={}
                        obj.record = post1[i].record[0]
                        obj.recordId = post1[i]._id
                        obj.geometries =[post1[i].lat,post1[i].long];
                        obj.status = true;

                        console.log(obj.geometries)

                        records.push(obj)
                      };

                      var obj ={}
                      var mainObj = {}
                     
                      obj.records = records;

                      obj.formId = req.params.formId;
                      //obj.displayValues = formDisplayfields;
                      
                      //obj.workInstruction = workIns;
                      
                      //obj.references = workReferences;
                      var mainObj ={};
                      var arr =[];
                      arr.push(obj)

                      mainObj.latlngData = arr
                        res.json({
                              "data": mainObj,
                              "status": 200
                          });
                    } 
                  });
                } 
              });
        })
      })  
    
    
   
});

router.get('/getProjectCats/:projectId', function(req, res, next) {
    var cats =[];
    project.find({
        _id: req.params.projectId
    },{'category':1,_id:0}, function(err, post) {
        //if (err) return next(err);
        if(post.length > 0) 
        {
	         async.forEachSeries(post[0].category, function(catid, next) {
	         	formszCategory.find({
	        		_id: catid
	    		},function(err,data){
	    			cats.push(data[0]);
	    			next();
	    		})
	         },function(){
	         	res.json({
	                "data": cats,
	                "status": 200
            	});
	         });
	    }
	    else
	    {
	    	res.json({
                "message": "No categories available,Please create new category",
                "status": 204
            });
	    }
    });
});

router.get('/getProjectTaks/:projectId', function(req, res, next) {
    var cats =[];
    tasks.find({
        projectID: req.params.projectId,
        isDeleted: false
    },function(err, post) {
        //if (err) return next(err);
        if(post.length > 0) 
        {
	        res.json({
	                "data": post,
	                "status": 200
            });
	    }
	    else
	    {
	    	res.json({
                "message": "No tasks available",
                "status": 204
            });
	    }
    });
});

router.get('/getProjects/:departmentId/:groupAdmin/:type/:filter', function(req, res, next) {
   
   if(req.params.type == 3)
   {
     project.find({
        isDeleted:false,
        'zonalAdmins.0.id':req.params.groupAdmin

      }, function(err, post) {
       
          if (err) return next(err);

           if (post.length > 0) {
            if(req.params.filter=="All"){
               res.json({
                   "data": post,
                   "status": 200
               });
           }
           else
           {
            project.find({
                        deparmentId:req.params.departmentId,'zonalAdmins.0.id':req.params.groupAdmin
                      }, function(err, post1) {
          if (err) return next(err);
             if (post1.length > 0) {
                 res.json({
                     "data": post1,
                     "status": 200
                 });
             } else {
              
                 res.json({
                     "message": "No projects available,Please create new project",
                     "status": 204
                 });
             }

                   });
           }
          } else {
              res.json({
                  "message": "No projects available,Please create new project",
                  "status": 204
              });
          }

      });
    }
    else if(req.params.type == 1)
    {
     project.find({
        isDeleted:false,
        deparmentId:req.params.departmentId
      }, function(err, post) {
          if (err) return next(err);
          if (post.length > 0) {
              res.json({
                  "data": post,
                  "status": 200
              });
          } else {
              res.json({
                  "message": "No projects available,Please create new project",
                  "status": 204
              });
          }

      });
    }
    
});

/*router.get('/getUsers/:departmentId', function(req, res, next) {
	Users.find({type:2,"groupname":{$elemMatch:{_id:req.params.departmentId}}},{username:1},function(err,userList)
	{	console.log("userList")
		console.log(userList)
		console.log(err)
		res.json({
		                "data": userList,
		                "status": 200
	     });
	})

});*/

router.get('/getUsers/:departmentId', function(req, res, next) {
	//db.getCollection('users').find({"groupname":{$elemMatch:{_id:req.params.departmentId,type},type:2}})
	Users.find({type:2,"groupname":{$elemMatch:{_id:{$in:[req.params.departmentId]}}}},{username:1},function(err,userList)
	{	
		res.json({
		                "data": userList,
		                "status": 200
	     });
	})

});

router.get('/getProjectTaksDetails/:projectId/:taskId', function(req, res, next) {
    var projectTaskInfo = {};
    tasks.find({
        projectID: req.params.projectId,
        _id:req.params.taskId
    },function(err, projectBasicInfo) {
        //if (err) return next(err);
        if(projectBasicInfo.length > 0) 
        {		
        	projectTaskInfo.basicInfo = projectBasicInfo[0];
        	taskAssignments.find({
        	 projectID: req.params.projectId,
        	 taskId : req.params.taskId	
        	},function(err,projectTaskAssingments){
	        		projectTaskInfo.projectTaskAssingments = projectTaskAssingments[0];
	        		res.json({
		                "data": projectTaskInfo,
		                "status": 200
	            	});
        	})
	       
	    }
	    else
	    {
	    	res.json({
                "message": "No tasks available",
                "status": 204
            });
	    }
    });
});

/*
@Type: Modified 
@developer: Santhosh Kumar Gunti

@description : Filter added for sending shared group forms for group admin 11-04-2018
*/
router.get('/getFormsOfCat/:category/:departmentId', function(req, res, next) {
    Formsz.find({
        formzCategory: {$elemMatch:{_id:{$in:[req.params.category]}}},
        formType     : 'form',
//        userGroup    : req.params.departmentId
        $or:[{userGroup    : req.params.departmentId}, {shareGroup:{ $elemMatch: { _id: req.params.departmentId } }}]
    }, function(err, post) {
        //if (err) return next(err);
        if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
        } else {
            res.json({
                "message": "No projects available,Please create new project",
                "status": 204
            });
        }

    });
});

router.get('/getFormsz/:departmentId', function(req, res, next) {
	Users.find({groupname:req.params.departmentId},function(err,userList)
	{
		res.json({
		                "data": userList,
		                "status": 200
	     });
	})

});

router.get('/getFormsForProjects/:departmentId', function(req, res, next) {
	Formsz.find({userGroup:req.params.departmentId,formzCategory:{$ne:null}},{name:1},function(err,formsList)
	{
		res.json({
		                "data": formsList,
		                "status": 200
	     });
	})

});

router.get('/getProjectTaskUsers/:taskId', function(req, res, next) {
	tasks.find({_id:req.params.taskId},{users:1,_id:0},function(err,usersList)
	{
		res.json({

	        "data": usersList[0].users,
	        "status": 200
	     });
	})

});

//form name need to add
router.get('/getProjectTaskForms/:taskId', function(req, res, next) {
	
	taskAssignments.find({taskId:req.params.taskId},{formId:1,formName:1,_id:0},function(err,formId)
	{	

		res.json({

	        "data": formId,
	        "status": 200
	     });
	})

});

/*router.get('/getHistroy/:formid/:user/:taskId', function(req, res, next) {
    FormszDetails.find({formId: req.params.formid,updatedBy: req.params.user,taskId: req.params.taskId}).sort({updatedTime:1}).exec(
  function(err, records) {
        if (records.length > 0) {
            var records1 = [];
            async.forEachSeries(records, function(info, next) {
                    var recorddata = {};
                    DisplayValues.find({
                        recordId: info._id
                    }, function(err, displayFields) {
                        if (displayFields.length > 0) {
                            for (var i = 0; i < displayFields.length; i++) {
                                recorddata.recordId = displayFields[i].recordId;
                                recorddata.displayFields = displayFields[i].displayValues;
                                recorddata.createdTime = displayFields[i].createdTime;

                                records1.push(recorddata);


                next();
                               
                
                            }
                        }
            else {
               next();    
                }
                    })
                },
                function(err) {
                    res.json({
                        data: records1,
                        status: 200
                    });
                })

        }
    else {
      res.json({
                "message": "No data found",
                "status": "204"
            });
    }
    }) 
  
});*/


router.get('/getHistroy/:formid/:taskId/:userId', function (req, res, next) {
  var updatedby = req.params.user;
  var records =[];
  Formsz.find({
    _id : req.params.formid,
    isVisible : true
  }, function (err, post) {
    if (post.length > 0) {
      FormszDetails.find({formId :req.params.formid, taskId : req.params.taskId, updatedBy:req.params.userId},{record:1,updatedTime:1,generatedId:1},function (err, post1) {
        if (post1.length > 0) {
          for (var i = 0; i < post1.length; i++) {
            var obj ={}
            delete post1[i].record[0].RUID
            console.log(post1[i])
            console.log("post1[i]")
            obj.record = post1[i].record[0]
            obj.recordId = post1[i]._id
            obj.updatedTime = post1[i].updatedTime
            obj.generatedId = post1[i].generatedId
            records.push(obj)
          };

          res.json({
            data:records,
            "status" :200
          })

        } else {
          res.json({
            "message" : "No Data Found",
            "status" : 204
          });
        }
      });
    } else {
      res.json({
        "message" : "No Data Found",
        "status" : 204
      });

    }
  });
});

router.get('/getReassignRecords/:formid/:taskId/:userId', function (req, res, next) {
  var updatedby = req.params.user;
  var records =[];
  Formsz.find({
    _id : req.params.formid,
    isVisible : true
  }, function (err, post) {
    if (post.length > 0) {
      FormszDetails.find({formId :req.params.formid, taskId : req.params.taskId, assignedTo:req.params.userId,IsReassign:true},{record:1,updatedTime:1},function (err, post1) {
        if (post1.length > 0) {
          
          for (var i = 0; i < post1.length; i++) {
            var obj ={}
            delete post1[i].record[0].RUID
            obj.record = post1[i].record[0]
            obj.updatedTime = post1[i].updatedTime
            obj.recordId = post1[i]._id
            records.push(obj)
          };

          res.json({
            data:records,
            "status" :200
          })

        } else {
          res.json({
            "message" : "No Data Found",
            "status" : 204
          });
        }
      });
    } else {
      res.json({
        "message" : "No Data Found",
        "status" : 204
      });

    }
  });
});

router.get('/getPendingRecords/:formId/:taskId/:startDate/:endDate', function (req, res, next) {
  var updatedby = req.params.user;
  var preepopRecords = []
  var recordinfo = [];
  Formsz.find({
    _id : req.params.formId,
    isVisible : true
  }, function (err, formSkelton) {
        taskAssignments.find({
        formId : req.params.formId,
        isDeleted:false,
        taskId:req.params.taskId
      }, function (err, taskAssignments) {
        if(taskAssignments.length > 0) {
          var taskAssignmentsRecords = taskAssignments[0].records

          getRecordds(req,taskAssignmentsRecords,taskAssignments[0].formFrequency,req.params.startDate,req.params.endDate,preepopRecords,recordinfo,function(preepopRecords)
          {
              res.json({
                  Skelton : formSkelton[0].FormSkeleton,
                  records : preepopRecords,
                  recordIdsList : [],
                  formInfo : [],
                  reassignRecordIds : [],
                  comments : [],
                  "recordInfo" : recordinfo
                });
          })
        
        } else {
          res.json({
            "message" : "No Data Found",
            "status" : 204
          });

        }
      });
  });
      
});

router.post('/deletePendingRecords', function (req, res, next) {
	taskAssignments.update({formId:req.body.formId,taskId:req.body.taskId},{$pull:{records:{"RUID":{$in :req.body.ruid}}}},{ multi: true }, function(err,success) {
	    if(err) {
	      return err
	    }
	    else {
            res.json({
                "message": "Records deleted successfully",
                "status": 200
            });
	    }
	});
});

router.post('/addPendingRecords', function (req, res, next) {
	taskAssignments.update({formId:req.body.formId,taskId:req.body.taskId},{$push:{records:{"id":"test"}}},{ multi: true }, function(err,success) {
	    if(err) {
	      return err
	    }
	    else {
	      res.json({
                "message": "Records added successfully",
                "status": 200
            });
	    }
	});
});

router.get('/gettasksofproject/:id',function(req,res,next){
   tasks.find({projectID:req.params.id,isDeleted:false},function(err,post){
            if (post.length > 0) {
            res.json({
                "data": post,
                "status": 200
            });
            
        } else {
            res.json({
                "message": "No tasks available",
                "status": 204
            });
          }

   });

});

router.get('/getAssignedTask/:taskId', function(req, res, next) {
   
   taskAssignments.find({taskId:req.params.taskId},function(err,data){
          console.log(data)
          if(data.length>0){
            res.json({
              "data" :data,
              "status":200
             });
          }
         else{
          res.json({
              "message" :"No Assigmnets done",
              "status":204
             });
         }

   });


});

router.put('/updateProjectTask/:taskId', function(req, res, next) {
    console.log( req.body)
    tasks.findByIdAndUpdate(req.params.taskId, req.body, function(err, post) {

         if(err){
          console.log(err);
         }
         else{
          res.json({ "message": "ProjectTask updated successfully",
                        "status": 200})
         }
          
    });   

});

/*
Who         : SV
Date        : 04-Apr-2018
Description : Added this service to fix enable/disable of '+' icon in project task page in mobile
*/
// Begin 
router.get('/getFormSubmittedStatus/:formId/:taskId/:formFrequency/:userName', function(req, res, next)
{
 
  var records =[] 
  var formDisplayfields = [];
  var workIns ;
  var workReferences;
  var version;
  var formFrequency;
  var formSubmittedStatus = 0;

  Formsz.find({_id:req.params.formId},{requiredField:1,workInstruction:1,references:1,version:1,_id:0,FormSkeleton:1,dependentFields:1},function(err,form){
    versionManage.find({formId : req.params.formId}, function(err,vm){
      version = vm[0].version;

      formDisplayfields = form[0].requiredField; 
      formSkeleton =  form[0].FormSkeleton;
      workIns =  form[0].workInstruction;
      workReferences = form[0].references;
      formFrequency = req.params.formFrequency;

      if(formFrequency == "None")
      {

        FormszDetails.find({
          formId : req.params.formId,
          taskId : req.params.taskId,
          updatedBy : req.params.userName},
          {_id:0,records:1,formFrequency:1},
          function(err, formszRecs)
          {
            if (formszRecs.length > 0) 
            {
              formSubmittedStatus = 1;
            }
            else
            {
              formSubmittedStatus = 0;
            }
            var obj ={}
            obj.formSkeleton = formSkeleton;
            obj.records = records;
            obj.requiredField = formDisplayfields;
            obj.workInstruction = workIns;
            obj.references = workReferences
            obj.dependentFields = form[0].dependentFields;
            obj.version = version;
            obj.formSubmittedStatus = formSubmittedStatus;               
            res.json(obj);
          });
      }
      else if (formFrequency == "Daily")
      {
        console.log('In Daily');
        todayStart = new Date();
        todayStart.setHours(0,0,0,0);
                    
        todayEnd = new Date();
        todayEnd.setHours(23,59,59,59);

        FormszDetails.find({
          formId : req.params.formId,
          taskId : req.params.taskId,
          updatedBy : req.params.userName,
          updatedTime: { $gte:todayStart, $lt: todayEnd }},
          {updatedTime:1},
          function(err,formszRecs)
          {
            console.log('formszRecs length'+formszRecs.length);
            if (formszRecs.length >0) 
            {
              formSubmittedStatus = 1;             
            }
            else
            {
              formSubmittedStatus = 0;
            }
            var obj ={}
            obj.formSkeleton = formSkeleton;
            obj.records = records;
            obj.requiredField = formDisplayfields;
            obj.workInstruction = workIns;
            obj.references = workReferences
            obj.dependentFields = form[0].dependentFields;
            obj.version = version;
            obj.formSubmittedStatus = formSubmittedStatus;               
            res.json(obj);
          });
      }
      else if (formFrequency == "Weekly")
      {
        var curr = new Date; // get current date
        var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
        var last = first + 6; // last day is the first day + 6

        var firstDay = new Date(curr.setDate(first));
        var lastDay = new Date(curr.setDate(last));
        //var storedDate = new Date(recordObject.updatedTime);

        FormszDetails.find({
          formId : req.params.formId,
          taskId : req.params.taskId,
          updatedBy : req.params.userName,
          updatedTime: { $gte:firstDay, $lt: lastDay }},
          {updatedTime:1},
          function(err,formszRecs)
          {
            if (formszRecs.length > 0)
            {
              formSubmittedStatus = 1;
            }
            else
            {
              formSubmittedStatus = 0;
            }
            var obj ={}
            obj.formSkeleton = formSkeleton;
            obj.records = records;
            obj.requiredField = formDisplayfields;
            obj.workInstruction = workIns;
            obj.references = workReferences
            obj.dependentFields = form[0].dependentFields;
            obj.version = version;
            obj.formSubmittedStatus = formSubmittedStatus;               
            res.json(obj);
          });                
                
        }
        else if (formFrequency == "Monthly")
        {
          //var storedDate = new Date(recordObject.updatedTime);
          var date = new Date(), y = date.getFullYear(), m = date.getMonth();
          var firstDay = new Date(y, m, 1);
          var lastDay = new Date(y, m + 1, 0);

          FormszDetails.find({
          formId : req.params.formId,
          taskId : req.params.taskId,
          updatedBy : req.params.userName,
          updatedTime: { $gte:firstDay, $lt: lastDay }},
          {updatedTime:1},
          function(err,formszRecs)
          {
            if (formszRecs.length > 0)
            {
              formSubmittedStatus = 1;
            }
            else
            {
              formSubmittedStatus = 0;
            }
            var obj ={}
            obj.formSkeleton = formSkeleton;
            obj.records = records;
            obj.requiredField = formDisplayfields;
            obj.workInstruction = workIns;
            obj.references = workReferences
            obj.dependentFields = form[0].dependentFields;
            obj.version = version;
            obj.formSubmittedStatus = formSubmittedStatus;               
            res.json(obj);
          });         
        }
        else if (formFrequency == "Yearly")
        {
          var dt = new Date()
          var month = dt.getMonth()
          var year = dt.getFullYear()
          //current year 
          if(month == 0 || month == 1 || month == 2)
          {
            var firstDay = new Date(year-1, 3, 1);
            var lastDay = new Date(year, 3, 0);
          }
          else
          {
            var firstDay = new Date(year, 3, 1)
            var lastDay = new Date(year+1, 3, 0);
          }

          FormszDetails.find({
          formId : req.params.formId,
          taskId : req.params.taskId,
          updatedBy : req.params.userName,
          updatedTime: { $gte:firstDay, $lt: lastDay }},
          {updatedTime:1},
          function(err,formszRecs)
          {
            if (formszRecs.length > 0)
            {
              formSubmittedStatus = 1;
            }
            else
            {
              formSubmittedStatus = 0;
            }
            var obj ={}
            obj.formSkeleton = formSkeleton;
            obj.records = records;
            obj.requiredField = formDisplayfields;
            obj.workInstruction = workIns;
            obj.references = workReferences
            obj.dependentFields = form[0].dependentFields;
            obj.version = version;
            obj.formSubmittedStatus = formSubmittedStatus;               
            res.json(obj);
          });
        }
    });      
  });
});

function getRecordds(req,taskAssignmentsRecords,frequency,fromDate,toDate,preepopRecords,recordinfo,callback)
{ 
    var obj = {}
    obj.incompleteDate = fromDate;
    recordinfo.push(obj);

    var d = new Date(fromDate);
    d.setHours(d.getHours() + 5);
	var dayStart = d.setMinutes(d.getMinutes() + 30);
	var dayEnd = d.setHours(d.getHours() + 24);

	var curr = new Date(dayStart); // get current date
	var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
	var last = first + 6; // last day is the first day + 6

	var firstday = new Date(curr.setDate(first)).toUTCString();
	var lastday = new Date(curr.setDate(last)).toUTCString();

	var date = new Date(dayStart), y = date.getFullYear(), m = date.getMonth();
	var firstDay = new Date(y, m, 1);
	var lastDay = new Date(y, m + 1, 0);

	var date = new Date(dayStart), y = date.getFullYear(), m = date.getMonth();
	var firstDayInYear = new Date(y, 0, 1);
	var lastDayInYear = new Date(y, 12, 0);


         if(frequency =="None")
         {

    FormszDetails.find({formId :req.params.formId, taskId : req.params.taskId },{record:1,updatedTime:1,RUID:1,},function (err, submutedRecords) {
            if (submutedRecords.length > 0)
            {
                  var submiotedRecordIdsList = []
                  for (var i = 0; i < submutedRecords.length; i++) 
                  {
                      submiotedRecordIdsList.push(submutedRecords[i].RUID)
                  }
                  for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                    
                    if(submiotedRecordIdsList.indexOf(taskAssignmentsRecords[i].RUID))
                    {
                        var obj ={}
                        preepopRecords.push(taskAssignmentsRecords[i])
                    }
                 
                  }
                  callback(preepopRecords)
            } 
            else 
            {
                for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                        preepopRecords.push(taskAssignmentsRecords[i])
                  }

                  callback(preepopRecords)
            }
          });

         }else if (frequency =="Daily"){

    FormszDetails.find({formId :req.params.formId, taskId : req.params.taskId,updatedTime:{ $gte:dayStart,$lte:dayEnd }},{record:1,updatedTime:1,RUID:1,},function (err, submutedRecords) {
            if (submutedRecords.length > 0)
            {
                  var submiotedRecordIdsList = []
                  for (var i = 0; i < submutedRecords.length; i++) 
                  {
                      submiotedRecordIdsList.push(submutedRecords[i].RUID)
                  }
                  for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                    
                    if(submiotedRecordIdsList.indexOf(taskAssignmentsRecords[i].RUID))
                    {
                        var obj ={}
                        preepopRecords.push(taskAssignmentsRecords[i])
                    }
                 
                  }
                  callback(preepopRecords)
            } 
            else 
            {
                for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                        preepopRecords.push(taskAssignmentsRecords[i])
                  }

                  callback(preepopRecords)
            }
          });

         }else if (frequency =="Weekly"){
    FormszDetails.find({formId :req.params.formId, taskId : req.params.taskId,updatedTime:{ $gte:firstday,$lte:lastday } },{record:1,updatedTime:1,RUID:1,},function (err, submutedRecords) {
            if (submutedRecords.length > 0)
            {
                  var submiotedRecordIdsList = []
                  for (var i = 0; i < submutedRecords.length; i++) 
                  {
                      submiotedRecordIdsList.push(submutedRecords[i].RUID)
                  }
                  for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                    
                    if(submiotedRecordIdsList.indexOf(taskAssignmentsRecords[i].RUID))
                    {
                        var obj ={}
                        preepopRecords.push(taskAssignmentsRecords[i])
                    }
                 
                  }
                  callback(preepopRecords)
            } 
            else 
            {
                for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                        preepopRecords.push(taskAssignmentsRecords[i])
                  }

                  callback(preepopRecords)
            }
          });


         }else if (frequency =="Monthly"){

    FormszDetails.find({formId :req.params.formId, taskId : req.params.taskId,updatedTime:{ $gte:firstDay,$lte:lastDay } },{record:1,updatedTime:1,RUID:1,},function (err, submutedRecords) {
            if (submutedRecords.length > 0)
            {
                  var submiotedRecordIdsList = []
                  for (var i = 0; i < submutedRecords.length; i++) 
                  {
                      submiotedRecordIdsList.push(submutedRecords[i].RUID)
                  }
                  for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                    
                    if(submiotedRecordIdsList.indexOf(taskAssignmentsRecords[i].RUID))
                    {
                        var obj ={}
                        preepopRecords.push(taskAssignmentsRecords[i])
                    }
                 
                  }
                  callback(preepopRecords)
            } 
            else 
            {
                for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                        preepopRecords.push(taskAssignmentsRecords[i])
                  }

                  callback(preepopRecords)
            }
          });


         }else if (frequency =="Yearly"){

    FormszDetails.find({formId :req.params.formId, taskId : req.params.taskId,updatedTime:{ $gte:firstDayInYear,$lte:lastDayInYear } },{record:1,updatedTime:1,RUID:1,},function (err, submutedRecords) {
            if (submutedRecords.length > 0)
            {
                  var submiotedRecordIdsList = []
                  for (var i = 0; i < submutedRecords.length; i++) 
                  {
                      submiotedRecordIdsList.push(submutedRecords[i].RUID)
                  }
                  for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                    
                    if(submiotedRecordIdsList.indexOf(taskAssignmentsRecords[i].RUID))
                    {
                        var obj ={}
                        preepopRecords.push(taskAssignmentsRecords[i])
                    }
                 
                  }
                  callback(preepopRecords)
            } 
            else 
            {
                for (var i = 0; i < taskAssignmentsRecords.length; i++) 
                  {
                        preepopRecords.push(taskAssignmentsRecords[i])
                  }

                  callback(preepopRecords)
            }
          });


         }



}
function getDateFormate(date){
		var currentDate = new Date(date);
		return new Date(currentDate.getDate(),currentDate.getMonth(),currentDate.getFullYear())
}

/*
Who         : SV
Date        : 06-Apr-2018
Description : Modified the function isStriedDateBetweenSandEDate to correct the logic
*/
// Begin
// function isStriedDateBetweenSandEDate(startDate,endDate,storedDate)
// {
   
//     var fDate,lDate,cDate;
   
//     from = new Date(startDate)
//     to = new Date(endDate)

//     check = new Date(storedDate)
    
//     var fromdd = from.getDate();
//     var frommm = from.getMonth()+1; //January is 0!

//     var fromyyyy = from.getFullYear();
      
//     var todd = to.getDate();
//     var tomm = to.getMonth()+1; //January is 0!

//     var toyyyy = to.getFullYear();
    
//     var checkdd = check.getDate();
//     var checkmm = check.getMonth()+1; //January is 0!

//     var checkyyyyy = check.getFullYear();
    

//     var fromDate1 = frommm+'/'+fromdd+'/'+fromyyyy;
//     var toDate1 = tomm+'/'+todd+'/'+toyyyy;
//     var storedDate1 = checkmm+'/'+checkdd+'/'+checkyyyyy;

//     var d1 = fromDate1.split("/");
//     var d2 = toDate1.split("/");
//     var c = storedDate1.split("/");

//     var from = new Date(d1[2], parseInt(d1[1])-1, d1[0]);  // -1 because months are from 0 to 11
//     var to   = new Date(d2[2], parseInt(d2[1])-1, d2[0]);
//     var check = new Date(c[2], parseInt(c[1])-1, c[0]);
    
//     return check > from && check < to
    

    
// }
function isStriedDateBetweenSandEDate(startDate,endDate,storedDate)
{
  from = new Date(startDate)
  to = new Date(endDate)
  check = new Date(storedDate)
  
  var fromdd = from.getDate();
  var frommm = from.getMonth()+1; //January is 0!
  var fromyyyy = from.getFullYear();
    
  var todd = to.getDate();
  var tomm = to.getMonth()+1; //January is 0!
  var toyyyy = to.getFullYear();
  
  var checkdd = check.getDate();
  var checkmm = check.getMonth()+1; //January is 0!
  var checkyyyy = check.getFullYear();

  var from = new Date(fromyyyy, parseInt(frommm)-1, fromdd);  // -1 because months are from 0 to 11
  var to   = new Date(toyyyy, parseInt(tomm)-1, todd);
  var check = new Date(checkyyyy, parseInt(checkmm)-1, checkdd);
 
  return check > from && check < to

}


function dateCheck(from,to,check) {

    var fDate,lDate,cDate;
    from = new Date(from)
    to = new Date(to)

    check = new Date(check)

    fDate = Date.parse(from);
    lDate = Date.parse(to);
    cDate = Date.parse(check);
    if((cDate <= lDate && cDate >= fDate)) {
        return true;
    }
    return false;
}
module.exports = router;
