var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var projectTaskRecords = new mongoose.Schema({
		taskId:{ type: String, default: null },
		formId:{type:String,default:null},
		record:[],
		lastDataReqTime:{ type: Date, default:null}
		
});
projectTasks.plugin(mongoosePaginate);
module.exports = mongoose.model('projectTaskRecords', projectTaskRecords);

