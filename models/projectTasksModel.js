var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var projectTasks = new mongoose.Schema({
		name:{ type: String, default: null },
		description:{type:String,default:null},
		createdDateTime:{ type: Date, default: Date.now },
		isClosed:{type:Boolean,default:false}, //false-open,true-closed
		isDeleted:{type:Boolean,default:false},
		startDate:{ type: Date, default: Date.now },
		endDate:{ type: Date, default: Date.now },
		lastDataReqTime:{ type: Date, default:null},
		deparmentId:{type:String,default:null},
		users:[],
		zonalAdminId:{ type: String, default: null },
		projectID: { type: String, default: null },
		taskType:{type:Boolean,default:false},
		formzCategory : [],		
		isDeleted :{type:Boolean,default:false}

		
});
projectTasks.plugin(mongoosePaginate);
module.exports = mongoose.model('projectTasks', projectTasks);

