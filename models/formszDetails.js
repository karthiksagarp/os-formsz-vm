var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var formszDetailsSchema = new mongoose.Schema({
  formId :{type:String,default:null},
  taskId:{type:String,default:null},
  updatedTime:{ type: Date, default: Date.now },
  updatedBy:{type:String,default:null},
  record:[],
  lastUpdatedBy:{type:String,default:null},
  isDeleted:{type:Boolean,default:false},
  lat:{type:String,default:null},
  long:{type:String,default:null},
  IsReassign:{type:Boolean,default:false},
  comments:{type:String,default:null},
  assignedTo:{type:String,default:null},
  version:{type:String,default:null},
  status:{type:Boolean,default:false},
  fromType:{type:Boolean,default:false},
  DSAssignedTo:{type:String,default:null},
  prepopulatedRecord:[],
  //add for project by venki
  RUID:{type:String,default:null},
  sqliteDBId:{type:String,default:null}, //Added by Divya For Sync Service TPCL
  videoId :{type:String,default:null},
  lastModifiedBy:{type:String,default:null},
  lastModifiedDate:{type:String,default:null},
  generatedId:{type:String,default:null}



});
formszDetailsSchema.plugin(mongoosePaginate)
module.exports = mongoose.model('FormszDetails', formszDetailsSchema);
