var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var downloadFormInfo = new mongoose.Schema({
    formId:{ type: String, default: null },
    taskId:{ type: String, default: null },
    username:{type:String,default:null},
    dowmloadedDateTime:{ type: Date, default: Date.now }
    
});
downloadFormInfo.plugin(mongoosePaginate);
module.exports = mongoose.model('downloadFormInfo', downloadFormInfo);

