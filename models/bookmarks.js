
var mongoose = require('mongoose');
var bookmarkInfo = new mongoose.Schema({
 		bookmarkName: {type: String,  default:null  },
		northEast: {type: Object,  default:null  },
		southWest:{type: Object,  default:null  },
		zoomLevel:{type: String, default: null },
		username:{type: String, default: null }
});
//userSchema.plugin(mongoosePaginate); 
module.exports = mongoose.model('bookmarkInformation', bookmarkInfo);

