var mongoose = require('mongoose');
//var mongoosePaginate = require('mongoose-paginate');
var versionSchema = new mongoose.Schema({
		/*_id:String,*/
 		name: String,
		version:{type:Number,default:0},
		formType :{type:String,default:null},
		createdTime:{ type: Date, default: Date.now },
		createdBy:{type:String,default:null},
		isVisible:{type:Boolean,default:true},
		// category:{type:String,default:null},
		// FormSkeleton:{},
		// templateoriginalName:{type:String,default:null},
		// allocatedUsers:{type:String,default:null},
		// formzCategory:{type:String,default:null},
		userGroup:{type:String,default:null},
		updatedTime:{ type: Date, default: Date.now },
		requiredField:[],
		BsicLevelChanges:[],
		formId:{type:String,default:null},
		formSkeletonLevelChanges:{},
		requiredVersion:{},
		derivedFieldVersion:{},
		originalFormDetails:{}
    // isAllowMap:{type:Boolean,default:false},
    // description:{type:String,default:null}
});
//formszSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('versionManagement', versionSchema);
