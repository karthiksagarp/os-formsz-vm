var mongoose = require('mongoose');
var deviceManagementSchema = new mongoose.Schema({
  Manufacturer :{type:String,default:null},
  Model:{type:String,default:null},
  MacAddress : {type:String,default:null},
  UUID : {type:String,default:null},
  status : {type:String,default:null},
  platform : {type:String,default:null},
  version : {type:String,default:null},
  groupname:{type:String,default:null},
  createdTime:{ type: Date, default: Date.now},
/*
	@Developer: Santhosh Kumar Gunti
	@Type: modify
	@Date : 19th april 2018
	@Description : Added requestedUser to show the first time device requested user in device management table 
*/
  requestedUser : {type:String,default:null},
  isDeleted:{type:Boolean,default:false}
});
module.exports = mongoose.model('deviceManagement', deviceManagementSchema);
