var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var projectTasks = new mongoose.Schema({
		formId:{type:String,default:null},
		formName:{type:String,default:null},
		formCat:[],
		lastPickTime:{ type: Date, default:null},
		lastPickBy:{type:String,default:null},
		isClosed:{type:Boolean,default:false}, //false-open,true-closed
		isDeleted:{type:Boolean,default:false},
		startDate:{ type: Date, default: Date.now },
		endDate:{ type: Date, default: Date.now },
		formFrequency:{type:String,default:null},
		users:[],
		records:[],
		taskId:{ type: String, default: null },
		version:{ type: String, default: null },
		projectID: { type: String, default: null },

		isDeleted :{type:Boolean,default:false}

		
});
projectTasks.plugin(mongoosePaginate);
module.exports = mongoose.model('projectTasksAssign', projectTasks);

