var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var projects = new mongoose.Schema({
		name:{ type: String, default: null },
		aliasName:{type:String,default:null},
		zone:{type:String,default:null},
		category:[],
		description:{type:String,default:false},
		startDate:{type: Date, default: Date.now},
		endDate:{type: Date, default: Date.now},
		remarks1:{ type:String,default:null },
		remarks2:{ type:String,default:null},
		createdBy:{ type:String,default:null},
		deparmentId:{ type:String,default:null},
		zonalAdmins:[],
		createdDateTime:{ type: Date, default: Date.now },
		isDeleted :{type:Boolean,default:false}
});
projects.plugin(mongoosePaginate);
module.exports = mongoose.model('Projects', projects);

