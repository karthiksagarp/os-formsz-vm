var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var activityLogSchema = new mongoose.Schema({
  recordId:{type:String,default:null},
  formName:{type:String,default:null},
  activity:{type:String,default:null},
  adminname:{type:String,default:null},
  username:{type:String,default:null},
  timestamp:{ type: Date, default: Date.now },
  formId : {type:String,default:null},
  usergroup : {type:String,default:null},
  source :{type:String,default:null},
  deviceUUID:{type:String,default:null},
  deviceModel :{type:String,default:null},
  taskId : {type:String,default:null},
  taskName : {type:String,default:null},
/*
	@Developer : Santhosh Kumar Gunti
	@Description :Added version to show in activity log
	@Type: Modify
	@Date : 19th april 2018

*/
  version : {type:String,default:null},
/*
  @Developer : Santhosh Kumar Gunti
  @Description :Added formDescription to show in activity log
  @Type: Modify
  @Date : 19th april 2018

*/
  formDescription:{type:String,default:null}


});
activityLogSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('activitylogHistorymodel', activityLogSchema);
