var mongoose = require('mongoose');
//var mongoosePaginate = require('mongoose-paginate');
var notificationDetailsSchema = new mongoose.Schema({
  recordId:{type:String,default:null},
  taskName:{type:String,default:null},
  taskId:{type:String,default:null},
  actionType:{type:String,default:null},
  displayValues:{type:String,default:null},
  adminName:{type:String,default:null},
  comments:{type:String,default:null},
  user:{type:String,default:null},
 status :{type:Boolean,default:false},
  createdTime:{ type: Date, default: Date.now },
  formId : {type:String,default:null}

  
});
//formszDetailsHistorySchema.plugin(mongoosePaginate)
module.exports = mongoose.model('notificationmodel', notificationDetailsSchema);