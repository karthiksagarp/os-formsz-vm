var mongoose = require('mongoose');
var organizationSchema = new mongoose.Schema({
	//id:String,
	email:{type:String,default:null},
	organizationName:{type:String,default:null},
	organizationAddress1:{type:String,default:null},
	organizationAddress2:{type:String,default:null},
	organizationContact1:{type:String,default:null},
	organizationContact2:{type:String,default:null},
	imageurl:{type:String,default:null},
	createdDateTime:{ type: Date, default: Date.now }
});
module.exports = mongoose.model('organizationalInfo', organizationSchema);

