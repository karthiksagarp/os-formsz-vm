var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var userSchema = new mongoose.Schema({
		id:String,
 		username: String,
		password: {type:String,default:'b896556066b3ca6977'},
		name:{type:String,default:null},
		type :{type:String,default:null},
		email:{type:String,default:null},
		phone:{type:String,default:null},
		// groupname:{type:String,default:null},
		//groupname:[],
		groupname:[],
		isDeleted:{type:Boolean,default:false},
		adminType:{type:String,default:null},
		privilage:{type:String,default:null},
		imageurl:{type:String,default:null},

		// isUserLocatorActive :{type:Boolean,default:true},
		// @Swathi 05-Jan-2018
		isUserLocatorActive :{type:Boolean,default:false},
		// -----------------------------------------------
		selectedGroupList :[],
		isUserUpdatePermissionActive:{type:Boolean,default:true},
		admingroup:{type:String,default:null},
		assignedLayers:[],
		/*TPCL Customization*/
		bloodgroup: {type:String,default:null},
		adderess2: {type:String,default:null},
		adderess1: {type:String,default:null},
		activity: {type:String,default:null},
		zone: {type:String,default:null},
		vender: {type:String,default:null},
		doj:{ type: Date, default: Date.now },
		dob:{ type: Date, default: Date.now },
		createdDateTime:{ type: Date, default: Date.now },
		//-->Umamaheswari B: Added: TPCL Customizations
		//Notes: Added to fetch associated dept admin details from groups table in login
		adminlist:[],
		//End
		//-->Siva Kella: Added: TPCL Customizations
		//Notes: Added to include createdBy and createdByMailID
		createdBy: 		{type:String,default:null},
		createdByMailID:{type:String,default:null},
		//<-- End
		
		//-->Santhosh Kumar Gunti: Added: TPCL Customizations
		//Notes: Added to include isFirstLogin
		isFirstLogin: 		{type:Boolean,default:true},
		//<-- End

		deviceDetails :[],
		lastLoggedInTime:{type:Date,default:null}

});
//userSchema.index({username:1,type: 1, email: 1 },{unique: true});

userSchema.index({type: 1, email: 1 },{unique: true});
userSchema.index({username:1,type:1},{unique:true});
userSchema.plugin(mongoosePaginate);
module.exports = mongoose.model('Users', userSchema);

