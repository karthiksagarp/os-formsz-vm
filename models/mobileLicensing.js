var mongoose = require('mongoose');
var MobileLicenceSchema = new mongoose.Schema({
		licenseId:{type:String,default:null},
		isActive:{type:Boolean,default:false},
		assignedTo:{type:String,default:null},
		timestamp:{ type: Date, default: Date.now },
		department :{type:String,default:null},
		loggedInTime : { type: Date, default: null },
		logouttime:{ type: Date, default:null },
		deviceDetails:{type:String,default:null}
});
module.exports = mongoose.model('MobileLicence', MobileLicenceSchema);

