var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var formszSchema = new mongoose.Schema({
 		name: String,
		version:{type:String,default:0},
		formType :{type:String,default:null},
		createdTime:{ type: Date, default: Date.now },
		createdBy:{type:String,default:null},
		isVisible:{type:Boolean,default:true},
		category:{type:String,default:null},
		FormSkeleton:{},
		urlAttachment:String,
		isoNumber:String,
		alternativeMailid:{type:String,default:null},
		allocatedUsers:{type:String,default:null},
		//formzCategory:{type:String,default:null},
	  	// @TPCL Change formzCategory to Array
	  	formzCategory:[],
	  //userGroup:{type:String,default:null},
	  // @TPCL Change userGroup to Array
	  userGroup:[],

/*
@developer : Santhosh Kumar Gunti
TPCL customization
forattaching files to form
*/
	  generatedIdForFileAttachment:[],

/*
@developer : Santhosh Kumar Gunti
TPCL customization
for sharing user groups
*/
	  shareGroup:[],

		//formzCategory:{type:String,default:null},
		//userGroup:{type:String,default:null},
		templateoriginalName:{type:String,default:null},
		requiredField:[],
		allocateduserGroups:[],
 		allocatedcategories:[],
		dependentFields:[],
		
 		/*numOfUsersDownloaded:{type:Number,default:0},
 		lastDownloadedDate:{type:Number,default:0},
 		taskAssignedUsers:{type:Number,default:0},*/
    isAllowMap:{type:Boolean,default:false},
    geoFields:[],
    description:{type:String,default:null},
    lastModifiedBy : {type: Date, default:null},
/*
@developer : Santhosh Kumar Gunti
TPCL customization
Added work instruction and references in form creation
*/
    workInstruction:{type:String,default:null},
    references:{type:String,default:null},
    isMediaAvailable:{type:Boolean,default:false}
});
formszSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Formsz', formszSchema);
