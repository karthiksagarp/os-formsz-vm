var express = require('express');
var conn;
var router = express();
var mongoose = require('mongoose');
var formidable = require('formidable');
var Grid = require('gridfs-stream');
var fs = require('fs');
var FormszDetails = require('../models/formszDetails.js');

Grid.mongo = mongoose.mongo;

  //var conn = mongoose.createConnection('mongodb://localhost:27017/TPCLDFormsz', function(err) {
  var conn = mongoose.createConnection('mongodb://appconnect1:cTpcl!32018@localhost:2718/TPCLDFormsz', function(err) {

 //var conn = mongoose.createConnection('mongodb://localhost:32018/formsz', function(err) {

    if(err) {
        console.log('err '+err);
    } else {
        console.log('connection established ');
    }
});  
var gfs = Grid(conn.db);
//console.log(gfs);
conn.once('open', function () {
  var gfs = Grid(conn.db);
  router.set('gfs',gfs);
console.log("gfss..")
  // all set!
})
//router.set('gfs',gfs);
router.post('/addvideo/:id',function(req,res){
 // console.log("img ")
   var form = new formidable.IncomingForm();
   form.parse(req, function(err, fields, files){
   if(files.video){
		var path=files.video.path;
		var buffer=fs.readFileSync(path);
		var contentType1=files.video.type;
    var writestream = gfs.createWriteStream({
      filename: files.video.name,
      mode: "w",
       content_type: contentType1,
	    metadata:{
		fieldId:JSON.parse(fields.params).fieldId,
		generatedId:req.params.id
	  }
    });
    fs.createReadStream(path).pipe(writestream);
     writestream.on('close', function (file) {
		 res.json(file)
/*         FormszDetails.find({generatedId:req.params.id},function(err,record) {
          console.log(record)
   //       console.log(record.record)
          console.log("...........................................")
          console.log(fields)
        if(record.length>0) {
          record[0].record[0][JSON.parse(fields.params).fieldId] =file._id.toString();
          var data = record[0].record;
          FormszDetails.update({_id:record[0]._id},{$set:{record:data}}, function(err,success) {
            if(err) {
              return err
            }
            else {
              console.log("success");
               res.json(file)
            }

          })
        
        }
        else {
          console.log("im in else else");
        }
      }) */

     
     });
     }
     else {
     	console.log("im in else");
     }
   }); 
});
//for direct submit
router.post('/addRecordvideo/:id',function(req,res){
 console.log("imgggggggggggggggggggggggggg ")
   var form = new formidable.IncomingForm();
   form.parse(req, function(err, fields, files){
   if(files.video){
		var path=files.video.path;
		var buffer=fs.readFileSync(path);
		var contentType1=files.video.type;
    var writestream = gfs.createWriteStream({
      filename: files.video.name,
      mode: "w",
       content_type: contentType1,
	   metadata:{
		fieldId:fields.fieldId,
		generatedId:req.params.id
	  }

    });
    fs.createReadStream(path).pipe(writestream);
     writestream.on('close', function (file) {
		  res.json(file)
    /*   FormszDetails.find({generatedId:req.params.id},function(err,records) {
      console.log(records)
        if(records.length!=0) {
      console.log(records[0].record[0]);
          records[0].record[0][fields.fieldId] =file._id.toString();
          var data = records[0].record[0];
  
          updateRecord(records,data,file,res)   
        }
        else {
          console.log("im in else else");
        }
      }); */

  
     });
     }
     else {
     	console.log("im in else");
     }
   }); 
});
router.get('/fetchVideo/:generatedId/:fieldId',function(req,res){
  gfs.findOne({"metadata.generatedId":req.params.generatedId,"metadata.fieldId":req.params.fieldId}, function (err, data) {	
	console.log(data);
		if(data.length>0){
			console.log("length");
			res.json({
                    "data": data._id,
                    "status": 200
                })
		}else{
			res.json({
                    "data":"No Video Found",
                    "status": 204
                })
		}
	}); 
});
 function updateRecord(records,data,file,res){
   FormszDetails.update({_id:records[0]._id},{$set:{"record":data}}, function(err,success) {
            if(err) {
              return err
            }
            else {
              console.log("success");
                  res.json(file)
            }
          })  
 }
router.get('/getvideo/:vid',function(req,res){
	console.log(req.params.vid)
				 gfs.findOne({_id: req.params.vid}, function (err, file) {
    if (err) {
        return res.status(400).send(err);
    } else if (!file) {
        return res.status(404).send(' ');
    } else {
        res.header("Content-Type","video/mp4");
        res.header("X-Content-Type-Options", "nosniff");
        res.header("Accept-Ranges", "bytes");
        res.header("Content-Length",file.length);

        var readStream = gfs.createReadStream({_id: file._id});
        readStream.on('open', function () {
            console.log('Starting download...');
        });
        readStream.on('data', function (chunk) {
            console.log('Loading...');
        });
        readStream.on('end', function () {
            console.log('Video is ready to play');
        });
        readStream.on('error', function (err) {
            console.log('There was an error with the download' + err);
            res.end();
        });
        readStream.pipe(res);
    }
});
});

module.exports = router;
