var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Groups = require('../models/group.js');
var formidable = require('formidable');
var Users = require('../models/user.js');
var log = require('./log')(module);
var Formsz = require('../models/formsz.js');
var async = require('async');
//var pushNotifications = require('../routes/utils').pushNotifications;
var getdeviceKeys = require('../routes/utils').getdeviceKeys;
var admins = [];
var finalObj ={};
var updatedGroupList = [];
/* Get All UserGroups. */
router.get('/getgrouplist/:limit/:offset', function(req, res, next) {
  var limit = parseInt(req.params.limit);
    Groups.paginate({isDeleted: false}, {page: req.params.offset,limit:limit,sort: {createdDateTime: -1}}, function(err, Groups) {
        if (err) return next(err);
        res.json(Groups);
    });
});
//Get All UserGroups filter By UserGroup
router.get('/getgroupadmin/:groupname', function(req, res, next) {
    Groups.find({isDeleted: false,name: req.params.groupname}, function(err, Groups) {
        if (Groups.length > 0) {
            Users.find({groupname: data.name,type: 1,isDeleted: false}, function(err, Users) {
                if (Users.length > 0) {
                    res.json(Users.username);

                } else {
                    res.json({
                        "message": "No Data Found",
                        "status": 204
                    });
                }
            });
            res.json(Groups);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
        if (err) return next(err);

    });
});

/* Get UnGroup List. */
router.get('/UMgrouplist', function(req, res, next) {
    Groups.find({isActive: false,isDeleted: false},{name:1}, function(err, post) {
        if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "no data",
                "status": 204
            });
        }
    });
});
/* Get All mapped or unmapped For TPCL List.
    @Umamaheswari
 */
router.get('/Allgrouplist', function(req, res, next) {
    Groups.find({$or: [{
                        isActive: true,
                        isDeleted: false
                    }, {
                        isActive: false,
                        isDeleted: false
                    }]},{name:1}, function(err, post) {
        if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "no data",
                "status": 204
            });
        }
    });
});

router.get('/Allgrouplist/:id', function(req, res, next) {
   
    Groups.find({isDeleted: false}, function(err, post) {
        if (post.length > 0) {
            async.forEachSeries(post, function (post1, next) {
                Users.find({_id: req.params.id,'selectedGroupList.id':post1.id,type:3},function(err1,user){
                    if(user.length == 0)
                        next();
                    else {
                       updatedGroupList = user[0].selectedGroupList;

                        next();
                    }
                    
                })
               
            }, function(err) {
            finalObj['totalGroups'] =post;
            finalObj['selectedGroupList'] =updatedGroupList;
            res.json({data:finalObj,status:200});

            });
            
        } else {
            res.json({
                "message": "no data",
                "status": 204
            });
        }
    });
});

// Get All UserGroups without pagination for filter activity. /
router.get('/getAllgrouplist', function(req, res, next) {
    Groups.find({isDeleted: false},{name:1},function(err,post){
        if (err) return next(err);
        res.json(post);
    });
});
/*  Create group */
/*router.post('/create', function(req, res, next) {
        // Added by renuka for tpcl
        var Active;
        if(req.body.userList!=null||req.body.adminList!=null||req.body.groupadminList!=null)
         Active=true
        else
         Active=false
       var Groups1=new Groups({
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        isActive:Active,
        description:req.body.description||null,
        userlist:req.body.userList,
        groupadminlist:req.body.groupadminList
       });

       //group Creation
              Groups.find({name:req.body.name,isDeleted:false}, function(err,groups){
                if(groups.length>0) {
                  res.json({"message":"Group already exists","status":204});
                }
                else {
                  Groups1.save(function (err, post) {
                       var groupId = post._id.toString();
                      Users.find({username:req.body.adminList},function (err, Users)
                          {
                            if(Users.length>0)
                            {
                              Users.forEach(function(dbUserObj)
                              {log.info(req.body.name);
                                dbUserObj.groupname=groupId;
                                dbUserObj.save(function (err, post) {

                                });
                              });

                            }

                          });
                      //adeedd by renuka

                         //mapping user with usergroup by renuka
                          req.body.userList.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    console.log(dbUserObj.groupname)
                                    dbUserObj.groupname.push(groupId);
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                          //mapping groupadmin with usergroup by renuka
                          req.body.groupadminList.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    console.log(dbUserObj.groupname)
                                    dbUserObj.groupname.push(groupId);
                                    console.log(dbUserObj);
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });

                  });
                  log.info("Group:"+req.body.name+" Created Successfully!");
                  res.json({"message":"Group Created Successfully","status":200});
                }
              })

});*/

/*/  Create group /*/
/*router.post('/create', function(req, res, next) {
    // Added by renuka for tpcl
        var Active;
        var adminarray=[];
        if(req.body.userList!=null||req.body.adminList!=null||req.body.groupadminList!=null)
         Active=true
        else
         Active=false
        adminarray.push(req.body.adminList);
     
           var Groups1=new Groups({
            name:req.body.name,
            email:req.body.email,
            phone:req.body.phone,
            description:req.body.description||null,
            adminlist:adminarray,
            isActive:Active,
            userlist:req.body.userList,
            groupadminlist:req.body.groupadminList

           });
      
       //group Creation
              Groups.find({name:req.body.name,isDeleted:false}, function(err,groups){
                if(groups.length>0) {
                  res.json({"message":"Group already exists","status":204});
                }
                else {
                  Groups1.save(function (err, post) {
                    var groupobj ={_id:post._id.toString(),name:post.name}
                     if(req.body.adminList!=null){
                      Users.find({username:req.body.adminList.username},function (err, Users)
                          {
                            if(Users.length>0)
                            {
                              Users.forEach(function(dbUserObj)
                              {log.info(req.body.name);

                                dbUserObj.groupname= groupobj;
                                dbUserObj.save(function (err, post) {

                                });
                              });

                            }

                          });
                        }
                         //mapping user with usergroup by renuka
                          req.body.userList.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    console.log(dbUserObj.groupname)
                                    if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                    dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                          //mapping groupadmin with usergroup by renuka
                          req.body.groupadminList.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    console.log(dbUserObj.groupname)
                                     if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                    dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                     
                  });
                  log.info("Group:"+req.body.name+" Created Successfully!");
                  res.json({"message":"Group Created Successfully","status":200});
                }
              })

});*/

router.post('/create', function(req, res, next) {
    // Added by renuka for tpcl
    var Active;
    var adminarray=[];
    //Modified by Renuka for TPCL Customizations on 22-01-2018
     /* if(req.body.adminList!=null||req.body.adminList!=undefined || req.body.userList!=null||req.body.groupadminList!=null)
      {
       Active=true;
       adminarray.push(req.body.adminList);
      }
      else{
       Active=false;
       adminarray=[];
      }*/
        // adminarray.push(req.body.adminList);
        //Code commented by Divya Bandaru on 27-03-2018 
        //New code added on 27-03-2018 by Divya Bandaru
        
        if(req.body.adminList == undefined || Object.keys(req.body.adminList) ==undefined) {
          Active =false;
        }
        else if(Object.keys(req.body.adminList).length>0) {
          console.log("im here");
          Active=true;
          adminarray = req.body.adminList;
        }
        //<--
       
     console.log(adminarray);
      var Groups1=new Groups({
        name:req.body.name,
        email:req.body.email,
        phone:req.body.phone,
        description:req.body.description||null,
        adminlist:adminarray||[],
        isActive:Active,
        userlist:req.body.userList || [],
        groupadminlist:req.body.groupadminList || []
      });
      
       //group Creation
              Groups.find({name:req.body.name,isDeleted:false}, function(err,groups){
                if(groups.length>0) {
                  res.json({"message":"Group already exists","status":204});
                }
                else {
                  Groups1.save(function (err, post) {
                    var groupobj ={_id:post._id.toString(),name:post.name}
                     if(req.body.adminList!=null){
                      Users.find({username:req.body.adminList.username,type:1},function (err, Users)
                          {
                            if(Users.length>0)
                            {
                              Users.forEach(function(dbUserObj)
                              {
                                log.info(req.body.name);

                                dbUserObj.groupname= groupobj;
                                dbUserObj.save(function (err, post) {

                                });
                              });

                            }

                          });
                        }
                         //mapping user with usergroup by renuka
                          if(req.body.userList!=null){
                          req.body.userList.forEach(function(user){
                             Users.find({username:user.username,type:2},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {
                                    log.info(req.body.name);
                                    if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                    //dbUserObj.groupname.push(groupobj);
                                    //Modified by Renuka on 22-01-2018 for TPCL
                                    var exists = false;
                                      dbUserObj.groupname.forEach(function(group){
                                        if(group._id==groupobj._id)
                                           exists= true;
                                      });
                                      if(exists== false)
                                       dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                      if(err) {
                                        console.log(err);
                                      }
                                      else {
                                        console.log("succcess");
                                      }
                                         
                                    });
                                  });
                                }
                              });
                         });
                        }
                          //mapping groupadmin with usergroup by renuka
                           if(req.body.groupadminList!=null||req.body.groupadminList!=undefined){
                          req.body.groupadminList.forEach(function(user){
                             Users.find({username:user.username,type:3},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                     if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                   // dbUserObj.groupname.push(groupobj);
                                    //Modified by Renuka on 22-01-2018 for TPCL

                                   var exists = false;
                                      dbUserObj.groupname.forEach(function(group){
                                        if(group._id==groupobj._id)
                                           exists= true;
                                      });
                                      if(exists== false)
                                       dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                      }
                  });

                  log.info("Group:"+req.body.name+" Created Successfully!");
                  res.json({"message":"Group Created Successfully","status":200});
                }
              })

});

/* GET /get Group by id */
/*router.get('/:id', function(req, res, next) {
    Groups.find({_id: req.params.id}, function(err, post, next) {
        if (err) return next(err);
        if (post.length > 0) {
			
            var admindata = [];
            post.forEach(function(dbUserObj) {
                Users.find({groupname: null,type: 1,isDeleted: false}, function(err, admins) {
						

                    if (admins.length > 0) {
						
                        admins.forEach(function(dbUserObj1) {
                            admindata.push(dbUserObj1.username);
                        });
                        Users.find({groupname: dbUserObj._id,type: 1,isDeleted: false}, function(err, alladmins) {
                            if (alladmins.length > 0) {
                                res.json({
                                    _id: dbUserObj.id,
                                    name: dbUserObj.name,
                                    email: dbUserObj.email,
                                    phone: dbUserObj.phone,
                                    description: dbUserObj.description,
                                    Admin: alladmins[0].username,
                                    userlist: dbUserObj.userlist,
                                    groupadminlist: dbUserObj.groupadminlist,
                                    adminlist: admindata
                                });
                            } else {
                                res.json({
                                    _id: dbUserObj.id,
                                    name: dbUserObj.name,
                                    email: dbUserObj.email,
                                    phone: dbUserObj.phone,
                                    description: dbUserObj.description,
                                    Admin: null,
                                    userlist: dbUserObj.userlist,
                                    groupadminlist: dbUserObj.groupadminlist,
                                    adminlist: admindata
                                });
                            }
                        });
                    } else {
                        Users.find({groupname: dbUserObj._id,type: 1,isDeleted: false}, function(err, alladmins) {
                            if (alladmins.length > 0) {
                                res.json({
                                    _id: dbUserObj.id,
                                    name: dbUserObj.name,
                                    email: dbUserObj.email,
                                    phone: dbUserObj.phone,
                                    description: dbUserObj.description,
                                    userlist: dbUserObj.userlist,
                                    groupadminlist: dbUserObj.groupadminlist,
                                    Admin: alladmins[0].username,
                                    adminlist: []
                                });

                            } else {
                                res.json({
                                    _id: dbUserObj.id,
                                    name: dbUserObj.name,
                                    email: dbUserObj.email,
                                    phone: dbUserObj.phone,
                                    description: dbUserObj.description,
                                    userlist: dbUserObj.userlist,
                                    groupadminlist: dbUserObj.groupadminlist,
                                    Admin: null,
                                    adminlist: []
                                });
                            }

                        });
                    }
                });
            });
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});*/

router.get('/:id', function(req, res, next) {
    
    Groups.find({_id: req.params.id}, function(err, post, next) {
        console.log(err)
        if (err) return next(err);
        if (post.length > 0) {
           
         /* Users.find({groupname:null,isDeleted:false,type:1},function(err,post1))*/
        res.json(post[0]);

        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});

/* Update Group details by ID*/

 /* router.put('/Update/:id', function(req, res, next) {
    Groups.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
           // var form = new formidable.IncomingForm();
          //  form.parse(req, function(err, fields, files) {
             //   if (!err) {
                    // var data = JSON.parse(fields.data);
                    post.email = req.body.email;
                    post.phone = req.body.phone; 
                    post.description = req.body.description || null;
                    if (req.body.Admin) //If admin is alloted to the group make it is=true;
                    {
                        post.isActive = true;
                    }
                    log.info(req.body.Admin);
                    post.save(function(err, post1) {
                        if (err) {
                            res.json({
                                "message": "Group already exits",
                                "status": 208
                            });

                        } else {
                             var groupId = post._id.toString();
                            //Set new Admin to the group
                            Users.find({
                                username: req.body.Admin
                            }, function(err, Users) {
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = groupId;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                             //udating user with new user group for tpcl @ renuka
                           req.body.userlist.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    dbUserObj.groupname.push(groupId);
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                           //udating groupadmin with new user group for tpcl @ renuka
                            req.body.groupadminlist.forEach(function(user){
                            Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {

                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    dbUserObj.groupname.push(groupId);
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                            
                            //assign all form to new admin
                            Formsz.update({
                                "createdBy": req.body.oldAdmin
                            }, {
                                $set: {
                                    "createdBy": req.body.Admin
                                }
                            }, {
                                multi: true
                            });


                            // make it Old Admin into  UMAdminlist
                            log.info(post.oldAdmin);
                            Users.find({
                                username: req.body.oldAdmin
                            }, function(err, Users) {
                                log.info(req.body.oldAdmin);
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = null;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                            
                             var value = getdeviceKeys(post1.name, function(data) {
                                 pushNotifications(value, "Formsz-UG Notification", "Your Profile details has been updated by riot,please check once.", {
                                     data: "Updated Profile"
                                 });
                             });

                            log.info("Group details updated successfully!");
                            res.json({
                                "message": "Details updated successfully",
                                "status": 200
                            });
                        }
                    });
              //  }
          //  });
        }
    });
});*/

/*router.put('/Update/:id', function(req, res, next) {
    Groups.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
           // var form = new formidable.IncomingForm();
          //  form.parse(req, function(err, fields, files) {
             //   if (!err) {
                    // var data = JSON.parse(fields.data);
                    post.email = req.body.email;
                    post.phone = req.body.phone; 
                    post.description = req.body.description || null;
                    if (req.body.Admin) //If admin is alloted to the group make it is=true;
                    {
                        post.isActive = true;
                    }
                    log.info(req.body.Admin);
                    post.save(function(err, post1) {
                        if (err) {
                            res.json({
                                "message": "Group already exits",
                                "status": 208
                            });

                        } else {
                             var groupobj ={_id:post._id.toString(),name:post.name}
                            //Set new Admin to the group
                            Users.find({
                                username: req.body.Admin.username
                            }, function(err, Users) {
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = groupobj;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                           //udating user with new user group for tpcl @ renuka
                           req.body.userlist.forEach(function(user){
                             Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    console.log(dbUserObj.groupname)
                                    if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                    else
                                    {
                                    dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                           //udating groupadmin with new user group for tpcl @ renuka
                            req.body.groupadminlist.forEach(function(user){
                            Users.find({username:user.username},function (err, Users)
                              {
                                if(Users.length>0)
                                {

                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                     if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                    dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                            //assign all form to new admin
                            Formsz.update({
                                "createdBy": req.body.oldAdmin
                            }, {
                                $set: {
                                    "createdBy": req.body.Admin
                                }
                            }, {
                                multi: true
                            });


                            // make it Old Admin into  UMAdminlist
                            log.info(post.oldAdmin);
                            Users.find({
                                username: req.body.oldAdmin.username
                            }, function(err, Users) {
                                log.info(req.body.oldAdmin);
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = null;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                            
                             var value = getdeviceKeys(post1.name, function(data) {
                                 pushNotifications(value, "Formsz-UG Notification", "Your Profile details has been updated by riot,please check once.", {
                                     data: "Updated Profile"
                                 });
                             });

                            log.info("Group details updated successfully!");
                            res.json({
                                "message": "Details updated successfully",
                                "status": 200
                            });
                        }
                    });
              //  }
          //  });
        }
    });
}); */ 

/*router.put('/Update/:id', function(req, res, next) {
   
    Groups.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
           // var form = new formidable.IncomingForm();
          //  form.parse(req, function(err, fields, files) {
             //   if (!err) {
                    // var data = JSON.parse(fields.data);
                    post.email = req.body.email;
                    post.phone = req.body.phone; 
                    post.description = req.body.description || null;
                    if (req.body.Admin) //If admin is alloted to the group make it is=true;
                    {
                        post.isActive = true;
                    }
                    log.info(req.body.Admin);
                    post.save(function(err, post1) {
                        if (err) {
                            res.json({
                                "message": "Group already exits",
                                "status": 208
                            });

                        } else {
                             var groupobj ={_id:post._id.toString(),name:post.name}
                            //Set new Admin to the group
                            if(req.body.Admin!=null||req.body.Admin!=undefined){
                            Users.find({
                                username: req.body.Admin.username
                            }, function(err, Users) {
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = groupobj;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                        }
                           //udating user with new user group for tpcl @ renuka
                          if(req.body.userlist!=null||req.body.userlist!=undefined){
                                req.body.userlist.forEach(function(user){
                                 Users.find({username:user.username},function (err, Users)
                                  {
                                    if(Users.length>0)
                                    {
                                      Users.forEach(function(dbUserObj)
                                      {log.info(req.body.name);
                                        console.log(dbUserObj.groupname)
                                        if(dbUserObj.groupname==null)
                                        {
                                            dbUserObj.groupname= groupobj;
                                        }
                                        else
                                        {
                                        dbUserObj.groupname.push(groupobj);
                                        }
                                        dbUserObj.save(function (err, post) {
                                             
                                        });
                                      });
                                    }
                                  });
                                });
                          }
                           //udating groupadmin with new user group for tpcl @ renuka
                            if(req.body.groupadminlist!=null||req.body.groupadminlist!=undefined){
                              req.body.groupadminlist.forEach(function(user){
                                  Users.find({username:user.username},function (err, Users)
                                    {
                                      if(Users.length>0)
                                      {

                                        Users.forEach(function(dbUserObj)
                                        {log.info(req.body.name);
                                           if(dbUserObj.groupname==null)
                                          {
                                              dbUserObj.groupname= groupobj;
                                          }
                                           else
                                          {
                                          dbUserObj.groupname.push(groupobj);
                                          }
                                          dbUserObj.save(function (err, post) {
                                               
                                          });
                                        });
                                      }
                                    });
                              });
                            }
                            //assign all form to new admin
                            
                            Formsz.update({
                                "createdBy": req.body.oldAdmin
                            }, {
                                $set: {
                                    "createdBy": req.body.Admin
                                }
                            }, {
                                multi: true
                            });


                            // make it Old Admin into  UMAdminlist
                            log.info(post.oldAdmin);
                            if(req.body.oldAdmin!=null){
                            Users.find({
                                username: req.body.oldAdmin.username
                            }, function(err, Users) {
                                log.info(req.body.oldAdmin);
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = null;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                            }
                             var value = getdeviceKeys(post1.name, function(data) {
                                 pushNotifications(value, "Formsz-UG Notification", "Your Profile details has been updated by riot,please check once.", {
                                     data: "Updated Profile"
                                 });
                             });

                            log.info("Group details updated successfully!");
                            res.json({
                                "message": "Details updated successfully",
                                "status": 200
                            });
                        }
                    });
              //  }
          //  });
        }
    });
});*/ 

router.put('/Update/:id', function(req, res, next) {
   var Active;
    if(req.body.userlist!=null || req.body.adminlist!=null||req.body.adminlist!=undefined||req.body.groupadminlist!=null)
      Active=true
    else
      Active=false
    Groups.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
           // var form = new formidable.IncomingForm();
          //  form.parse(req, function(err, fields, files) {
             //   if (!err) {
                    // var data = JSON.parse(fields.data);
                    post.email = req.body.email;
                    post.phone = req.body.phone; 
                    post.description = req.body.description || null;
                    post.isActive = Active;
                    if (req.body.Admin) //If admin is alloted to the group make it is=true;
                    {
                        post.isActive = true;
                    }
                    log.info(req.body.Admin);
                    post.save(function(err, post1) {
                        if (err) {
                            res.json({
                                "message": "Group already exits",
                                "status": 208
                            });

                        } else {
                             var groupobj ={_id:post._id.toString(),name:post.name}
                            //Set new Admin to the group
                            if(req.body.Admin!=null||req.body.Admin!=undefined){
                            Users.find({
                                username: req.body.Admin.username,type:1
                            }, function(err, Users) {
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = groupobj;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                        }
                           //updating user with new user group for tpcl @ renuka
                           if(req.body.userlist!=null||req.body.userlist!=undefined){
                           req.body.userlist.forEach(function(user){
                             Users.find({username:user.username,type:2},function (err, Users)
                              {
                                if(Users.length>0)
                                {
                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                    if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                    else
                                    {
                                    //dbUserObj.groupname.push(groupobj);
                                    //Code Modified by Renuka on 22-01-2018
                                    var exists = false;
                                      dbUserObj.groupname.forEach(function(group){
                                        if(group._id==groupobj._id)
                                           exists= true;
                                      });
                                      if(exists== false)
                                       dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                      if(err) {

                                      }
                                      else {
                                        console.log("entered");
                                      }
                                         
                                    });
                                  });
                                }
                              });
                         });
                         }
                           //updating groupadmin with new user group for tpcl @ renuka
                           if(req.body.groupadminlist!=null||req.body.groupadminlist!=undefined){
                            req.body.groupadminlist.forEach(function(user){
                            Users.find({username:user.username,type:3},function (err, Users)
                              {
                                if(Users.length>0)
                                {

                                  Users.forEach(function(dbUserObj)
                                  {log.info(req.body.name);
                                     if(dbUserObj.groupname==null)
                                    {
                                        dbUserObj.groupname= groupobj;
                                    }
                                     else
                                    {
                                   // dbUserObj.groupname.push(groupobj);
                                   //Code Modified by Renuka on 22-01-2018
                                    var exists = false;
                                      dbUserObj.groupname.forEach(function(group){
                                        if(group._id==groupobj._id)
                                           exists= true;
                                      });
                                      if(exists== false)
                                       dbUserObj.groupname.push(groupobj);
                                    }
                                    dbUserObj.save(function (err, post) {
                                         
                                    });
                                  });
                                }
                              });
                         });
                          }
                            //assign all form to new admin
                            if(req.body.oldAdmin!=null){
                            Formsz.update({
                                "createdBy": req.body.oldAdmin.username
                            }, {
                                $set: {
                                    "createdBy": req.body.Admin.username
                                }
                            }, {
                                multi: true
                            });

                           
                            // make it Old Admin into  UMAdminlist
                            log.info(post.oldAdmin);
                           
                            Users.find({
                                username: req.body.oldAdmin.username,type:1
                            }, function(err, Users) {
                                log.info(req.body.oldAdmin);
                                Users.forEach(function(dbUserObj) {
                                    dbUserObj.groupname = null;
                                    dbUserObj.save(function(err, post) {
                                        //if (err) return next(err);
                                    });
                                });
                            });
                          }
                          console.log("unmap group in user");
                          console.log(req.body.UMList);
                          if(req.body.UMList!= undefined ) {
                            console.log("lor");
                            var unmappedUserIds = [];
                            req.body.UMList.forEach(function(unmappedsingleUser) {
                              console.log(unmappedsingleUser);
                              unmappedUserIds.push(unmappedsingleUser._id);
                            })
                            console.log(unmappedUserIds);
                            Users.find({_id:{$in:unmappedUserIds}}, function(err,users) {
                              console.log(err);
                              console.log(users);
                              if(users.length>0) {
                                users.forEach(function(user) {
                                  console.log(user);
                                  var result = user.groupname.find(groupid=>groupid._id===req.params.id);
                                  console.log("result====" + JSON.stringify(result));
                                  Users.update({_id:user._id},{$pull:{groupname:result}}, function(err,updated) {
                                    if(err) {
                                      console.log(err);
                                    }
                                    else {
                                      console.log("updated");
                                    }
                                  })

                                })
                              }

                            })
                          }
                             /*var value = getdeviceKeys(post1.name, function(data) {
                                 pushNotifications(value, "Formsz-UG Notification", "Your Profile details has been updated by riot,please check once.", {
                                     data: "Updated Profile"
                                 });
                             });*/

                            log.info("Group details updated successfully!");
                            res.json({
                                "message": "User Group Details Update Successfully",
                                "status": 200
                            });
                        }
                    });
              //  }
          //  });
        }
    });
}); 

/* DELETE Group using id */
router.delete('/delete/:id', function(req, res, next) {
  Groups.findById(req.params.id, function(err, post) {
    if (post) {
      // Developer : Phani Kumar Narina
      // Desc : UserGroup has been Converted as Array
      Users.find({groupname:{$elemMatch:{_id:req.params.id}},isDeleted:false}, function(err,users){
			// Users.find({groupname:post._id,isDeleted:false}, function(err,users){
				if(users.length>0) {
					res.json({"message":"Cannot delete the usergroup,as it is assosciated with the admin "+users[0].name+".","status":204})
				}
				else {
					// console.log(post);
					post.isDeleted = true;
					post.save(function(err) {
						//if (err) throw err;
					});
						 /* var value = getdeviceKeys(post.name,function(data){
				pushNotifications(value,"Formsz-UG Notification","User Group Deleted.",{data:"User Group Deleted"});
				});*/
				res.json({
					"message": "User Group deleted successfully",
					"status": 200
				})
				}
			})

            //return next(err);
        } else {
			res.json({"message":"No data found","status":204})
           
        }
    });
  
});

module.exports = router;
