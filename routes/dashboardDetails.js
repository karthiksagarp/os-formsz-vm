var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var mongoid = mongoose.Types.ObjectId();
var Users = require('../models/user.js');//for users data
var Group = require('../models/group.js');//for departments data
var deviceManagement =  require('../models/deviceManagement.js');//for device data
var tasks = require('../models/projectTasksModel.js');//projectTask
var project = require('../models/projectModel.js');//projects
var FormszTask = require('../models/formszTasks.js');;//formsTask
var mobileLicensing = require('../models/mobileLicensing.js');//mobileLicensing

var fs = require('fs');
var formidable = require('formidable');
var Store = require('../models/store.js');
var sendautomail = require('../routes/utils').sendautomail;
var defoultImageurl = require('../routes/utils').defoultimageurl;
var ObjectID = require("bson-objectid");
var log = require('./log')(module);
var async = require('async');
var mobileLicensing = require('../models/mobileLicensing.js');


//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getUsersCount Service.
router.get('/getUsersCount/:userType/:groupId', function(req, res, next) {

    var users = {};
    if (req.params.userType == '0') { //for root login

        Users.find({ type: 2,isDeleted:false }, {
                                  name: 1,
                                  username: 1,
                                  zone: 1,
                                  vender: 1,
                                  groupname: 1
        }).exec(function(err1, post) {

            var totalUsers = {};
            users.totalUsersCount = post;

            Users.find({
                groupname: [],
                type: 2,isDeleted:false 
            }, {
                name: 1,
                username: 1,
                zone: 1,
                vender: 1,
                groupname: 1
            }).exec(function(err, results) {

                var departmentuserscount = [];
                var deptcountobject = {};

                Group.find({}, {
                    name: 1,
                    userlist: 1
                }).exec(function(err, groupnames) {
                    users.totaldepartmentusers = groupnames;
                })
                users.groupsUnAssigned = results;
                //when user login from mobile isUserLocatorActive key cheged to true  and user logout from mobile this key chnge to false then only this will work
                Users.find({
                    isUserLocatorActive: true,
                    type: 2,isDeleted:false 
                }, {
                    name: 1,
                    username: 1,
                    zone: 1,
                    vender: 1,
                    groupname: 1,
                    deviceDetails:1
                }).exec(function(err2, activeusers) {
                    users.activeUsers = activeusers;
                    res.json({
                        data: users,
                        status: 200
                    });
                })
            })

        })
    } else { //admin login

        Users.find({
           // groupname: req.params.groupId,
           groupname:{ $elemMatch: { _id: req.params.groupId }},
            type: 2,isDeleted:false 
        }, {
            name: 1,
            username: 1,
            zone: 1,
            vender: 1,
            groupname: 1
        }).exec(function(err1, post) {
            users.totalUsersCount = post;
            Users.find({
                isUserLocatorActive: true,
                type: 2,isDeleted:false ,
                groupname: { $elemMatch: { _id: req.params.groupId }}
            }, {
                name: 1,
                username: 1,
                zone: 1,
                vender: 1,
                groupname: 1,
                deviceDetails:1
            }).exec(function(err2, results) {
                users.activeUsers = results;
                res.json({
                    data: users,
                    status: 200
                });
            })
        })
    }
});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDevicesCount Service.
router.get('/getDevicesCount/:userType/:groupId', function(req, res, next) {
    var devices = {};
    var departmentuserscount = [];
    if (req.params.userType == '0') { //root login
        deviceManagement.find({isDeleted:false}, {
            Model: 1,
            Manufacturer: 1,
            MacAddress: 1,
            status: 1,
            _id: 0,
            groupname: 1
        }).exec(function(err, post) {
            devices.totalDevicesCount = post;

            deviceManagement.find({
                status: {
                    $eq: "Pending"
                },isDeleted:false
            }, {
                Model: 1,
                Manufacturer: 1,
                MacAddress: 1,
                status: 1,
                _id: 0,
                groupname: 1
            }).exec(function(err, results) {

                Group.find({}, {
                    name: 1
                }).exec(function(err, groupnames) {

                    async.forEachSeries(groupnames, function(item, next) {


                        deviceManagement.find({
                            groupname: item._id,isDeleted:false
                        }, {
                            Manufacturer: 1,
                            Model: 1,
                            _id: 0
                        }).exec(function(err3, devicesCount) {
                            var departmentObject = {};
                            if (devicesCount.length > 0) {
                                departmentObject.departmentAdmin_id = item._id;
                                departmentObject.departmentAdmin = item.name;
                                departmentObject.devices = devicesCount
                                departmentuserscount.push(departmentObject);

                                devices.totaldepartmentusers = departmentuserscount;
                            }

                            next();

                        })
                    }, function(err) {

                        devices.pendingdevices = results;
                        res.json({
                            data: devices,
                            status: 200
                        });

                    })

                })
            });
        });
    } else { //admin login
        deviceManagement.find({
            groupname: req.params.groupId,isDeleted:false
        }, {
            Manufacturer: 1,
            Model: 1,
            MacAddress: 1,
            status: 1,
            _id: 0
        }).exec(function(err3, devicesCount) {

            devices.totalDevicesCount = devicesCount;
            deviceManagement.find({
                status: {
                    $eq: "Pending"
                },
                groupname: req.params.groupId,isDeleted:false
            }, {
                Model: 1,
                Manufacturer: 1,
                MacAddress: 1,
                status: 1,
                _id: 0
            }).exec(function(err, results) {
                devices.pendingdevices = results;
                res.json({
                    data: devices,
                    status: 200
                });
            });
        })
    }

});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
router.get('/getDepartmentsCount',function(req,res,next){
    var admins = {};
    Users.find({type:1,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0}).exec(function(err,post) {
       
       admins.totalDepartmentCount = post;
       
       Users.find({groupname:[],type:1,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0}).exec(function(err1,unassigned){
          admins.unassignedDepartments = unassigned; })

        Users.find({type:3,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0}).exec(function(err,post) {
          admins.totaladminsCount = post;
      
          Users.find({groupname:[],type:3,isDeleted:false},{name:1,username:1,zone:1,vender:1,_id:0}).exec(function(err1,unassigned){
              admins.unassignedAdmins = unassigned;
      
              res.json({data:admins,status:200});
          })
        });
    });
});

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
/*router.get('/getDashboardData',function(req,res,next){
    var dashboardData = {};
    var projectuserscount = [];
    Users.find({isUserLocatorActive : true,type:2},{name:1,username:1,vender:1,zone:1,groupname:1}).exec(function (err, users) {
    
       dashboardData.totalUsersCount =users;
       var todate = new Date((new Date().getTime() - (90 * 24 * 60 * 60 * 1000)))
       var currentDate = new Date()    
        // modified createdTime to lastLoggedInTime by Swathi on 25-01-2018
        Users.find({lastLoggedInTime:{$gte: todate, $lte: currentDate },type:2,isUserLocatorActive:false},{name:1,username:1,vender:1,zone:1,groupname:1}) .exec(function(err1,inactive){
         
         //Swathi updated startdate enddate in query on 19-01-2018    
        tasks.find({endDate: {$lte:currentDate},isClosed:{$eq:false}},{name:1,users:1,projectID:1,startDate:1,endDate:1}).exec(function(err3,taskDetails){
                  
                async.forEachSeries(taskDetails, function(item,next) {

                project.find({_id:item.projectID} ,{name:1}).exec(function(err,projectnames){
                    if(projectnames.length>0){
                        var taskObject = {};
                        taskObject.projectname = projectnames[0].name;
                        taskObject.task = item 
                        projectuserscount.push(taskObject);
                        dashboardData.totalprojecttasks=projectuserscount;
                    }
              
                    next();
                    })
                }, function(err)
                  {
                      dashboardData.inactiveUsers = inactive;
                      res.json({data:dashboardData,status:200});
          })

  })  

  })
    
  });

  });*/

//--> Swathi: Added: 24-Dec:TPCL:Dashboards
// Notes: Added TPCl getDepartmentsCount Service.
router.get('/getDashboardData',function(req,res,next){
    var dashboardData     = {};
    var projectuserscount = [];
    var projectCount      = [];
    var todate            = new Date((new Date().getTime() - (90 * 24 * 60 * 60 * 1000)))
    var currentDate       = new Date()  
    Users.count({type:2,isDeleted:false }).exec(function (err, Totuserscnt) {
       dashboardData.totaluserscnt = Totuserscnt; 
    });
    mobileLicensing.count().exec(function(err, results) {
        dashboardData.totallicCount = results; 
    });   

    // Users.find({isUserLocatorActive : true,type:2},{name:1,username:1,vender:1,zone:1,groupname:1}).exec(function (err, activeusers) {
       // mobileLicensing.find({ isActive: true},{licenseId:0}).exec(function(err, results) {
       mobileLicensing.aggregate([{ 
        $match : {isActive: true} 
                        },{ 
        $group : { _id : "$department", docs: { $push: { assignedTo: "$assignedTo", deviceDetails: "$deviceDetails",loggedInTime: "$loggedInTime" , department: "$department"} } } ,
                        }]).exec(function(err1,results){

       dashboardData.consumedLics =results;
       // var todate = new Date((new Date().getTime() - (90 * 24 * 60 * 60 * 1000)))

        Users.aggregate([{ 
        $match : {lastLoggedInTime:{$lte: todate.toISOString()},type: '2',isUserLocatorActive : false,isDeleted:false } 
                        },{ 
        $group : { _id : "$groupname.name", docs: { $push: { username: "$username", vender: "$vender",zone: "$zone" , groupname: "$groupname",name: "$name" } } } ,
                        }]).exec(function(err1,inactive){

         //Swathi updated startdate enddate in query on 27-01-2018    
        FormszTask.find({endDate: {$lte:currentDate},isClosed:{$eq:false},isDeleted:false},{name:1,userGroup:1,startDate:1,endDate:1}).exec(function(err3,taskDetails){
                  
                async.forEachSeries(taskDetails, function(item,next) {

                Group.find({_id:item.userGroup} ,{name:1}).exec(function(err,deptadminName){
                    if(deptadminName.length>0){
                        var taskObject = {};
                        taskObject.adminName = deptadminName[0].name;
                        taskObject.task = item 
                        projectuserscount.push(taskObject);
                        dashboardData.totaltasks=projectuserscount;
                    }
              
                    next();
                    })
                }, function(err)
                  {})
         project.find({},{name:1,deparmentId:1,startDate:1,endDate:1,createdBy:1}).exec(function(err3,projectDetails){      
           
                async.forEachSeries(projectDetails, function(item,next) {

                Group.find({_id:item.deparmentId} ,{name:1}).exec(function(err,deptadminName){
                    if(deptadminName.length>0){
                        var projectObject = {};
                        projectObject.adminName = deptadminName[0].name;
                        projectObject.projectData = item 
                        projectCount.push(projectObject);
                        dashboardData.totalprojects=projectCount;
                    }
              
                    next();
                    })
                }, function(err)
                  {
                      dashboardData.inactiveUsers = inactive;
                      res.json({data:dashboardData,status:200});
          })
        })
  })  

  })
    
  });

  });

router.get('/getProjectDashboardData/:departmentAdmin_id/:userType',function (req,res,next) {
     var dashboardData = {};
     if (req.params.userType == 1) {//for department Admin
     project.find({deparmentId:req.params.departmentAdmin_id,isDeleted:false },{name:1,startDate:1,endDate:1}).exec(function(err3,projectDetails){      
            dashboardData.totalprojects=projectDetails;
             res.json({data:dashboardData,status:200});
        })
        }
     else if (req.params.userType == 3){//for group Admin
         project.find({'zonalAdmins.0.id':req.params.departmentAdmin_id,isDeleted:false},{name:1,startDate:1,endDate:1}).exec(function(err3,projectDetails){      
            dashboardData.totalprojects=projectDetails;
             res.json({data:dashboardData,status:200});
        })
     }   

});
module.exports = router;
