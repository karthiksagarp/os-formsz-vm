
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var deviceManagement =  require('../models/deviceManagement.js');
var Users = require('../models/user.js');
var fs = require('fs');
var formidable = require('formidable');
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var sendautomail = require('../routes/utils').sendautomail;
var log = require('./log')(module);
var activityLog = require('formsz-tpcl-customizations');
var mobileLicensing = require('../models/mobileLicensing.js');




router.post('/addDevice', function(req, res, next) {
    // Check if device is already registered.
    deviceManagement.find({$or:[{MacAddress: req.body.MacAddress}]}, function(err, post) {
        if (post.length > 0) {
                res.json({
                "message": "Device already registered",
                "status": 208
            }); 
        }
        else{
            // Register Device.
            var deviceinfo = new deviceManagement({
                Manufacturer: req.body.Manufacturer,
                Model: req.body.Model,
                MacAddress: req.body.MacAddress || null,
                UUID: req.body.UUID || null,
                status: "Pending",
                platform : req.body.platform || null,
                version : req.body.version || null,
                groupname : req.body.groupname || null
            });

            
            deviceinfo.save(function(err, post) {
                if (err) {
                    console.log("Error Occured. Insertion Failed");
                }
                else{
                    //Added by Diva for TPCL Customisation

                    activityLog.deviceLog(post,req.body.adminname,"New Device Registered");
                    res.json({
                       "message": "Device Registered Successfully",
                       "status": 200
                    });
                }
            });
        }
    });
});

/**
 * Module: Adding new device From Mobile Device
 * query:  Create if MAC is not avilable in collection
 * @Developer: Phani kumar Narina
**/
router.post('/addMobileDevice', function(req, res, next) {
    Users.find({username: req.body.userName}, function(err, userInfo) {
		var filter, dataToInsert; 
		if(req.body.platform == "iOS"){
				filter = {UUID : userInfo[0].UUID};
				dataToInsert = {
	                status: "Pending", groupname : userInfo[0].groupname || null, Manufacturer : req.body.Manufacturer,Model : req.body.Model,platform : req.body.platform,UUID:req.body.UUID,version:req.body.version
	            }
		}
		else{
				filter = {MacAddress : req.body.MacAddress,UUID : req.body.UUID};
				dataToInsert = {
	                MacAddress: req.body.MacAddress,status: "Pending", groupname : userInfo[0].groupname || null, Manufacturer : req.body.Manufacturer,Model : req.body.Model,platform : req.body.platform,UUID:req.body.UUID,version:req.body.version
	            }
		}
        deviceManagement.find(filter, function(err, post) {
            if (post.length > 0) {
                    res.json({
                    "message": "Device information already exists",
                    "status": 208
                }); 
            }
            else{
                var deviceinfo = new deviceManagement(dataToInsert);
                
                deviceinfo.save(function(err, post) {
                    if (err) {
                        console.log("not inserted");
                    }
                    else{

                        //Added by Diva for TPCL Customisation
                        activityLog.deviceLog(post,req.body.adminname,"New Device Registered");

                        // GET ADMIN INFO from login and bind it to below  variables
                        var name = "Admin";
                        var email = "siva.kella@magikminds.com";
                        var html = "<html><body><pre>"+
                            "Dear Admin, <br><br>"+
                            "Auto request received from "+data.username+"  from Department "+data.groupname[0].name+"."+
                            "The below device has been auto registered to TPCLDynamic Forms Application and it is pending for approval. Please verify and accept/reject the device."+
                            "<br><br>"+
                            "Device Details:"+"<br>"+
                            "Mac-Address :"+post.MacAddress+"<br>"+
                            "Manufacturer: "+post.Manufacturer+"<br><br><br>"+
                            "Thanks,"+"<br>"+
                            "DFormszteam"+
                            "</pre></body></html>"
                      //  var html = "<body>Dear " + name + ",<br><br> device "+post.Manufacturer+" with MacAddress ("+post.MacAddress +")"+" has been "+post.status+"<br><br></body>"
                        sendautomail(email, html, "Device Auto registered: Pending Approval");
                        res.json({
                           "message": "Your Device has been Registered Successfully, Please contact administrator to Approve the device.",
                           "status": 200
                        });
                    }
                });
            }
        });
    });
});

/**
 * Module: Get Device List  
 * query:  Get Device info based on Root/ NonRoot
 * @Developer: Phani kumar Narina
**/
router.get('/getDeviceList/:groupname/:adminType',function(req, res, next){
    // Get Details of Root
    // No groupname query required
    if (req.params.adminType == "0") {
        deviceManagement.find({isDeleted: false},function(err, DeviceInfo) {
            if (DeviceInfo.length > 0) {
                res.json({
                    "data": DeviceInfo,
                    "status": 200
                });
            } else {
                res.json({
                    "message": "Not Data found",
                    "status": 204
                });
            }
        });
    }
    else{
        // Get Details of NonRoot
        // groupname query required to diffrentiate Admins
        deviceManagement.find({
            groupname:req.params.groupname, isDeleted: false
        }, function(err, DeviceInfo) {
            if (DeviceInfo.length > 0) {
                res.json({
                    "data": DeviceInfo,
                    "status": 200
                });
            } else {
                res.json({
                    "message": "Not Data found",
                    "status": 204
                });
            }
        });
    }
});

/**
 * Module: Changing the Status in the collection  
 * query:  Approve/Reject/Suspend/Resume/Pending
 * @Developer: Phani kumar Narina
**/
router.put('/deviceStatus/:id/:type',function(req, res, next){
    var ob = {};
    if (req.params.type === "0") {
        ob.status ="Pending";
    }
    else if (req.params.type === "1") {
        ob.status ="Approved";
    }
    else if (req.params.type === "2") {
        ob.status ="Suspended";
    }
    else if (req.params.type === "3") {
        ob.status ="Pending";
    }
    else if (req.params.type === "4") {
        ob.status ="Rejected";
    }
        
    deviceManagement.findByIdAndUpdate(req.params.id,ob,{new:true},function(err,post){
    if(err) throw err;
        if(post!=null){
            if (post.status == "Pending") {
                 activityLog.deviceLog(post,req.body.adminname,post.status);

                res.json({
                    "message": "Your device is "+post.status,
                    "status": 200
                });
            }
            else{
                var deviceInfo = post.Manufacturer+"-"+post.Model+"-"+post.UUID
                if(post.status == "Suspended") {
                    mobileLicensing.find({isActive:true,deviceDetails:deviceInfo}, function(err,licenses) {
                        if(licenses.length>0) {
                            licenses.forEach(function(license) {
                                mobileLicensing.update({_id:license._id},{$set:{assignedTo:null,deviceDetails:null,isActive:false}},function(err,inserted) {
                                    if(err) {
                                        console.log(err);
                                        return err;
                                    }
                                    else {
                                        console.log("Licenses released for suspended device.")
                                    }
                                })
                            })
                        }
                        else if(licenses.length==0) {
                            console.log("No license is available to release");

                        }
                    })

                }
                // Note: To get proper admin details need to fetch using groupname.
                // var name = "Admin";
                // var email = "phanikumar.narina@magikminds.com";
                // var html = "<body>Dear " + name + ",<br><br> device "+post.Manufacturer+" "+"with MacAddress ("+post.MacAddress +")"+" has been "+post.status+"<br><br></body>"
                // var html = "<body>Dear " + name + ",<br><br> Your device "+post.Manufacturer+" has been "+post.status+"<br><br></body>"
                // sendautomail(email, html, "Automatic Reply: Device Status");

            //Added by Divya for TPCL Customisation
           activityLog.deviceLog(post,req.body.adminname,post.status);


                res.json({
                    "message": "Your device is "+post.status,
                    "status": 200
                });
            }
        }
        else{
            res.json({
                "message": "No Record Found",
                "status": 205
            });
        }
    });

}); 

/**
 * Module: Checking the DeviceInfo and updating the collection  
 * query: Getting IMEI & MAC from plugin and validating if the params exists in collection and updating the UUID,Version,platform,Manufacturer & Model in the collection.
 * @Developer: Phani kumar Narina
**/
router.put('/CheckISExists/:type/:UUID',function(req, res, next){
	var filter, dataToInsert; 
	if(req.body.platform == "iOS"){
			filter = {UUID : req.params.UUID};
			dataToInsert = {
                Manufacturer : req.body.Manufacturer,Model : req.body.Model,platform : req.body.platform,UUID:req.params.UUID,version:req.body.version
            }
	}
	else{
			filter = {MacAddress : req.params.type};
			dataToInsert = {
                MacAddress: req.params.type,Manufacturer : req.body.Manufacturer,Model : req.body.Model,platform : req.body.platform,UUID:req.params.UUID,version:req.body.version
    		}
    }
    deviceManagement.find(filter,function(err,post){
        if (post.length > 0) {
            deviceManagement.update(filter,{$set:dataToInsert},function (err, a) {
                if (err) throw err;
                else {
                        res.json({
                        "data": post,
                        "message":"Updated Successfully!",
                        "status": 200
                    });
                }
            })
        } else {

            // var html = "<body>Dear " + user.name + ",<br><br> You are registered as a User and your details as below<br><br> <strong>Username : </strong>" + user.username + "<br> <strong>Password :</strong> " + passwordGenerateds + "<br><br>Please login using with above credentials.<br><br></body>"
            // sendautomail(user.email, html, "Automatic Reply: Account Confirmation");

            res.json({
                "message": "Device information not avilable!",
                "status": 204
            });
        }
    });
});

router.delete('/deleteDevice/:id', function(req, res, next) {
     deviceManagement.find({_id: req.params.id}, function(err, post) {
     if (post.length > 0) {
     deviceManagement.update({_id : post[0]._id},{$set:{
                                isDeleted : true
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                                  res.json({"message":"Device deleted successfully","status":200});

                         }
                    });
     } else {
     res.json({
     "message": "No data found",
     "status": 204
     })
     }
     });
});

router.post('/createDeviceFromFile', function(req, res, next) {
    var data = req.body.deviceData;
    var i = 0;
    var  existingDevices = [];
    var MacAddresses;
    getDeviceList(data);

    function getDeviceList(data) {
        if (i < data.length) {
            deviceManagement.find({MacAddress: data[i].MacAddress}, function(err, DeviceInfo) {
                mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000 + i);
               
                MacAddresses = data[i].MacAddress;
                var devices = new deviceManagement({
                     _id: mongoid,
                     version: data[i].Version,
                     platform: data[i].Platform,
                     status:  "Pending",
                     MacAddress : data[i].MacAddress,
                     UUID : data[i].UUID,
                     Manufacturer : data[i].Manufacturer, 
                     Model : data[i].Model,
                     groupname: req.body.groupname
                    
                    
                })
                if (DeviceInfo.length <= 0) {
                    devices.save(function(err, user) {
                        i++;
                        getDeviceList(data);
                        
                    });
                } else {
                    existingDevices.push(MacAddresses);
                    i++;
                    getDeviceList(data);
                }
            });
        }
        if (i == data.length) {
            if (existingDevices.length == 0) {
                res.json({
                    "message": "Devices Registered Successfully",
                    "status": "200"
                });
            } else {
                if (existingDevices.length == data.length) {
                    res.json({
                        "message": "All Devices already in use ",
                        "status": "208"
                    });
                } else {
                    res.json({
                        "message": " Devices Registered Successfully except " + existingDevices.toString() + ".because they are already in use",
                        "status": "208"
                    });
                }
            }
        }
    }
});
module.exports = router;
