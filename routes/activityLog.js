var express = require('express');
var router = express.Router();
var activitylog = require('../models/activityLog.js');
var activityLogHistory = require('formsz-tpcl-customizations');

//var mongoose = require('mongoose');

/*router.get('/getAllActivities/:groupname/:type', function(req,res,next) {
	 activityLogHistory.activityLogService(req.params.type,req.params.groupname,function(response) {
		console.log(response);
		res.json(response);

	});
	
})*/
//Service changed by Divya on 18-01-2018 for activity logs
router.get('/getAllActivities/:groupname/:type/:fromdate/:todate/:limit/:offset', function(req,res,next) {
var limit = parseInt(req.params.limit);
	//var offset = parseInt(req.params.offset);		
	 activityLogHistory.activityLogService(req.params.type,req.params.groupname,req.params.fromdate,req.params.todate,limit,req.params.offset,function(response) {
		res.json(response);
		/*activityLogHistory.paginationFunction(function(response) {
			console.log(response);
					res.json(response);

		})*/

	});
	
});

module.exports = router;
