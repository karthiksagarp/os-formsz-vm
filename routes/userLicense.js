
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var userLicense =  require('../models/mobileLicensing.js');
var Users = require('../models/user.js');
var fs = require('fs');
var formidable = require('formidable');
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var log = require('./log')(module);


// Sample Test
router.get('/test', function(req, res, next) {
	console.log("hi");
	// res.json({
 //        "data": "DeviceInfo",
 //        "status": 200
 //    });
	userLicense.find({},function(err, DeviceInfo) {
		console.log(DeviceInfo);
        if (DeviceInfo.length > 0) {
            res.json({
                "data": DeviceInfo,
                "status": 200
            });
        } else {
            res.json({
                "message": "Not Data found",
                "status": 204
            });
        }
    });

});

module.exports=router;