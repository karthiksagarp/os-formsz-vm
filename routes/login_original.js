var jwt = require('jwt-simple');
var passwordHash = require('password-hash');
var crypto = require('crypto');
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Users = require('../models/user.js');
var mailer = require("nodemailer");
var sendautomail = require('../routes/utils').sendautomail;
var Groups = require('../models/group.js');
var passport = require('passport');
var LdapStrategy = require('passport-ldapauth');

/**
     * @description LDAP options settings  
     * @developer Venkatesh bendi
     * @moreInfo this funtionality backporting from UE implemenation,by venkates.hbendi
*/
var OPTS = {
   
    server: {
        "url": "ldap://wdvdc001.domain.dev.int:389",
        "bindDN": "CN=Service_GIS_LDAP,OU=Service Accounts,OU=Resources,DC=domain,DC=dev,DC=int",
        "bindCredentials": "5rV_g15_LD@P",
        "searchBase": "DC=domain,DC=dev,DC=int",
        "searchFilter": "(&(objectcategory=person)(objectclass=user)(|(samaccountname={{username}})(mail={{username}})))"
    }
};
//pasport module for ldap conectivity
passport.use(new LdapStrategy(OPTS));


//var pushNotifications = require('../routes/utils').pushNotifications;
//Service Test
router.get('/Encript/:data', function(req, res, next) {
    res.json({
        "message": encrypt(req.params.data)
    });

});
router.get('/Dcript/:data', function(req, res, next) {
    res.json({
        "message": decrypt(req.params.data)
    });

});
//Forgot password
router.post('/forgotpwd', function(req, res, next) {
    Users.find({
        $or: [{
            username: req.body.username
        }, {
            email: req.body.username
        }]
    }, function(err, post) {
        if (post.length > 0) {
            var decryptpassword = decrypt(post[0].password);

            body = "<body>Dear " + post[0].name + ",<br><br>Please find your new credentials in below<br><br> <strong>Username:</strong>" + req.body.username + "<br> <strong>Password :</strong> " + decryptpassword + "<br><br>Please login using the URL<br><br></body>"
            sendautomail(post[0].email, body, 'Automatic Reply:Account Credentials')


            res.json({
                "message": "Password sent to your mail,Please check once ",
                "status": 200
            });
        } else {
            res.json({
                "message": "Invalid credentials",
                "status": 204
            });
        }
    });

});


//LDAP login 
 /**
     * @description user validation on ldap System  
     * @requestParams {objcet} username and password 
     * @developer Venkatesh bendi
     * @moreInfo this funtionality backporting from UE implemenation,by venkates.hbendi
*/

router.post('/loginWithLdap', function(req, res, next) {

    if (req.body.username == "root") {
        var encryptpwd = encrypt(req.body.password);
        formszSuperUserProcess(req, res, encryptpwd)
    } else {
        passport.authenticate('ldapauth', {
            session: false
        }, function(err, user, info) {
            console.log(user);
            if (err) {
                return next(err); // will generate a 500 error
            }
            // Generate a JSON response reflecting authentication status
            if (!user) {

                return res.json({
                    "status": 204,
                    "message": "Invalid credentials"
                });
            } else {
                var roles = getRoles(user.memberOf);
                var userType = getUserType(roles);
                //app_carding_portal

                var defaultGroup = "UEGroup"
                var ldapUser = new Users({
                    username: user.sAMAccountName,
                    name: user.name,
                    //email: user.sAMAccountName+"@ue.com.au",
                    email: "ksagar@ue.com.au",
                    groupname: defaultGroup,
                    type: userType,
                });

                if (userType == 2) {


                    if (userType == req.body.type) {
                        CreateUSer(user, defaultGroup, userType, res, ldapUser);

                    } else {
                        res.json({
                            "status": 204,
                            "message": "You are unuthorized to access this portal"
                        });
                    }
                } else if (userType == 1) {

                    CreateUSer(user, defaultGroup, userType, res, ldapUser);

                } else if (userType == 3) {
                    res.json({
                        "status": 204,
                        "message": "You are unuthorized to access this portal"
                    });
                }

            }

        })(req, res, next);
    }
});


//Login
router.post('/login', function(req, res, next) {
    var encryptpwd = encrypt(req.body.password);
    Users.find({
        $or: [{
            username: req.body.username,
            password: encryptpwd,
            isDeleted: false
        }, {
            email: req.body.username,
            password: encryptpwd,
            isDeleted: false
        }]
    },{"isUserUpdatePermissionActive":0,"__v":0,"isDeleted":0},function(err,data){
        var response= genToken(data);
        if(data.length>0)
            {
                res.json(response)
            }
        else
        {
           res.json({
                    "status": 204,
                    "message": "Invalid credentials"
                }); 
        }
    });
    /*console.log("Login Initiated...");
    
    //var  encryptpwd=crypto.createHash('md5').update(req.body.password).digest("hex") ;
    var encryptpwd = encrypt(req.body.password);
    console.log(encryptpwd);
    console.log(req.body.username)
    Users.find({
        $or: [{
            username: req.body.username,
            password: encryptpwd,
            isDeleted: false
        }, {
            email: req.body.username,
            password: encryptpwd,
            isDeleted: false
        }]
    },{},, function(err, post) {
        console.log("----------")
        console.log(post)
        if (post.length > 0) {
            if (post[0].type == 0) {

                console.log("Login Success...");
                res.json(genToken(post[0]));
            } else if (post[0].type == 1 || post[0].type == 2 || post[0].type == 3) {
                console.log(post[0].groupname)
                console.log("post[0].groupname._id")
                Groups.find({
                    _id: post[0].groupname[0]._id,
                    isActive: true,
                    isDeleted: false
                }, function(err, group) {
                    var json = {};
                    console.log(group)
                    if (group.length > 0) {
                        json.username = post[0].username;
                        json.userid = post[0]._id;
                        json.phone = post[0].phone;
                        json.email = post[0].email;
                        json.createdDateTime = post[0].createdDateTime;
                        json.admingroup = post[0].admingroup;
                        json.imageurl = post[0].imageurl;
                        json.name = post[0].name;
                        json.groupname = group[0].name;
                        json.groupid = group[0]._id;
                        json.type = post[0].type;
                        json.password = post[0].password;
                        json.assignedLayers = post[0].assignedLayers;
                        json.adminType = post[0].adminType;
                        json.privilage = post[0].privilage;
                        res.json(genToken(json));

                    } else {
                        console.log("error")
                        res.json({
                            "status": 204,
                            "message": "Invalid credentials"
                        });

                    }

                });

            }


        } else {
            console.log("eee")
            res.json({
                "status": 204,
                "message": "Invalid credentials"
            });
        }
    });*/

});

function genToken(user) {
    var expires = expiresIn(1); // 7 days
    var token = jwt.encode({
        exp: expires
    }, require('../config/secret')());
    return {
        token: token,
        expires: expires,
        user: user,
        "message": "Login Success",
        "status": 200
    };
}

function expiresIn(numDays) {
    var dateObj = new Date();
    return dateObj.setDate(dateObj.getDate() + numDays);
}

function encrypt(text) {
    var cipher = crypto.createCipher('aes-256-ctr', 'magikminds');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text) {
    var decipher = crypto.createDecipher('aes-256-ctr', 'magikminds');
    console.log(decipher);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}
module.exports = router;
