
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var bookmarks =  require('../models/bookmarks.js');
var Users = require('../models/user.js');
var fs = require('fs');
var formidable = require('formidable');
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var log = require('./log')(module);

router.post('/addBookmark', function(req, res, next) {
        bookmarks.find({bookmarkName: req.body.bookmarkName}, function(err, post) {
            if (post.length > 0) {
                    res.json({
                    "message": "bookmark already exists",
                    "status": 208
                }); 
            }
            else{
                var bookmarksInfo = new bookmarks({
                    bookmarkName: req.body.bookmarkName|| null,
                    northEast: req.body.northEast|| null,
                    southWest: req.body.southWest || null,
                    zoomLevel: req.body.zoomLevel || null,
                    username : req.body.username || null
                });

                
                bookmarksInfo.save(function(err, post) {
                    if (err) {
                        console.log("not inserted");
                    }
                    else{
                        res.json({
                           "message": "Bookmark created successfully",
                           "status": 200
                        });
                    }
                });
            }
        });
});

router.get('/getBookmarks/:username', function(req, res, next) {

    bookmarks.find({"username" : req.params.username}, function(err, data) {
        res.json({
           "data": data,
           "status": 200
        });
    });
});

router.get('/deleteBookmark/:bookmarkName', function(req, res, next) {
    bookmarks.remove({"bookmarkName" : req.params.bookmarkName}, function(err, data) {
        res.json({
           "message": "Bookmark deleted successfully",
           "status": 200
        });
    });
});

router.get('/editBookmark/:oldBookmarkName/:newBookmarkName', function(req, res, next) {
    bookmarks.find({bookmarkName:req.params.oldBookmarkName},function(err,post){
        if (post.length > 0) {
            bookmarks.update({bookmarkName : post[0].bookmarkName},{$set:{
                    bookmarkName:req.params.newBookmarkName
                }},function (err, a) {
                if (err) throw err;
                else {
                        res.json({
                        "data": post,
                        "message":"Bookmark updated sucessfully!",
                        "status": 200
                    });
                }
            })
        } else {

            // var html = "<body>Dear " + user.name + ",<br><br> You are registered as a User and your details as below<br><br> <strong>Username : </strong>" + user.username + "<br> <strong>Password :</strong> " + passwordGenerateds + "<br><br>Please login using with above credentials.<br><br></body>"
            // sendautomail(user.email, html, "Automatic Reply: Account Confirmation");

            res.json({
                "message": "Device information not avilable!",
                "status": 204
            });
        }
    });

/*    bookmarks.find({"bookmarkName" : req.params.bookmarkName}, function(err, data) {
        res.json({
           "message": "Bookmark edited successfully",
           "status": 200
        });
    });
*/

});

module.exports = router;
