var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var FormszDetails = require('../models/formszDetails.js');
var DisplayValues = require('../models/displayValues.js');
var Formsz = require('../models/formsz.js');
var mongoid = mongoose.Types.ObjectId();
var ObjectId = require("bson-objectid");
var excelbuilder = require('msexcel-builder');
var sendAutomailwithAttachment = require('../routes/utils').sendAutomailwithAttachment;
var fs = require('fs');
var request = require('request');
// var dbConfiguration = require('../routes/dbConfiguration.js');
var Tasks = require('../models/formszTasks.js');
var async = require('async');
var notificationmodel = require('../models/notificationmodel.js');
var pdf = require('html-pdf');
var activityLog = require('formsz-tpcl-customizations');
var deviceManagement =  require('../models/deviceManagement.js');



//back porting form Ue by venki 
router.get('/getLatLongWIthForm/:formId/:taskId/:user', function(req, res, next) {
    var data = {};

    Tasks.find({
        _id: req.params.taskId,
        isDeleted: false
    }, function(err, tasks) {
        if (tasks.length > 0) {
            latlongjson(req.params.formId, req.params.taskId, req.params.user, function(ress) {
                data = {
                    latlngData: ress
                }
            });
            setTimeout(function() {
                res.json({
                    "data": data,
                    "status": 200
                })
            }, 1000);

        } else {
            res.json({
                "message": "No data found",
                "status": 204
            });
        }
    })
});

function latlongjson(formId, taskId, user, callback) {
    Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, formsz) {
        var latlongData = [];
        var latlongDataDetails = {};
        var task = [];
        var locationDetails = {};
        if (formsz.length > 0) {
            var mapFields = [];
            latlongDataDetails.formId = formsz[0]._id;
            mapFields = formsz[0].geoFields;
            if(mapFields.length > 0){
                FormszDetails.find({
                    $or: [{
                        formId: formId,
                        taskId: taskId,
                        DSAssignedTo: user
                    }, {
                        formId: formId,
                        taskId: taskId,
                        updatedBy: user
                    }]
                }, function(err, details) {
                    var taskdata = {};
                    var locations = [];
                    if (details.length > 0) {
                        async.forEachSeries(details, function(recorddata, next) {
                            taskdata.recordId = recorddata._id;
                            //--> UE Implementation:11-07-2017
                            //Addition:Divya
                            //Added this variable to check the address is carded or non-carded address
                            taskdata.status = recorddata.status;
                            //added on 19th Sept 2017
                          //  taskdata.markerStyleCode = recorddata.markerStyleCode;
                            //<--

                            for (var i = 0; i < mapFields.length; i++) {
                                var values = recorddata.record[0][mapFields[i]];
                               // var res = values.split(",");
                                if (values == "") {
                                    locationDetails.lat = "";
                                    locationDetails.long = "";
                                    locations.push(locationDetails);
                                    locationDetails = {};
                                    taskdata.geometries = locations;
                                } else {
                                 if(values!= undefined) {

                                    var res = values.split(",");
                                    locationDetails.lat = res[0];
                                    locationDetails.long = res[1];
                                    locations.push(locationDetails);
                                    locationDetails = {};
                                    taskdata.geometries = locations;
                                }
                                }
                            }
                            setTimeout(function() {}, 1000);
                            task.push(taskdata);
                            taskdata = {};
                            locations = [];
                            next();
                        })
                    }

                })
            }
            latlongDataDetails.records = task;
            latlongData.push(latlongDataDetails);
            callback(latlongData);

        }
    })
}
//backporting end
router.get('/getPrePopulatedDataforUser/:taskId/:user/:formId', function (req, res, next) {
    var data = [];
    var taskdata = {}
    var DisplayValues = []
    var DisplayValuesdata = {}
    Tasks.find({_id : req.params.taskId}, function (err, post) {
        if (post.length > 0) {
            var formsids = []
            var count = 0;
            //async.forEachSeries(post[0].assignedFormsz, function (formsz, callback) {
                prepopdatajson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data.push(ress);
                });
                //callback();
                setTimeout(function () {
                    res.json(data)
                }, 1000);
        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 200
            });
        }
    });
});

//Get All Re-Assign Records
router.get('/getReAssignForms', function (req, res, next) {
    FormszDetails.find({IsReassign : true}, req.body, function (err, post) {
        res.json(post);
    });
});
router.get('/getRe-assginedRecods/:id/:user', function (req, res, next) {
    FormszDetails.find({IsReassign : true,updatedBy : req.params.user,formId : req.params.id,status : true}, function (err, post1) {
        if (post1.length > 0) {
            res.json(post1);

        } else {
            res.json({
                message : "No data found",
                status : 204
            });
        }
    });
});

//get latlong data
router.get('/getLatLongWIthForm/:formId/:taskId/:user', function (req, res, next) {
    var data = {};

    Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                latlongjson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data = {
                        latlngData : ress
                    }
                });
                setTimeout(function () {
                    res.json({
                        "data" : data,
                        "status" : 200
                    })
                }, 1000);

        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    })
});


// @PHANI PDF Generator
// router.post('/generatePDF', function(req,res,next) {
//  console.log("Entered");
//  var html = fs.readFileSync('./routes/sample.html', 'utf8');
//  console.log("html");
//  console.log(html);
//   var options = { format: 'Letter' };
 
//  pdf.create(html).toFile('./sample.pdf', function(err, res1) {
//    if (err) return console.log(err);
//    else {
//      res.json({"message":"PDF created","status":200});
//    }// { filename: '/app/businesscard.pdf' }

//  });


//  // var html = "<html><body><caption>RECORDS</caption></body></html>";
//  // var options = { format: 'Letter' }; 
//  // pdf.create(html, options).toFile('./sample.pdf', function(err, res) {
//  //   if (err) return console.log(err);
//  //   console.log(res); // { filename: '/app/businesscard.pdf' }
//  // });
// });

// PHANI-Divya PDF SERVER LOGIC
//var mailData;
router.post('/generatePDF', function(req,res,next) {
    console.log("req.body");
    var headers = [];
    var headerslbl = [];
    var mailData = req.body;
    Formsz.find({_id:req.body.formid},function(err,post1){
        if(post1.length>0){
            post1[0].FormSkeleton.forEach(function (recorddata) {

                if(recorddata.type.view=="section" ){
                    headers.push(recorddata.lable)
                    headerslbl.push(recorddata.lable)
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            if(data[i].data.type.view == 'group') {
                                headers.push(data[i].data.lable)
                                headerslbl.push(data[i].data.lable)
                                var group = data[i].data.type.fields
                                for (var j=0;j < group.length;j++) {
                                    headers.push(group[j].id);
                                    headerslbl.push(group[j].lable)
                                }
                            }
                            else {
                                headers.push(data[i].data.id);                                                         
                                headerslbl.push(data[i].data.lable) 
                            }
                        }
                    }  
                }
                else if(recorddata.type.view=="group"){
                    headers.push(recorddata.lable);
                    headerslbl.push(recorddata.lable)
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            headers.push(recorddata.type.fields[i].id);
                            headerslbl.push(recorddata.type.fields[i].lable)
                        }
                    }
                } 
                else{
                    headers.push(recorddata.id);
                    headerslbl.push(recorddata.lable)
                }
            });
            var recordids = [];
            recordids = req.body.records;
            FormszDetails.find({_id:{$in:recordids},isDeleted:false}, function(err,records) {
                
                fileid = mongoid;
                /*var html = "<html><head><body><caption>RECORDS</caption>";*/
                var html = "<html><head><body><caption>RECORDS</caption>";
                html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'> "  ;
                html = html + "<tr style='border: 3px solid'> <td> <img height='100;' width='100;' src='./images/mainpage_logo.png' alt=''></td>" ;
                html = html + "<td>" + "<h1 style='text-align: center;'>" + post1[0].name+ "</td>" + "</tr></table>" ;
            //    html = html + "<td>" + "<h1 style='text-align: center;'>" + post1[0].name+"</h1><br><h3><span style='float:left;'>Group Admin"+req.body.metaDataInfo[0].groupAdmin+"</span><span style='float:right;'>Department Admin:"+req.body.metaDataInfo[0].departmentAdmin+"</span></h3></td>" + "</tr></table>" ;

                if(records.length>0) {
                    var count = 0;
                    async.forEachSeries(records,function(data,next) {
                    html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'> "  ;
                    var objectArray = [];
                    var object = {};
                    var date = data.updatedTime;
                        date = date.toUTCString();                  
                        count++;
                        for(var k=0, i=0;k<headers.length,i<headerslbl.length;k++,i++) {
                            if(headers[k].includes('timestamp')==true) {
                                object.key = headerslbl[i];
                                object.value = date;
                                objectArray.push(object)
                                object ={};
                            }
                            else {
                                object.key = headerslbl[i];
                                object.value = data.record[0][headers[k]]
                                objectArray.push(object)
                                object ={};
                            }
                        }  
                        html = html + "<tr><td colspan = '2'><pre>" + ""+count+"." + "<br>" +"Form Name :" +post1[0].name+ "<br>Submitted Time : "+ date  +"<br>Submitted By :"+ data.updatedBy +"<br></pre></tr>"  
                            for(var j = 0; j<objectArray.length;j++) {
                                if(objectArray[j].value == undefined) {
                                    html = html + "<tr><th colspan='2'> "+ objectArray[j].key + "</th></tr>";
                                } 
                                else {
                                    var value = JSON.stringify(objectArray[j].value);
                                    if(value.indexOf('base64') != -1){
                                        html = html +"<tr><td>"+objectArray[j].key +"</td><td><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                    }
                                    else {
                                        html = html + "<tr> <td>"+ objectArray[j].key + "</td><td>" + objectArray[j].value + "</td></tr>";
                                    }
                                }
                            }
                            html = html + "</table><br>"
                            next();
                    }, function(err) {
                    html = html + "</head></body></html>"
                        pdf.create(html).toFile("./"+ mongoid +".pdf", function(err, response) {
                              if (err) 
                                  return console.log(err);
                               else 
                               
                                 console.log('congratulations, your pdf created');
                                  var buffer = fs.readFileSync("./" + mongoid + ".pdf", {
                                  encoding : 'base64'
                                 });
                                //sendAutomailwithAttachment(""+post1[0].altemail+post1[0].altemail1+post1[0].altemail2+post1[0].altemail3+post1[0].altemail4+"", "Dear User,<br> Please find requested record Information in the attachement for the Form : "+post1[0].name+"<br><br>Thanks,<br>DFormsTeam", "RECORD INFO", "./" + mongoid + ".pdf", ""+post1[0].altemail+post1[0].altemail1+post1[0].altemail2+post1[0].altemail3+post1[0].altemail4+"");
                                /*
                                @dexcription" Modification done for allow multiple reciepantes while mailing
                                @modified Date: 03-03-18
                                @developer: Umamaheswari B
                                */
                               sendAutomailwithAttachment(mailData.mailid,"Dear User,<br> Please find requested record Information in the attachement for the Form : "+post1[0].name+"<br><br>Thanks,<br>DFormsTeam", "RECORD INFO", "./" + mongoid + ".pdf",mailData.altemail,mailData.altemail1,mailData.altemail2,mailData.altemail3,mailData.altemail4);
                                /*sendAutomailwithAttachment(mailData.mailid,"Dear User,<br> Please find requested record Information in the attachement for the Form : "+post1[0].name+"<br><br>Thanks,<br>DFormsTeam", "RECORD INFO", "./" + mongoid + ".pdf",mailData.altemail, function(mailresponse) {
                                    if(mailresponse==true) {
                                                console.log("enterted")
                                                 fs.unlink("./"+ mongoid +".pdf",function(err){
                                                if(err) return console.log(err);
                                           });
                                           }
                                           else {
                                                res.json({"message":"Failed to send mail","status":500});


                                           } 
                                });*/


                                         /*   if(mailresponse==true) {
                                                console.log("enterted")
                                                 fs.unlink("./"+ mongoid +".pdf",function(err){
                                                if(err) return console.log(err);
                                           }); */ 
                                    /*res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); */
                                        //}
                                       /* else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }*/
                                //}); 
                            
                                 
                                
                            });
                             /* res.json({
                                    "message" : "Mail sent successfully",
                                    "status" : 200
                                });*/
                        
                            
                    });
                    }               
            });                 
        }       
    }); 
});

//PDF for mobile requests
/*router.post('/mobilegeneratePDF', function(req,res,next) {
    var headers = [];
    var headerslbl = [];
    Formsz.find({_id:req.body.formid},function(err,post1){
        if(post1.length>0){
            post1[0].FormSkeleton.forEach(function (recorddata) {

                if(recorddata.type.view=="section" ){
                    headers.push(recorddata.lable)
                    headerslbl.push(recorddata.lable)
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            if(data[i].data.type.view == 'group') {
                                headers.push(data[i].data.lable)
                                headerslbl.push(data[i].data.lable)
                                var group = data[i].data.type.fields
                                for (var j=0;j < group.length;j++) {
                                    headers.push(group[j].id);
                                    headerslbl.push(group[j].lable)
                                }
                            }
                            else {
                                headers.push(data[i].data.id);                                                         
                                headerslbl.push(data[i].data.lable) 
                            }
                        }
                    }  
                }
                else if(recorddata.type.view=="group"){
                    headers.push(recorddata.lable);
                    headerslbl.push(recorddata.lable)
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            headers.push(recorddata.type.fields[i].id);
                            headerslbl.push(recorddata.type.fields[i].lable)
                        }
                    }
                } 
                else{
                    headers.push(recorddata.id);
                    headerslbl.push(recorddata.lable)
                }
            });
            var recordids = [];
            recordids = req.body.records;
            FormszDetails.find({_id:{$in:recordids},updatedBy:req.body.username,isDeleted:false}, function(err,records) {
                
                fileid = mongoid;
                var html = "<html><head><body><caption>RECORDS</caption>";
                if(records.length>0) {
                    var count = 0;
                    async.forEachSeries(records,function(data,next) {
                    html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'> "  ;
                    var objectArray = [];
                    var object = {};
                    var date = data.updatedTime;
                        date = date.toUTCString();                  
                        count++;
                        for(var k=0, i=0;k<headers.length,i<headerslbl.length;k++,i++) {
                            if(headers[k].includes('timestamp')==true) {
                                object.key = headerslbl[i];
                                object.value = date;
                                objectArray.push(object)
                                object ={};
                            }
                            else {
                                object.key = headerslbl[i];
                                object.value = data.record[0][headers[k]]
                                objectArray.push(object)
                                object ={};
                            }
                        }  
                        html = html + "<tr><td colspan = '2'><pre>" + ""+count+"." + "<br>" +"Form Name :" +post1[0].name+ "<br>Submitted Time : "+ date  +"<br>Submitted By :"+ data.updatedBy +"<br></pre></tr>"  
                            for(var j = 0; j<objectArray.length;j++) {
                                if(objectArray[j].value == undefined) {
                                    html = html + "<tr><th colspan='2'> "+ objectArray[j].key + "</th></tr>";
                                } 
                                else {
                                    var value = JSON.stringify(objectArray[j].value);
                                    if(value.indexOf('base64') != -1){
                                        html = html +"<tr><td>"+objectArray[j].key +"</td><td><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                    }
                                    else {
                                        html = html + "<tr> <td>"+ objectArray[j].key + "</td><td>" + objectArray[j].value + "</td></tr>";
                                    }
                                }
                            }
                            html = html + "</table><br>"
                            next();
                    }, function(err) {
                    html = html + "</head></body></html>"
                        pdf.create(html).toFile("./"+ mongoid +".pdf", function(err, response) {
                              if (err) 
                                  return console.log(err);
                               else 
                               
                                 console.log('congratulations, your pdf created');
                                  var buffer = fs.readFileSync("./" + mongoid + ".pdf", {
                                  encoding : 'base64'
                                 });
                                sendAutomailwithAttachment(req.body.sendTo,"<html><pre>"+req.body.body+"</pre><html>", "RECORD INFO","./"+ mongoid +".pdf", ""+post1[0].alternativeMailid+"", function(mailresponse) {
                                         if(mailresponse==true) {
                                                 fs.unlink("./"+ mongoid +".pdf",function(err){
                                                if(err) return console.log(err);
                                           }); 
                                    res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }
                                });
                                         
                                        //}
                                       
                                //}); 
                            
                                 
                                
                            });
                             /* res.json({
                                    "message" : "Mail sent successfully",
                                    "status" : 200
                                });*/
                        
                 /*           
                    });
                    }               
            });                 
        }       
    }); 
});*/

router.post('/mobilegeneratePDF', function(req,res,next) {
    console.log("IN Mobile Generate PDF");
    console.log(req.body.formid);
    console.log(req.body);

    var headers = [];
    var headerslbl = [];
    Formsz.find({_id:req.body.formid},function(err,post1){
        if(post1.length>0){
            //console.log("Found FormID");

            post1[0].FormSkeleton.forEach(function (recorddata) {

                if(recorddata.type.view=="section" ){
                    headers.push(recorddata.lable)
                    headerslbl.push({"SN": recorddata.lable})
                    var data =recorddata.type.fields
                    //-->Siva Kella:Added
                    var tempObj = {};
                    //<-- End
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            if(data[i].data.type.view == 'group') {
                                headers.push(data[i].data.lable)
                                headerslbl.push({"SN": recorddata.lable , "GN": data[i].data.lable})
                                var group = data[i].data.type.fields
                                for (var j=0;j < group.length;j++) {
                                    headers.push(group[j].id);
                                    headerslbl.push({"SN": recorddata.lable , "GN": data[i].data.lable, "FN": group[j].lable})
                                }
                            }
                            else {
                                headers.push(data[i].data.id);                                                         
                                headerslbl.push({"SN": recorddata.lable , "FN": data[i].data.lable}) 
                            }
                        }
                    }  
                }
                else if(recorddata.type.view=="group"){
                    headers.push(recorddata.lable);
                    headerslbl.push({"GN":recorddata.lable})
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            headers.push(recorddata.type.fields[i].id);
                            headerslbl.push({"GN":recorddata.lable, "FN": recorddata.type.fields[i].lable})
                        }
                    }
                } 
                else{
                    headers.push(recorddata.id);
                    headerslbl.push({"FN": recorddata.lable})
                }
            });

            // console.log(headers);
            // console.log(headerslbl);

            var recordids = [];
            recordids = req.body.records;
            FormszDetails.find({_id:{$in:recordids},updatedBy:req.body.username,isDeleted:false}, function(err,records) {
                //console.log(records);
                

                fileid = mongoid;
                var html = "<html><head><body><caption>RECORDS</caption>";
                html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'> "  ;
                html = html + "<tr style='border: 3px solid'> <td> <img height='100;' width='100;' src='file:///D:/TPCLRelease/UAT-V3/TPCLDynamicFormsz/TPCLServer/images/mainpage_logo.png' alt=''></td>" ;
                html = html + "<td>" + "<h1 style='text-align: center;'>" + post1[0].name+ "</td>" + "</tr></table>" ;

                if(records.length>0) {
                    var count = 0;
                    async.forEachSeries(records,function(data,next) {
                        //console.log(data);
                        //console.log("-------------------------------------");

                    var objectArray = [];
                    var object = {};
                    var date = data.updatedTime;
                        date = date.toUTCString();                  
                        count++;

                        for(var k=0, i=0;k<headers.length,i<headerslbl.length;k++,i++) {
                            // console.log(headers[k]);
                            // console.log(headerslbl[i].SN , headerslbl[i].GN , headerslbl[i].FN );

                            if(headers[k].includes('timestamp')==true) {
                                object.key = headerslbl[i];
                                object.value = date;
                                objectArray.push(object)
                                object ={};
                            }
                            else {
                                object.key = headerslbl[i];
                                object.value = data.record[0][headers[k]]
                                objectArray.push(object)
                                object ={};
                            }
                        }
                        //console.log(objectArray);
                        
                        html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'>"

                        


                        html = html + "<tr><td>" + count + "." + "<br>Submitted Time : "+ date  +"<br></td>" + "<td>" + "<br>Submitted By :"+ data.updatedBy +"<br></td></tr>"
                        //html = html + "<tr><td colspan = '2'><pre>" + ""+count+"." + "<br>" +"Form Name :" +post1[0].name+ "<br>Submitted Time : "+ date  +"<br>Submitted By :"+ data.updatedBy +"<br></pre></tr>"  
                        var disKey = '';
                        var tableStart = false;
                        var value = '';
                            for(var j = 0; j<objectArray.length;j++) {

                                    if (objectArray[j].key.SN != undefined && objectArray[j].key.GN == undefined && objectArray[j].key.FN == undefined) { 
                                        // SECTION CASE
                                        disKey = objectArray[j].key.SN; 
                                        // html = html + "<tr><th colspan='3' style='background-color:lightblue; float: left' > "+ disKey + "</th></tr>";  
                                        // tableStart = true ;

                                    }
                                    else if ( objectArray[j].key.SN != undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN == undefined) {
                                        // SECTION With Only GROUP
                                         disKey =  objectArray[j].key.GN; 
                                        //html = html + "<tr><th colspan='3' style='background-color:gray; float: left' > "+ disKey + "</th></tr>";   

                                        
                                    }
                                    else if (objectArray[j].key.SN != undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN != undefined) {
                                         // SECTION With GROUP --> FIELD
                                         disKey =  objectArray[j].key.FN; 

                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }

                                    }
                                    else if (objectArray[j].key.SN == undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN == undefined) {
                                         // GROUP Case
                                         disKey =  objectArray[j].key.GN; 
                                         // html = html + "<table><tr><th colspan='3' style='background-color: gray;float: left'> "+ disKey + "</th></tr>";  
                                     }
                                     else if (objectArray[j].key.SN == undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN != undefined) {
                                         // GROUP --> FIELD
                                         disKey =  objectArray[j].key.FN;

                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }
                                     }
                                     else {
                                        // FIELD Case
                                         disKey =  objectArray[j].key.FN;
                                         
                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }
                                     }



                                if(objectArray[j].value == undefined) {
                                    //console.log("UNDEFINED--->>>>",objectArray[j].key.SN, objectArray[j].key.GN,objectArray[j].key.FN);
                                    
                                    //console.log(disKey);
                                    html = html + "<tr style='background-color:gray' ><td colspan='2' > "+ disKey + "</td></tr>";
                                } 
                                else {
                                    var value = JSON.stringify(objectArray[j].value);
                                    //console.log("NORMAL ***>>>>",objectArray[j].key.SN, objectArray[j].key.GN,objectArray[j].key.FN);
                                    
                                    
                                    

                                    
                                    //console.log(disKey);
                                    if(value.indexOf('base64') != -1){
                                        html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                    }
                                    else {
                                        html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                    }
                                }
                            }

                            html = html + "</table><br>"
                            next();
                    }, function(err) {
                    html = html + "</head></body></html>"
                    //console.log(html);

                        pdf.create(html).toFile("./"+ post1[0].name +".pdf", function(err, response) {
                              if (err) 
                                  return console.log(err);
                               else 
                               
                                 console.log('congratulations, your pdf created');
                                  var buffer = fs.readFileSync("./" + post1[0].name + ".pdf", {
                                  encoding : 'base64'
                                 });

                                //sendAutomailwithAttachment(req.body.sendTo,"<html><pre>"+req.body.body+"</pre><html>", "RECORD INFO","./"+ post1[0].name +".pdf", ""+post1[0].altemail+post1[0].altemail1+post1[0].altemail2+post1[0].altemail3+post1[0].altemail4+"", function(mailresponse) {
                                    sendAutomailwithAttachment(req.body.sendTo,"<html><pre>"+req.body.body+"</pre><html>", "RECORD INFO","./"+ post1[0].name +".pdf", ""+post1[0].altemail+"", function(mailresponse) {


                                         if(mailresponse==true) {
                                                console.log("enterted")
                                                 fs.unlink("./"+ post1[0].name +".pdf",function(err){
                                                if(err) return console.log(err);
                                           }); 
                                    res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }
                                });
                                         
                                        //}
                                       
                                //}); 
                            
                                 
                                
                            });
                             /* res.json({
                                    "message" : "Mail sent successfully",
                                    "status" : 200
                                });*/
                        
                            
                    });
                    }               
            });                 
        }       
    }); 
});


/*function latlongjson(formId, taskId, user, callback) {

    Formsz.find({_id : formId,isVisible : true}, function (err, formsz) {
        var latlongData = [];
        var latlongDataDetails = {};
        var task = [];
        var locationDetails = {};
        if (formsz.length > 0) {
            var mapFields = [];
            latlongDataDetails.formId = formsz[0]._id;
            mapFields = formsz[0].geoFields;
            FormszDetails.find({formId : formId,taskId : taskId,DSAssignedTo : user}, function (err, details) {
                var taskdata = {};
                var locations = [];
                if (details.length > 0) {
                    async.forEachSeries(details, function (recorddata, next) {
                        taskdata.recordId = recorddata._id;
                       // taskdata.status = recorddata.status;

                        for (var i = 0; i < mapFields.length; i++) {
                            var values = recorddata.record[0][mapFields[i]];
                            if (values == "") {
                                locationDetails.lat = "";
                                locationDetails.long = "";
                                locations.push(locationDetails);
                                locationDetails = {};
                                taskdata.geometries = locations;
                            } else {
                                var res = values.split(",");
                                locationDetails.lat = res[0];
                                locationDetails.long = res[1];
                                locations.push(locationDetails);
                                locationDetails = {};
                                taskdata.geometries = locations;
                            }
                        }
                        setTimeout(function () {}, 1000);
                        task.push(taskdata);
                        taskdata = {};
                        locations = [];
                        next();
                    })
                }

            })
            latlongDataDetails.records = task;
            latlongData.push(latlongDataDetails);
            callback(latlongData);

        }
    })
}*/


//download Service for prepopulatedData
router.get('/downloadService/:taskId/:formId/:user', function (req, res, next) {
    var data = [];
        Tasks.find({_id : req.params.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                downloadjson(req.params.formId, req.params.taskId, req.params.user, function (ress) {
                    data.push(ress);
                });
                setTimeout(function () {
                    res.json(data)
                }, 1000);

        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    });
});

router.post('/downloadService', function (req, res, next) {
  //  /:taskId/:formId/:user
    var data = [];
        Tasks.find({_id : req.body.taskId,isDeleted : false}, function (err, tasks) {
        if (tasks.length > 0) {
                downloadjson(req.body.formId, req.body.taskId, req.body.user, function (ress) {
                    data.push(ress);
                });
                //Activity log for form Download
               activityLog.activityLogFortaskOrFormDownload(req.body,"Form Downloaded");


                setTimeout(function () {
                    res.json(data)
                }, 1000);

        //  });
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            });
        }
    });
});

function downloadjson(id,taskid, user, callback) {
    var reqfields = {};
    var taskdata = {};
    var DisplayValues = [];
    var DisplayValuesData = {};
    var DownloadRecordsData = {};
    var DownloadRecords = [];
    var AllFieldsData = {};
    var AllFields = [];
    var i = 0;
    var DownloadedFormsz = [];
    var values = {};
    var DownloadedFormszData = {};
    Formsz.find({_id : id}, function (err, formsz) {
        if (formsz.length > 0) {
            if (formsz[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = formsz[0]._id;
                taskdata.FormName = formsz[0].name;
                taskdata.isMediaAvailable = formsz[0].isMediaAvailable;

                callback(taskdata);
            } else {
                reqfields = Object.keys(formsz[0].requiredField[0]);
                taskdata.formId = formsz[0]._id;
                taskdata.FormName = formsz[0].name;
                taskdata.isMediaAvailable = formsz[0].isMediaAvailable;
                //FormszDetails.find({formId : id,taskId : taskid,DSAssignedTo : user}, function (err, formszDetails) {
                    FormszDetails.find({
                            $or: [{
                                formId: id,
                                taskId: taskid,
                                DSAssignedTo: user
                            }, {
                                formId: id,
                                taskId:taskid,
                                updatedBy: user,
                                IsReassign: true
                            }]    }, function(err, formszDetails) {

                    if (formszDetails.length > 0) {
                        formszDetails.forEach(function (recorddata) {
                            values = Object.keys(recorddata.record[0]);
                            DownloadRecordsData.recordId = recorddata._id,
                            DownloadRecordsData.IsReassign = recorddata.IsReassign,
                            DownloadRecordsData.comments = recorddata.comments
                                for (var i = 0; i < reqfields.length; i++) {
                                    DisplayValuesData.fieldId = reqfields[i];
                                    DisplayValuesData.fieldIdName = recorddata.record[0][reqfields[i]];
                                    DisplayValues.push(DisplayValuesData);
                                    DisplayValuesData = {};
                                }
                                setTimeout(function () {}, 1000);
                            DownloadRecordsData.DisplayValues = DisplayValues;
                            DisplayValues = [];

                            for (var i = 0; i < values.length; i++) {
                                //AllFieldsData.fieldId = values[i];
                                //AllFieldsData.fieldIdName = recorddata.record[0][values[i]];
                                 AllFieldsData[values[i]] = recorddata.record[0][values[i]];
                                AllFields.push(AllFieldsData);
                                AllFieldsData = {};
                            }
                            setTimeout(function () {}, 1000);
                            DownloadRecordsData.AllFields = AllFields;
                            AllFields = [];
                            DownloadRecords.push(DownloadRecordsData);
                            taskdata.DownloadRecords = DownloadRecords;
                            DownloadRecordsData = {}
                        });
                        callback(taskdata);
                    } else {
                        taskdata.DownloadRecords = DownloadRecords;
                        callback(taskdata);
                    }
                });
            }
        }
    });
}

router.get('/getRe-assginedRecods/:formId/:user', function (req, res, next) {
 var allrecords = [];
 var data = [];
 var resultJson = {};
 FormszDetails.find({updatedBy : req.params.user,IsReassign:true,formId:req.params.formId}, function (err, post) {
  if (post.length > 0) {
   post.forEach(function (displayobject) {
    DisplayValues.find({recordId : displayobject._id}, function (err, post1) {
     Formsz.find({  _id : post1[0].formId,  isVisible : true}, function (err, formobj) {
      Tasks.find({"assignedFormsz.formId" : formobj[0]._id,isVisible : true}, function (err, task) {
       resultJson.displayValues = post1[0].displayValues
        resultJson.type = task.length > 0 ? "Task" : "form"
        resultJson.startDate = task.length > 0 ? task[0].startDate : ""
        resultJson.endDate = task.length > 0 ? task[0].endDate : ""
        resultJson.description = task.length > 0 ? task[0].description : formobj[0].description
        resultJson.formId = formobj[0]._id
        resultJson.formName = formobj[0].name
                resultJson.FormSkeleton = formobj[0].FormSkeleton
                resultJson.requiredField = formobj[0].requiredField
                resultJson.recordId=    displayobject._id
        resultJson.taskId = task.length > 0 ? task[0]._id : ""
        resultJson.taskName = task.length > 0 ? task[0].name : ""
        resultJson.comments = displayobject.comments
        data.push(resultJson);
       resultJson = {};
      });
     });
    });
   });
   setTimeout(function () {
    res.json(data)
   }, 1000);
  } else {
   res.json({
    "message" : "No data found",
    "status" : 204
   })
  }
 });
});

router.get('/getReForms/:user', function (req, res, next) {
    FormszDetails.distinct("formId", {IsReassign : true,updatedBy : req.params.user,status : true}, function (err, post1) {
        if (post1.length > 0) {
            getFormname(post1, function (ress) {
                res.json(ress);
            });
        } else {
            res.json({
                message : "No data found",
                status : 204
            });
        }
    });
});

/* GET /get user records based on FormId */
router.get('/:formid/:user/:taskId', function(req, res, next) {
    FormszDetails.find({formId: req.params.formid,updatedBy: req.params.user,taskId: req.params.taskId,status:true}).sort({updatedTime:1}).exec(
    function(err, records) {
        if (records.length > 0) {
            var records1 = [];
            async.forEachSeries(records, function(info, next) {
                    var recorddata = {};
                    DisplayValues.find({
                        recordId: info._id
                    }, function(err, displayFields) {
                        if (displayFields.length > 0) {
                            for (var i = 0; i < displayFields.length; i++) {
                                recorddata.recordId = displayFields[i].recordId;
                                recorddata.displayFields = displayFields[i].displayValues;
                                recorddata.createdTime = displayFields[i].createdTime;
                                records1.push(recorddata);

                                next();
                               
                                
                            }
                        }
                        else {
                             next();        
                                }
                    })
                },
                function(err) {
                    res.json({
                        data: records1,
                        status: 200
                    });
                })

        }
        else {
            res.json({
                "message": "No data found",
                "status": "204"
            });
        }
    }) 
    
});

// get prePOPRecords for user
router.get('/getprePOPRecords/:user/:formId/:recordId', function(req, res, next) {
   // FormszDetails.find({DSAssignedTo: req.params.user,formId: req.params.formId,_id: req.params.recordId,isDeleted: false}, function(err, userdetails) {
        FormszDetails.find({
        $or: [{
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            DSAssignedTo: req.params.user
        }, {
            formId: req.params.formId,
            _id: req.params.recordId,
            isDeleted: false,
            updatedBy: req.params.user,
            IsReassign: true
        }]
    }, function(err, userdetails) {
        var data = [];
        if (userdetails.length > 0) {
            async.forEach(userdetails, function(item) {
                data.push({
                    prepopulatedData: item.record,
                    taskId: item.taskId
                });
            });
            res.json(data);
        } else {
            res.json({
                "message": "No data found",
                "status": "204"
            });
        }
    });
});
router.get('/getPrePOPData/:formid/:user/:taskId', function (req, res, next) {

    Formsz.find({_id : req.params.formid,isVisible : true}, function (err, post) {
        if (post.length > 0) {
            FormszDetails.find({formId : req.params.formid,DSAssignedTo : req.params.user,updatedBy : null,taskId : req.params.taskId,isDeleted : false}, function (err, post1) {
                if (post1.length > 0) {
                    var userdata = [];
                    post1.forEach(function (dbUserObj1) {
                        userdata.push({
                            record : dbUserObj1.record[0],
                            recordId : dbUserObj1._id
                        });
                    });
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : userdata,
                        DisplayFields : post[0].requiredField
                    });
                } else {
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : [],
                        DisplayFields : post[0].requiredField
                    });
                }
            });

        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            })
        }
    });
});

//Get Formsz Details from Date and Todate
router.get('/getformszDetail/:formid/:user/:fromdate/:todate/:taskid', function (req, res, next) {
    var updatedby = req.params.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
    Formsz.find({
        _id : req.params.formid,
        isVisible : true
    }, function (err, post) {
        if (post.length > 0) {
       //  FormszDetails.find([{$or:[{formId : req.params.formid,isDeleted : false,updatedTime : {$gte : new Date(req.params.fromdate),$lte : new Date(new Date(req.params.todate).setDate(new Date(req.params.todate).getDate() + 1))},updatedBy : updatedby,taskId : req.params.taskid}]}, function (err, post1) {

            FormszDetails.find({formId : req.params.formid,isDeleted : false,updatedTime : {$gte : new Date(req.params.fromdate),$lte : new Date(new Date(req.params.todate).setDate(new Date(req.params.todate).getDate() + 1))},updatedBy : updatedby,taskId : req.params.taskid}, function (err, post1) {
                if (post1.length > 0) {
                    var userdata = [];
                    var recordIds = [];
                    var formInfo = {};
                    var comment = [];
                    var recordinfo = [];
                    var reassinegdReocrdIds = [];
                    formInfo.formId = post[0]._id;
                    formInfo.createdBy = post[0].createdBy;
                    formInfo.createdTime = post[0].createdTime;
                    formInfo.description = post[0].description;
                    formInfo.version = post1[0].version;
                    post1.forEach(function (dbUserObj1) {
                        var lat = "";
                        var long ="";
                         lat = dbUserObj1.lat;
                         long = dbUserObj1.long;
                          if(lat==null && long==null) {
                            InsertedLocation = "";  
                        }else{
                            InsertedLocation = lat + "," + long 
                        }
                        if (dbUserObj1.IsReassign) {
                            reassinegdReocrdIds.push(dbUserObj1._id);
                        }
                        userdata.push(dbUserObj1.record[0]);
                        dbUserObj1.record[0]._id = dbUserObj1._id;
                        //userdata.push({"record":dbUserObj1.record[0],"videoId":dbUserObj1.videoId});
                        recordIds.push(dbUserObj1._id);
                        comment.push({
                            "id" : dbUserObj1._id,
                            "Comments" : dbUserObj1.comments,
                            "updatedBy" : dbUserObj1.updatedBy,
                            "lastUpdatedBy" : dbUserObj1.lastUpdatedBy,
                            "assignedTo" : dbUserObj1.assignedTo,
                        });
                        recordinfo.push({
                            "UpdatedBy" : dbUserObj1.updatedBy,
                            "lastModifiedBy":dbUserObj1.lastModifiedBy,
                            "UpdatedTime" : dbUserObj1.updatedTime,
                            "lastUpdatedBy" : dbUserObj1.lastUpdatedBy,
                            "InsertedLocation" :  lat +","+ long 
                        });
                    });
                    res.json({
                        Skelton : post[0].FormSkeleton,
                        records : userdata,
                        recordIdsList : recordIds,
                        formInfo : formInfo,
                        reassignRecordIds : reassinegdReocrdIds,
                        comments : comment,
                        "recordInfo" : recordinfo
                    });

                    //res.json({Skelton:post[0].FormSkeleton,records:userdata});
                } else {
                    res.json({
                        "message" : "No Data Found",
                        "status" : 204
                    });
                }
            });
        } else {
            res.json({
                "message" : "No Data Found",
                "status" : 204
            });

        }
    });
});

//Get all Formsz Records
router.get('/getformszDetails', function (req, res, next) {
    FormszDetails.find({isDeleted : false}, function (err, todos) {
        res.json(todos);
    });
});

//get single record based on the ID
router.get('/:id', function (req, res, next) {
    FormszDetails.findOne({_id : req.params.id,isDeleted : false}, function (err, post) {
        if (err)
            return next(err);
        res.json(post);
    });
});

// Formsz Details
router.post('/create', function (req, res, next) {
     deviceManagement.find({UUID:req.body.UUID,isDeleted:false}, function(err,device) {
        if(device.length>0) {
            if(device[0].status=="Suspended"|| device[0].status=="Rejected") {

                res.json({
                    "message":"Your device is Suspended,so you cannot submit the record",
                    "status":203
                })

            }
            else {
    Formsz.find({_id : req.body.formId,isVisible : false}, function (err, post1) {
        if (post1.length > 0) {
            res.json({
                "message" : "This action cannot be performed as the form no longer exists",
                status : 203
            });
        } else {
            var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id : req.body.formId,
                isVisible : true
            }, function (err, formObj) {
                
                displayrecords = Object.keys(formObj[0].requiredField[0]);
            });
            req.body.status = true;
            FormszDetails.create(req.body, function (err, post) {
                setTimeout(function () {
                    var i = 0;
                    for (i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue=post.record[0][displayrecords[i]]||"";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues
                        ({
                            formId : post.formId,
                            recordId : post._id,
                            displayValues : data
                        });
                    displayJosn.save(function (err, a) {
                        // if (err) throw err;
                    })
                    res.json({
                        "recordId":post._id,
                        "sqliteDBId":post.sqliteDBId,
                        "message" : "Submitted Successfully",
                        status : 200
                    })
                }, 1000);
            });
        }
    });
}
}
else {
    res.json({"message":"Device details not available","status":203});
}
});
});
//PUT /Update records/:id  online -offline
/*router.put('/:id', function (req, res, next) {

    req.body.DSAssignedTo = null;
    Formsz.find({_id : req.body.formId,isVisible : false}, function (err, post1) {
        if (post1.length > 0) {
            res.json({
                "message" : "This action cannot be performed as the form no longer exists",
                status : 203
            });
        } else {
            req.body.status = true;
            var displayrecords = [];
            var displaydata = {};
            var data = [];
            Formsz.find({
                _id : req.body.formId,
                isVisible : true
            }, function (err, formObj) {
                
                displayrecords = Object.keys(formObj[0].requiredField[0]);
                /* var requiredFieldArray = formObj[0].requiredField;
                for(var i=0;i<requiredFieldArray.length;i++) {
                    displayrecords.push(requiredFieldArray[i].id);
                    
                } */
            /*});
            FormszDetails.findByIdAndUpdate(req.params.id, req.body,{new:true}, function (err, post) {
                setTimeout(function () {
                    var i = 0;
                    for (i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue=post.record[0][displayrecords[i]]||"";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    DisplayValues.update({formId : post.formId,
                            recordId : post._id},{$set:{
                                displayValues : data
                            }},function (err, a) {
                         if (err) throw err;
                         else {
                             console.log("success");
                         }
                    })
                    res.json({
                        "message" : "Updated Successfully",
                        status : 200
                    })
                }, 1000);*/

                /* res.json({
                    "message" : "Updated Successfully",
                    status : 200
                }); */
/*          });
        }
    });
});*/


router.put('/:id', function(req, res, next) {
    deviceManagement.find({UUID:req.body.UUID,isDeleted:false}, function(err,device) {
        if(device.length>0) {
            if(device[0].status=="Suspended"|| device[0].status=="Rejected") {
                res.json({
                    "message":"Your device is Suspended,so you cannot submit the record",
                    "status":203
                })

            }
            else {
                    req.body.DSAssignedTo = null;
                    req.body.status = true;
              Formsz.find({
            _id: req.body.formId,
            isVisible: false
        }, function(err, post1) {
        if (post1.length > 0) {
            res.json({
                "message": "This action cannot be performed as the form no longer exists",
                status: 203
            });
        } else {
            req.body.status = true;
            req.body.DSAssignedTo = null;

            FormszDetails.findByIdAndUpdate(req.params.id, req.body, function(err, post) {
                FormszDetails.find({
                    taskId: post.taskId,
                    status: false
                }, function(err, results) {
                    if (err) {
                        //console.log(err);
                    } else {
                        var count = results.length;
                        var offlineJobIdArray=[];
                        if (count <= 0) {
                            Tasks.update({
                                _id: post.taskId
                            }, {
                                $set: {
                                    isClosed: true
                                }
                            }, function(err, a) {
                                if (err) {
                                    console.log(err);
                                } else {
                                    offlineJobIdArray.push(post.taskId);
                                    
                                    res.json({
                                "message": "Updated Successfully",
                                "offlineJobIdArray":offlineJobIdArray,
                                status: 200
                            });
                                } // else loop ends here
                            }) // tasks update ends here

                          
                            
                        } else {
                            res.json({
                                "message": "Updated Successfully",
                                "offlineJobIdArray":offlineJobIdArray,
                                status: 200
                            });
                        }

                    }
                });

            });
           
        }
    });

            }
        }
        else {
            res.json({"message":"Device details not available","status":203});
        }
    })


        
    
});


// Re-Assign
/*router.post('/ReAssign', function (req, res, next) {
    var commnets = [];
    var data = req.body.recordsId;
    var i = 0;
    for (i = 0; i <= data.length; i++) {
        FormszDetails.find({_id : data[i]}, function (err, post) {
            if (post.length > 0) {
                post.forEach(function (reassginobjects) {
                    var json = [];
                    if (JSON.parse(reassginobjects.comments)) {
                        json = JSON.parse(reassginobjects.comments);
                    }
                    json.push({
                        "Comment" : req.body.comments,
                        "UpdatedTime" : new Date()
                    });
                    reassginobjects.IsReassign = true;
                    reassginobjects.status = true;
                    reassginobjects.assignedTo = req.body.assignedTo;
                    reassginobjects.comments = JSON.stringify(json);
                    reassginobjects.save(function (err) {});

                });

            }

        });
    }
    res.json({
        "message" : "Re-Assign Successfully",
        "status" : 200
    })
});*/
router.post('/ReAssign', function(req, res, next) {
    var commnets = [];
    var recIdsArray = req.body.recordsId;
    var i = 0;
    async.forEachSeries(recIdsArray, function(recIdObj, next) {
            FormszDetails.find({
                _id: recIdObj
            }, function(err, post) {
                if (post.length > 0) {
                    post.forEach(function(reassginobjects) {
                        var json = [];
                        if (JSON.parse(reassginobjects.comments)) {
                            json = JSON.parse(reassginobjects.comments);
                        }
                        json.push({
                            "Comment": req.body.comments,
                            "UpdatedTime": new Date()
                        });
                        
                        reassginobjects.IsReassign = true;
                        reassginobjects.status = false;
                        reassginobjects.assignedTo = req.body.assignedTo;
                        reassginobjects.comments = JSON.stringify(json);
                        reassginobjects.save(function(err, reassignedObj) {
                            if (reassignedObj) {
                                // Roja edited code  
                                /*users.find({
                                    type: "1"
                                }, function(err, userObj) {*/
                                    Tasks.find({
                                        _id: reassignedObj.taskId
                                    }, function(err, taskObj) {
                                        if (taskObj.length > 0) {
                                           Tasks.update({_id:taskObj[0]._id}, {
                                            $set: {
                                                isClosed: false
                                            }
                                        }, function(err, a) {
                                            if (err) throw err;
                                            else {
                                                console.log("updated");
                                            }
                                        }); 
                                         var reassignedfrom;

                                            var formId = taskObj[0].assignedFormsz[0].formId
                                            DisplayValues.find({
                                                recordId: reassginobjects._id
                                            }, {
                                                displayValues: 1
                                            }, function(err, dpvalsObj) {
                                                if(req.body.reassignedfrom=="task") {
                                                    reassignedfrom = 1;
                                                } else {
                                                    reassignedfrom = 2
                                                }
                                                var dpvalue = dpvalsObj[0].displayValues[0];
                                                notificationmodelObject = new notificationmodel({
                                                   // adminName: userObj[0].name,
                                                    taskName: taskObj[0].name,
                                                    taskId: reassginobjects.taskId,
                                                    recordId: reassginobjects._id,
                                                    comments: reassignedObj.comments,
                                                    user: req.body.assignedTo,
                                                    displayValues: dpvalue.filedValue,
                                                    actionType: reassignedfrom,
                                                    formId: formId,
                                                    adminId: req.body.adminId

                                                });
                                                notificationmodelObject.save(function(err, notifyObj) {
                                                    if (err)
                                                        return next(err);
                                                    else {
                                                        console.log("inserted");
                                                    }

                                                });
                                            });
                                        } else {
                                            console.log("no length");
                                        }
                                    }); //tasks.find end

                             //   }); // roja code end 
                            } else {
                                console.log(err);
                            }



                        });
                    });
                    next();
                } else {
                    res.json({
                        "message": "No data found",
                        "status": 204
                    });
                }

            });
        },
        function(err) {
            res.json({
                "message": "Re-Assign Successfully",
                "status": 200
            })
        });

});

//Delete Formsz Records
router.delete ('/delete/:id', function (req, res, next) {
    FormszDetails.find({_id : req.params.id}, function (err, post) {
        if (post.length > 0) {
            post.forEach(function (post1) {
                post1.isDeleted = true;
                post1.save(function (err, post) {
                    if (err)
                        return next(err);
                    res.json({
                        "status" : 200,
                        "message" : "Deleted Successfully"
                    });
                });
            });
        } else {
            res.json({
                "status" : 204,
                "message" : "No Data Found"
            });
        }
    });
});

router.get('/getRe-assginedRecodsMobile/:id', function (req, res, next) {
    //res.json({message:"hiiiii"})
    var allrecords = [];
    var data = [];
    var resultJson = {};
    FormszDetails.find({updatedBy : req.params.id,IsReassign:true}, function (err, post) {
        if (post.length > 0) {
            post.forEach(function (displayobject) {
                DisplayValues.find({recordId : displayobject.id}, function (err, post1) {
                    Formsz.find({_id : post1[0].formId,isVisible : true}, function (err, formobj) {
                        if(formobj.length>0)
                        {
                            if(displayobject.taskId=="form")
                            {
                                        resultJson.displayValues = post1[0].displayValues
                                        resultJson.recordId= displayobject._id
                                        resultJson.type = "form"
                                        resultJson.startDate = ""
                                        resultJson.endDate = ""
                                        resultJson.description = formobj[0].description
                                        resultJson.formId = formobj[0]._id
                                        resultJson.formName = formobj[0].name
                                        resultJson.taskId = ""
                                        resultJson.taskName = ""
                                        resultJson.comments = displayobject.comments
                                        data.push(resultJson);
                                        resultJson = {};
                             }else
                            {
                            
                                        resultJson.displayValues = post1[0].displayValues
                                        resultJson.recordId= displayobject._id
                                        resultJson.type = "task"
                                        resultJson.startDate = ""
                                        resultJson.endDate = ""
                                        resultJson.description = formobj[0].description
                                        resultJson.formId = formobj[0]._id
                                        resultJson.formName = formobj[0].name
                                        resultJson.taskId = displayobject.taskId
                                        resultJson.taskName = ""
                                        resultJson.comments = displayobject.comments
                                        data.push(resultJson);
                                        resultJson = {};
                            }
                        }
                    });
                });
            });
            setTimeout(function () {
                res.json(data)
            }, 1000);
        } else {
            res.json({
                "message" : "No data found",
                "status" : 204
            })
        }
    });
});
/*router.post('/download', function (req, res, next) {
    var headers=[];
    var headerslbl=[];
    Formsz.find({_id:req.body.formid},function(err,post1)
        {
             if(post1.length>0)
              {
               post1[0].FormSkeleton.forEach(function (recorddata) {
                 if(recorddata.type.view=="section" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   if(data[i].data.type.view == 'group') {
                    
                    var group = data[i].data.type.fields
                    for (var j=0;j < group.length;j++) {
                    //console.log(group[j])   
                    headers.push(group[j].id);
                    headerslbl.push(group[j].lable)
                    }
                   }
                   else {
                    headers.push(data[i].data.id);
                    headerslbl.push(data[i].data.lable);
                   }
                   
                  }
                  
                 }
                 
                 
                }
                 else if(recorddata.type.view=="group" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   headers.push(recorddata.type.fields[i].id);
                   headerslbl.push(recorddata.type.fields[i].lable)
                  }
                  
                 }
                 
                } 
                else
                {
                headers.push(recorddata.id);
                headerslbl.push(recorddata.lable)
                }
                
               });

              }         
        })
    var updatedby = req.body.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
    var emailids=[]
    emailids=req.body.records;
    FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
        if (post.length > 0) {
            fileid = mongoid;
            var workbook = excelbuilder.createWorkbook('./', mongoid + ".xlsx");
            var sheet1 = workbook.createSheet('sheet1', 100, 10000);
            var k = 1;
            var j = 1;
            var count=1
            for (var i = 0; i < post.length; i++) {
                for (var j = 0; j < headerslbl.length; j++) 
                {
                    if (i == 0) {
                        sheet1.set(j + 1, k, headerslbl[j]);
                    }
                }
                var data={}
                var data=post[i].record[0];
                var theTypeIs = Object.keys(post[i].record[0]);
                  for (var j = 0; j < theTypeIs.length; j++) 
                {
                    if(headers.indexOf(theTypeIs[j])>-1)
                    {
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);
                    }
                    
                    
                }                   
            count=count+1
            }
            workbook.save(function (err) {
                if (err)
                    console.log(err);
                else
                console.log('congratulations, your workbook created');
                var buffer = fs.readFileSync("./" + mongoid + ".xlsx", {
                        encoding : 'base64'
                    });
                /*
                    @dexcription" Modification done for allow multiple reciepantes while mailing
                    @modified Date: 03-03-18
                    @developer: Umamaheswari B
                    */
                //--> TPCL Customizations: Uma: Modified: To handle Multiple mail receipents under CC
              /*  sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail, function(mailresponse) {
                    if(mailresponse==true) {
                        console.log("enterted")
                                                 fs.unlink("./"+ mongoid +".pdf",function(err){
                                                if(err) return console.log(err);
                                           });
                                                  res.json({
                    "message" : "Mail sent successfully",
                    "status" : 200
                });

                    }else {
                         res.json({"message":"Failed to send mail","status":500});


                    }
                });    
               // sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4);
                //<-- End
               
            });
        }
        
    }); 
});*/

router.post('/download', function (req, res, next) {
    var headers=[];
    var headerslbl=[];
    Formsz.find({_id:req.body.formid},function(err,post1)
        {
             if(post1.length>0)
              {
               post1[0].FormSkeleton.forEach(function (recorddata) {
                 if(recorddata.type.view=="section" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   if(data[i].data.type.view == 'group') {
                    
                    var group = data[i].data.type.fields
                    for (var j=0;j < group.length;j++) {
                    //console.log(group[j])   
                    headers.push(group[j].id);
                    headerslbl.push(group[j].lable)
                    }
                   }
                   else {
                    headers.push(data[i].data.id);
                    headerslbl.push(data[i].data.lable);
                   }
                   
                  }
                  
                 }
                 
                 
                }
                 else if(recorddata.type.view=="group" )
                {
                 var data =recorddata.type.fields
                 if(data.length>0)
                 {
                  for (var i=0;i<data.length;i++)
                  {
                   headers.push(recorddata.type.fields[i].id);
                   headerslbl.push(recorddata.type.fields[i].lable)
                  }
                  
                 }
                 
                } 
                else
                {
                headers.push(recorddata.id);
                headerslbl.push(recorddata.lable)
                }
                
               });

              }         
        })
    var updatedby = req.body.user;
    if (updatedby == "All") {
        updatedby = {
            $ne : null
        }
    }
    var emailids=[]
    emailids=req.body.records;
    FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
 //   FormszDetails.find({formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedBy : updatedby}, function (err, post) {
        if (post.length > 0) {
            fileid = mongoid;
            var workbook = excelbuilder.createWorkbook('./', mongoid + ".xlsx");
            var sheet1 = workbook.createSheet('sheet1', 100, 10000);
            var k = 1;
            var j = 1;
            var count=1;
            headerslbl.push("DepartmentAdmin","GroupAdmin");
            for (var i = 0; i < post.length; i++) {
                for (var j = 0; j < headerslbl.length; j++) 
                {
                    if (i == 0) {
                        sheet1.set(j + 1, k, headerslbl[j]);
                    }
                }
                var data={}
                var data=post[i].record[0];
                var theTypeIs = Object.keys(post[i].record[0]);
               theTypeIs.push("DepartmentAdmin","GroupAdmin");
               headers.push("DepartmentAdmin","GroupAdmin");
               console.log("headers");
               console.log(headers);
              
                
                  for (var j = 0; j < theTypeIs.length; j++) 
                {
                    
                  

                    if(headers.indexOf(theTypeIs[j])>-1)
                    {
                        console.log("im in if ");
                        console.log(theTypeIs[j]);
                        if(theTypeIs[j]=="GroupAdmin") {
                            console.log("grp admin");
                            sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].groupAdmin);

                        }
                        else if(theTypeIs[j]=="DepartmentAdmin") {
                            console.log("dept admin");
                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1,req.body.metaDataInfo[0].departmentAdmin);

                    }else {
                        console.log("im in else");
                        console.log(headers.indexOf(theTypeIs[j])+1);
                    

                        sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);

                    }
                        

                    }

                   // else if(headers.indexOf(theTypeIs[j])==-1) {
                        
                  //  }
                    
                    
                }                   
            count=count+1
            }
            console.log("workbook");
           // console.log(workbook);
            workbook.save(function (err) {
                if (err)
                  //  console.log(err);
                return err;
                else
                console.log('congratulations, your workbook created');
                var buffer = fs.readFileSync("./" + mongoid + ".xlsx", {
                        encoding : 'base64'
                    });
                /*
                    @dexcription" Modification done for allow multiple reciepantes while mailing
                    @modified Date: 03-03-18
                    @developer: Umamaheswari B
                    */
                //--> TPCL Customizations: Uma: Modified: To handle Multiple mail receipents under CC
                sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail, function(callbackResponse){
                    if(callbackResponse==true) {
                         res.json({
                    "message" : "Mail sent successfully",
                    "status" : 200
                });

                    }
                    else {
                        console.log("entered else");
                     res.json({"message":"Failed to send mail","status":500});

                    }
                });    
               // sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4);
                //<-- End
               
            });
        }
        
    }); 
});

function getFormname(id, callback) {
    Formsz.find({_id : {$in : id}}, function (err, data) {
        callback(data);
    });
}

/*function prepopdatajson(id, taskid, user, callback) {
    var data = [];
    var taskdata = {}
    var reqfields = []
    var DisplayValues = [];
    var DisplayValuesData = {};
    var records = [];
    var recordsdata = {};
    var i = 0;
    Formsz.find({_id : id}, function (err, post1) {
        if (post1.length > 0) {
            if (post1[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                data.push(taskdata);
                taskdata = {};
                callback(data);

            } else {
                reqfields = Object.keys(post1[0].requiredField[0]);

                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;

            //  FormszDetails.find({formId : id,taskId : taskid,DSAssignedTo : user}, function (err, post3) {
                 FormszDetails.find({
                    $or: [{
                        formId: id,
                        taskId: taskid,
                        DSAssignedTo: user
                    }, {
                        formId: id,
                        taskId: taskid,
                        updatedBy: user,
                        IsReassign: true
                    }]
                }, function(err, post3) {
                    if (post3.length > 0) {
                        post3.forEach(function (recorddata) {
                            DisplayValuesData.recordId = recorddata._id
                                for (var i = 0; i < reqfields.length; i++) {
                                    recordsdata.fieldId = reqfields[i];
                                    recordsdata.fieldIdName = recorddata.record[0][reqfields[i]];
                                    records.push(recordsdata);
                                    recordsdata = {};
                                }
                                setTimeout(function () {}, 1000);
                            DisplayValuesData.record = records
                                records = [];
                            DisplayValues.push(DisplayValuesData);
                            taskdata.DisplayValues = DisplayValues;
                            DisplayValuesData = {}
                        });
                        data.push(taskdata);
                        taskdata = {};
                        callback(data);
                    } else {
                        data.push(taskdata);
                        taskdata.DisplayValues = DisplayValues;
                        callback(data);
                    }
                });
            }
        }
    });
};*/
function prepopdatajson(id, taskid, user, callback) {
    var data = [];
    var taskdata = {}
    var reqfields = []
    var DisplayValues = [];
    var DisplayValuesData = {};
    var records = [];
    var recordsdata = {};
    var i = 0;
    Formsz.find({
        _id: id
    }, function(err, post1) {
        if (post1.length > 0) {
            if (post1[0].requiredField[0] == null) {
                reqfields = [];
                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                data.push(taskdata);
                taskdata = {};
                callback(data);

            } else {
                reqfields = Object.keys(post1[0].requiredField[0]);

                taskdata.formId = post1[0]._id;
                taskdata.FormName = post1[0].name;
                taskdata.isAllowMap = post1[0].isAllowMap;
                
                FormszDetails.find({
                    $or: [{
                        formId: id,
                        taskId: taskid,
                        DSAssignedTo: user
                    }, {
                        formId: id,
                        taskId: taskid,
                        updatedBy: user,
                        IsReassign: true
                    }]
                }, function(err, post3) {

               
                    if (post3.length > 0) {
                        post3.forEach(function(recorddata) {
                            DisplayValuesData.recordId = recorddata._id,
                        
                                DisplayValuesData.status = recorddata.status,
                                DisplayValuesData.IsReassign = recorddata.IsReassign,
                                DisplayValuesData.comments = recorddata.comments
                            
                            for (var i = 0; i < reqfields.length; i++) {
                                recordsdata.fieldId = reqfields[i];
                                recordsdata.fieldIdName = recorddata.record[0][reqfields[i]];
                                records.push(recordsdata);
                                recordsdata = {};
                            }
                            setTimeout(function() {}, 1000);
                            DisplayValuesData.record = records
                            records = [];
                            DisplayValues.push(DisplayValuesData);
                            taskdata.DisplayValues = DisplayValues;
                            DisplayValuesData = {}
                        });
                        data.push(taskdata);
                        taskdata = {};
                        callback(data);
                    } else {
                        data.push(taskdata);
                        taskdata.DisplayValues = DisplayValues;
                        callback(data);
                    }
                });
            }
        }
    });
};

//bacporting from Ue
router.get('/DashboardStatistics/:user', function(req, res, next) {
    var resultJson = {};
    Tasks.distinct("_id", {
        'assignedUsers.userName': req.params.user,
        isClosed: false
    }, function(err, taskObj) {
        resultJson.taskPending = taskObj.length > 0 ? taskObj.length : 0;
        Tasks.distinct("_id", {
            'assignedUsers.userName': req.params.user,
            isClosed: true
        }, function(err, taskComp) {
            resultJson.taskComp = taskComp.length > 0 ? taskComp.length : 0;
            FormszDetails.find({
                taskId: {
                    $ne: null
                },
                status: true,
                updatedBy: req.params.user
            }, function(err, cardedAddress) {
                resultJson.cardedAddress = cardedAddress.length > 0 ? cardedAddress.length : 0;
                
                    FormszDetails.find({
                $or: [{
                    taskId: {
                        $ne: null
                    },
                    status: false,
                    DSAssignedTo: req.params.user
                }, {taskId: {
                        $ne: null
                    },
                    status: false,
                    updatedBy: req.params.user}]
                }, function(err, noncardedAddress) {

                    resultJson.noncardedAddress = noncardedAddress.length > 0 ? noncardedAddress.length : 0;
                    res.json({
                        data: resultJson,
                        status: 200
                    });
                });
            });
        });
    });
});
//<--

// notifications service
//--> UE Implementation : 11-07-2017
//Addition: Roja 
router.get('/getNotifications/:user', function(req, res, next) {
    notificationmodel.find({
        user: req.params.user
    }).sort({createdTime:-1}).exec(
    function(err, resultObj) {
        if (resultObj.length > 0) {
            res.json({
                data: resultObj,
                "status": 200
            });
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    }
    ) 
    //);

});
//<--
// for notification service change for mobile
//--> UE Implementation : 12-07-2017
//Addition: Roja 
router.put('/notificationChange/:id', function(req, res, next) {
    notificationmodel.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, notification) {
        res.json({
            "message": "Updated successfully",
            "status": 200
        })
    })

});


router.post('/task/download', function (req, res, next) {
 var headers=[];
 var headerslbl=[];
 var formname ="";

Formsz.find({_id:req.body.formid},function(err,post1)
 {
    if(post1.length>0)
  {
    formname = post1[0].name;
   post1[0].FormSkeleton.forEach(function (recorddata) {
     if(recorddata.type.view=="section" )
    {
     var data =recorddata.type.fields
     if(data.length>0)
     {
      for (var i=0;i<data.length;i++)
      {
       if(data[i].data.type.view == 'group') {
        var group = data[i].data.type.fields
        for (var j=0;j < group.length;j++) {
        //console.log(group[j])   
        headers.push(group[j].id);
        headerslbl.push(group[j].lable)
        }
       }
       else {
        headers.push(data[i].data.id);
        headerslbl.push(data[i].data.lable);
       }
       
      }
      
     }
     
     
    }
     else if(recorddata.type.view=="group" )
    {
     var data =recorddata.type.fields
     if(data.length>0)
     {
      for (var i=0;i<data.length;i++)
      {
       headers.push(recorddata.type.fields[i].id);
       headerslbl.push(recorddata.type.fields[i].lable)
      }
      
     }
     
    } 
    else
    {
    headers.push(recorddata.id);
    headerslbl.push(recorddata.lable)
    }
    
   });
  } 
 })
 var updatedby = req.body.user;
 if (updatedby == "All") {
  updatedby = {
   $ne : null
  }
 }
 var emailids=[]
 emailids=req.body.records;
 FormszDetails.find({taskId:req.body.taskid,formId : req.body.formid,_id:{$in:emailids},isDeleted : false,updatedTime : {$gte : new Date(req.body.fromdate),$lte : new Date(new Date(req.body.todate).setDate(new Date(req.body.todate).getDate() + 1))},updatedBy : updatedby}, function (err, post) {
  if (post.length > 0) {
   fileid = mongoid;
   var workbook = excelbuilder.createWorkbook('./', formname + ".xlsx");
   var sheet1 = workbook.createSheet('sheet1', 100, 10000);
   var k = 1;
   var j = 1;
   var count=1
   for (var i = 0; i < post.length; i++) {
       
    for (var j = 0; j < headerslbl.length; j++) 
    {
     if (i == 0) {
      sheet1.set(j + 1, k, headerslbl[j]);
     }
    }
    var data={}
    var data=post[i].record[0];
    var theTypeIs = Object.keys(post[i].record[0]);
      for (var j = 0; j < theTypeIs.length; j++) 
    {
     if(headers.indexOf(theTypeIs[j])>-1)
     {
      sheet1.set(headers.indexOf(theTypeIs[j])+1,count+1, data[theTypeIs[j]]);
     }
     
     
    }      
   count=count+1
   }
   workbook.save(function (err) {
    if (err)
     console.log(err);
    else
    console.log('congratulations, your workbook created');
     var buffer = fs.readFileSync("./" + formname + ".xlsx", {
      encoding : 'base64'
     }); 
    //sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx", req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4, function(mailresponse) {
      /*  sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement. ", "Formsz Records", "./" + mongoid + ".xlsx", req.body.altemail){
        if(mailresponse==true) {
                                    fs.unlink("./"+ formname +".xlsx",function(err){
                                    if(err) return console.log(err);
                                        }); 
                                        res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }

    });*/
                   

//     sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx" ,req.body.altemail,req.body.altemail1,req.body.altemail2,req.body.altemail3,req.body.altemail4, function(mailresponse) {
         sendAutomailwithAttachment(req.body.mailid, "Dear User, Please find requested records in the attachement.<br><br>Thanks,<br>DFormsTeam ", "Formsz Records", "./" + formname + ".xlsx" ,req.body.altemail, function(mailresponse) {

    //console.log("2144444444444444444444")

        if(mailresponse==true) {
                                    fs.unlink("./"+ formname +".xlsx",function(err){
                                    if(err) return console.log(err);
                                        }); 
                                        res.json({
                                             "message" : "Mail sent successfully",
                                             "status" : 200
                                            }); 
                                }
                                 else {
                                            res.json({"message":"Failed to send mail","status":500});
                                        }

    });
    res.json({
     "message" : "Mail sent successfully",
     "status" : 200
    });
   });
  }
  
 }); 
});

router.post('/syncMultipleRecords', function(req,res,next) {
    var data =[];
    var updateFormArray =[];
    var insertFormArray =[];
    var formIdArray=[];

  async.forEachSeries(req.body, function(singleTaskForm,next) {
    var response ={};
   
    multipleUpdateServiceHit(singleTaskForm.formId,singleTaskForm.updateArray,function(value,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
     if(value == true) {
        if(singleTaskForm.insertArray.length==0) {
        /*   var singleFormObject = {};
            singleFormObject.updateFormArray = updateJobArray;
            singleFormObject.updateFormIdArray=offlineJobIdArray;*/

           // updateFormArray.push(singleFormObject);
          response = {"updatedFormArray":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray};
            data.push(response);
           response ={};
            next();
          // res.json({"message":"sync successfull","updateJobArray":updatejobArray,"offlineJobIdArray":offlineJobIdArray,"status":200});
        }
        else {
         multipleCreateServiceHit(singleTaskForm.formId,singleTaskForm.insertArray,function(serviceHitValue,insertJobArray) {
            
              if(serviceHitValue== true) {
                /*   var singleFormObject = {};
                singleFormObject.updateFormArray = updateJobArray;
                singleFormObject.updateFormIdArray=offlineJobIdArray;
                singleFormObject.insertFormArray = insertJobArray
                updateFormArray.push(singleFormObject);*/
                response = {"updatedFormArray":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedFormArray":insertJobArray};

               //  response = {"updatedFormArray":updatejobArray,"insertedFormArray":insertJobArray,"offlineFormIdArray":offlineJobIdArray};
                data.push(response);
                response ={};
                next();
             // res.json({"message":"sync successfull","updateJobArray":updatejobArray,"insertJobArray":insertJobArray,"offlineJobIdArray":offlineJobIdArray,"status":200});
                }
                else {
                   // res.json({"message":"failed","status":204});
                   data.push({"message":"failed","status":204});
                }
              });


        }
             

      }  
    });

 //   }
 

   }, function(err) {
    res.json({data,status:200,"message":"Sync successfull"});
   })
  
});
/*function multipleUpdateServiceHit(formId,updateArray,callback) {
      
    Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, post1) {
        if(post1.length>0) {
              var updateArrayData = [];
        var offlineFormIdArray=[];
        var offlineTaskIdArray=[];
         
        
             async.forEachSeries(updateArray,function(singleUpdate,next) {
            singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;
  
                              FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
                                   updateArrayData.push(post._id);
                                    FormszDetails.find({formId:formId,DSAssignedTo:{$ne:null},taskId:post.taskId}, function(err,norecordsAvailable) {
                                        if(norecordsAvailable.length==0) {
                                            console.log("entered here");
                                            offlineFormIdArray.push(formId);
                                            FormszDetails.find({taskId:post.taskId,status:false}, function(err,taskIds) {
                                                if(taskIds.length==0) {
                                                     Tasks.update({
                                                    _id: post.taskId
                                                }, {
                                                    $set: {
                                                        isClosed: true
                                                    }
                                                }, function(err, a) {
                                                    if(err) {
                                                        console.log(err);
                                                    }
                                                    else {
                                                        console.log("true");
                                                        offlineTaskIdArray.push(taskIds);
                                                        next();


                                                    }
                                                });

                                                }
                                           


                                            })
                                        }
                                        else {
                                            next();
                                        }
                                    })

               

            });
       
     
            
            
                    
            

    }, function(err) {
        console.log("2026");
        console.log(updateArrayData);
        console.log(offlineFormIdArray);
       callback(true,updateArrayData,offlineFormIdArray,offlineTaskIdArray);
    })

        }
        else {
                   console.log("failed as form doesnt exist");

        }
    });
  

}*/
function multipleCreateServiceHit(formId,insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    console.log("failed as form doesnt exist");
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

router.post('/syncMultipleRecordsForMultipleTasks', function(req,res,next) {
     var updateJobArray =[];
    var offlineJobIdArray =[];
    var updatejobArray =[];
    var formIdArray =[];

async.forEachSeries(req.body,function(taskObject,next1) {

   
    Tasks.find({_id:taskObject.taskId}, function(err,task) {

            multipleUpdateServiceHitForTasks(task[0]._id,taskObject.updateArray,function(value,updateAddressesArray,offlineJobIdArray,formIdArray) {
     if(value == true) {

        if( taskObject.insertArray==undefined) {
         var updateJobObject ={};

            updateJobObject.updateAddressesArray = updateAddressesArray;
            updateJobObject.offlineJobIdArray =offlineJobIdArray;
            updateJobObject.formIdArray =formIdArray;
            updateJobArray.push(updateJobObject);
            next1();
        }
        else {
             var updateJobObject ={};

            updateJobObject.updateAddressesArray = updateAddressesArray;
            updateJobObject.offlineJobIdArray =offlineJobIdArray;
            updateJobObject.formIdArray =formIdArray;
           
         multipleCreateServiceHitForTasks(task[0]._id,taskObject.insertArray,function(serviceHitValue,insertJobArray) {
                if(serviceHitValue== true) {
                    updateJobObject.insertAddresses = insertJobArray;
                    updateJobArray.push(updateJobObject);
                    next1();
                }
                else {
                    console.log("something failed");
                    next1();
                }
              });


        }
             

      }  
    });

    })

}, function(err) {
    res.json({"message":"Sync Successfull", data:updateJobArray,status:200 })


})
    
   

});

function multipleUpdateServiceHitForTasks(taskId,updateArray,callback) {

         var updateArrayData = [];
        var offlineJobIdArray=[];
        var formIdArray = [];

             async.forEachSeries(updateArray,function(singleUpdate,next) {
            
            singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;

            FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
                if(err) {
                    return err;
                }
                else {
                    updateArrayData.push(post._id);
                    FormszDetails.find({formId:post.formId,taskId:post.taskId,DSAssignedTo:{$ne:null},status:false}, function(err,recordsWithForm) {
                        if(recordsWithForm.length==0) {
                              formIdArray.push(post.formId);
                              FormszDetails.find({taskId:taskId,DSAssignedTo:{$ne:null},status:false}, function(err,taskIdArray) {
                                if(taskIdArray.length==0) {
                                    offlineJobIdArray.push(taskId);
                                    next();
                                }
                                else {
                                    next();
                                }
                              })


                        }
                        else {
                            next();

                        }

                    })
                }


            });


    }, function(err) {

       callback(true,updateArrayData,offlineJobIdArray,formIdArray);
    })

  

}
function multipleCreateServiceHitForTasks(taskId,insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: singleInsert.formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   /* if(singleInsert.record[0]["Dropdown__name_161930"].trim().toLowerCase()=="yes"){
                            console.log("if");
                            singleInsert.markerStyleCode =4;
                        }
                        else {
                            singleInsert.markerStyleCode =2;
                        }*/
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

router.post('/syncmultipleFormRecords', function(req,res,next){
    multipleUpdateServiceHit(req.body.updateArray,function(status,updatedFormIdList,updatedPreppopList,updatedtaskIdList) {
        if(status==true) {
            //updateArrayData,offlineFormIdArray,offlineTaskIdArray
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    res.json({updatedFormIdList:updatedPreppopList,updatedPreppopList:updatedFormIdList,updatedtaskIdList:updatedtaskIdList,insertArrayList:insertArrayList})
                }
                if(status==false) {
                    console.log("entered false");
                }
            })
        }
        else {
            console.log("entered update service else");
        }

    })
});

/*router.post('/syncmultipleFormRecords', function(req,res,next){
    if(req.body.updateArray.length>0) {
        multipleUpdateServiceHit(req.body.updateArray,function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
        //updateArrayData,offlineFormIdArray,offlineTaskIdArray
        if(status==true) {
            console.log(updatedFormIdList);
            console.log(updatedPreppopList);
            console.log(updatedtaskIdList);
            if(req.body.insertArray.length>0) {
                 multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    console.log(insertArrayList);
                    res.json({updatedFormIdList:offlineFormIdArray,updatedPreppopList:updateArrayData,updatedtaskIdList:offlineTaskIdArray,insertArrayList:insertArrayList,"message":"Sync Successfully done"});
                }
                
            })

            }
            else {
                res.json({updatedFormIdList:offlineFormIdArray,updatedPreppopList:updateArrayData,updatedtaskIdList:offlineTaskIdArray,"message":"Sync Successfully done"});


            }
           
        }
        else {
            console.log("entered update service else");
        }

    })

    }
    else {
          multipleCreateServiceHitService(req.body.insertArray, function(status,insertArrayList) {
                if(status==true) {
                    console.log(insertArrayList);
                    res.json({insertArrayList:insertArrayList,"message":"Sync Successfully done"});
                }
                
            })

    }
    
});*/
function multipleUpdateServiceHit(updateArray,callback) {
      
   /* Formsz.find({
        _id: formId,
        isVisible: true
    }, function(err, post1) {*/
     //   if(post1.length>0) {
        var updateArrayData = [];
        var offlineFormIdArray=[];
        var offlineTaskIdArray=[];
         
        
             async.forEachSeries(updateArray,function(singleUpdate,next) {
                Formsz.find({
                _id: singleUpdate.formId,
                isVisible: true
            }, function(err, post1) {
                 singleUpdate.status = true;
            singleUpdate.DSAssignedTo = null;
            FormszDetails.findByIdAndUpdate(singleUpdate._id, singleUpdate,{new:true}, function(err, post) {
            updateArrayData.push(post._id);
                FormszDetails.find({formId:singleUpdate.formId,DSAssignedTo:{$ne:null},taskId:post.taskId}, function(err,norecordsAvailable) {
                    if(norecordsAvailable.length==0) {
                         offlineFormIdArray.push(singleUpdate.formId);
                        FormszDetails.find({taskId:post.taskId,status:false}, function(err,taskIds) {
                        if(taskIds.length==0) {
                        Tasks.update({
                        _id: singleUpdate.taskId
                            }, {
                             $set: {
                                    isClosed: true
                                    }
                                }, function(err, a) {
                                    if(err) {
                                        console.log(err);
                                    }
                                    else {
                                        offlineTaskIdArray.push(singleUpdate.taskId);
                                        next();


                                    }
                                });

                                }
                                else {
                                    console.log("task cannot be updated");
                                    next();
                                }
                                           


                                            })
                                        }
                                        else {
                                            next();
                                        }
                                    })

               

            });
       
     

            });

           
            
            
                    
            

    }, function(err) {
       callback(true,updateArrayData,offlineFormIdArray,offlineTaskIdArray);
    })

        /*}
        else {
                   console.log("failed as form doesnt exist");

        }*/
 //   });
  

}

function multipleCreateServiceHitService(insertArray,callback) {
             var insertArrayRecord = [];

        async.forEachSeries(insertArray,function(singleInsert,next) {
         var displayrecords = [];
            var displaydata = {};
            var data = [];

            Formsz.find({
                _id: singleInsert.formId,
                isVisible: true
            }, function(err, formObj) {
                if(formObj.length>0) {
              displayrecords = Object.keys(formObj[0].requiredField[0]);
                    singleInsert.status = true;
                   
                     FormszDetails.create(singleInsert, function(err, post) {
                     insertArrayRecord.push(post.sqliteDBId);
                    for (var i = 0; i < displayrecords.length; i++) {
                        displaydata.filedId = displayrecords[i]
                        displaydata.filedValue = post.record[0][displayrecords[i]] || "";
                        data.push(displaydata);
                        displaydata = {};
                    }
                    var displayJosn = new DisplayValues({
                        formId: post.formId,
                        recordId: post._id,
                        displayValues: data
                    });
                    displayJosn.save(function(err, a) {
                        if (err) throw err;
                        else {
                            next();
                        }
                    });
                    
                    
            });


                }
                else {
                    console.log("failed as form doesnt exist");
                    next();

                }


    })
}, function(err) {
  // var status = true;
  callback(true,insertArrayRecord);
})
}

//Task Sync Multiple records
/*router.post('/syncMultipleRecordsForMultipleTasksDownloaded', function(req,res,next) {
    if(req.body.updateArray.length>0) {

multipleUpdateServiceHit(req.body.updateArray, function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
    if(status==true) {
        if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"message":"Sync successfully"});
        }
        

    }

})        
    }
    else {
         if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            console.log("insert array missing");
        }

    }

});*/

router.post('/syncMultipleRecordsForMultipleTasksDownloaded', function(req,res,next) {
    console.log(req.body);

multipleUpdateServiceHit(req.body.updateArray, function(status,updateArrayData,offlineFormIdArray,offlineTaskIdArray) {
    if(status==true) {
        if(req.body.insertArray.length>0) {
            multipleCreateServiceHitService(req.body.insertArray, function(status,insertedArray) {
            if(status==true) {
                res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"insertedArray":insertedArray,"message":"Sync successfully done"});

            }
        })

        }
        else {
            res.json({"updatedPreppopList":updateArrayData,"offlineFormIdArray":offlineFormIdArray,"offlineTaskIdArray":offlineTaskIdArray,"message":"Sync successfully"});
        }
        

    }

})        
   

});

module.exports = router;
