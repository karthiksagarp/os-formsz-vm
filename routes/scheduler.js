var express = require('express');
var formidable = require('formidable');
var mongoose = require('mongoose');
var fs = require('fs');
var FormszDetails = require('../models/formszDetails.js');
var Project = require('../models/projectModel.js');
var Tasks = require('../models/formszTasks.js');
var Projecttasks = require('../models/projectTaksAssignMents.js');
var projectTaskModel = require('../models/projectTasksModel.js')
var mkdirp = require('mkdirp');
var router = express.Router();
var async = require('async');
var Formsz = require('../models/formsz.js');
var pdf = require('html-pdf');
var autoExportPDFScheduler = require('../models/autoexportpdfScheduler.js');
var fs = require('fs');

autoExportPDFSchedulerFunction();


function autoExportPDFSchedulerFunction() {
    autoExportPDFScheduler.find({}, function(err,autoExportTime) {
      
        if(autoExportTime.length>0) {
            console.log(autoExportTime[0].LastPDFExportTime);
            var DBdate = autoExportTime[0].LastPDFExportTime;
            var currentdate = new Date().toISOString();
        
           var dif = Date.parse(currentdate) - Date.parse(DBdate);
          
                var Seconds_from_T1_to_T2 = dif / 1000;
                 var Seconds_Between_Dates = Math.abs(Seconds_from_T1_to_T2);
            
               //Scheduler code
                setInterval(function() {
                    console.log("Scheduler started");
                   writeFile("\t  Auto Export PDF scheduler started at-- " + new Date().toISOString()+"\t");

                        getprojectRelatedTasks();
                       // taskPDFScheduler();
                    }, Seconds_Between_Dates*1000);
                        }
                    })

}



router.post('/test', function(req,res,next) {
    getprojectRelatedTasks();
})

function getprojectRelatedTasks() {
    var count =0;
    Project.find({}, function(err,projects) {
          writeFile("\n  Total Projects fetched from DB at "+new Date().toISOString() +"-- " + projects.length+"\n");

        if(projects.length>0) {
            async.forEachSeries(projects, function(project,next) {
                projectTaskModel.find({projectID:project._id},{name:1,projectID:1}, function(err,projectTaskNames) {
                    async.forEachSeries(projectTaskNames, function(projectTaskName,nextProjectTaskname) {
                        Projecttasks.find({projectID:projectTaskName.projectID},{taskId:1,formName:1,projectId:1,users:1,formId:1}, function(err,projectTasks) {
                    if(projectTasks.length>0) {
                         writeFile("\n No of tasks fetched for "+projectTaskName.name+" are -- " + projectTasks.length+"\n");

                        async.forEachSeries(projectTasks,function(projectTask,nextprojectTask){
                             writeFile("\n Names of users for project name "+projectTaskName.name+" are -- " + projectTask.users+ "\n");

                            async.forEachSeries(projectTask.users,function(user,nextUser) {
                              

                                var month =new Date().getMonth()+ 1;
                               var currentTimeStamp = new Date().getDate() +"-"+ month +"-"+new Date().getFullYear();

                              FormszDetails.distinct("_id",{updatedBy:user,taskId:projectTask.taskId,status:true,formId:projectTask.formId,updatedTime : {$lte : new Date(),$gte : new Date(new Date().setDate(new Date().getDate() - 1))}}, function(err,userRecords) {
                                    if(userRecords.length>0) {
                                     writeFile("\n Records submitted by User "+user+" for project name "+projectTaskName.name+" on "+currentTimeStamp+" are -- " + userRecords+"\n");

                                        //var path = 'D:/02022018/ProjectTasks/'+project.name+"-"+projectTask.name+"-"+user;
                                       var path = 'D:/02022018/ProjectTasks/'+currentTimeStamp+'/';
                                        //var path = '//192.168.110.17/Releases/PDFS/'+currentTimeStamp+'/';
                                        mkdirp(path);
                                        PDFGeneration(path,projectTask.formName,projectTask.taskId,projectTask.formId,userRecords,projectTaskName.name,project.name,user,function(pdfCreated) {
                                            if(pdfCreated== true) {
                                               nextUser(); 
                                            }
                                            else {
                                                nextUser();
                                            }
                                        });

                                    }
                                    else {
                                     writeFile("\n Records submitted by User "+user+" for project name "+projectTaskName.name+" on "+currentTimeStamp+" are -- " + userRecords.length+"\n");

                                        nextUser();
                                    }


                            })
                                
                            }, function(err) {

                                nextprojectTask();
                            })
                        }, function(err) {
                           nextProjectTaskname();
                        })


                    }
                    else {
                        console.log("im in else");
                       nextProjectTaskname();
                    }
                })

                    }, function(err) {
                       next();
                    })

                }, function(err) {
                    next();
                })
                


            }, function(err) {
                
                console.log("completed all projects");
                 writeFile("\n Scheduler completed at "+new Date().toISOString()+"\n");

                autoExportPDFScheduler.find({}, function(err,LastPDFExportTime) {
                if(LastPDFExportTime.length>0) {
                autoExportPDFScheduler.update({_id:LastPDFExportTime[0]._id},{$set:{LastPDFExportTime:new Date(new Date().setDate(new Date().getDate() + 1))}}, function(err,newTime) {
                if(err) {
                    return err;
                }
                else {
                    console.log("updated");
                    writeFile("\n Modified time in DB "+new Date().toISOString()+"\n");

                }
            })
            }
        })
            })
        }
        else {
            console.log("no projects available");
        }
    })
}

function PDFGeneration(path,formname,taskId,formId,recordIds,taskname,projectname,username,callback) {
    console.log(formId);
     var headers = [];
    var headerslbl = [];
    Formsz.find({_id:formId},function(err,post1){
        if(post1.length>0){
            try {
                post1[0].FormSkeleton.forEach(function (recorddata) { 
                    if(recorddata.type.view=="section" ){
                    headers.push(recorddata.lable)
                    headerslbl.push({"SN": recorddata.lable})
                    var data =recorddata.type.fields
                    //-->Siva Kella:Added
                    var tempObj = {};
                    //<-- End
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            if(data[i].data.type.view == 'group') {
                                headers.push(data[i].data.lable)
                                headerslbl.push({"SN": recorddata.lable , "GN": data[i].data.lable})
                                var group = data[i].data.type.fields
                                for (var j=0;j < group.length;j++) {
                                    headers.push(group[j].id);
                                    headerslbl.push({"SN": recorddata.lable , "GN": data[i].data.lable, "FN": group[j].lable})
                                }
                            }
                            else {
                                headers.push(data[i].data.id);                                                         
                                headerslbl.push({"SN": recorddata.lable , "FN": data[i].data.lable}) 
                            }
                        }
                    }  
                }
                 else if(recorddata.type.view=="group"){
                    headers.push(recorddata.lable);
                    headerslbl.push({"GN":recorddata.lable})
                    var data =recorddata.type.fields
                    if(data.length>0){
                        for (var i=0;i<data.length;i++){
                            headers.push(recorddata.type.fields[i].id);
                            headerslbl.push({"GN":recorddata.lable, "FN": recorddata.type.fields[i].lable})
                        }
                    }
                } 
                else{
                    headers.push(recorddata.id);
                    headerslbl.push({"FN": recorddata.lable})
                }

                });
               


            } catch(e) {
                 writeFile("\n PDF creation Failed for form"+ formname +" due to skelton issue \n");


            }
            var recordids = [];
            recordids = recordIds;
            FormszDetails.find({_id:{$in:recordids},isDeleted:false}, function(err,records) { 
                 var html = "<html><head><body><caption>RECORDS</caption>";
                html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'> "  ;
                html = html + "<tr style='border: 3px solid'> <td> <img height='100;' width='100;' src='file:///D:/11-01-2018/12032018/13032018/TPCLDynamicFormsz/TPCLServer/images/mainpage_logo.png' alt=''></td>" ;
                html = html + "<td>" + "<h1 style='text-align: center;'>" + post1[0].name+ "</td>" + "</tr></table>" ;
                 
                if(records.length>0) {
                    //console.log("records");
                  //  console.log(records);
                    var count = 0;
                    async.forEachSeries(records,function(data,next) {
                        var objectArray = [];
                        var object = {};
                        var date = data.updatedTime;
                        date = date.toUTCString();
                      //  var objectArray=[];
                        try {
                          //  var object ={};
                            for(var k=0, i=0;k<headers.length,i<headerslbl.length;k++,i++) {
                            if(headers[k].includes('timestamp')==true) {
                                object.key = headerslbl[i];
                                object.value = date;
                                objectArray.push(object);
                               
                                object ={};
                            }
                            else {
                                object.key = headerslbl[i];
                                if(data.record[0][headers[k]]==undefined) {
                                    object.value ="";
                                }
                                else {
                                     object.value = data.record[0][headers[k]];
   
                                }
                                objectArray.push(object);

                                object ={};
                            }
                        }  
                         html = html + "<table border='1px solid black' style='width:100%;border-collapse :collapse'>"

                        

                         count = count+1;
                        html = html + "<tr><td>" + count + "." + "<br>Submitted Time : "+ date  +"<br></td>" + "<td>" + "<br>Submitted By :"+ data.updatedBy +"<br></td></tr>"
                        var disKey = '';
                        var tableStart = false;
                        var value = '';
                         for(var j = 0; j<objectArray.length;j++) {

                                    if (objectArray[j].key.SN != undefined && objectArray[j].key.GN == undefined && objectArray[j].key.FN == undefined) { 
                                        // SECTION CASE
                                        disKey = objectArray[j].key.SN; 
                                        // html = html + "<tr><th colspan='3' style='background-color:lightblue; float: left' > "+ disKey + "</th></tr>";  
                                        // tableStart = true ;

                                    }
                                    else if ( objectArray[j].key.SN != undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN == undefined) {
                                        // SECTION With Only GROUP
                                         disKey =  objectArray[j].key.GN; 
                                        //html = html + "<tr><th colspan='3' style='background-color:gray; float: left' > "+ disKey + "</th></tr>";   

                                        
                                    }
                                    else if (objectArray[j].key.SN != undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN != undefined) {
                                         // SECTION With GROUP --> FIELD
                                         disKey =  objectArray[j].key.FN; 

                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }

                                    }
                                    else if (objectArray[j].key.SN == undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN == undefined) {
                                         // GROUP Case
                                         disKey =  objectArray[j].key.GN; 
                                         // html = html + "<table><tr><th colspan='3' style='background-color: gray;float: left'> "+ disKey + "</th></tr>";  
                                     }
                                     else if (objectArray[j].key.SN == undefined && objectArray[j].key.GN != undefined && objectArray[j].key.FN != undefined) {
                                         // GROUP --> FIELD
                                         disKey =  objectArray[j].key.FN;

                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }
                                     }
                                     else {
                                        // FIELD Case
                                         disKey =  objectArray[j].key.FN;
                                         
                                         // value = JSON.stringify(objectArray[j].value);
                                         // if(value != undefined && value.indexOf('base64') != -1){
                                         //    html = html +"<tr><td >"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                         // }
                                         // else {
                                         //    html = html + "<tr> <td>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                         // }
                                     }



                                if(objectArray[j].value == undefined) {
                                    //console.log("UNDEFINED--->>>>",objectArray[j].key.SN, objectArray[j].key.GN,objectArray[j].key.FN);
                                    
                                    //console.log(disKey);
                                    html = html + "<tr style='background-color:gray' ><td colspan='2' > "+ disKey + "</td></tr>";
                                } 
                                else {
                                    var value = JSON.stringify(objectArray[j].value);
                                    //console.log("NORMAL ***>>>>",objectArray[j].key.SN, objectArray[j].key.GN,objectArray[j].key.FN);
                                    
                                    
                                    

                                    
                                    //console.log(disKey);
                                    if(value.indexOf('base64') != -1){
                                        html = html +"<tr><td style='250px;' word-wrap: break-word;>"+disKey +"</td><td colspan='2'><img src = "+ value +"style='width:250px;height:250px';</td></tr>"
                                    }
                                    else {
                                        html = html + "<tr> <td style='250px;' word-wrap: break-word;>"+ disKey + "</td><td colspan='2'>" + objectArray[j].value + "</td></tr>";
                                    }
                                }
                            }

                        
                            html = html + "</table><br>";
                          
                            next();

                        }
                        catch(e) {
                            console.log("exception occured" + e);

                        }

                         }, function(err) {
                             html = html + "</head></body></html>";
                        var options = {timeout: 60000};
                        pdf.create(html,options).toFile(path+"/"+projectname+"---"+taskname+"---"+username+"---"+formname +".pdf", function(err, response) {
                            console.log("create pdf");
                        
                              if (err)  {

                                   console.log(err);
                                   writeFile("\n" +  html +"\n");
                                  writeFile("\n PDF creation failed for "+ path+"/"+projectname+"---"+taskname+"---"+username+"---"+formname +"\n");

                                    callback(false);

                              }
                               else if (response) 
                                 {
                                     console.log('congratulations, your pdf created');
                                  writeFile("\n PDF creation success at "+ path+"/"+projectname+"---"+taskname+"---"+username+"---"+formname +"\n");

                             callback(true);

                                 }
                               
                                  
                                    
                            
                                 
                                
                            });
                         });
                }
            })
         }
     });

}

function taskPDFScheduler () {
    Tasks.find({},function(err,tasks) {
        if(tasks.length>0) {
            async.forEachSeries(tasks, function(individualTask,nextTask) {
                async.forEachSeries(individualTask.users, function(taskUser,nextUser) {
                    FormszDetails.distinct ("_id",{updatedBy:taskUser,taskId:taskUser._id,status:true}, function(err,records) {
                        
                    })
                    PDFGeneration(path,formname,taskId,formId,recordIds,callback)

                })

            })
        }
    })

}

function writeFile(text) {
    fs.appendFile(process.cwd() + '/logs/autoexportPdfSchedulerLog.log', text, function(err) {
        if (err) throw err;
    });
}



module.exports = router;
