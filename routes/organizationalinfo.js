
var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var organizationalinfo =  require('../models/organizationalinfo.js');
var fs = require('fs');
var formidable = require('formidable');
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var log = require('./log')(module);

// DUMMY SERVICE TO REGISTER ORGANIZATION INFO.
router.post('/orginfo', function(req, res, next) {
    // Register ORG-INFO.
    var orginfo = new organizationalinfo({
        email: req.body.email,
        organizationName: req.body.organizationName,
        organizationAddress1: req.body.organizationAddress1,
        organizationAddress2: req.body.organizationAddress2 || null,
        organizationContact1: req.body.organizationContact1 || null,
        organizationContact2: req.body.organizationContact2
    });

    orginfo.save(function(err, post) {
        if (err) {
            console.log("Error Occured. Insertion Failed");
        }
        else{
            res.json({
               "message": "Organization Info Registered Successfully",
               "status": 200
            });
        }
    });
});

// GET OrganizationInfo from collection
router.get('/getOrganizationInfo',function(req,res,next){
	organizationalinfo.find({},function(err, orgInfo) {
        if (orgInfo.length > 0) {
            res.json({
                "data": orgInfo,
                "status": 200
            });
        } else {
            res.json({
                "message": "Not Data found",
                "status": 204
            });
        }
    });
});


module.exports = router;
