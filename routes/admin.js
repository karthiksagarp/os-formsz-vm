var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var Users = require('../models/user.js');
var fs = require('fs');
var formidable = require('formidable');
var Store = require('../models/store.js');
var Gruops = require('../models/group.js');
var sendautomail = require('../routes/utils').sendautomail;
var defoultImageurl = require('../routes/utils').defoultimageurl;
var mongoid = mongoose.Types.ObjectId();
var ObjectID = require("bson-objectid");
var Type = require('type-of-is');
var log = require('./log')(module);
var crypto = require('crypto');
var async = require('async');


router.get('/getadminslist/:limit/:offset', function(req, res, next) {
    var limit = parseInt(req.params.limit);
    Users.paginate({
         $or: [{
                    isDeleted: false,
                    type: 1
                }, {
                    isDeleted: false,
                    type: 3
                }]
        /*isDeleted: false,
        type: 1 */
    }, {
        page: req.params.offset,
        limit: limit,
        sort: {
            createdDateTime: -1
        }
    }, function(err, Users) {
        if (err) return next(err);
        res.json(Users);
    });
});


router.get('/getadminslist/:groupname/:Types/:limit/:offset', function(req, res, next) {
  var limit = parseInt(req.params.limit);
   if (req.params.Types == '1') {
        if (req.params.groupname == 'All') {
            Users.paginate({type: 1,isDeleted: false}, {page: req.params.offset,limit: limit, sort: {createdDateTime: -1 } }, function(err, Users) {
                if (err) return next(err);
                res.json(Users);
            });
        }
        else{
            Users.paginate({groupname:{$elemMatch:{_id:{$in:[req.params.groupname]}}},type: 1,isDeleted: false}, {page: req.params.offset,limit: limit, sort: {createdDateTime: -1 } }, function(err, Users) {
                if (err) return next(err);
                res.json(Users);
            });
        }
    }
    if (req.params.Types == '3') {
        if (req.params.groupname == 'All') {
            Users.paginate({type: 3,isDeleted: false}, {page: req.params.offset,limit:limit, sort: {createdDateTime: -1 } }, function(err, Users) {
                if (err) return next(err);
                res.json(Users);
            });
        }
        else{
            Users.paginate({groupname:{$elemMatch:{_id:{$in:[req.params.groupname]}}},type: 3,isDeleted: false}, {page: req.params.offset,limit:limit, sort: {createdDateTime: -1 } }, function(err, Users) {
                if (err) return next(err);
                res.json(Users);
            });
        }
    }
});
/* Get all group admins by renuka*/
router.get('/getgroupadmins',function(req, res, next){
 Users.find({type:3,isDeleted: false},{username:1},function(err,post){
    if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
 });
});

/* Get all UnGrouped Admins */
router.get('/UMadminslist', function(req, res, next) {
    Users.find({
        groupname: [],
        type: 1,
        isDeleted: false
    },{username:1}, function(err, post) {
        //Username parameter added by Renuka on 18-01-2018
        if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});
//Get Mapped Users List
router.get('/mappedadminlist', function(req, res, next) {
    Users.find({
        groupname: null,
        type: 1
    }, function(err, post) {
        if (err) return next(err);
        if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});
/* Create Admins */
router.post('/create', function(req, res, next) {
    mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {

        if (!err) {
            var data = JSON.parse(fields.data);
          
                    var Imgurl = "";
                    if (files.img) {
                        Imgurl = "store/" + mongoid;
                    } else {
                        Imgurl = defoultImageurl;
                    }
                    
                    //if(data.groupname.isArray(data.groupname))
                    //Differentiated for group name in single and multiple seklection
                    //By Uma
                    if(Type.string(data.groupname) == 'Object')
                    {
                        
                       var Users1 = new Users({
                        username: data.username,
                        name: data.name,
                        email: data.email,
                        phone: data.phone,
                        groupname: [data.groupname] || null,
                        imageurl: Imgurl,
                        type: data.type,
                        adminType:data.adminType,
                        /*TPCL Customization*/
                        bloodgroup: data.bloodgroup,
                        adderess2: data.adderess2,
                        adderess1: data.adderess1,
                        activity: data.activity,
                        zone: data.zone,
                        vender: data.vender,
                        
                        //-->Siva Kella: Added:TPCL Customizations
                        //NOTES: To include createdBy and createdByMailID
                        createdBy: data.createdBy,
                        createdByMailID: data.createdByMailID,
                        //<-- End

                        doj: data.doj,
                        dob: data.dob,
                        //adminType:
                        privilage:data.privilage
                        }); 
                    }
                    else
                    {
                       var Users1 = new Users({
                        username: data.username,
                        name: data.name,
                        email: data.email,
                        phone: data.phone,
                        groupname: data.groupname || null,
                        imageurl: Imgurl,
                        type: data.type,
                        adminType:data.adminType,
                        /*TPCL Customization*/
                        bloodgroup: data.bloodgroup,
                        adderess2: data.adderess2,
                        adderess1: data.adderess1,
                        activity: data.activity,
                        zone: data.zone,
                        vender: data.vender,

                        //-->Siva Kella: Added:TPCL Customizations
                        //NOTES: To include createdBy and createdByMailID
                        createdBy: data.createdBy,
                        createdByMailID: data.createdByMailID,
                        //<-- End
                        
                        doj: data.doj,
                        dob: data.dob,
                        //adminType:
                        privilage:data.privilage
                        });  
                    }
                    //Uma changes done
                    //Admin Creation
                    Users1.save(function(err, post) {
                        if (err) {
                           res.json({"message":"Username / Email already exists","status":208});
                        }
                         else 
                         {
                        var password= decrypt(post.password);

                            var obj = {username: post.username,_id:post._id.toString()}
                            //group mapping and group table update By Uma
                            //30-12-17
                            if(post.groupname!=undefined)
                            {
                                post.groupname.forEach(function(groupid)
                                    {
                                        if(post.type == 1)
                                            {
                                                Gruops.find({
                                                //_id: [data.groupname._id],
                                                _id: groupid._id,
                                                isDeleted: false
                                                },{adminlist:1}, function(err, Gruops) {
                                                    Gruops.forEach(function(group){
                                                            if(group.adminlist==null || group.adminlist.length==0){
                                                                group.adminlist.push(obj);
                                                            }
                                                            else{
                                                            group.adminlist.push(obj);
                                                        }
                                                    group.save(function (err, post) {
                                                        });
                                                        });
                                                    });
                                            }
                                        else if(data.type == 3)
                                        {
                                          Gruops.find({
                                            //_id: [data.groupname._id],
                                            _id: groupid._id,
                                            isDeleted: false
                                            },{groupadminlist:1}, function(err, Gruops) {
                                                Gruops.forEach(function(group){
                                                        if(group.groupadminlist==null || group.groupadminlist.length==0){
                                                            group.groupadminlist.push(obj);
                                                        }
                                                        else{
                                                            var flagFormatch = false;
                                                            group.groupadminlist.forEach(function(adminLst){
                                                                if(adminLst._id != obj._id)
                                                                {
                                                                    flagFormatch =true;
                                                                }
                                                            });
                                                            if(flagFormatch == true)
                                                                    group.groupadminlist.push(obj);
                                                        
                                                    }
                                                group.save(function (err, post) {
                                                    });
                                                    });
                                               
                                                });  
                                        }
                                        if(data.groupname)
                                        {
                                            Gruops.find({
                                            _id: data.groupname._id
                                            }, function(err, post1) {
                								
                                                if (post1.length > 0) {
                									
                                                    post1[0].isActive = true;
                                                    post1[0].save(function(err, post2) {
                										if(err) {
                											console.log("err");
                										}
                										else {
                											console.log("group data changed");
                										}
                									});
                                                }
                                            });
                                        }
                                    });
                            }
                         //   var mail = "<body>Dear " + data.name + ",<br><br> You are registered as a administrator  and  your details are below<br><br> <strong>Username:</strong>" + data.username + "<br> <strong>Password :</strong>"+password+"<br><br>Please login with above credentials,using this <a href='http://dforms-gis-dev1.tatapower.com:83/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a><br><br>Thanks,br>DFormsTeam</body>"
                            if(data.type==1) {
                              var mail = "<body>Dear "+data.name+ "<br><br>You are registered as a Department administrator on Tata Power Dynamic-Forms system and your login details are as below<br><br><br>Username:"+data.username+"<br>Password:"+password+"<br><br>Please login with above credentials using this <a href='https://dforms-gis-dev1.tatapower.com/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a> and change the password on first login.<br><br>Thanks,<br>DForms, GIS Team</body>"

                            }
                            else if(data.type==3) {
                             var mail = "<body>Dear "+data.name+ "<br><br>You are registered as a Group administrator on Tata Power Dynamic-Forms system and your login details are as below<br><br><br>Username:"+data.username+"<br>Password:"+password+"<br><br>Please login with above credentials using this <a href='https://dforms-gis-dev1.tatapower.com/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a> and change the password on first login.<br><br>Thanks,<br>DForms, GIS Team</body>"

                            }
                            sendautomail(data.email, mail, "Automatic Reply: Account Confirmation");
                            log.info("Admin:" + data.username + " Created Successfully!");
                            if (files.img) {
                                log.info(files.img.path);
                                var path = files.img.path;
                                var buffer = fs.readFileSync(path);
                                var contentType1 = files.img.contentType;
                                var a = new Store({
                                    fileid: mongoid,
                                    data: buffer,
                                    contentType: contentType1
                                });
                                //Upload Image
                                a.save(function(err, a) {
                                    if (err) throw err;
                                    log.info("Upload Image Successfully!");
                                });

                                Imgurl = "store/" + mongoid;
                            }
                            if(data.type == 1)
                            {
                                res.json({
                                "message": "Department Admin Created Successfully",
                                "status": 200
                                });
                            }
                            else
                            {
                                res.json({
                                "message": "Group Admin Created Successfully",
                                "status": 200
                                });
                            }
                           
                        }
                    });

              //  }
           // });
        }
    });

});
function decrypt(text) {
    var decipher = crypto.createDecipher('aes-256-ctr', 'magikminds');
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}
// Get Admin Details filter by ID
router.get('/:id', function(req, res, next) {
    Users.find({
        _id: req.params.id
    }, function(err, post) {
        if (err) return next(err);
        if (post.length > 0) {
			var data=[];
			var gname={};
			
            Gruops.find({
                /*
                @Description Commented for reteiving all the groups
                @Purpose For showing the mapped/unmapped groups
                @developer Umamaheswari
                */
                isActive: false,
                isDeleted: false
            }, function(err, group) {
                if (group.length > 0) {
                    var UMGrouplist = []
                    group.forEach(function(grouplist) {
                        UMGrouplist.push({"_id":grouplist._id,"name":grouplist.name});
                    });
					setTimeout(function () {
                    res.json({
                        _id: post[0].id,
                        username: post[0].username,
                        name: post[0].name,
                        email: post[0].email,
                        phone: post[0].phone,
                        groupname: post[0].groupname,
                        imageurl: post[0].imageurl,
                        privilage:post[0].privilage,
                        adminType:post[0].adminType,
                        type: post[0].type,
                        /*TPCL Cutomization*/
                        bloodgroup: post[0].bloodgroup,
                        adderess2: post[0].adderess2,
                        adderess1: post[0].adderess1,
                        activity: post[0].activity,
                        zone: post[0].zone,
                        vender: post[0].vender,
                        doj: post[0].doj,
                        dob: post[0].dob,
                        grouplist:UMGrouplist
                    });
					},1000)
                } else {
					setTimeout(function () {
                    res.json({
                        _id: post[0].id,
                        username: post[0].username,
                        name: post[0].name,
                        email: post[0].email,
                        phone: post[0].phone,
                        groupname:post[0].groupname,
                        imageurl: post[0].imageurl,
                        privilage:post[0].privilage,
                        adminType:post[0].adminType,
                        /*TPCL Cutomization*/
                        bloodgroup: post[0].bloodgroup,
                        adderess2: post[0].adderess2,
                        adderess1: post[0].adderess1,
                        activity: post[0].activity,
                        zone: post[0].zone,
                        vender: post[0].vender,
                        doj: post[0].doj,
                        dob: post[0].dob,
                        grouplist: []
                    });
					},1000);
                }
            });
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});
/*  -----------Update Admin details*/
/*router.put('/Update/:id', function(req, res, next) {
    var Imgurl = "";
    mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
    Users.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                if (!err) {
                    var imgurl = "";

                    if (files.img) {
                        console.log(console.log("img not null"))
                        Imgurl = "store/" + mongoid;
                    } else {

                        Imgurl = defoultImageurl;
                    }
                    var data = JSON.parse(fields.data);
                    post.name = data.name;
                    post.email = data.email;
                    post.phone = data.phone;
                    post.adminType = data.adminType;
                    post.privilage = data.privilage;
                    /*TPCL Customization*/
                    /*post.bloodgroup= data.bloodgroup,
                    post.adderess2= data.adderess2,
                    post.adderess1= data.adderess1,
                    post.activity= data.activity,
                    post.zone= data.zone,
                    post.vender= data.vender,
                    post.doj= data.doj,
                    post.dob= data.dob,
                    post.groupname = data.groupname || null;
                    if (files.img) {
                        post.imageurl = Imgurl;
                    }

                    post.type = data.type;
                    post.admingroup = data.admingroup || null;*/
                    //User or Admin Creation
                    /*post.save(function(err, post1) {
                        if (err) {
                            res.status(208);
                        } else {
                            var obj = {username: post1.username,_id:post1._id}
                            //group mapping and group table update By Uma
                            //30-12-17
                            if(data.groupname !=undefined)
                            {
                                data.groupname.forEach(function(groupid){
                                if(data.type == 1)
                                    {
                                        Gruops.find({
                                        //_id: [data.groupname._id],
                                        _id: groupid._id,
                                        isDeleted: false
                                        },{adminlist:1}, function(err, Gruops) {
                                           // Gruops.forEach(function(group){
                                                 //   if(Gruops[0].adminlist==null || Gruops[0].adminlist.length==0){
                                                        Gruops[0].adminlist.push(obj);
                                                   // }
                                                  /*  else{
                                                        console.log("elseeeeee")
                                                    group.adminlist.push(obj);
                                                }*/
                                           // group.save(function (err, post) {
                                            //    });
                                               // });
                                            /*if (Gruops.length > 0) {
                                                Gruops[0].isActive = true;
                                                Gruops[0].save(function(err, post1) {});
                                            }*/
                                         //   });
                                   // }
                              /*  else if(data.type == 3)
                                    {
                                      Gruops.find({
                                        //_id: [data.groupname._id],
                                        _id: groupid._id,
                                        isDeleted: false
                                        },{groupadminlist:1}, function(err, Gruops) {
                                            Gruops.forEach(function(group){
                                                    if(group.groupadminlist==null || group.groupadminlist.length==0){
                                                        group.groupadminlist.push(obj);
                                                    }
                                                    else{
                                                        var isObjectExists = false;
                                                        group.groupadminlist.forEach(function(adminLst){
                                                            if(adminLst._id == obj._id)
                                                            {
                                                                isObjectExists =true;
                                                            }
                                                            if(data.oldGroupname!==null && data.oldGroupname.indexOf(adminLst._id)!==-1)
                                                            {
                                                                console.log("OldAdminnnn");
                                                                console.log(adminLst)
                                                               group.groupadminlist.splice(adminLst); 
                                                            }*/
                                                          /*  Gruops.find({_id: {$in: data.oldGroupname}}, function(err,deleted){
                                                           });
                                                    
                                                        });
                                                        if(isObjectExists == false)
                                                                group.groupadminlist.push(obj);
                                                    
                                                }
                                            group.save(function (err, post) {
                                                });
                                                });
                                            if (Gruops.length > 0) {
                                                Gruops[0].isActive = true;
                                                Gruops[0].save(function(err, post1) {});
                                            }
                                            });  
                                    }

                                });   
                            }
                            if (data.oldGroupname != null&&data.oldGroupname !=data.groupname) {

                                Gruops.find({

                                    _id: data.oldGroupname._id,
                                    isDeleted: false
                                }, function(err, Gruops) {
                                    if (Gruops.length > 0) {
                                        Gruops[0].isActive = false;
                                        Gruops[0].save(function(err, post1) {});
                                    }
                                });
                            }
                            if (files.img) {
                                var path = files.img.path;
                                var buffer = fs.readFileSync(path);
                                var contentType1 = files.img.contentType;
                                var a = new Store({
                                    fileid: mongoid,
                                    data: buffer,
                                    contentType: contentType1
                                });*/
                                //Upload Image
                               // a.save(function(err, a) {
                                    //log.info("Upload Image Successfully!");
                               // });

                           // }
                            //log.info("Admin Details Updated Successfully!");
                            // Delete old images
                            /*Store.findByIdAndRemove({
                                fileid: req.params.id
                            }, req.body, function(err, post) {});
                            var html = "<body>Dear " + post.name + ",<br><br> Your account has been modified by the administrator.Please login and check the updates.<br></body>"

                            sendautomail(post.email, html, "Automatic Reply: Account Updates");

                        }
                    });

                    res.json({
                        "message": "Admin Details Updated Successfully",
                        status: 200
                    });
                }
            });
        }
    });
});*/

//New modification done to admin update by Renuka on 22-08-2018 @TPCL Project Customization 
router.put('/Update/:id', function(req, res, next) {
    var Imgurl = "";
    mongoid = ObjectID.createFromTime(new Date().getTime() + 15 *60*1000);
    Users.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, post) {
        if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                if (!err) {
                    var imgurl = "";

                    if (files.img) {
                        console.log(console.log("img not null"))
                        Imgurl = "store/" + mongoid;
                    } else {

                        Imgurl = defoultImageurl;
                    }
                    var data = JSON.parse(fields.data);
                    post.name = data.name;
                    post.email = data.email;
                    post.phone = data.phone;
                    post.adminType = data.adminType;
                    post.privilage = data.privilage;
                    /*TPCL Customization*/
                    post.bloodgroup= data.bloodgroup,
                    post.adderess2= data.adderess2,
                    post.adderess1= data.adderess1,
                    post.activity= data.activity,
                    post.zone= data.zone,
                    post.vender= data.vender,
                    post.doj= data.doj,
                    post.dob= data.dob,
                    post.groupname = data.groupname || null;
                    if (files.img) {
                        post.imageurl = Imgurl;
                    }

                    post.type = data.type;
                    post.admingroup = data.admingroup || null;
                    //User or Admin Creation
                    post.save(function(err, post1) {
                        if (err) {
                            res.status(208);
                        } else {
                            var obj = {username: post1.username,_id:post1._id}
                            //group mapping and group table update By Uma
                            //30-12-17
                            if (data.groupname != undefined) {

                                if (data.type == 1) {
                                    if (data.oldGroupname.length > 0) {

                                        Gruops.update({
                                            _id: data.oldGroupname[0]
                                        }, {
                                            $pull: obj
                                        },{$set:{isActive:false}}, function(err, removeOldGroup) {
                                            if (err) {
                                                return err;
                                            } else {
                                                console.log("removed old group");
                                            }
                                        })


                                    }

                                    Gruops.find({
                                        //_id: [data.groupname._id],
                                        _id: data.groupname._id,
                                        isDeleted: false
                                    }, {
                                        adminlist: 1
                                    }, function(err, post) {
                                        console.log("______________");
                                        console.log(post)
                                        // Gruops.forEach(function(group){
                                        //   if(Gruops[0].adminlist==null || Gruops[0].adminlist.length==0){
                                        if (post.length > 0) {
                                            post[0].adminlist.splice(0, 1);
                                            post[0].adminlist.push(obj);
                                            post[0].isActive = true;
                                            post[0].save(function(err, post) {});
                                        }

                                        // }
                                        /*  else{
                                                        console.log("elseeeeee")
                                                    group.adminlist.push(obj);
                                                }*/

                                        // });
                                        /*if (Gruops.length > 0) {
                                            Gruops[0].isActive = true;
                                            Gruops[0].save(function(err, post1) {});
                                        }*/
                                    });
                                } else if (data.type == 3) {
                                    if (data.groupname.length > 0) {
                                        data.groupname.forEach(function(groupid) {
                                        Gruops.find({
                                            //_id: [data.groupname._id],
                                            _id: groupid._id,
                                            isDeleted: false
                                        }, {
                                            groupadminlist: 1
                                        }, function(err, Gruops) {

                                            Gruops.forEach(function(group) {

                                                if (group.groupadminlist == null || group.groupadminlist.length == 0) {
                                                    group.groupadminlist.push(obj);


                                                } else {
                                                    var isObjectExists = false;
                                                    group.groupadminlist.forEach(function(adminLst) {
                                                        if (adminLst._id == obj._id) {
                                                            isObjectExists = true;
                                                        }
                                                        /*if(data.oldGroupname!==null && data.oldGroupname.indexOf(adminLst._id)!==-1)
                                                        {
                                                            console.log("OldAdminnnn");
                                                            console.log(adminLst)
                                                           group.groupadminlist.splice(adminLst); 
                                                        }*/
                                                        /* Gruops.find({_id: {$in: data.oldGroupname}}, function(err,deleted){
                                                         console.log("Idddsadsa")
                                                         console.log(err)
                                                        });*/

                                                    });
                                                    if (isObjectExists == false) {
                                                        group.groupadminlist.push(obj);

                                                    }
                                                    if (data.UMGroupList.length > 0) {
                                                        async.forEachSeries(data.UMGroupList, function(singleUMGrouplist, next) {
                                                            console.log("singleUMGrouplist");
                                                            console.log(singleUMGrouplist);
                                                            console.log(obj);
                                                            Gruops.find({
                                                                _id: singleUMGrouplist._id
                                                            }, {
                                                                groupadminlist: 1,
                                                                _id: 0
                                                            }, function(err, groupinfo) {
                                                                if (groupinfo.length > 0) {
                                                                    for (var i = 0; i < groupinfo[0].groupadminlist.length; i++) {
                                                                        console.log("groupinfo[0].groupadminlist");
                                                                        console.log(groupinfo[0].groupadminlist);

                                                                        if (groupinfo[0].groupadminlist[i]._id == obj._id) {
                                                                            Gruops.update({
                                                                                _id: singleUMGrouplist._id
                                                                            }, {
                                                                                $pull: {
                                                                                    userlist: groupinfo[0].groupadminlist[i]
                                                                                }
                                                                            }, function(err, saved) {
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                } else {
                                                                                    console.log("saved");
                                                                                    next();
                                                                                }
                                                                            })


                                                                        }



                                                                    }
                                                                } else {
                                                                    console.log("group is unavailable");
                                                                }


                                                            })


                                                            //  }
                                                        })


                                                    }

                                                }
                                                group.save(function(err, post) {
                                                    console.log("save");
                                                });
                                            });

                                        });
                                    });


                                    }
                                    else {
                                            if (data.UMGroupList.length > 0) {
                                                        async.forEachSeries(data.UMGroupList, function(singleUMGrouplist, next) {
                                                            console.log("singleUMGrouplist");
                                                            console.log(singleUMGrouplist);
                                                            Gruops.find({
                                                                _id: singleUMGrouplist._id
                                                            }, {
                                                                groupadminlist: 1,
                                                                _id: 0
                                                            }, function(err, groupinfo) {
                                                                console.log(groupinfo);
                                                                console.log("obj==");
                                                                console.log(obj);
                                                                if (groupinfo.length > 0) {
                                                                    for (var i = 0; i < groupinfo[0].groupadminlist.length; i++) {
                                                                        if (groupinfo[0].groupadminlist[i]._id == obj._id) {
                                                                            Gruops.update({
                                                                                _id: singleUMGrouplist._id
                                                                            }, {
                                                                                $pull: {
                                                                                    userlist: groupinfo[0].groupadminlist[i]
                                                                                }
                                                                            }, function(err, saved) {
                                                                                if (err) {
                                                                                    console.log(err);
                                                                                } else {
                                                                                    console.log("saved");
                                                                                    next();
                                                                                }
                                                                            })


                                                                        }



                                                                    }
                                                                } else {
                                                                    console.log("group is unavailable");
                                                                }


                                                            })


                                                            //  }
                                                        })


                                                    }

                                    }
                                    
                                }
                                /* if (Gruops.length > 0) {
                                            Gruops[0].isActive = true;
                                            Gruops[0].save(function(err, post1) {});
                                        }*/


                            }
                            /* if(data.groupname)
                            {
                                if (data.oldGroupname != null&&data.oldGroupname[0] !=data.groupname._id) {

                                    Gruops.find({

                                        _id: data.oldGroupname[0],
                                        isDeleted: false
                                    }, function(err, Gruops) {
                                        if (Gruops.length > 0) {
                                            Gruops[0].isActive = false;
                                            Gruops[0].adminlist= [];
                                            Gruops[0].save(function(err, post1) {
                                            });
                                        }
                                    });
                                }
                        }*/
                            if (files.img) {
                                var path = files.img.path;
                                var buffer = fs.readFileSync(path);
                                var contentType1 = files.img.contentType;
                                var a = new Store({
                                    fileid: mongoid,
                                    data: buffer,
                                    contentType: contentType1
                                });
                                //Upload Image
                                a.save(function(err, a) {
                                    //log.info("Upload Image Successfully!");
                                });

                            }
                            //log.info("Admin Details Updated Successfully!");
                            // Delete old images
                            Store.findByIdAndRemove({
                                fileid: req.params.id
                            }, req.body, function(err, post) {});
                            //var html = "<body>Dear " + post.name + ",<br><br> Your account has been modified by the administrator.Please login and check the updates.<br>Thanks,<br>DFormsTeam</body>"
                            var html = "<body>Dear " + post.name + ",<br><br>Your account has been modified by the administrator.Please login and check the updates.<br>Thanks,<br>DForms, GIS Team</body>"

                            sendautomail(post.email, html, "Automatic Reply: Account Updates");

                        }
                    });
                    if (data.type == 1) {
                        res.json({
                            "message": "Department Admin Details Updated Successfully",
                            status: 200
                        });
                    } else {
                        res.json({
                            "message": "Group Admin Details Updated Successfully",
                            status: 200
                        });
                    }
                    // res.json({
                    //     "message": "Admin Details Updated Successfully",
                    //     status: 200
                    // });
                }
            });
        }
    });
});



/* DELETE /Admins/:id */
router.delete('/:id', function(req, res, next) {
    Users.findById(req.params.id, function(err, post) {
		if(post){
            if(post.groupname != null)
			{
                Gruops.find({_id:post.groupname[0]._id,isDeleted:false},function (err,group){
				if(group.length>0) {
					res.json({"message":"Cannot delete admin account, as it is associated with user group "+ group[0].name +".", "status":204})
				}
				else {
					 post.isDeleted = true;
            		 post.save(function(err,a) {
                    if (err) {
    					throw err;
    				}
                    
                else {
				  //  isActive:false;
                    res.json({
                        "message": "Admin deleted successfully",
                        "status": 200
                    })
					//var html = "<body>Dear" + post.name + ",<br><br> Your account has been deleted by the administrator.<br>Thanks,<br>DFormsTeam</body>"
                     var html = "<body>Dear" + post.name + ",<br><br> Your account has been deleted by the administrator from Dynamic-Forms system.<br>Thanks,<br>DForms, GIS Team</body>"

            sendautomail(post.email, html, "Automatic Reply: Account Deletion");
                }

            });
			  
	
				}	
			});
				
		}
        //Description: If groupname not there delete admin directly by Uma
        // modified On: 30-12-17
        else
        {
            post.isDeleted = true;
            post.save(function(err,a) 
            {
                if (err) 
                {
                    throw err;
                }
                    
                else 
                {
                  //  isActive:false;
                    res.json({
                        "message": "Admin deleted successfully",
                        "status": 200
                    });
                var html = "<body>Dear" + post.name + ",<br><br> Your account has been deleted by the administrator.<br></body>";
                sendautomail(post.email, html, "Automatic Reply: Account Deletion");    
                }
            });
        }
    }
		else {
			res.json({"message":"No data found","status":204})
		}
	});
});

module.exports = router;
