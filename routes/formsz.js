var express = require('express');
var router = express.Router();
var Formsz = require('../models/formsz.js');
var Formszallocation = require('../models/formszAllocation.js');
var FormszDetails = require('../models/formszDetails.js');
var Users = require('../models/user.js');
var versionManagement = require('../models/versionManagement.js');
var ObjectID = require("bson-objectid");
var log = require('./log')(module);
var Tasks = require('../models/formszTasks.js');
//var pushNotifications = require('../routes/utils').pushNotifications;
var getdeviceKeys = require('../routes/utils').getdeviceKeys;
var formszCategory = require('../models/formszCategory.js');
var Groups = require('../models/group.js');
var activitylog = require('../models/activityLog.js');
var activityLog = require('formsz-tpcl-customizations');
//Added on 18-01-2018 by Divya for TPCL Project
var async = require('async');



/* GET All Templates  */
/*router.get('/getallTemplates/:limit/:offset', function(req, res, next) {
    Formsz.paginate({
        formType: 'template',
        isVisible: true
    }, {
        page: req.params.offset,
        limit: req.params.limit,
        sort: {
            createdTime: -1
        }
    }, function(err, templates) {
        if (templates.docs.length > 0) {
            res.json({
                "templateList": templates,
                "status": 200
            });
        } else {
            res.json({
                "message": "No data found",
                "status": 204,
                templateList: {
                    docs: [],
                    limit: 0,
                    total: 0
                }
            });
        }
    });
});*/

/*
@description Service written for group admin
    group admin can see the selected forms
@developer Renuka
@Purpose TPCL Customization
*/
router.get('/getformsgroupadmin/:groupname',function(req,res,next){
    Formsz.find({userGroup:req.params.groupname,isVisible: true,formType:"form"}, function(err, post) {
    if(err)
        return next(err);
        res.json({
            "formslist": post,
            "status": 200
        }); 
    });
});

/*
@description Service written for group admin
    group admin can see all the  forms mapped to it.
@developer Renuka
@Purpose TPCL Customization
*/
router.post('/allformsgroupadmin',function(req,res,next){
    Formsz.find({userGroup:{$in:req.body},isVisible: true,formType:"form"},function(err, post){
    if(err)
    return next(err);
        res.json({
            "formslist": post,
            "status": 200
        }); 
    });
});

router.get('/getallAdminTemplates/:limit/:offset/:groupId',function(req, res, next) {
    var limit = parseInt(req.params.limit);
        Formsz.paginate({$or:[
            {category:"Public",formType:"template"},
            {formType:"template",userGroup:req.params.groupId,isVisible: true}
        ]}, 
        {
            page: req.params.offset,
            limit: limit,
            sort: {
                createdTime: -1
            }
        }, function(err, templates) {
            if (templates.docs.length > 0) {
                res.json({
                    "templateList": templates,
                    "status": 200
                });
            } else {
                res.json({
                    "message": "No data found",
                    "status": 204,
                    templateList: {
                        docs: [],
                        limit: 0,
                        total: 0
                    }
                });
            }
        });
    
});
router.get('/getallTemplates/:limit/:offset/:groupId', function(req, res, next) {
    var limit = parseInt(req.params.limit);
    // @TPCL CUSTOMIZATION:
    // IF Admintype is root(SuperAdmin)
    if (req.params.groupId == "All"||req.params.groupId==undefined) {
        Formsz.paginate({
            formType: 'template',
            isVisible: true
        }, {
            page: req.params.offset,
            limit: limit,
            sort: {
                createdTime: -1
            }
        }, function(err, templates) {
            if (templates.docs.length > 0) {
                res.json({
                    "templateList": templates,
                    "status": 200
                });
            } else {
                res.json({
                    "message": "No data found",
                    "status": 204,
                    templateList: {
                        docs: [],
                        limit: 0,
                        total: 0
                    }
                });
            }
        });
    }
    else{
        Formsz.paginate({$or:[
            {category:"Public",formType:"template"},
            {formType:"template",userGroup:req.params.groupId,isVisible: true}
        ]}, 
        {
            page: req.params.offset,
            limit: limit,
            sort: {
                createdTime: -1
            }
        }, function(err, templates) {
            if (templates.docs.length > 0) {
                res.json({
                    "templateList": templates,
                    "status": 200
                });
            } else {
                res.json({
                    "message": "No data found",
                    "status": 204,
                    templateList: {
                        docs: [],
                        limit: 0,
                        total: 0
                    }
                });
            }
        });
    }
});
router.get('/isFormszexits/:name', function(req, res, next) {
    Formsz.find({
        name: req.params.name,
        isVisible: true
    }, function(err, templates) {
        if (templates.length > 0) {
            res.json({
                "message": "Already exists",
                "status": 208
            });
        } else {
            res.json({
                "message": "Not exists",
                "status": 204
            });
        }
    });
});
// Get All Forms filter  by usergroup ,user
/*
@developer : Santhosh Kumar Gunti
TPCL customization
Added work instruction and references field in form creation
*/
router.get('/getformszlist/:userid/:groupname', function(req, res, next) {
						
                Formsz.find({
                    $or: [{
                        userGroup: req.params.groupname,
                        isVisible: true
                    }, {
                        category: 'General',
                        isVisible: true
                    }]
                }, function(err, post1) {

                    if (post1.length > 0) {
						
                        var userdata = [];
                        post1.forEach(function(dbUserObj1) {
                            var alluser = dbUserObj1.allocatedUsers.split(",")
                            if (dbUserObj1.category == 'Private') {
                                var a = alluser.indexOf(req.params.userid);
                                if (a > -1) {
                                    userdata.push({
                                        _id: dbUserObj1.id,
                                        name: dbUserObj1.name,
                                        version: dbUserObj1.version,
                                        createdBy: dbUserObj1.createdBy,
                                        createdTime: dbUserObj1.createdTime,
                                        allocatedUsers: alluser,
                                        category: dbUserObj1.category,
                                        alternativeMailid: dbUserObj1.alternativeMailid,
                                        description: dbUserObj1.description,
                                        workInstruction : dbUserObj1.workInstruction,
                                        references : dbUserObj1.references,

                                    });
                                }
                            } else {
                                userdata.push({
                                    _id: dbUserObj1.id,
                                    name: dbUserObj1.name,
                                    version: dbUserObj1.version,
                                    createdBy: dbUserObj1.createdBy,
                                    createdTime: dbUserObj1.createdTime,
                                    allocatedUsers: alluser,
                                    category: dbUserObj1.category,
                                    alternativeMailid: dbUserObj1.alternativeMailid,
                                    description: dbUserObj1.description,
                                    workInstruction : dbUserObj1.workInstruction,
                                    references : dbUserObj1.references,
                                });
                            }
                        });
                        res.json({
                            "formslist": userdata,
                            "total": post1.total,
                            "limit": post1.limit,
                            "pages": post1.pages,
                            "status": 200
                        }); 
                    } else {
                        res.json({
                            "message": "No data found",
                            "status": 204
                        });
                    }

                });

       
  

});

//Get Skeleton Of Forms
router.get('/formSkeleton/:id', function(req, res, next) {
    Formsz.findById(req.params.id, function(err, post) {
        //if (err) return next(err);
        if (post) {
            res.json(post.FormSkeleton);

            //return next(err);
        } else {
            res.json({"message":"No data found","status":204})
           
        }
    });
  
});

//web
/*
@developer : Santhosh Kumar Gunti
TPCL customization
Added work instruction and references field in form creation
*/

router.get('/getformszlistWeb/:groupname/:userid', function(req, res, next) {
    Users.find({
        groupname: req.params.groupname,
        type: 1,
        isDeleted: false
    }, function(err, post) {
        if (post.length > 0) {
            post.forEach(function(dbUserObj) {
                Formsz.find({
                    $or: [{
                        createdBy: dbUserObj.username,
                        isVisible: true
                    }, {
                        category: 'General',
                        isVisible: true
                    }]
                }, function(err, post1) {
                    if (post1.length > 0) {
                        var userdata = [];
                        post1.forEach(function(dbUserObj1) {
                            var alluser = dbUserObj1.allocatedUsers.split(",")
                            if (dbUserObj1.category == 'Private') {
                                var a = alluser.indexOf(req.params.userid);
                                if (a > -1) {
                                    userdata.push({
                                        _id: dbUserObj1.id,
                                        name: dbUserObj1.name,
                                        version: dbUserObj1.version,
                                        createdBy: dbUserObj1.createdBy,
                                        createdTime: dbUserObj1.createdTime,
                                        allocatedUsers: alluser,
                                        category: dbUserObj1.category,
                                        alternativeMailid: dbUserObj1.alternativeMailid,
                                        description: dbUserObj1.description,
                                        workInstruction : dbUserObj1.workInstruction,
                                        references : dbUserObj1.references

                                    });
                                }
                            } else {
                                userdata.push({
                                    _id: dbUserObj1.id,
                                    name: dbUserObj1.name,
                                    version: dbUserObj1.version,
                                    createdBy: dbUserObj1.createdBy,
                                    createdTime: dbUserObj1.createdTime,
                                    allocatedUsers: alluser,
                                    category: dbUserObj1.category,
                                    alternativeMailid: dbUserObj1.alternativeMailid,
                                    description: dbUserObj1.description,
                                    workInstruction : dbUserObj1.workInstruction,
                                    references : dbUserObj1.references
                                });
                            }

                        });
                        res.json({
                            "formslist": userdata,
                            "total": post1.total,
                            "limit": post1.limit,
                            "pages": post1.pages,
                            "status": 200
                        });
                    } else {
                        res.json({
                            "message": "No data found",
                            "status": 204
                        });
                    }

                });

            });
        } else {
            res.json({
                "message": "No data found",
                "status": 204
            });
        }

    });

});
/*
@developer : Santhosh Kumar Gunti
TPCL customization
Added work instruction and references field in form creation
*/

// Get All Forms filter  by Usergroup & catagory
/*router.get('/getformszlists/:groupname/:catagory', function(req, res, next) {
    var fcategory = req.params.catagory;
    var publifcategory = "";
    if (fcategory == "All") {
        fcategory = {
            $ne: null
        }
        publifcategory="General"
    }
    if (fcategory == "General") {
        fcategory = "General"
        publifcategory="General"
    }
    var groupname = "";
    if(req.params.groupname=="root") {
        console.log("enrerd here")
        groupname = {
            $ne: null
        }
    }
    else {
        groupname= req.params.groupname;
    }
                Formsz.find({
                    $or: [{
                        isVisible: true,
                        category: fcategory,
                        formType:"form",
                        userGroup:groupname}*/
                        
                   /*,{
                        category: publifcategory,
                        isVisible: true
                    }]*/
/*                }, function(err, post1) {
                    if (post1.length > 0) {
                        var userdata = [];
                        
                        post1.forEach(function(dbUserObj1) {
                            var alluser = [];
                            if (dbUserObj1.allocatedUsers) {
                                alluser = dbUserObj1.allocatedUsers.split(",");
                            }
                            var catagory={}*/
                            //here replaced dbUserObj1 with post1 bcz unable to get forms
  /*                           formszCategory.find({_id:post1.formzCategory},function(error,obj)
                                {
                                    console.log("***********")
                                console.log(obj);
                                    if(obj.length>0)
                                    {
                                        catagory._id=obj[0]._id
                                        catagory.name=obj[0].name
                                        
                                    }
                                     userdata.push(
                                     {
                                        _id: dbUserObj1.id,
                                        formzCategory: catagory,
                                        requiredField: dbUserObj1.requiredField,
                                        name: dbUserObj1.name,
                                        version: dbUserObj1.version,
                                        createdBy: dbUserObj1.createdBy,
                                        createdTime: dbUserObj1.createdTime,
                                        allocatedUsers: alluser,
                                        category: dbUserObj1.category,
                                        geoFields :dbUserObj1.geoFields,
                                        urlAttachment :dbUserObj1.urlAttachment,
                                        isoNumber :dbUserObj1.isoNumber,
                                        alternativeMailid: dbUserObj1.alternativeMailid,
                                        description: dbUserObj1.description,
                                        workInstruction : dbUserObj1.workInstruction,
                                        isAllowMap : dbUserObj1.isAllowMap,
                                        groupname : dbUserObj1.userGroup

                                    });
                                })
                            
                           
                        });
                    
                      setTimeout(function () {res.json({"formslist": userdata,"total": post1.total,"limit": post1.limit,"pages": post1.pages,"status": 200});
                    },1000);
                        
                        
                    } else {
                        res.json({
                            "message": "No data found",
                            "status": 204
                        });
                    }
                });

});*/

// Get All Forms filter  by Usergroup & catagory
router.get('/getformszlists/:groupname/:catagory', function(req, res, next) {
    var fcategory = req.params.catagory;
    var publifcategory = "";
    if (fcategory == "All") {
       fcategory = {
            $ne: []
        }
       // publifcategory="General"
    }
    /*if (fcategory == "General") {
        fcategory = "General"
        publifcategory="General"
    }*/
    var groupname = "";
    if(req.params.groupname=="root") {
        groupname = {
            $ne: []
        }
    }
    else {
        groupname= req.params.groupname;
    }
                Formsz.find({
                    $or: [{
                        isVisible: true,
                        category: fcategory,
                        formType:"form",
                        userGroup:groupname
                        
                    }/*,{
                        category: publifcategory,
                        isVisible: true */
                    //}
                    ]
                }, function(err, post1) {
                    if (post1.length > 0) {
                        var userdata = [];
                        var data =[];
                        
                        async.forEachSeries(post1,function(dbUserObj1,nextIteration) {
                           // console.log(post1);
                            var alluser = [];
                            if (dbUserObj1.allocatedUsers) {
                                alluser = dbUserObj1.allocatedUsers.split(",");
                            }
                            

                                var categoryArray =[];
                                 var numOfUsersDownloaded=0 ;
                                var recordsCount=0;
                                var lastDownloadedTimeValue=0;
                                var assignmentAbtForms = false;
                            async.forEachSeries(dbUserObj1.formzCategory,function(eachcategory,next3) {
                                formszCategory.find({_id:eachcategory._id},function(error,obj)
                                {
                                    var catagory ={};
                                  //  console.log("***********")
                                    if(obj.length>0)
                                    {
                                        catagory._id=obj[0]._id
                                        catagory.name=obj[0].name
                                        categoryArray.push(catagory);

                                        next3();
                                        
                                    }
                                });
                                //<!-- Uma:Added:TPCL Customization: to fetch assigned/unassigned forms on click -->
                               /* formAssignment(dbUserObj1.id, function(value) {
                                    if(value.length>0)
                                            assignmentAbtForms = true;
                                        else
                                            assignmentAbtForms = false;
                                        }),*/
                                //End
                                  countfun(dbUserObj1.id, function(value) {
                                            numOfUsersDownloaded = value;
                                        }),
                                    countForRecords(dbUserObj1.id, function(value1) {
                                            recordsCount = value1
                                        })
                                    lastDownloadedTime(dbUserObj1.id, function(lastDownloadedDate) {
                                        lastDownloadedTimeValue=lastDownloadedDate
                                    })

                            }, function(err) {
                                
                                //console.log(activitylog.find({activity:"Form Downloaded"}))
                                //console.log(activitylog.count({activity:"Form Downloaded",formId:dbUserObj1.id}))
                                   userdata.push(
                                     {
                                        _id: dbUserObj1.id,
                                        formzCategory: categoryArray,
                                        requiredField: dbUserObj1.requiredField,
                                        name: dbUserObj1.name,
                                        version: dbUserObj1.version,
                                        createdBy: dbUserObj1.createdBy,
                                        createdTime: dbUserObj1.createdTime,
                                        allocatedUsers: alluser,
                                        category: dbUserObj1.category,
                                        geoFields :dbUserObj1.geoFields,
                                        urlAttachment :dbUserObj1.urlAttachment,
                                        isoNumber :dbUserObj1.isoNumber,
                                        alternativeMailid: dbUserObj1.alternativeMailid,
                                        description: dbUserObj1.description,
                                        workInstruction : dbUserObj1.workInstruction,
                                        references : dbUserObj1.references,
                                        isAllowMap : dbUserObj1.isAllowMap,
                                        groupname : dbUserObj1.userGroup,
                                        lastModifiedBy : dbUserObj1.lastModifiedBy,
                                        numOfUsersDownloaded :numOfUsersDownloaded,
                                        recordsCount: recordsCount,
                                        lastDownloadedDate: lastDownloadedTimeValue,
                                        taskAssignedUsers:countTaskUsers
                                        //<!-- Uma:Added:TPCL Customization: to fetch assigned/unassigned forms on click -->
                                        //formAssigned: assignmentAbtForms
                                        //End
                                        //recordsCount: FormszDetails.count({"formId":"79d2b8f70000000000000000"})
                                       


                                    });
                                   /* activitylog.find({formId:"79d3add70000000000000000"}).count(function(err,data){
                                           userdata[0].numOfUsersDownloaded = data;
                                        })*/
                                   nextIteration();

                            });
                           // console.log("1231232131");
                           // console.log(userdata);
                            //here replaced dbUserObj1 with post1 bcz unable to get forms

                             
                                  
                               // })
                            
                          
                        }, function(err) {
                             res.json({"formslist": userdata,"total": post1.total,"limit": post1.limit,"pages": post1.pages,"status": 200});
                    

                        });
                    
                     
                        
                        
                    } else {
                        res.json({
                            "message": "No data found",
                            "status": 204
                        });
                    }
                });

});
 function countfun(formId1,callback)
{
     activitylog.find({formId:formId1,username:{$ne:null}}).count(function(err,data){
              callback(data);

                });
}

function countTaskUsers(formId1,callback)
{
     Tasks.find({formId:formId1}).count(function(err,data){
              callback(data);

                });
}
//<!-- Uma:Added:TPCL Customization: to fetch assigned/unassigned forms on click -->
/*function formAssignment(formId1,callback)
{
    Tasks.find({assigned:{$elemMatch:{form:formId1}}},function(err,data){
              callback(data);

                });
}*/
//End
function countForRecords(formId1,callback){
FormszDetails.count({formId:formId1},function(err,data){
    callback(data);
 })
}
 function lastDownloadedTime(formId,callback) {
    console.log("formId");
    console.log(formId);
    activitylog.find({formId:formId,username:{$ne:null}}).sort({timestamp:-1}).exec(function(err,data){
        if(data.length>0) {
                 callback(data[0].timestamp);
   
        }
        else {
            callback(null)
        } })
}
// Get All Forms filter  by Usergroup,From Date,Todate
router.get('/getformszlist/:groupname/:fromdate/:todate', function(req, res, next) {
    Users.find({
        groupname: req.params.groupname,
        type: 1,
        isDeleted: false
    }, function(err, post) {
        if (post.length > 0) {
            post.forEach(function(dbUserObj) {
                Formsz.find({
                    createdBy: dbUserObj.name,
                    createdTime: {
                        $gte: new Date(req.params.fromdate),
                        $lt: new Date(req.params.todate)
                    }
                }, function(err, post1) {
                    if (post1.length > 0) {
                        var userdata = [];
                        post1.forEach(function(dbUserObj1) {
                            userdata.push({
                                _id: dbUserObj1.id,
                                name: dbUserObj1.name,
                                version: dbUserObj1.version,
                                createBy: dbUserObj1.createdBy,
                                createdTime: dbUserObj1.createdTime,
                                category: dbUserObj1.category,
                                alternativeMailid: dbUserObj1.alternativeMailid
                            });
                        });
                        res.json(userdata);
                    }
                });
            });
        } else {
            res.json({
                "message": "No records inserted",
                "status": 204
            });
        }
    });
});

// Update Formsz Data by Siva Kella on 14-03-2018
router.put('/:id', function(req, res, next) {
    /*var reqdatadata=[]
    reqdatadata.push(req.body.requiredField);
    req.body.requiredField=reqdatadata*/
    var updateForm = req.body;
    /*var formdbValue = req.body;
     delete formdbValue['BsicLevelChanges'];*/
    
    Formsz.findByIdAndUpdate(req.params.id, updateForm,{new:true}, function(err, post) {
       // console.log(post);
        if(post) {
             versionManagement.find({formId:post._id},function(err,version)
        {
            if(err) {
                console.log(err);
            }
            else {
                var versionLength =version.length;
               // req.body.version = version.length+1;
                req.body.version = version.length;
                req.body.formId = post._id;
                /*if(req.body.requiredVersion['updated']!==undefined|| Object.keys(req.body.BsicLevelChanges[0]).length!==0 || Object.keys(req.body.derivedFieldVersion).length!==0 ||req.body.formSkeletonLevelChanges['update'].length!=0 ||req.body.formSkeletonLevelChanges['deleted'].length!=0 || req.body.formSkeletonLevelChanges['added'].length!=0)
                {
                 versionManagement.create(req.body,function(err,post1)
                    {
                        //console.log(post1);
                        if(post1)
                            {
                                console.log("Successfully created");
                            }
                            else
                            {
                                console.log(err);
                            }
                    });
                }*/
                 /*if(req.body.requiredVersion['updated']!==undefined|| Object.keys(req.body.BsicLevelChanges[0]).length!==0 || Object.keys(req.body.derivedFieldVersion).length!==0 )
                    {
                 */       /*console.log(req.body.formSkeletonLevelChanges)
                        if(req.body.formSkeletonLevelChanges)
                        {*/
                          //  console.log("---------------------")
                           // console.log(req.body)



                           req.body.formSkeletonLevelChanges['update'] =req.body.formSkeletonLevelChanges['update'] || [];

                           req.body.formSkeletonLevelChanges['deleted'] =req.body.formSkeletonLevelChanges['deleted'] || [];
                           req.body.formSkeletonLevelChanges['added'] =req.body.formSkeletonLevelChanges['added'] || [];



                          //   if(!req.body.formSkeletonLevelChanges['update'] != undefined && req.body.requiredVersion['updated']!==undefined)
                          //   {
                          //       req.body.formSkeletonLevelChanges['update'] =[];
                          //   }
                          // if(!req.body.formSkeletonLevelChanges['deleted'] && req.body.requiredVersion['deleted']!==undefined)
                          //   {
                          //       req.body.formSkeletonLevelChanges['deleted'] = [];
                          //   }
                          //    if(!req.body.formSkeletonLevelChanges['added'] && req.body.requiredVersion['added']!==undefined)
                          //   {
                          //       req.body.formSkeletonLevelChanges['added'] = [];
                          //   }
                          

                           

                            if(Object.keys(req.body.BsicLevelChanges[0]).length>0 ||Object.keys(req.body.requiredVersion).length>1 || Object.keys(req.body.derivedFieldVersion).length>0 || req.body.formSkeletonLevelChanges['update'].length!==0 ||req.body.formSkeletonLevelChanges['deleted'].length!==0 || req.body.formSkeletonLevelChanges['added'].length!==0)
                            {
                                versionManagement.create(req.body,function(err,post1)
                                {
                                    //console.log(post1);
                                    if(post1)
                                        {
                                            console.log("Successfully created");
                                        }
                                        else
                                        {
                                            console.log(err);
                                        }
                                });
                            }

                       /* }
                        else
                        {
                            versionManagement.create(req.body,function(err,post1)
                            {
                                //console.log(post1);
                                if(post1)
                                    {
                                        console.log("Successfully created");
                                    }
                                    else
                                    {
                                        console.log(err);
                                    }
                            });
                        }*/
                     
                    /*}*/

                 //Code added for TPCL Divya.B
                 activityLog.activityLogForForm(post,"Form Modified with version " + req.body.version);

             }
        });

        }
      //  console.log(req.body);
       
      //  if (err) {
           /* res.json({
                "message": "data not found",
                "status": 204
            });*/
     //   }
        //if(true)
        //{
        //delForms(req.params.id, req.body.FormSkeleton,function(ress){

        //updateforms(req.params.id, req.body.FormSkeleton,function(){
        //res.json({"Message":"No Data Found","status":200});
        //});

        //})

        //}
        res.json({
            "message": "Updated Successfully",
            "status": 200
        });
    });
});


// Update Formsz Data backup
router.put('/:idBackup', function(req, res, next) {
	/*var reqdatadata=[]
	reqdatadata.push(req.body.requiredField);
	req.body.requiredField=reqdatadata*/
	var updateForm = req.body;
    /*var formdbValue = req.body;
     delete formdbValue['BsicLevelChanges'];*/
	
    Formsz.findByIdAndUpdate(req.params.id, updateForm,{new:true}, function(err, post) {
       // console.log(post);
        if(post) {
             versionManagement.find({formId:post._id},function(err,version)
        {
            if(err) {
                console.log(err);
            }
            else {
                var versionLength =version.length;
               // req.body.version = version.length+1;
                req.body.version = version.length;
                req.body.formId = post._id;
                /*if(req.body.requiredVersion['updated']!==undefined|| Object.keys(req.body.BsicLevelChanges[0]).length!==0 || Object.keys(req.body.derivedFieldVersion).length!==0 ||req.body.formSkeletonLevelChanges['update'].length!=0 ||req.body.formSkeletonLevelChanges['deleted'].length!=0 || req.body.formSkeletonLevelChanges['added'].length!=0)
                {
                 versionManagement.create(req.body,function(err,post1)
                    {
                        //console.log(post1);
                        if(post1)
                            {
                                console.log("Successfully created");
                            }
                            else
                            {
                                console.log(err);
                            }
                    });
                }*/
                 /*if(req.body.requiredVersion['updated']!==undefined|| Object.keys(req.body.BsicLevelChanges[0]).length!==0 || Object.keys(req.body.derivedFieldVersion).length!==0 )
                    {
                 */       /*console.log(req.body.formSkeletonLevelChanges)
                        if(req.body.formSkeletonLevelChanges)
                        {*/
                          //  console.log("---------------------")
                           // console.log(req.body)

                           //<!-- Uma:Added:TPCL Customization: for softa changes create a version in version table -->
                           req.body.formSkeletonLevelChanges['update'] = req.body.formSkeletonLevelChanges['update'] || [];
                           req.body.formSkeletonLevelChanges['deleted'] = req.body.formSkeletonLevelChanges['deleted'] || [];
                           req.body.formSkeletonLevelChanges['added'] = req.body.formSkeletonLevelChanges['added'] || [];
                                if(Object.keys(req.body.BsicLevelChanges[0]).length>0 ||Object.keys(req.body.requiredVersion).length>1 || Object.keys(req.body.derivedFieldVersion).length>0 || req.body.formSkeletonLevelChanges['update'].length!==0 ||req.body.formSkeletonLevelChanges['deleted'].length!==0 || req.body.formSkeletonLevelChanges['added'].length!==0)
                                {
                                    versionManagement.create(req.body,function(err,post1)
                                    {
                                        //console.log(post1);
                                        if(post1)
                                            {
                                                console.log("Successfully created");
                                            }
                                            else
                                            {
                                                console.log(err);
                                            }
                                    });
                                }
                         
                            //End
                       /* }
                        else
                        {
                            versionManagement.create(req.body,function(err,post1)
                            {
                                //console.log(post1);
                                if(post1)
                                    {
                                        console.log("Successfully created");
                                    }
                                    else
                                    {
                                        console.log(err);
                                    }
                            });
                        }*/
                     
                    /*}*/

                 //Code added for TPCL Divya.B
                 activityLog.activityLogForForm(post,"Form Modified with version " + req.body.version);

             }
        });

        }
      //  console.log(req.body);
       
      //  if (err) {
           /* res.json({
                "message": "data not found",
                "status": 204
            });*/
     //   }
        //if(true)
        //{
        //delForms(req.params.id, req.body.FormSkeleton,function(ress){

        //updateforms(req.params.id, req.body.FormSkeleton,function(){
        //res.json({"Message":"No Data Found","status":200});
        //});

        //})

        //}
        res.json({
            "message": "Updated Successfully",
            "status": 200
        });
    });
});

//Get the Formsz filter by Id
router.get('/:id', function(req, res, next) {
    Formsz.findById(req.params.id, function(err, post) {
        if (err) return next(err);

        res.json(post);
    });
});

//Service to download for form
router.post('/formDownload', function(req, res, next) {
    Formsz.findById(req.body.id, function(err, post) {
        if (err) return next(err);
        else {
              activityLog.activityLogFortaskOrFormDownload(req.body,"Form Downloaded");
                res.json(post);
        }

    });
});

//Create Formsz
router.post('/create', function(req, res, next) {
    var formdbValue = req.body;
     delete formdbValue['BsicLevelChanges'];
     delete formdbValue['formSkeletonLevelChanges'];
    // delete formdbValue['version'];
     delete formdbValue['derivedFieldVersion'];
	 delete formdbValue['requiredVersion'];
     delete formdbValue['originalFormDetails'];
	/*var reqdatadata=[]
	reqdatadata.push(req.body.requiredField);
	req.body.requiredField=reqdatadata*/
	
    var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
    req.body._id = mongoid;
    if (req.body.category != 'Private') {
        req.body.isVisible = true;
    }
    req.body._id = mongoid;
    Formsz.find({
        name: req.body.name,
        isVisible: true,
        formType: req.body.formType
    }, function(err, post) {
        if (post.length > 0) {
            res.json({
                "message": "Formsz already  exits",
                "status": 208
            })

        } else {
            /*if (req.body.formType == "form") {
                if (req.body.requiredField != undefined) {
                    if (req.body.requiredField[0] == null && req.body.requiredField.length == 1) {
                        var data = []
                        data.push({
                            "message": "No records found"
                        });
                        req.body.requiredField = data;
                    }
                } else {
                    var data = []
                    data.push({
                        "message": "No records found"
                    });
                    req.body.requiredField = data;
                }
            }*/
            Formsz.create(formdbValue, function(err, post) {
                req.body.formId = post._id
                    versionManagement.create(req.body,function(err,version)
                    {
                        
                        if(version)
                            {
                                
                                console.log("Successfully created");
                            }
                    });

                    //Added for TPCL Customisation by Divya.B
                activityLog.activityLogForForm(post,"Form Created");

                /*var activityLog = new activitylog({
                    formId:post._id,
                    formName:post.name,
                    activity :"Form Created",
                    adminname:post.createdBy,
                    usergroup:post.userGroup,
                    source:"NA"

                });
                activityLog.save(function(err,activityLog) {

                    if(err) 
                        return err;
                    else {
                        console.log("saved activitylog");
                    }
                    
                })*/

                //Get the Private formsz
                if (post.category == 'Private') {
                    if (req.body.allocatedUsers) {
                        var allusers = req.body.allocatedUsers;
                        for (i = 0; i < allusers.length; i++) {

                            var formszallocation = new Formszallocation({
                                formId: mongoid,
                                allotateTo: allusers[i]
                            });

                            formszallocation.save(function(err, a) {
                                if (err) throw err;

                            });
                        }
                    }
                   

                }
                 /*if (post.category == 'Private') {
                     var ids = [];
                    Users.find({
                        'username': {
                             $in: allusers
                         }
                    }, function(err, result) {
                        for (var i = 0; i < result.length; i++) {
                            ids.push(result[i]._id);
                         }
                          var value = getdeviceKeysByUser(ids, function(data) {
                             pushNotifications(value, "New Form Created", "New Form Created", {
                                 data: "New Form Created"
                              });
                          });
								
                     });
                 } else {
                     var value = getdeviceKeys(post.userGroup, function(data) {
                     pushNotifications(value, "New Form Created", "New Form Created", {
                        data: "New Form Created"
                    });
                 });
                 }*/
                log.info(req.body + "Created Successfully");
                res.json({
                    "status": "200",
                    "message": "Created Successfully"
                });
            });
        }
    });
});
// Delete Formsz by Id
router.delete('/delete/:id', function(req, res, next) {
    //check the tasks if aready exists then this should not be deleted
    Tasks.find({
        'assignedFormsz.formId': req.params.id
    }, function(err, post1) {
        if (post1.length > 0) {
            res.json({
                "status": 203,
                "message": "Task[s] are alloted to this form ,so you cannot delete it."
            });
        } else {
            Formsz.find({
                _id: req.params.id
            }, function(err, post) {
                if (post.length > 0) {
                    post[0].isVisible = false;
                    post[0].save(function(err, post) {
                        if(err) return err;
                        else {
                            // Added for TPCL Project Divya B
                               activityLog.activityLogForForm(post,"Form Deleted");

                        }
                        /* var value = getdeviceKeys(post.userGroup, function(data) {
											
                             pushNotifications(value, "Form deleted", "Form deleted", {
                                 data: "Form deleted"
                            });
                         });*/
                        res.json({
                            "status": 200,
                            "message": "Deleted Successfully"
                        });
                    });
                } else {
                    res.json({
                        "message": "No Data Found",
                        "status": 204
                    });
                }
            });

        }

    });
});

function updateforms(id, body, callback) {
    Formsz.find({
        _id: id
    }, function(err, post) {
        if (post.length > 0) {
            FormszDetails.find({
                formId: id
            }, function(err, post1) {
                if (post1.length > 0) {
                    //var obj2=JSON.parse(body);
                    var obj2 = body;
                    var fomrszkeys = [];
                    var i = 0;
                    for (i = 0; i < obj2.length; i++) {
                        fomrszkeys.push(obj2[i].lable);
                    }

                    var itemsProcessed = 0;
                    post1.forEach(function(dbUserObj1) {
                        itemsProcessed++;
                        //var obj=JSON.parse(dbUserObj1.record);
                        var obj = dbUserObj1.record;
                        var objstr = [];
                        var childkeys = [];
                        var i = 0;
                        for (i = 0; i < obj.length; i++) {
                            childkeys.push(obj[i].fieldName);
                            objstr.push(obj[i]);
                        }


                        var isUpdated = false;
                        var j = 0;
                        for (j = 0; j < fomrszkeys.length; j++) {
                            if (childkeys.indexOf(fomrszkeys[j]) == -1) {
                                isUpdated = true;
                                //objstr.push(JSON.stringify({fieldName:fomrszkeys[j],fieldValue:"",isPrimary:false}));
                                objstr.push({
                                    fieldName: fomrszkeys[j],
                                    fieldValue: "",
                                    isPrimary: false
                                });
                            }
                        }
                        if (isUpdated) {

                            dbUserObj1.record = "[" + objstr + "]";
                            dbUserObj1.save(function(err, a) {
                            });
                        }
                        if (itemsProcessed === post1.length) {
                            callback(true);
                        }
                    });



                } else {
                    callback(true);
                }

            });
        } else {
            callback(true);
        }
    });

}

function delForms(id, body, callback) {
    Formsz.find({
        _id: id
    }, function(err, post) {
        if (post.length > 0) {
            FormszDetails.find({
                formId: id
            }, function(err, post1) {
                if (post1.length > 0) {
                    var obj2 = body;
                    var fomrszkeys = [];
                    var i = 0;
                    for (i = 0; i < obj2.length; i++) {
                        fomrszkeys.push(obj2[i].lable);
                    }

                    var itemsProcessed = 0;
                    post1.forEach(function(dbUserObj1) {
                        itemsProcessed++;
                        //var obj=JSON.parse(dbUserObj1.record);
                        var obj = dbUserObj1.record;
                        var objstr = [];
                        var childkeys = [];
                        var i = 0;
                        for (i = 0; i < obj.length; i++) {
                            childkeys.push(obj[i].fieldName);
                            //objstr.push(JSON.stringify(obj[i]));
                            objstr.push(obj[i]);
                        }


                        var isDeleted = false;
                        var j = 0;
                        for (j = 0; j < childkeys.length; j++) {
                            if (fomrszkeys.indexOf(childkeys[j]) == -1) {
                                isDeleted = true;

                                obj.splice([j]);
                            }
                        }
                        if (isDeleted) {
                            //dbUserObj1.record=JSON.stringify(obj)
                            dbUserObj1.record = obj
                            dbUserObj1.save(function(err, a) {
                            });
                        }
                        if (itemsProcessed === post1.length) {
                            callback(true);
                        }
                    });

                } else {
                    callback(true);
                }

            });
        } else {
            callback(true);
        }

    });
}

module.exports = router;
