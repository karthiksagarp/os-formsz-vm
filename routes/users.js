var crypto = require('crypto');
var express = require('express');
var router = express.Router();
var Users = require('../models/user.js');
var fs = require('fs');
var formidable = require('formidable');
var Store = require('../models/store.js');
var mailer = require("nodemailer");
var ObjectID = require("bson-objectid");
var mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
var sendautomail = require('../routes/utils').sendautomail;
var deviceinfo = require('../routes/utils').deviceinfo;
var defoultImageurl = require('../routes/utils').defoultimageurl;
var Gruops = require('../models/group.js');
var getdeviceKeysByUser = require('../routes/utils').getdeviceKeysByUser;
//var pushNotifications = require('../routes/utils').pushNotifications;
var generatePassword = require('../routes/utils').generatePassword;
var passwordGenerated = require('../routes/utils');
var Tasks = require('../models/formszTasks.js');
var Formsz = require('../models/formsz.js');
var log = require('./log')(module);
var async = require('async');
var activityLog = require('formsz-tpcl-customizations');
var groupNameForMultiUserDel = [];
var mobileLicensing = require('../models/mobileLicensing.js');
var multer = require('multer');
//-->Naveen:Added: 
var parseXlsx = require('excel');
//<-- End

router.post('/createUserFromFile', function(req, res, next) {
    var data = req.body.userData;
    // console.log(data);
    var i = 0;
    var existingUsers = [];
    var userNames;
    getUserList(data);

    //-->SIva Kella: Added: TPCL Customizations
    //Notes: Added to include createdBy and CreatedByMailID
    var createdBy = req.body.userData.createdBy;
    var createdByMailID = req.body.userData.createdByMailID;
    //<-- End

    function getUserList(data) {
        if (i < data.length) {
            Users.find({username: data[i].username}, function(err, userInfo) {
                mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000 + i);
                //var passwordGenerateds = passwordGenerated.generatePassword();
              //  hashpassword = encrypt(passwordGenerateds);
                userNames = data[i].username;
                var users = new Users({
                    _id: mongoid,
                    name: data[i].name,
                    phone: data[i].phone,
                    email: data[i].email,
                    username: data[i].username,
                  //  password: hashpassword,
                    imageurl: defoultImageurl,
                    //-->Siva Kella:Modified to set default value to groupname
                    //NOTES:
                    // groupname: req.body.groupName ,
                    groupname: req.body.groupName || [ { "name" : null,"_id" : null }],
                    //<-- End
                    assignedLayers : req.body.assignedLayers,
                    /*TPCL Customization : Uma */ 
                        bloodgroup: data[i].bloodgroup,
                        adderess2:  data[i].adderess2,
                        adderess1:  data[i].adderess1,
                        activity:   data[i].activity,
                        zone:       data[i].zone,
                        vender:     data[i].vender,
                        doj:        data[i].doj,
                        dob:        data[i].dob,
                        //-->Siva Kella: Added:TPCL Customizations
                        //NOTES: To include createdBy and createdByMailID
                        createdBy: createdBy,
                        createdByMailID: createdByMailID,
                        //<-- End
                    //<-- End 
                    type: "2"
                })
                if (userInfo.length <= 0) {
                    users.save(function(err, user) {
                        if(err) {
                            res.json({"message":"Username / Email already exists",status:204})
                        }
                        i++;
                        getUserList(data);
                        // Mail Confirmation
                      //  var html = "<body>Dear " + user.name + ",<br><br>You are registered as a group user on Tata Power Dynamic-Forms system and your login details are as below<br><br> <strong>Username : </strong>" + user.username + "<br> <strong>Password :</strong> " + decrypt(user.password)+"<br><br>Please login with above credentials using this link and change the password on first login.<br><br>Thanks,<br>DForms, GIS Team</body>"
                     var html = "<body>Dear " + user.name + ",<br><br> You are registered as a group user on Tata Power Dynamic-Forms system and your login details are as below<br> <strong>Username : </strong>" + user.username + "<br> <strong>Password :</strong>"+decrypt(user.password)+"<br><br>Please login with above credentials using this <a href='https://dforms-gis-dev1.tatapower.com/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a> and change the password on first login.<br><br>Thanks,<br><br>DForms, GIS Team</body>"

                        sendautomail(user.email, html, "Automatic Reply: Account Confirmation");
                        //Activity log for user added for TPCL project on 30-01-2018
                         activityLog.activityLogForUsers(user,"User Created");


                    });
                } else {
                    existingUsers.push(userNames);
                    i++;
                    getUserList(data);
                }
            });
        }
        if (i == data.length) {
            
            if (existingUsers.length == 0) {
                res.json({
                    "message": "Users Created Successfully",
                    "status": "200"
                });
            } else {
                if (existingUsers.length == data.length) {
                    res.json({
                        "message": "All Users already in use ",
                        "status": "208"
                    });
                } else {
                    res.json({
                        "message": " Users Created Successfully except " + existingUsers.toString() + ".because they are already in use",
                        "status": "208"
                    });
                }
            }
        }
    }
});

/* GET all users. */
router.get('/getuserslist/:limit/:offset', function(req, res, next) {
    Users.paginate({isDeleted: false,type: 2}, { page: req.params.offset,limit: req.params.limit,sort: {createdDateTime: -1}}, function(err, Users) {
        if (err) return next(err);
        res.json(post);
    });
});
/*Get usernames for all users by renuka */
router.get('/getusernames',function(req, res, next){
    Users.find({isDeleted: false,type:2},{username:1},function(err,post){
        if (err) return next(err);
        // console.log(post);
        res.json(post);
    });
});


/*
@description Service written for group admin
    group admin can see the selected users 
@developer Renuka
@Purpose TPCL Customization
*/
router.get('/getusersgroupadmin/:groupname',function(req,res,next){
   
     Users.find({groupname:{$elemMatch:{_id:req.params.groupname}},type: 2,isDeleted: false}, function(err, post) {
            if(err)
            return next(err);
            res.json(post);
        });
    

});

/*
@description Service written for group admin
    group admin can see all the  users mapped to it.
@developer Renuka
@Purpose TPCL Customization
*/
router.post('/allusersgroupadmin',function(req,res,next){
   
     Users.find({groupname:{$elemMatch:{_id:{$in:req.body}}},type: 2,isDeleted: false},function(err, post){
           
           if(err)
            return next(err);
             // console.log(post)
              res.json(post);
             });
})

/* Get all group admins by renuka*/
router.get('/getgroupadmins',function(req, res, next){
 Users.find({type:3,isDeleted: false},{username:1},function(err,post){
    if (post.length > 0) {
            res.json(post);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
 });
});

//Phani code
// @TPCL DEVELOPMENT
router.get('/getusergrouplist/:groupname/:limit/:offset', function(req, res, next) {
    var limit = parseInt(req.params.limit);
    
    if (req.params.groupname == 'All') {
        Users.paginate({type: 2,isDeleted: false}, {page: req.params.offset,limit:limit, sort: {createdDateTime: -1 } }, function(err, post) {
            res.json(post);
        });
    }
    else{
        //Fetch users base on group mapped with single/multiple by Uma
         Users.paginate({groupname: {$elemMatch: {_id:req.params.groupname}},type: 2,isDeleted: false}, {page: req.params.offset,limit:limit, sort: {createdDateTime: -1 } }, function(err, post) {
             res.json(post);
         });
        // Users.paginate({groupname: req.params.groupname,type: 2,isDeleted: false}, {page: req.params.offset,limit: req.params.limit, sort: {createdDateTime: -1 } }, function(err, post) {
        //     res.json(post);
        // });

    }
});

//<------------TPCL Customizations - Added @ developer : Renuka
        // Added isFirst login for web and mobile
            //--> End
// Change Password
//--> Code modified by Divya Bandaru on 14-03-2018
router.post('/pwdchange', function(req, res, next) {
    var hashpassword = encrypt(req.body.oldpassword);
    Users.find({username: req.body.username,password: hashpassword}, function(err, post) {
        if (post.length > 0) {
                 Users.update({
                                username: req.body.username
                            }, {
                                $set: {
                                    password: encrypt(req.body.newpassword),
                                    isFirstLogin: false,
                                    //--> Added paramater on 14-03-2018
                                    isUserLocatorActive:false
                                    //<--
                                }
                            },{multi:true}, function(err, a) {
                            if(err) {
                                return err;
                            } else {
                                console.log("password changed for user");
                                //--> code added to remove license if user goes to settings and changed password by Divya bandaru on 14-03-2018
                               mobileLicensing.find({assignedTo:req.body.username,isActive:true}, function (err,releaseLicense) {
                                if(err){
                                    return err;
                                }
                                else if(releaseLicense.length==0||releaseLicense.length==undefined) {
                                    console.log("No license available to release");
                                }
                                else if(releaseLicense.length>0) {
                                        mobileLicensing.update({_id:releaseLicense[0]._id},{$set:{isActive:false,assignedTo:false}}, function(err,releaseFinished) {
                                            if(err) {
                                                return err;
                                            }
                                            else {
                                                console.log("license released");
                                            }
                                        })
                                    }
                                })

                            }
                            //<-- code ended here
                            });

                 res.json({
                    "message": "Password changed successfully",
                    "status": 200
                });

          //  })
          //  post[0].password = encrypt(req.body.newpassword);
           // post[0].save(function(err, a) {
                //log.info(req.body.username+"Password changed successfully");
                //log.info(getdeviceKeysByUser(post[0]._id));
               /* var userid = [];
                userid.push(post[0]._id);
                 var value = getdeviceKeysByUser(userid, function(data) {
                     pushNotifications(value, "Password Changes", "Your password has been by Admin,please check your Email.", {
                         data: "Password Change"
                     });
                 })*/
               
            //});
        } else {
            res.json({
                "message": "Current password is Invalid,Please try again ",
                "status": 204
            });
        }
    });
});
/* GET usersGroups. */
/*router.get('/getusergrouplist/:groupname/:limit/:offset', function(req, res, next) {
    Users.paginate({groupname: req.params.groupname,type: 2,isDeleted: false}, {page: req.params.offset,limit: req.params.limit, sort: {createdDateTime: -1 } }, function(err, post) {
        res.json(post);
    });
});*/

/*
@description Service written for group admin
    group admin can see the all available users
@developer Umamaheswari
@Purpose TPCL Customization
*/
/*router.get('/getusergrouplist/:limit/:offset', function(req, res, next) {
    Users.paginate({type: 3,isDeleted: false}, {page: req.params.offset,limit: req.params.limit, sort: {createdDateTime: -1 } }, function(err, post) {
        res.json(post);
    });
});*/

//Get all users filter by GroupName
router.get('/getuserlistmaping/:groupname', function(req, res, next) {
    /*
@Description type has bees changed from 2 to 3
@purpose TPCL customization
@Developer Uma
    */
    Users.find({type:2,"groupname":{$elemMatch:{_id:{$in:[req.params.groupname]}}}}, function(err, post) {
        // console.log(post)
        var guserdata = [];
        if (post.length > 0) {
            post.forEach(function(grouplist) {
                guserdata.push({
                    name: grouplist.username
                });
            });
            res.json(guserdata);
        } else {
            res.json({
                "message": "No Data Found",
                "status": 204
            });
        }
    });
});



/* Create users */
router.post('/create', function(req, res, next) {
    mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
 // console.log(fields);
        if (!err) {
            var data = JSON.parse(fields.data);
           /* Users.find( { username: data.username ,type:2}, function(err, post) {
               if (post.length > 0) {
      
                    res.json({
                        "message": "User name already exists",
                        "status": 208
                    }); */
               // } else {
                    var Imgurl = "";
                    if (files.img) {
                        Imgurl = "store/" + mongoid;
                    } else {
                        Imgurl = defoultImageurl;
                    }
                    var data = JSON.parse(fields.data);

                    var Users1 = new Users({
                        username: data.username,
                        name: data.name,
                        email: data.email,
                        phone: data.phone,
                        groupname: data.groupname || null,
                        assignedLayers : data.assignedLayers || null,
                        imageurl: Imgurl,
                        selectedGroupList: data.selectedGroupList,
                        type: data.type,
                        bloodgroup: data.bloodgroup,
                        adderess2: data.adderess2,
                        adderess1: data.adderess1,
                        activity: data.activity,
                        zone: data.zone,
                        vender: data.vender,
                        
                        //-->Siva Kella: Added:TPCL Customizations
                        //NOTES: To include createdBy and createdByMailID
                        createdBy: data.createdBy,
                        createdByMailID: data.createdByMailID,
                        //<-- End

                        doj: data.doj,
                        dob: data.dob
                    });
                    //User or Admin Creation
                    Users1.save(function(err, post) {
      
                        if (err) {
                           console.log("err");
                           res.json({"message":"Username / Email already exists","status":208});
       
                            res.status(208);
                        } else {
                            var password =decrypt(post.password);
                            if(data.groupname!=null)
                            {
                                var obj = {username: post.username,_id:post._id.toString()}
                                data.groupname.forEach(function(groupid){
                                    Gruops.find({_id:groupid._id},{userlist:1},function(err,post1){
                                        post1.forEach(function(group){
                                            if(group.userlist==null)
                                                group.userlist=obj;
                                            else
                                            group.userlist.push(obj);
                                            
                                              group.save(function (err, post) {
                                         
                                    });
                                        });
                                       
                                    });
                                  

                                });
                            }
                            if (files.img) {
                                var path = files.img.path;
                                var buffer = fs.readFileSync(path);
                                var contentType1 = files.img.contentType;
                                var a = new Store({
                                    fileid: mongoid,
                                    data: buffer,
                                    contentType: contentType1
                                });
                                //Upload Image
                                a.save(function(err, a) {
                                    if (err) throw err;
                                    log.info(data.username + " : Upload Image Successfully!");
                                });

                                Imgurl = "store/" + mongoid;
                            }
                            // Mail Confirmation
                            //var html = "<body>Dear " + data.name + ",<br><br> You are registered as a group user and your details as below<br><br> <strong>Username : </strong>" + data.username + "<br> <strong>Password :</strong>"+password+"<br><br>Please login using with above credentials.<br><br></body>"
                             //   var html = "<body>Dear " + data.name + ",<br><br> You are registered as a group user and your details as below<br><br> <strong>Username : </strong>" + data.username + "<br> <strong>Password :</strong>"+password+"<br><br>Please login using with above credentials using this <a href='http://dforms-gis-dev1.tatapower.com:83/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a><br><br>Thanks,<br>DFormsTeam</body>"
                             var html = "<body>Dear " + data.name + ",<br><br> You are registered as a group user on Tata Power Dynamic-Forms system and your login details are as below<br> <strong>Username : </strong>" + data.username + "<br> <strong>Password :</strong>"+password+"<br><br>Please login with above credentials using this <a href='https://dforms-gis-dev1.tatapower.com/UAT-V1/TPCLDynamicFormsz/TPCLWeb/' target='_blank'>link</a> and change the password on first login.<br><br>Thanks,<br><br>DForms, GIS Team</body>"

                            sendautomail(data.email, html, "Automatic Reply: Account Confirmation");
                            /*  var ids=[];
                             ids.push()
                             deviceinfo(post._id,) */
                             //activity log added
                          activityLog.activityLogForUsers(post,"User Created");

                            log.info("User:" + data.username + " Created Successfully!");
                            res.json({
                                "message": "User Created Successfully",
                                "status": 200
                            });
                        }
                    });
            //    }
          //  });
        }
    });
});

/* GET /users/id */
router.get('/:id', function(req, res, next) {
    Users.findById(req.params.id, function(err, post) {
        if (err) return next(err);
        res.json(post);
    });
});
/* Update user details*/
router.put('/Update/:id', function(req, res, next) {
    var Imgurl = "";
    mongoid = ObjectID.createFromTime(new Date().getTime() + 15 * 60 * 1000);
    Users.findByIdAndUpdate(req.params.id, req.body,{new:true}, function(err, post) {
        //  if (err) return next(err);
        if (!post) {
            return next(err);
        } else {
            var form = new formidable.IncomingForm();
            form.parse(req, function(err, fields, files) {
                if (!err) {
                    var imgurl = ""
                    if (files.img) {
                        Imgurl = "store/" + mongoid;
                    } else {
                        Imgurl = defoultImageurl;
                    }
                    //console.log(fields);
                    var data = JSON.parse(fields.data);
                    post.name = data.name;
                    post.email = data.email;
                    post.phone = data.phone;
                    post.bloodgroup= data.bloodgroup;
                    post.adderess2 = data.adderess2;
                    post.adderess1 = data.adderess1;
                    post.activity = data.activity;
                    post.zone = data.zone;
                    post.vender = data.vender;
                    post.doj = data.doj;
                    post.dob = data.dob;
                    post.type = data.type;
                    post.selectedGroupList= data.selectedGroupList;
                    post.assignedLayers = data.assignedLayers;
                    post.groupname= data.groupname;

                    if (files.img) {
                        post.imageurl = Imgurl;
                    }
                    // console.log(JSON.stringify(post));

                    //Update the user details
                    post.save(function(err, post1) {
                        if (err) {		
                            res.json({"message":"Email already exists",status:208});
                        } else {
                            //group mapping and group table update By Uma
                            //30-12-17
                            if(data.groupname.length>0)
                            {
                                var obj = {username: post.username,_id:post._id.toString()};

                                data.groupname.forEach(function(groupid){
                                    Gruops.find({_id:groupid._id},{userlist:1},function(err,post1){
                                        post1.forEach(function(group){
                                            if(group.userlist==null)
                                                {
                                                    group.userlist=obj;
                                                }
                                            else
                                            {
                                                var isObjectExists = false;
                                                group.userlist.forEach(function(userLst){
                                                    if(userLst._id == obj._id)
                                                    {
                                                        isObjectExists =true;
                                                        
                                                    
                                                    }
                                                    
                                                });
                                                if(isObjectExists == false)
                                                    group.userlist.push(obj);
                                                
                                                //group.userlist.push(obj);
                                            }
                                            if(data.UMGroupList.length>0) {
                                                async.forEachSeries(data.UMGroupList,function(singleUMGrouplist,next) {
                                                    Gruops.find({_id:singleUMGrouplist._id},{userlist:1,_id:0}, function(err,groupinfo) {
                                                        if(groupinfo.length>0) {
                                                for(var i=0;i<groupinfo[0].userlist.length;i++) {
                                                    if(groupinfo[0].userlist[i]._id == obj._id) {
                                                         Gruops.update({_id:singleUMGrouplist._id},{$pull:{userlist:groupinfo[0].userlist[i]}}, function(err,saved) {
                                                            if(err) {
                                                                console.log(err);
                                                            }
                                                            else {
                                                                console.log("saved");
                                                                next();
                                                            }
                                                         })


                                                    }
                                                  
                                                        
                                
                                                }
                                            }else {
                                                console.log("group is unavailable");
                                            }
                                                

                                            })


                                                      //  }
                                                    })
                                                   

                                            }
                                            
                                            
                                              group.save(function (err, post) {
                                                console.log("post");
                                                console.log(post);
                                         
                                    });
                                        });
                                       
                                    });
                                  

                                });
                            }
                            else if(data.groupname.length==0) {
                                      var obj = {username: post.username,_id:post._id.toString()};


                                            if(data.UMGroupList.length>0) {
                                                async.forEachSeries(data.UMGroupList,function(singleUMGrouplist,next) {
                                                    Gruops.find({_id:singleUMGrouplist._id},{userlist:1,_id:0}, function(err,groupinfo) {
                                                        if(groupinfo.length>0) {
                                                for(var i=0;i<groupinfo[0].userlist.length;i++) {
                                                    if(groupinfo[0].userlist[i]._id == obj._id) {
                                                         Gruops.update({_id:singleUMGrouplist._id},{$pull:{userlist:groupinfo[0].userlist[i]}}, function(err,saved) {
                                                            if(err) {
                                                                console.log(err);
                                                            }
                                                            else {
                                                                console.log("saved");
                                                                next();
                                                            }
                                                         })


                                                    }
                                                  
                                                        
                                
                                                }
                                            }else {
                                                console.log("group is unavailable");
                                            }
                                                

                                            })


                                                      //  }
                                                    })
                                                   

                                            }
                                  

                            }
                           

                            if (files.img) {
                                var path = files.img.path;
                                var buffer = fs.readFileSync(path);
                                var contentType1 = files.img.contentType;
                                var a = new Store({
                                    fileid: mongoid,
                                    data: buffer,
                                    contentType: contentType1
                                });
                                //Upload Image
                                a.save(function(err, a) {
                                    //if (err) throw err;
                                    log.info(data.username + ": Upload Image Successfully!");
                                });
                            }
                            var html = "<body>Dear " + post1.name + ",<br><br> Your account details has been modified by the adminstrator.Please login and check the updates.<br><br><br>Thanks,<br>DForms, GIS Team</body>"
                            sendautomail(post1.email, html, "Automatic Reply: Account Updates");
                           activityLog.activityLogForUsers(post,"User details Updated");

                            /* var value = getdeviceKeysByUser(post1._id, function(data) {
                                 pushNotifications(value, "Updated Profile", "Updated Profile", {
                                     data: "Updated Profile"
                                 });
                             });*/
                            log.info(data.username + ": User Details Updated Successfully!");
                            res.json({
                                "message": "User Details Updated Successfully",
                                "status": 200
                            });
                        }
                    });
                }
            });
        }
    });
});

/* DELETE --users:id */
router.delete('/:id', function(req, res, next) {
    Users.find({_id: req.params.id}, function(err, post) {
        if (post.length > 0) {
            Tasks.find({
                'assignedUsers.userId': req.params.id,
                isDeleted: false
            }, function(err, userIds) {
                if (userIds.length > 0) {
                    res.json({
                        "message": "Task is assigned to the following user,so you cannot delete it.",
                        "status": 208
                    });
                } else {
                    post[0].isDeleted = true;
                    post[0].save(function(err) {
                        if (err) throw err;

                        var html = "<body>Dear " + post[0].name + ",<br><br> Your account has been deleted by the administrator from Tata Power Dynamic-Forms system.<br>Thanks,<br>DForms, GIS Team</body>"

                        sendautomail(post[0].email, html, "Automatic Reply: Account Deletion")
                        var arr = [];
                        arr.push(post[0]._id);
                        activityLog.activityLogForUsers(post[0],"User Deleted");

                        /* var value = getdeviceKeysByUser(arr, function(data) {
                             pushNotifications(value, "Profile Notification", "Your Profile has been Deleted by admin", {
                                 data: "Deleted Profile"
                             });
                         });*/

                        res.json({
                            "message": "User Deleted Successfully",
                            "status": 200
                        });
                    });
                }

            });
        } else {
            res.json({
                "message": "No data found",
                "status": 204
            })
        }
    });
});
/* Delete multiple users */
router.post('/multipleDelete/:groupname', function(req, res, next) {
    var nonDeletedUsers = [];
   async.forEachSeries(req.body.multiUsersObj, function(singleUser,next) {
       if(req.params.groupname=='root')
        {
         Users.find({_id: singleUser}, 
            function(err, post) {
           if(post.length>0) {
                Tasks.find({
                    'assignedUsers.userId': singleUser,
                    isDeleted: false
                }, function(err, userIds) {
                    if (userIds.length > 0) {
                        async.forEachSeries(function(value,next1){
                           nonDeletedUsers.push(value.name);
                           next1();
                        });
                    } else {
                        post[0].isDeleted = true;
                        post[0].save(function(err) {
                            if (err) throw err;
                        });
                    }

            });
            //checks wether a form has associated to private user or not
            Formsz.find({
                'assignedUsers.userId': singleUser,
                isDeleted: false
            }, function(err, userIds) {
                // console.log(userIds);
                if (userIds.length > 0) {
                    async.forEachSeries(function(value,next1){
                       nonDeletedUsers.push(value.name);
                       next1();
                    });
                } else {
                    post[0].isDeleted = true;
                    post[0].save(function(err) {
                        if (err) throw err;

                       // var html = "<body>Dear " + post[0].name + ",<br><br> Your account has been deleted by the adminstartor.<br></body>"

                    //    sendautomail(post[0].email, html, "Automatic Reply: Account Deletion")
                        

                        

                       
                    });
                }

            });
            
            next();
        }
        else {
                res.json({
                    "message": "No data found",
                    "status": 204
                })
            }
       });
    }
    else
    {
      Users.find({_id: singleUser,groupname:{$elemMatch:{_id:req.params.groupname}}}, 
            function(err, post) {
           if(post.length>0) {
                Tasks.find({
                    'assignedUsers.userId': singleUser,
                    isDeleted: false
                }, function(err, userIds) {
                    if (userIds.length > 0) {
                        async.forEachSeries(function(value,next1){
                           nonDeletedUsers.push(value.name);
                           next1();
                        });
                    } else {
                        post[0].isDeleted = true;
                        post[0].save(function(err) {
                            if (err) throw err;
                        });
                    }

            });
            //checks wether a form has associated to private user or not
            Formsz.find({
                'assignedUsers.userId': singleUser,
                isDeleted: false
            }, function(err, userIds) {
                // console.log(userIds);
                if (userIds.length > 0) {
                    async.forEachSeries(function(value,next1){
                       nonDeletedUsers.push(value.name);
                       next1();
                    });
                } else {
                    post[0].isDeleted = true;
                    post[0].save(function(err) {
                        if (err) throw err;

                       // var html = "<body>Dear " + post[0].name + ",<br><br> Your account has been deleted by the adminstartor.<br></body>"

                    //    sendautomail(post[0].email, html, "Automatic Reply: Account Deletion")
                        

                        

                       
                    });
                }

            });
            
            next();
        }
        else {
                res.json({
                    "message": "No data found",
                    "status": 204
                })
            }
       });  
    }
   }, function(err) {

    // console.log(nonDeletedUsers);
    res.json({"data":nonDeletedUsers,"message":"Users deleted successfully","status":"200"})
   })
   
  /* Users.remove({_id: {$in: req.body.multiUsersObj},usergroup:req.params.groupname}, function(err,deleted){
    console.log(deleted)
       
    });*/

      
});
function encrypt(text) {
    var cipher = crypto.createCipher('aes-256-ctr', 'magikminds');
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}
function decrypt(text) {
    var decipher = crypto.createDecipher('aes-256-ctr', 'magikminds');
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}


// Start of Change by Naveen Adepu for TPCL xls upload
// service to convert the uploaded users.xlsx to json format

var fileName;
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './public/')
    },
    filename: function (req, file, cb) {
        var datetimestamp = Date.now();
        fileName = file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1]
        cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
    }
});
var upload = multer({ //multer settings
                storage: storage
            }).single('file');
                


router.post('/xlsxtojsonuser', function(req,res,next){
    startimestamp = Date.now();
     upload(req,res,function(err){
        parseXlsx('public/'+fileName, function(err, data) {            
            if(err) throw err;              
            converttojson(data,function(response){
                fs.unlink('public/'+fileName);
               res.json({"data":response,"status":200})
            });
        });
    })       
});


function converttojson(data,callback){
    console.log(data);
    var jsondata = {};
    var fieldnames = data.shift();
    var jsondata=[];
    var record={};
    // console.log("showing dat....",data)
    for(var recordId=0; recordId < data.length; recordId++){
        for(var fieldId=0;fieldId<data[recordId].length;fieldId++){
                record[fieldnames[fieldId]] = data[recordId][fieldId];
        }
    jsondata.push(record);
    console.log(jsondata);
    record = {};
    }
    callback(jsondata);
}

// End of Chagne by Naveen Adepu 27-Jan-2018


module.exports = router;
