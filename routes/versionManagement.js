var express = require('express');
var router = express.Router();

var versionManagement = require('../models/versionManagement.js');


router.get('/getVersions/:formId', function (req, res, next) {
    console.log("-----------------")
    console.log(req.params.formId)
    versionManagement.find({formId : req.params.formId}, function (err, post) {
        console.log("---------*******--------")
        //console.log(post);
        if (post.length > 0) {
            //console.log(post)
            res.json(post);

        } else {
            res.json({
                "message" : "No data found",
                "status" : 200
            });
        }
    });
});

module.exports = router;
