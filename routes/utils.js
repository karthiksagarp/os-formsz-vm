var mailer = require("nodemailer");
var smtpTransport = require("nodemailer-smtp-transport");
var express = require('express');
var router = express.Router();
var fs = require('fs');
var deviceinfo = require('../models/deviceinfo.js');
var Users = require('../models/user.js');
var utils = {
  sendautomail: function(toemail,textBody,subject)
{
   /* var transport = mailer.createTransport(smtpTransport({
    host : "smtp.gmail.com",
	secureConnection: false, // use SSL                    
	ssl : true,
    port: 25,
    auth : {
        user : "tpcldynamicformsz@gmail.com",
        pass : "mm@12345"
    }
}));*/
var transport = mailer.createTransport({
        host: "172.16.200.203",
        port: 25, // port for secure         
        "strictSSL": false,
        "requireTLS": true,
        "tls": {
            "rejectUnauthorized": false
        },

    });

var mail = {
		from :"sw_gis_admin@tatapower.com",
		//from:"tpcldynamicformsz@gmail.com",
		to : toemail,
		subject : subject,
		html : textBody
		
	}	
	
transport.sendMail(mail, function(error, response) 
	{
		if (error) 
		{
			console.log(error);
			
		return false;

		}else
		{
		console.log("mail sent success");	
		}
	
		transport.close();
		return true;

	});	
	
},

 //sendAutomailwithAttachment: function(toemail,textBody,subject,file,cc,cc1,cc2,cc3,cc4,callback)
 sendAutomailwithAttachment: function(toemail,textBody,subject,file,cc,callback)
{

/*var transport = mailer.createTransport(smtpTransport({
     host : "smtp.gmail.com",
	secureConnection: false, // use SSL                    
	ssl : true,
    port: 25,
    auth : {
        user : "tpcldynamicformsz@gmail.com",
        pass : "mm@12345"
    }
}))*/;

var transport = mailer.createTransport({
        host: "172.16.200.203",
        port: 25, // port for secure         
        "strictSSL": false,
        "requireTLS": true,
        "tls": {
            "rejectUnauthorized": false
        },

    });
/*var mail = {
		from : "Formsz App<tpcldynamicformsz@gmail.com>",
		//from :"tpcldynamicformsz@gmail.com",
		to : toemail,
		cc:[cc,cc1,cc2,cc3,cc4],
		subject : subject,
		html : textBody,
		attachments: [  
         {   // filename and content type is derived from path
            path: file
         //  filename: "records.pdf",
           //contents: new Buffer(file, 'base64')
        } 
        ]   
	}*/
	console.log(mail)	

var mail = {
		//from : "Formsz App<tpcldynamicformsz@gmail.com>",
		//from :"tpcldynamicformsz@gmail.com",
		from :"sw_gis_admin@tatapower.com",
		to : toemail,
		//-->Uma:Modified
		// cc:[cc,cc1,cc2,cc3,cc4],
		cc:cc,
		//<-- End
		subject : subject,
		html : textBody,
		attachments: [  
         {   // filename and content type is derived from path
            path: file
         //  filename: "records.pdf",
           //contents: new Buffer(file, 'base64')
        } 
        ]   
}
	
transport.sendMail(mail, function(error, response) 
	{
		if (error) 
		{
			console.log(error);
			
		//return false;
		callback(false);

		}else
		{
		console.log("mail sent success");	
		}
	
		transport.close();
		//return true;
		callback(true)

	});	
	
},
resformatpagination: function(res,statuscode,message,data,success,totcount,pageno){
	var resformat = {
  StatusCode:statuscode,
  message:message,
  success:success,
  data:data,
  PageNo:pageno,
  Totcount:totcount
  
 }
  return res.json(resformat);
},
pushNotifications: function(ids,body,title,data){
var FCM = require('fcm-node');
	//var apiKey = 'AIzaSyA2n_wWHrJkCfriFJqojhCTqfBzH4nlA-8';
	var apiKey ="AAAA5uwnf3U:APA91bE2K2FaRZ1o7QVgoMKyQ2yoxawgUFiuJcBNxdSCcg6_7AVI25ERbdeffpJ0YC5h6c1FXuQLovdDc7sJCiMU2quVH0gsdv6626IjO7zwjV7oj8keeO3-YE_rOe_6_zrQCKlG5TLq";
	var fcm = new FCM(apiKey);

var message = { //this may vary according to the message type (single recipient, multicast, topic, et cetera)var 
  registration_ids:ids,

   collapse_key: 'TPCLDFormsz Notification',
    notification: {
        "body" : body,
      "title" : title,
	  "click_action":"FCM_PLUGIN_ACTIVITY",
	  "icon": "myicon"
      
    },
    
    data:data
};

	fcm.send(message, function(err, messageId){
		if (err) {
			console.log("Something has gone wrong!",err);
		} else {
			console.log("Sent with message ID: ", messageId);
		}
	});

},
getdeviceKeys:function(userGroupname,callback)
{
	console.log(userGroupname);
  var keys=[];
 Users.distinct('_id',{groupname:userGroupname}, function (err, post) {
 
  deviceinfo.find({userId:{$in: post}},function (err, post1) {
	  
   if(post1)
    {
		console.log(post1);
		for(var i=0;i<post1.length;i++){
			keys.push(post1[i].deviceKey);
		}
    }
       
   console.log("post1==" + keys);
   callback(keys);
  });
  
  });
  return keys;
},

getdeviceKeysByUser:function(id,callback)
{
	var keys=[];
	console.log("userids");
	console.log(id);
		deviceinfo.find({userId:{$in: id}}, function (err, post1) {
				if(post1.length>0)
				{
					
					post1.forEach(function(grouplist){
						console.log(post1.length);
					keys.push(grouplist.deviceKey);
				
					});	 
				}
		callback(keys);					
	});
	 return keys;
},

getdeviceKeysByUserName:function(names,callback)
{
	var keys=[];
	console.log("names");
	console.log(names);
	Users.distinct("_id",{username:{$in:names},type:"2"},function(err,userids) {
		console.log("userids");
		console.log(userids);
		if(userids.length>0) {
			deviceinfo.find({userId:{$in: userids}}, function (err, post1) {
				if(post1.length>0)
				{
					
					post1.forEach(function(grouplist){
						console.log(post1.length);
					keys.push(grouplist.deviceKey);
				
					});	 
				}
		callback(keys);					
	});

		}
		else {
			console.log("im in else");


		}
	})
		
	 return keys;
},
generatePassword: function() {
        var length = 8,
            charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
            retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }

        return retVal;
    },

defoultimageurl:"store/0672077b0000000000000000"

};
module.exports = utils;
